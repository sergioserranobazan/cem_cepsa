# Convenciones Git
1. Cada programador trabajará sobre forks del [proyecto principal](https://bitbucket.org/mrcem/cem_cepsa)
2. El procedimiento a seguir para cada tarea será:
    * Se abrirá una nueva rama a partir de `develop` con la prefijo `feature/` y un nombre
    descriptivo de la tarea a desarrollar (esta rama, salvo excepciones, no será
    pusheada al servidor).
    * Se irán realizando cambios progresivos lo más atómicos posibles commiteándolos
    * Una vez terminada la tarea, el programador se moverá a la rama `develop` y
    hará un pull de la rama `develop` del proyecto principal.
    * Se realizará un `merge --no-ff` desde `feature/NombreTarea` a `develop` y
    se resolverán los posibles conflictos.
    * Se pusheará al *fork* del desarrollador.
    * Se realizará el pull request a la rama `develop` del proyecto principal.

> Más información al respecto en [este blog post](http://nvie.com/posts/a-successful-git-branching-model/)
aunque no todos los conceptos en este son aplicables.

> También podría ser interesante leer la [siguiente sección](https://github.com/edgarshurtado/fchispano-recursos/blob/master/git_cheatsheet.md#contributing) sobre cómo se realiza una contribución
open-source.

## Consideraciones para los commit

1. Separar el título del cuerpo con una línea en blanco.
2. El título del commit será de 50 caracteres como máximo.
3. La primera palabra del título ha de ir en mayúscula.
4. No acabar la línea del título con un punto.
5. Usar el presente simple en el título del commit.
6. La líneas del cuerpo han de ser de 72 caracteres como máximo.
7. Usar el cuerpo del commit para explicar Qué y Por qué en lugar de Cómo.

> Más información al respecto en [este blog post](http://chris.beams.io/posts/git-commit/)

## Algunos comandos útiles para git

> Ver [git_cheatsheet](https://github.com/edgarshurtado/fchispano-recursos/blob/master/git_cheatsheet.md) de fcc-hispano.



Los filtros en la BD han de tener un campo nombre. Este Nombre en camelcase
debe coincidir con el nombre que tiene el filtro en la entidad encuesta ya que
para el filtrado de encuestas se hará (por ejemplo):

```php
$encuesta->getNegocio()->getNombre() == $valorFiltro;
```

Para las tablas de datos se usa la librería *DataTable*, de la cual se puede
leer más información aquí [recurso](https://datatables.net/examples/styling/bootstrap.html)


# Carga de js en maquetas

Los JS se cargan a través de base.html.twig
de app\Resources\views\base.html.twig

# Rutas para las rutinas de la aplicación

## Preparación para producción
`web/rutina/produccion`

## Adición de nueva rutina

La lógica para el lanzamiento de comandos symfony desde rutas url
se encuentra en el `DefaultController` del bundle Madison:Rutinas.

Añadir los comandos que se necesiten en la función `getComando`.

# Para crear el proyecto en desarrollo pre o produccción:

- Copiar los nuevos vendors de un proyecto local que los tenga o ejecutar composer install si tienes pormisos para ejecutar el comando
- lanzar la rutina web/rutina/produccion

# Comandos Symfony

* `madison:rutina-css` Comando para compilar scss y comprimir todo el css de la aplicación
* `madison:produccion` Comando para realizar los procedimientos necesarios para
la salida a producción de la aplicación. (Necesario para ejecutar la aplicación en
--env=prod)

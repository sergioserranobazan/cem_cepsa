<?php

namespace SurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EncuestaProceso
 *
 * @ORM\Table(name="survey_encuesta_proceso")
 * @ORM\Entity(repositoryClass="SurveyBundle\Repository\EncuestaProcesoRepository")
 */
class EncuestaProceso
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="SurveyBundle\Entity\Encuesta", inversedBy="encuestaProcesos")
     */
    private $encuesta;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\Proceso", inversedBy="encuestaProcesos")
     */
    private $proceso;

    /**
     * @var float $processIndex
     * @ORM\Column(name="process_index", type="float", nullable=true, options={"default":0})
     */
    private $processIndex;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set proceso
     *
     * @param \AdminBundle\Entity\Proceso $proceso
     * @return EncuestaProceso
     */
    public function setProceso(\AdminBundle\Entity\Proceso $proceso = null)
    {
        $this->proceso = $proceso;

        return $this;
    }

    /**
     * Get proceso
     *
     * @return \AdminBundle\Entity\Proceso
     */
    public function getProceso()
    {
        return $this->proceso;
    }

    /**
     * Set encuesta
     *
     * @param \SurveyBundle\Entity\Encuesta $encuesta
     * @return EncuestaProceso
     */
    public function setEncuesta(\SurveyBundle\Entity\Encuesta $encuesta = null)
    {
        $this->encuesta = $encuesta;

        return $this;
    }

    /**
     * Get encuesta
     *
     * @return \SurveyBundle\Entity\Encuesta
     */
    public function getEncuesta()
    {
        return $this->encuesta;
    }

    /**
     * Set processIndex
     *
     * @param string $processIndex
     * @return EncuestaProceso
     */
    public function setProcessIndex($processIndex)
    {
        $this->processIndex = $processIndex;

        return $this;
    }

    /**
     * Get processIndex
     *
     * @return string
     */
    public function getProcessIndex()
    {
        return $this->processIndex;
    }
}

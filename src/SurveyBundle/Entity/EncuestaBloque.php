<?php

namespace SurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EncuestaBloque
 *
 * @ORM\Table(name="survey_encuesta_bloque")
 * @ORM\Entity(repositoryClass="SurveyBundle\Repository\EncuestaBloqueRepository")
 */
class EncuestaBloque
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\OneToMany(targetEntity="EncuestaSubBloque", mappedBy="bloque")
     */
    private $subBloques;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return EncuestaBloque
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->preguntas = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Add subBloques
     *
     * @param \SurveyBundle\Entity\EncuestaSubBloque $subBloques
     * @return EncuestaBloque
     */
    public function addSubBloque(\SurveyBundle\Entity\EncuestaSubBloque $subBloques)
    {
        $this->subBloques[] = $subBloques;

        return $this;
    }

    /**
     * Remove subBloques
     *
     * @param \SurveyBundle\Entity\EncuestaSubBloque $subBloques
     */
    public function removeSubBloque(\SurveyBundle\Entity\EncuestaSubBloque $subBloques)
    {
        $this->subBloques->removeElement($subBloques);
    }

    /**
     * Get subBloques
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubBloques()
    {
        return $this->subBloques;
    }
}

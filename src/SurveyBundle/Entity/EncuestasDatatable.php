<?php

namespace SurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EncuestasDatatable
 *
 * @ORM\Table(name="nosql_encuestas_datatable")
 * @ORM\Entity(repositoryClass="SurveyBundle\Repository\EncuestasDatatableRepository")
 */
class EncuestasDatatable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="Encuesta")
     */
    private $encuesta;

    /**
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;

    /**
     * @ORM\Column(name="negocio", type="string", length=255)
     */
    private $negocio;

    /**
     * @ORM\Column(name="linea_negocio", type="string", length=255)
     */
    private $lineaNegocio;

    /**
     * @ORM\Column(name="canal", type="string", length=255)
     */
    private $canal;


    /**
     * @ORM\Column(name="detalle_canal", type="string", length=255)
     */
    private $detalleCanal;

    /**
     * @ORM\Column(name="procesos", type="string", length=255)
     */
    private $procesos;

    /**
     * @ORM\Column(name="experiencia", type="integer")
     */
    private $experiencia;

    /**
     * @ORM\Column(name="recomendacion", type="integer")
     */
    private $recomendacion;

    /**
     * @ORM\Column(name="sentimiento", type="integer")
     */
    private $sentimiento;

    /**
     * @ORM\Column(name="segmento", type="string", length=255)
     */
    private $segmento;

    /**
     * @ORM\Column(name="servicio", type="string", length=255)
     */
    private $servicio;

    /**
     * @ORM\Column(name="opi", type="string", length=255)
     */
    private $opi;

    /**
     * @ORM\Column(name="warning", type="boolean")
     */
    private $warning;

    // VARIABLES DE FILTRADO
    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\Segmento")
     */
    private $seg;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\Negocio")
     */
    private $neg;

    /**
     * @ORM\Column(name="nconcn", type="string", length=256)
     */
    private $nconcn;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\LineaNegocio")
     */
    private $org;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\Zona")
     */
    private $zon;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\Localidad")
     */
    private $loc;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\Provincia")
     */
    private $prov;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\Pais")
     */
    private $pais;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\Servicio")
     */
    private $ser1;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\DeterminacionCanal")
     */
    private $detCan;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return EncuestasDatatable
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set negocio
     *
     * @param string $negocio
     * @return EncuestasDatatable
     */
    public function setNegocio($negocio)
    {
        $this->negocio = $negocio;

        return $this;
    }

    /**
     * Get negocio
     *
     * @return string
     */
    public function getNegocio()
    {
        return $this->negocio;
    }

    /**
     * Set lNegocio
     *
     * @param string $lNegocio
     * @return EncuestasDatatable
     */
    public function setLNegocio($lNegocio)
    {
        $this->lNegocio = $lNegocio;

        return $this;
    }

    /**
     * Get lNegocio
     *
     * @return string
     */
    public function getLNegocio()
    {
        return $this->lNegocio;
    }

    /**
     * Set canal
     *
     * @param string $canal
     * @return EncuestasDatatable
     */
    public function setCanal($canal)
    {
        $this->canal = $canal;

        return $this;
    }

    /**
     * Get canal
     *
     * @return string
     */
    public function getCanal()
    {
        return $this->canal;
    }

    /**
     * Set detCanal
     *
     * @param string $detCanal
     * @return EncuestasDatatable
     */
    public function setDetCanal($detCanal)
    {
        $this->detCanal = $detCanal;

        return $this;
    }

    /**
     * Get detCanal
     *
     * @return string
     */
    public function getDetCanal()
    {
        return $this->detCanal;
    }

    /**
     * Set procesos
     *
     * @param string $procesos
     * @return EncuestasDatatable
     */
    public function setProcesos($procesos)
    {
        $this->procesos = $procesos;

        return $this;
    }

    /**
     * Get procesos
     *
     * @return string
     */
    public function getProcesos()
    {
        return $this->procesos;
    }

    /**
     * Set experiencia
     *
     * @param integer $experiencia
     * @return EncuestasDatatable
     */
    public function setExperiencia($experiencia)
    {
        $this->experiencia = $experiencia;

        return $this;
    }

    /**
     * Get experiencia
     *
     * @return integer
     */
    public function getExperiencia()
    {
        return $this->experiencia;
    }

    /**
     * Set recomendacion
     *
     * @param integer $recomendacion
     * @return EncuestasDatatable
     */
    public function setRecomendacion($recomendacion)
    {
        $this->recomendacion = $recomendacion;

        return $this;
    }

    /**
     * Get recomendacion
     *
     * @return integer
     */
    public function getRecomendacion()
    {
        return $this->recomendacion;
    }

    /**
     * Set sentimiento
     *
     * @param integer $sentimiento
     * @return EncuestasDatatable
     */
    public function setSentimiento($sentimiento)
    {
        $this->sentimiento = $sentimiento;

        return $this;
    }

    /**
     * Get sentimiento
     *
     * @return integer
     */
    public function getSentimiento()
    {
        return $this->sentimiento;
    }

    /**
     * Set opi
     *
     * @param string $opi
     * @return EncuestasDatatable
     */
    public function setOpi($opi)
    {
        $this->opi = $opi;

        return $this;
    }

    /**
     * Get opi
     *
     * @return string
     */
    public function getOpi()
    {
        return $this->opi;
    }

    /**
     * Set encuesta
     *
     * @param \SurveyBundle\Entity\Encuesta $encuesta
     * @return EncuestasDatatable
     */
    public function setEncuesta(\SurveyBundle\Entity\Encuesta $encuesta = null)
    {
        $this->encuesta = $encuesta;

        return $this;
    }

    /**
     * Get encuesta
     *
     * @return \SurveyBundle\Entity\Encuesta
     */
    public function getEncuesta()
    {
        return $this->encuesta;
    }

    /**
     * Set segmento
     *
     * @param string $segmento
     * @return EncuestasDatatable
     */
    public function setSegmento($segmento)
    {
        $this->segmento = $segmento;

        return $this;
    }

    /**
     * Get segmento
     *
     * @return string
     */
    public function getSegmento()
    {
        return $this->segmento;
    }

    /**
     * Set servicio
     *
     * @param string $servicio
     * @return EncuestasDatatable
     */
    public function setServicio($servicio)
    {
        $this->servicio = $servicio;

        return $this;
    }

    /**
     * Get servicio
     *
     * @return string
     */
    public function getServicio()
    {
        return $this->servicio;
    }

    /**
     * Set lineaNegocio
     *
     * @param string $lineaNegocio
     * @return EncuestasDatatable
     */
    public function setLineaNegocio($lineaNegocio)
    {
        $this->lineaNegocio = $lineaNegocio;

        return $this;
    }

    /**
     * Get lineaNegocio
     *
     * @return string
     */
    public function getLineaNegocio()
    {
        return $this->lineaNegocio;
    }

    /**
     * Set warning
     *
     * @param string $warning
     * @return EncuestasDatatable
     */
    public function setWarning($warning)
    {
        $this->warning = $warning;

        return $this;
    }

    /**
     * Get warning
     *
     * @return string
     */
    public function getWarning()
    {
        return $this->warning;
    }

    /**
     * Set detalleCanal
     *
     * @param string $detalleCanal
     * @return EncuestasDatatable
     */
    public function setDetalleCanal($detalleCanal)
    {
        $this->detalleCanal = $detalleCanal;

        return $this;
    }

    /**
     * Get detalleCanal
     *
     * @return string
     */
    public function getDetalleCanal()
    {
        return $this->detalleCanal;
    }

    /**
     * Set nconcn
     *
     * @param string $nconcn
     * @return EncuestasDatatable
     */
    public function setNconcn($nconcn)
    {
        $this->nconcn = $nconcn;

        return $this;
    }

    /**
     * Get nconcn
     *
     * @return string 
     */
    public function getNconcn()
    {
        return $this->nconcn;
    }

    /**
     * Set seg
     *
     * @param \AdminBundle\Entity\Segmento $seg
     * @return EncuestasDatatable
     */
    public function setSeg(\AdminBundle\Entity\Segmento $seg = null)
    {
        $this->seg = $seg;

        return $this;
    }

    /**
     * Get seg
     *
     * @return \AdminBundle\Entity\Segmento 
     */
    public function getSeg()
    {
        return $this->seg;
    }

    /**
     * Set neg
     *
     * @param \AdminBundle\Entity\Negocio $neg
     * @return EncuestasDatatable
     */
    public function setNeg(\AdminBundle\Entity\Negocio $neg = null)
    {
        $this->neg = $neg;

        return $this;
    }

    /**
     * Get neg
     *
     * @return \AdminBundle\Entity\Negocio 
     */
    public function getNeg()
    {
        return $this->neg;
    }

    /**
     * Set org
     *
     * @param \AdminBundle\Entity\LineaNegocio $org
     * @return EncuestasDatatable
     */
    public function setOrg(\AdminBundle\Entity\LineaNegocio $org = null)
    {
        $this->org = $org;

        return $this;
    }

    /**
     * Get org
     *
     * @return \AdminBundle\Entity\LineaNegocio 
     */
    public function getOrg()
    {
        return $this->org;
    }

    /**
     * Set zon
     *
     * @param \AdminBundle\Entity\Zona $zon
     * @return EncuestasDatatable
     */
    public function setZon(\AdminBundle\Entity\Zona $zon = null)
    {
        $this->zon = $zon;

        return $this;
    }

    /**
     * Get zon
     *
     * @return \AdminBundle\Entity\Zona 
     */
    public function getZon()
    {
        return $this->zon;
    }

    /**
     * Set loc
     *
     * @param \AdminBundle\Entity\Localidad $loc
     * @return EncuestasDatatable
     */
    public function setLoc(\AdminBundle\Entity\Localidad $loc = null)
    {
        $this->loc = $loc;

        return $this;
    }

    /**
     * Get loc
     *
     * @return \AdminBundle\Entity\Localidad 
     */
    public function getLoc()
    {
        return $this->loc;
    }

    /**
     * Set prov
     *
     * @param \AdminBundle\Entity\Provincia $prov
     * @return EncuestasDatatable
     */
    public function setProv(\AdminBundle\Entity\Provincia $prov = null)
    {
        $this->prov = $prov;

        return $this;
    }

    /**
     * Get prov
     *
     * @return \AdminBundle\Entity\Provincia 
     */
    public function getProv()
    {
        return $this->prov;
    }

    /**
     * Set pais
     *
     * @param \AdminBundle\Entity\Pais $pais
     * @return EncuestasDatatable
     */
    public function setPais(\AdminBundle\Entity\Pais $pais = null)
    {
        $this->pais = $pais;

        return $this;
    }

    /**
     * Get pais
     *
     * @return \AdminBundle\Entity\Pais 
     */
    public function getPais()
    {
        return $this->pais;
    }

    /**
     * Set ser1
     *
     * @param \AdminBundle\Entity\Servicio $ser1
     * @return EncuestasDatatable
     */
    public function setSer1(\AdminBundle\Entity\Servicio $ser1 = null)
    {
        $this->ser1 = $ser1;

        return $this;
    }

    /**
     * Get ser1
     *
     * @return \AdminBundle\Entity\Servicio 
     */
    public function getSer1()
    {
        return $this->ser1;
    }

    /**
     * Set detCan
     *
     * @param \AdminBundle\Entity\DeterminacionCanal $detCan
     * @return EncuestasDatatable
     */
    public function setDetCan(\AdminBundle\Entity\DeterminacionCanal $detCan = null)
    {
        $this->detCan = $detCan;

        return $this;
    }

    /**
     * Get detCan
     *
     * @return \AdminBundle\Entity\DeterminacionCanal 
     */
    public function getDetCan()
    {
        return $this->detCan;
    }
}

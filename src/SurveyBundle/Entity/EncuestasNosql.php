<?php

namespace SurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EncuestasNosql
 *
 * @ORM\Table(name="nosql_encuestas")
 * @ORM\Entity(repositoryClass="SurveyBundle\Repository\EncuestasNosqlRepository")
 */
class EncuestasNosql
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="Encuesta")
     */
    private $encuesta;

    /**
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;

    /**
     * @ORM\ManyToOne(targetEntity="EncuestaPreguntaOpi")
     */
    private $opiPregunta;

    /**
     * @ORM\Column(name="opi_id", type="string", length=255)
     */
    private $opiId;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\Pais")
     */
    private $pais;


    // VARIABLES DE FILTRADO
    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\Segmento")
     */
    private $seg;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\Negocio")
     */
    private $neg;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\LineaNegocio")
     */
    private $org;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\CanalEncuestacion")
     */
    private $can;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\DeterminacionCanal")
     */
    private $detCan;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\Servicio")
     */
    private $ser1;

    /**
     * @ORM\Column(name="lat", type="string", length=255, nullable=true)
     */
    private $lat;

    /**
     * @ORM\Column(name="lon", type="string", length=255, nullable=true)
     */
    private $lon;

    /**
     * @ORM\Column(name="nconcn", type="string", length=256, nullable=true)
     */
    private $nconcn;

    // --------- PREGUNTAS --------------
    //

    /**
     * @ORM\Column(name="g1", type="string", length=255, nullable=false)
     * Sentimiento
     */
    private $g1;

    /**
     * @ORM\Column(name="g1_valor", type="integer", nullable=false)
     * Valor numérico correspondiente al adjetivo presente en g1
     */
    private $g1Valor;

    /**
     * @ORM\Column(name="g2", type="integer", nullable=false)
     * Recomendación
     */
    private $g2;

    /**
     * @ORM\Column(name="g3", type="integer", nullable=false)
     * Experiencia
     */
    private $g3;

    /**
     * @ORM\Column(name="a1", type="integer", nullable=true)
     */
    private $a1;

    /**
     * @ORM\Column(name="of1", type="integer", nullable=true)
     */
    private $of1;

    /**
     * @ORM\Column(name="of2", type="integer", nullable=true)
     */
    private $of2;

    /**
     * @ORM\Column(name="of3", type="integer", nullable=true)
     */
    private $of3;

    /**
     * @ORM\Column(name="of4", type="integer", nullable=true)
     */
    private $of4;

    /**
     * @ORM\Column(name="pc1", type="integer", nullable=true)
     */
    private $pc1;

    /**
     * @ORM\Column(name="pc2", type="integer", nullable=true)
     */
    private $pc2;

    /**
     * @ORM\Column(name="e1", type="integer", nullable=true)
     */
    private $e1;

    /**
     * @ORM\Column(name="e2", type="integer", nullable=true)
     */
    private $e2;

    /**
     * @ORM\Column(name="e3", type="integer", nullable=true)
     */
    private $e3;

    /**
     * @ORM\Column(name="e4", type="integer", nullable=true)
     */
    private $e4;

    /**
     * @ORM\Column(name="p2", type="integer", nullable=true)
     */
    private $p2;

    /**
     * @ORM\Column(name="gc1", type="integer", nullable=true)
     */
    private $gc1;

    /**
     * @ORM\Column(name="gc2", type="integer", nullable=true)
     */
    private $gc2;

    /**
     * @ORM\Column(name="gc3", type="integer", nullable=true)
     */
    private $gc3;

    /**
     * @ORM\Column(name="gc4", type="integer", nullable=true)
     */
    private $gc4;

    /**
     * @ORM\Column(name="ps1", type="integer", nullable=true)
     */
    private $ps1;

    /**
     * @ORM\Column(name="ps2", type="integer", nullable=true)
     */
    private $ps2;

    /**
     * @ORM\Column(name="procesos", type="string", length=255, nullable=true)
     */
    private $procesos;

    /**
     * @ORM\Column(name="warning", type="boolean", nullable=true)
     */
    private $warning;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return EncuestasNosql
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set opiId
     *
     * @param string $opiId
     * @return EncuestasNosql
     */
    public function setOpiId($opiId)
    {
        $this->opiId = $opiId;

        return $this;
    }

    /**
     * Get opiId
     *
     * @return string
     */
    public function getOpiId()
    {
        return $this->opiId;
    }

    /**
     * Set encuesta
     *
     * @param \SurveyBundle\Entity\Encuesta $encuesta
     * @return EncuestasNosql
     */
    public function setEncuesta(\SurveyBundle\Entity\Encuesta $encuesta = null)
    {
        $this->encuesta = $encuesta;

        return $this;
    }

    /**
     * Get encuesta
     *
     * @return \SurveyBundle\Entity\Encuesta
     */
    public function getEncuesta()
    {
        return $this->encuesta;
    }

    /**
     * Set opiPreguntaId
     *
     * @param \SurveyBundle\Entity\EncuestaPreguntaOpi $opiPreguntaId
     * @return EncuestasNosql
     */
    public function setOpiPreguntaId(\SurveyBundle\Entity\EncuestaPreguntaOpi $opiPreguntaId = null)
    {
        $this->opiPreguntaId = $opiPreguntaId;

        return $this;
    }

    /**
     * Get opiPreguntaId
     *
     * @return \SurveyBundle\Entity\EncuestaPreguntaOpi
     */
    public function getOpiPreguntaId()
    {
        return $this->opiPreguntaId;
    }

    /**
     * Set segmento
     *
     * @param \AdminBundle\Entity\Segmento $segmento
     * @return EncuestasNosql
     */
    public function setSegmento(\AdminBundle\Entity\Segmento $segmento = null)
    {
        $this->segmento = $segmento;

        return $this;
    }

    /**
     * Get segmento
     *
     * @return \AdminBundle\Entity\Segmento
     */
    public function getSegmento()
    {
        return $this->segmento;
    }

    /**
     * Set negocio
     *
     * @param \AdminBundle\Entity\Negocio $negocio
     * @return EncuestasNosql
     */
    public function setNegocio(\AdminBundle\Entity\Negocio $negocio = null)
    {
        $this->negocio = $negocio;

        return $this;
    }

    /**
     * Get negocio
     *
     * @return \AdminBundle\Entity\Negocio
     */
    public function getNegocio()
    {
        return $this->negocio;
    }

    /**
     * Set linea_negocio
     *
     * @param \AdminBundle\Entity\LineaNegocio $lineaNegocio
     * @return EncuestasNosql
     */
    public function setLineaNegocio(\AdminBundle\Entity\LineaNegocio $lineaNegocio = null)
    {
        $this->linea_negocio = $lineaNegocio;

        return $this;
    }

    /**
     * Get linea_negocio
     *
     * @return \AdminBundle\Entity\LineaNegocio
     */
    public function getLineaNegocio()
    {
        return $this->linea_negocio;
    }

    /**
     * Set canal
     *
     * @param \AdminBundle\Entity\CanalEncuestacion $canal
     * @return EncuestasNosql
     */
    public function setCanal(\AdminBundle\Entity\CanalEncuestacion $canal = null)
    {
        $this->canal = $canal;

        return $this;
    }

    /**
     * Get canal
     *
     * @return \AdminBundle\Entity\CanalEncuestacion
     */
    public function getCanal()
    {
        return $this->canal;
    }

    /**
     * Set detalle_canal
     *
     * @param \AdminBundle\Entity\DeterminacionCanal $detalleCanal
     * @return EncuestasNosql
     */
    public function setDetalleCanal(\AdminBundle\Entity\DeterminacionCanal $detalleCanal = null)
    {
        $this->detalle_canal = $detalleCanal;

        return $this;
    }

    /**
     * Get detalle_canal
     *
     * @return \AdminBundle\Entity\DeterminacionCanal
     */
    public function getDetalleCanal()
    {
        return $this->detalle_canal;
    }

    /**
     * Set servicio
     *
     * @param \AdminBundle\Entity\Servicio $servicio
     * @return EncuestasNosql
     */
    public function setServicio(\AdminBundle\Entity\Servicio $servicio = null)
    {
        $this->servicio = $servicio;

        return $this;
    }

    /**
     * Get servicio
     *
     * @return \AdminBundle\Entity\Servicio
     */
    public function getServicio()
    {
        return $this->servicio;
    }

    /**
     * Set g1
     *
     * @param string $g1
     * @return EncuestasNosql
     */
    public function setG1($g1)
    {
        $this->g1 = $g1;

        return $this;
    }

    /**
     * Get g1
     *
     * @return string
     */
    public function getG1()
    {
        return $this->g1;
    }

    /**
     * Set g2
     *
     * @param integer $g2
     * @return EncuestasNosql
     */
    public function setG2($g2)
    {
        $this->g2 = $g2;

        return $this;
    }

    /**
     * Get g2
     *
     * @return integer
     */
    public function getG2()
    {
        return $this->g2;
    }

    /**
     * Set g3
     *
     * @param integer $g3
     * @return EncuestasNosql
     */
    public function setG3($g3)
    {
        $this->g3 = $g3;

        return $this;
    }

    /**
     * Get g3
     *
     * @return integer
     */
    public function getG3()
    {
        return $this->g3;
    }

    /**
     * Set a1
     *
     * @param integer $a1
     * @return EncuestasNosql
     */
    public function setA1($a1)
    {
        $this->a1 = $a1;

        return $this;
    }

    /**
     * Get a1
     *
     * @return integer
     */
    public function getA1()
    {
        return $this->a1;
    }

    /**
     * Set gc1
     *
     * @param integer $gc1
     * @return EncuestasNosql
     */
    public function setGc1($gc1)
    {
        $this->gc1 = $gc1;

        return $this;
    }

    /**
     * Get gc1
     *
     * @return integer
     */
    public function getGc1()
    {
        return $this->gc1;
    }

    /**
     * Set gc2
     *
     * @param integer $gc2
     * @return EncuestasNosql
     */
    public function setGc2($gc2)
    {
        $this->gc2 = $gc2;

        return $this;
    }

    /**
     * Get gc2
     *
     * @return integer
     */
    public function getGc2()
    {
        return $this->gc2;
    }

    /**
     * Set gc3
     *
     * @param integer $gc3
     * @return EncuestasNosql
     */
    public function setGc3($gc3)
    {
        $this->gc3 = $gc3;

        return $this;
    }

    /**
     * Get gc3
     *
     * @return integer
     */
    public function getGc3()
    {
        return $this->gc3;
    }

    /**
     * Set gc4
     *
     * @param integer $gc4
     * @return EncuestasNosql
     */
    public function setGc4($gc4)
    {
        $this->gc4 = $gc4;

        return $this;
    }

    /**
     * Get gc4
     *
     * @return integer
     */
    public function getGc4()
    {
        return $this->gc4;
    }

    /**
     * Set of1
     *
     * @param integer $of1
     * @return EncuestasNosql
     */
    public function setOf1($of1)
    {
        $this->of1 = $of1;

        return $this;
    }

    /**
     * Get of1
     *
     * @return integer
     */
    public function getOf1()
    {
        return $this->of1;
    }

    /**
     * Set of2
     *
     * @param integer $of2
     * @return EncuestasNosql
     */
    public function setOf2($of2)
    {
        $this->of2 = $of2;

        return $this;
    }

    /**
     * Get of2
     *
     * @return integer
     */
    public function getOf2()
    {
        return $this->of2;
    }

    /**
     * Set of3
     *
     * @param integer $of3
     * @return EncuestasNosql
     */
    public function setOf3($of3)
    {
        $this->of3 = $of3;

        return $this;
    }

    /**
     * Get of3
     *
     * @return integer
     */
    public function getOf3()
    {
        return $this->of3;
    }

    /**
     * Set of4
     *
     * @param integer $of4
     * @return EncuestasNosql
     */
    public function setOf4($of4)
    {
        $this->of4 = $of4;

        return $this;
    }

    /**
     * Get of4
     *
     * @return integer
     */
    public function getOf4()
    {
        return $this->of4;
    }

    /**
     * Set p2
     *
     * @param integer $p2
     * @return EncuestasNosql
     */
    public function setP2($p2)
    {
        $this->p2 = $p2;

        return $this;
    }

    /**
     * Get p2
     *
     * @return integer
     */
    public function getP2()
    {
        return $this->p2;
    }

    /**
     * Set pc1
     *
     * @param integer $pc1
     * @return EncuestasNosql
     */
    public function setPc1($pc1)
    {
        $this->pc1 = $pc1;

        return $this;
    }

    /**
     * Get pc1
     *
     * @return integer
     */
    public function getPc1()
    {
        return $this->pc1;
    }

    /**
     * Set pc2
     *
     * @param integer $pc2
     * @return EncuestasNosql
     */
    public function setPc2($pc2)
    {
        $this->pc2 = $pc2;

        return $this;
    }

    /**
     * Get pc2
     *
     * @return integer
     */
    public function getPc2()
    {
        return $this->pc2;
    }

    /**
     * Set ps1
     *
     * @param integer $ps1
     * @return EncuestasNosql
     */
    public function setPs1($ps1)
    {
        $this->ps1 = $ps1;

        return $this;
    }

    /**
     * Get ps1
     *
     * @return integer
     */
    public function getPs1()
    {
        return $this->ps1;
    }

    /**
     * Set ps2
     *
     * @param integer $ps2
     * @return EncuestasNosql
     */
    public function setPs2($ps2)
    {
        $this->ps2 = $ps2;

        return $this;
    }

    /**
     * Get ps2
     *
     * @return integer
     */
    public function getPs2()
    {
        return $this->ps2;
    }

    /**
     * Set opiPregunta
     *
     * @param \SurveyBundle\Entity\EncuestaPreguntaOpi $opiPregunta
     * @return EncuestasNosql
     */
    public function setOpiPregunta(\SurveyBundle\Entity\EncuestaPreguntaOpi $opiPregunta = null)
    {
        $this->opiPregunta = $opiPregunta;

        return $this;
    }

    /**
     * Get opiPregunta
     *
     * @return \SurveyBundle\Entity\EncuestaPreguntaOpi
     */
    public function getOpiPregunta()
    {
        return $this->opiPregunta;
    }

    /**
     * Set opiPreguntaString
     *
     * @param string $opiPreguntaString
     * @return EncuestasNosql
     */
    public function setOpiPreguntaString($opiPreguntaString)
    {
        $this->opiPreguntaString = $opiPreguntaString;

        return $this;
    }

    /**
     * Get opiPreguntaString
     *
     * @return string
     */
    public function getOpiPreguntaString()
    {
        return $this->opiPreguntaString;
    }

    /**
     * Set segmentoString
     *
     * @param string $segmentoString
     * @return EncuestasNosql
     */
    public function setSegmentoString($segmentoString)
    {
        $this->segmentoString = $segmentoString;

        return $this;
    }

    /**
     * Get segmentoString
     *
     * @return string
     */
    public function getSegmentoString()
    {
        return $this->segmentoString;
    }

    /**
     * Set negocioString
     *
     * @param string $negocioString
     * @return EncuestasNosql
     */
    public function setNegocioString($negocioString)
    {
        $this->negocioString = $negocioString;

        return $this;
    }

    /**
     * Get negocioString
     *
     * @return string
     */
    public function getNegocioString()
    {
        return $this->negocioString;
    }

    /**
     * Set lineaNegocioString
     *
     * @param string $lineaNegocioString
     * @return EncuestasNosql
     */
    public function setLineaNegocioString($lineaNegocioString)
    {
        $this->lineaNegocioString = $lineaNegocioString;

        return $this;
    }

    /**
     * Get lineaNegocioString
     *
     * @return string
     */
    public function getLineaNegocioString()
    {
        return $this->lineaNegocioString;
    }

    /**
     * Set canalString
     *
     * @param string $canalString
     * @return EncuestasNosql
     */
    public function setCanalString($canalString)
    {
        $this->canalString = $canalString;

        return $this;
    }

    /**
     * Get canalString
     *
     * @return string
     */
    public function getCanalString()
    {
        return $this->canalString;
    }

    /**
     * Set detalleCanalString
     *
     * @param string $detalleCanalString
     * @return EncuestasNosql
     */
    public function setDetalleCanalString($detalleCanalString)
    {
        $this->detalleCanalString = $detalleCanalString;

        return $this;
    }

    /**
     * Get detalleCanalString
     *
     * @return string
     */
    public function getDetalleCanalString()
    {
        return $this->detalleCanalString;
    }

    /**
     * Set servicioString
     *
     * @param string $servicioString
     * @return EncuestasNosql
     */
    public function setServicioString($servicioString)
    {
        $this->servicioString = $servicioString;

        return $this;
    }

    /**
     * Get servicioString
     *
     * @return string
     */
    public function getServicioString()
    {
        return $this->servicioString;
    }

    /**
     * Set g1Valor
     *
     * @param integer $g1Valor
     * @return EncuestasNosql
     */
    public function setG1Valor($g1Valor)
    {
        $this->g1Valor = $g1Valor;

        return $this;
    }

    /**
     * Get g1Valor
     *
     * @return integer
     */
    public function getG1Valor()
    {
        return $this->g1Valor;
    }

    /**
     * Set lat
     *
     * @param string $lat
     * @return EncuestasNosql
     */
    public function setLat($lat)
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * Get lat
     *
     * @return string
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set lon
     *
     * @param string $lon
     * @return EncuestasNosql
     */
    public function setLon($lon)
    {
        $this->lon = $lon;

        return $this;
    }

    /**
     * Get lon
     *
     * @return string
     */
    public function getLon()
    {
        return $this->lon;
    }

    /**
     * Set nconcn
     *
     * @param string $nconcn
     * @return EncuestasNosql
     */
    public function setNconcn($nconcn)
    {
        $this->nconcn = $nconcn;

        return $this;
    }

    /**
     * Get nconcn
     *
     * @return string
     */
    public function getNconcn()
    {
        return $this->nconcn;
    }

    /**
     * Set pais
     *
     * @param \AdminBundle\Entity\Pais $pais
     * @return EncuestasNosql
     */
    public function setPais(\AdminBundle\Entity\Pais $pais = null)
    {
        $this->pais = $pais;

        return $this;
    }

    /**
     * Get pais
     *
     * @return \AdminBundle\Entity\Pais
     */
    public function getPais()
    {
        return $this->pais;
    }

    /**
     * Set seg
     *
     * @param \AdminBundle\Entity\Segmento $seg
     * @return EncuestasNosql
     */
    public function setSeg(\AdminBundle\Entity\Segmento $seg = null)
    {
        $this->seg = $seg;

        return $this;
    }

    /**
     * Get seg
     *
     * @return \AdminBundle\Entity\Segmento
     */
    public function getSeg()
    {
        return $this->seg;
    }

    /**
     * Set neg
     *
     * @param \AdminBundle\Entity\Negocio $neg
     * @return EncuestasNosql
     */
    public function setNeg(\AdminBundle\Entity\Negocio $neg = null)
    {
        $this->neg = $neg;

        return $this;
    }

    /**
     * Get neg
     *
     * @return \AdminBundle\Entity\Negocio
     */
    public function getNeg()
    {
        return $this->neg;
    }

    /**
     * Set org
     *
     * @param \AdminBundle\Entity\LineaNegocio $org
     * @return EncuestasNosql
     */
    public function setOrg(\AdminBundle\Entity\LineaNegocio $org = null)
    {
        $this->org = $org;

        return $this;
    }

    /**
     * Get org
     *
     * @return \AdminBundle\Entity\LineaNegocio
     */
    public function getOrg()
    {
        return $this->org;
    }

    /**
     * Set can
     *
     * @param \AdminBundle\Entity\CanalEncuestacion $can
     * @return EncuestasNosql
     */
    public function setCan(\AdminBundle\Entity\CanalEncuestacion $can = null)
    {
        $this->can = $can;

        return $this;
    }

    /**
     * Get can
     *
     * @return \AdminBundle\Entity\CanalEncuestacion
     */
    public function getCan()
    {
        return $this->can;
    }

    /**
     * Set detCan
     *
     * @param \AdminBundle\Entity\DeterminacionCanal $detCan
     * @return EncuestasNosql
     */
    public function setDetCan(\AdminBundle\Entity\DeterminacionCanal $detCan = null)
    {
        $this->detCan = $detCan;

        return $this;
    }

    /**
     * Get detCan
     *
     * @return \AdminBundle\Entity\DeterminacionCanal
     */
    public function getDetCan()
    {
        return $this->detCan;
    }

    /**
     * Set ser1
     *
     * @param \AdminBundle\Entity\Servicio $ser1
     * @return EncuestasNosql
     */
    public function setSer1(\AdminBundle\Entity\Servicio $ser1 = null)
    {
        $this->ser1 = $ser1;

        return $this;
    }

    /**
     * Get ser1
     *
     * @return \AdminBundle\Entity\Servicio
     */
    public function getSer1()
    {
        return $this->ser1;
    }

    /**
     * Set e1
     *
     * @param integer $e1
     * @return EncuestasNosql
     */
    public function setE1($e1)
    {
        $this->e1 = $e1;

        return $this;
    }

    /**
     * Get e1
     *
     * @return integer
     */
    public function getE1()
    {
        return $this->e1;
    }

    /**
     * Set e2
     *
     * @param integer $e2
     * @return EncuestasNosql
     */
    public function setE2($e2)
    {
        $this->e2 = $e2;

        return $this;
    }

    /**
     * Get e2
     *
     * @return integer
     */
    public function getE2()
    {
        return $this->e2;
    }

    /**
     * Set e3
     *
     * @param integer $e3
     * @return EncuestasNosql
     */
    public function setE3($e3)
    {
        $this->e3 = $e3;

        return $this;
    }

    /**
     * Get e3
     *
     * @return integer
     */
    public function getE3()
    {
        return $this->e3;
    }

    /**
     * Set e4
     *
     * @param integer $e4
     * @return EncuestasNosql
     */
    public function setE4($e4)
    {
        $this->e4 = $e4;

        return $this;
    }

    /**
     * Get e4
     *
     * @return integer
     */
    public function getE4()
    {
        return $this->e4;
    }

    /**
     * Set procesos
     *
     * @param string $procesos
     * @return EncuestasNosql
     */
    public function setProcesos($procesos)
    {
        $this->procesos = $procesos;

        return $this;
    }

    /**
     * Get procesos
     *
     * @return string 
     */
    public function getProcesos()
    {
        return $this->procesos;
    }

    /**
     * Set warning
     *
     * @param boolean $warning
     * @return EncuestasNosql
     */
    public function setWarning($warning)
    {
        $this->warning = $warning;

        return $this;
    }

    /**
     * Get warning
     *
     * @return boolean 
     */
    public function getWarning()
    {
        return $this->warning;
    }
}

<?php

namespace SurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EncuestaPregunta
 *
 * @ORM\Table(name="survey_encuesta_pregunta")
 * @ORM\Entity(repositoryClass="SurveyBundle\Repository\EncuestaPreguntaRepository")
 */
class EncuestaPregunta
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(name="descripcion", type="string", length=255, nullable=true)
     */
    private $descripcion;

    /**
     * @ORM\Column(name="descripcion_completa", type="string", length=255, nullable=true)
     */
    private $descripcion_completa;

    /**
     * @ORM\OneToMany(targetEntity="EncuestaRespuesta", mappedBy="encuestaPregunta")
     */
    private $encuestaRespuestas;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\TipoCem", inversedBy="preguntas")
     */
    private $cemTipo;

    /**
     * @ORM\ManyToOne(targetEntity="EncuestaSubBloque", inversedBy="preguntas")
     */
    private $subBloque;

    /**
     * @ORM\Column(name="orden_pregunta_proceso", type="integer", nullable=false)
     * Es el ordinal que ocupa la pregunta dentro del proceso al que pertenece
     */
    private $ordenPreguntaProceso;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return EncuestaPregunta
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->encuestaRespuestas = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add encuestaRespuestas
     *
     * @param \SurveyBundle\Entity\EncuestaRespuesta $encuestaRespuestas
     * @return EncuestaPregunta
     */
    public function addEncuestaRespuesta(\SurveyBundle\Entity\EncuestaRespuesta $encuestaRespuestas)
    {
        $this->encuestaRespuestas[] = $encuestaRespuestas;

        return $this;
    }

    /**
     * Remove encuestaRespuestas
     *
     * @param \SurveyBundle\Entity\EncuestaRespuesta $encuestaRespuestas
     */
    public function removeEncuestaRespuesta(\SurveyBundle\Entity\EncuestaRespuesta $encuestaRespuestas)
    {
        $this->encuestaRespuestas->removeElement($encuestaRespuestas);
    }

    /**
     * Get encuestaRespuestas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEncuestaRespuestas()
    {
        return $this->encuestaRespuestas;
    }

    /**
     * Set cemTipo
     *
     * @param \SurveyBundle\Entity\CemTipo $cemTipo
     * @return EncuestaPregunta
     */
    public function setCemTipo($cemTipo = null)
    {
        $this->cemTipo = $cemTipo;

        return $this;
    }

    /**
     * Get cemTipo
     *
     * @return \SurveyBundle\Entity\CemTipo
     */
    public function getCemTipo()
    {
        return $this->cemTipo;
    }

    /**
     * Set encuestaBloque
     *
     * @param \SurveyBundle\Entity\EncuestaBloque $encuestaBloque
     * @return EncuestaPregunta
     */
    public function setEncuestaBloque(\SurveyBundle\Entity\EncuestaBloque $encuestaBloque = null)
    {
        $this->encuestaBloque = $encuestaBloque;

        return $this;
    }

    /**
     * Get encuestaBloque
     *
     * @return \SurveyBundle\Entity\EncuestaBloque
     */
    public function getEncuestaBloque()
    {
        return $this->encuestaBloque;
    }

    /**
     * Set subBloque
     *
     * @param \SurveyBundle\Entity\EncuestaSubBloque $subBloque
     * @return EncuestaPregunta
     */
    public function setSubBloque(\SurveyBundle\Entity\EncuestaSubBloque $subBloque = null)
    {
        $this->subBloque = $subBloque;

        return $this;
    }

    /**
     * Get subBloque
     *
     * @return \SurveyBundle\Entity\EncuestaSubBloque
     */
    public function getSubBloque()
    {
        return $this->subBloque;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return EncuestaPregunta
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set descripcion_completa
     *
     * @param string $descripcionCompleta
     * @return EncuestaPregunta
     */
    public function setDescripcionCompleta($descripcionCompleta)
    {
        $this->descripcion_completa = $descripcionCompleta;

        return $this;
    }

    /**
     * Get descripcion_completa
     *
     * @return string
     */
    public function getDescripcionCompleta()
    {
        return $this->descripcion_completa;
    }


    /**
     * Set ordenPreguntaProceso
     *
     * @param string $ordenPreguntaProceso
     * @return EncuestaPregunta
     */
    public function setOrdenPreguntaProceso($ordenPreguntaProceso)
    {
        $this->ordenPreguntaProceso = $ordenPreguntaProceso;

        return $this;
    }

    /**
     * Get ordenPreguntaProceso
     *
     * @return string
     */
    public function getOrdenPreguntaProceso()
    {
        return $this->ordenPreguntaProceso;
    }
}

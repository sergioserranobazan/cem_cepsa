<?php

namespace SurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EncuestaProcesosNosql
 *
 * @ORM\Table(name="nosql_encuesta_procesos")
 * @ORM\Entity(repositoryClass="SurveyBundle\Repository\EncuestaProcesosNosqlRepository")
 */
class EncuestaProcesosNosql
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Encuesta")
     */
    private $encuesta;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\Proceso")
     */
    private $proceso;

    /**
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\Pais")
     */
    private $pais;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\LineaNegocio")
     */
    private $org;

    /**
     * @ORM\Column(name="suma_valoracion", type="integer")
     */
    private $sumaValoracion;

    /**
     * @ORM\Column(name="num_preguntas_valoracion", type="integer")
     */
    private $numPreguntasValoracion;

    /**
     * @ORM\Column(name="opi_id", type="string", length=255)
     */
    private $opiId;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\Segmento")
     */
    private $seg;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\Negocio")
     */
    private $neg;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\CanalEncuestacion")
     */
    private $can;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\DeterminacionCanal")
     */
    private $detCan;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\Servicio")
     */
    private $ser1;

    /**
     * @ORM\ManyToOne(targetEntity="SurveyBundle\Entity\EncuestaPreguntaOpi")
     */
    private $opiPregunta;

    /**
     * @ORM\Column(name="warning", type="boolean", nullable=true)
     */
    private $warning;


    // --------- PREGUNTAS --------------
    //

    /**
     * @ORM\Column(name="g1", type="string", length=255, nullable=false)
     * Sentimiento
     */
    private $g1;

    /**
     * @ORM\Column(name="g1_valor", type="integer", nullable=false)
     * Valor numérico correspondiente al adjetivo presente en g1
     */
    private $g1Valor;

    /**
     * @ORM\Column(name="g2", type="integer", nullable=false)
     * Recomendación
     */
    private $g2;

    /**
     * @ORM\Column(name="g3", type="integer", nullable=false)
     * Experiencia
     */
    private $g3;

    /**
     * @ORM\Column(name="p1", type="integer", nullable=true)
     * Pregunta de proceso 1
     */
    private $p1;

    /**
     * @ORM\ManyToOne(targetEntity="EncuestaPregunta")
     * Id Pregunta de proceso 1
     */
    private $p1_pregunta;

    /**
     * @ORM\Column(name="p2", type="integer", nullable=true)
     * Pregunta de proceso 1
     */
    private $p2;

    /**
     * @ORM\ManyToOne(targetEntity="EncuestaPregunta")
     * Id Pregunta de proceso 2
     */
    private $p2_pregunta;

    /**
     * @ORM\Column(name="p3", type="integer", nullable=true)
     * Pregunta de proceso 1
     */
    private $p3;

    /**
     * @ORM\ManyToOne(targetEntity="EncuestaPregunta")
     * Id Pregunta de proceso 3
     */
    private $p3_pregunta;

    /**
     * @ORM\Column(name="p4", type="integer", nullable=true)
     * Pregunta de proceso 1
     */
    private $p4;

    /**
     * @ORM\ManyToOne(targetEntity="EncuestaPregunta")
     * Id Pregunta de proceso 4
     */
    private $p4_pregunta;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sumaValoracion
     *
     * @param integer $sumaValoracion
     * @return EncuestaProcesosNosql
     */
    public function setSumaValoracion($sumaValoracion)
    {
        $this->sumaValoracion = $sumaValoracion;

        return $this;
    }

    /**
     * Get sumaValoracion
     *
     * @return integer
     */
    public function getSumaValoracion()
    {
        return $this->sumaValoracion;
    }

    /**
     * Set numPreguntasValoracion
     *
     * @param integer $numPreguntasValoracion
     * @return EncuestaProcesosNosql
     */
    public function setNumPreguntasValoracion($numPreguntasValoracion)
    {
        $this->numPreguntasValoracion = $numPreguntasValoracion;

        return $this;
    }

    /**
     * Get numPreguntasValoracion
     *
     * @return integer
     */
    public function getNumPreguntasValoracion()
    {
        return $this->numPreguntasValoracion;
    }

    /**
     * Set procesoString
     *
     * @param string $procesoString
     * @return EncuestaProcesosNosql
     */
    public function setProcesoString($procesoString)
    {
        $this->procesoString = $procesoString;

        return $this;
    }

    /**
     * Get procesoString
     *
     * @return string
     */
    public function getProcesoString()
    {
        return $this->procesoString;
    }

    /**
     * Set encuesta
     *
     * @param \SurveyBundle\Entity\Encuesta $encuesta
     * @return EncuestaProcesosNosql
     */
    public function setEncuesta(\SurveyBundle\Entity\Encuesta $encuesta = null)
    {
        $this->encuesta = $encuesta;

        return $this;
    }

    /**
     * Get encuesta
     *
     * @return \SurveyBundle\Entity\Encuesta
     */
    public function getEncuesta()
    {
        return $this->encuesta;
    }

    /**
     * Set proceso
     *
     * @param \AdminBundle\Entity\Proceso $proceso
     * @return EncuestaProcesosNosql
     */
    public function setProceso(\AdminBundle\Entity\Proceso $proceso = null)
    {
        $this->proceso = $proceso;

        return $this;
    }

    /**
     * Get proceso
     *
     * @return \AdminBundle\Entity\Proceso
     */
    public function getProceso()
    {
        return $this->proceso;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return EncuestaProcesosNosql
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set opiId
     *
     * @param string $opiId
     * @return EncuestaProcesosNosql
     */
    public function setOpiId($opiId)
    {
        $this->opiId = $opiId;

        return $this;
    }

    /**
     * Get opiId
     *
     * @return string
     */
    public function getOpiId()
    {
        return $this->opiId;
    }

    /**
     * Set lineaNegocio
     *
     * @param \AdminBundle\Entity\LineaNegocio $lineaNegocio
     * @return EncuestaProcesosNosql
     */
    public function setLineaNegocio(\AdminBundle\Entity\LineaNegocio $lineaNegocio = null)
    {
        $this->lineaNegocio = $lineaNegocio;

        return $this;
    }

    /**
     * Get lineaNegocio
     *
     * @return \AdminBundle\Entity\LineaNegocio
     */
    public function getLineaNegocio()
    {
        return $this->lineaNegocio;
    }

    /**
     * Set segmento
     *
     * @param \AdminBundle\Entity\Segmento $segmento
     * @return EncuestaProcesosNosql
     */
    public function setSegmento(\AdminBundle\Entity\Segmento $segmento = null)
    {
        $this->segmento = $segmento;

        return $this;
    }

    /**
     * Get segmento
     *
     * @return \AdminBundle\Entity\Segmento
     */
    public function getSegmento()
    {
        return $this->segmento;
    }

    /**
     * Set negocio
     *
     * @param \AdminBundle\Entity\Negocio $negocio
     * @return EncuestaProcesosNosql
     */
    public function setNegocio(\AdminBundle\Entity\Negocio $negocio = null)
    {
        $this->negocio = $negocio;

        return $this;
    }

    /**
     * Get negocio
     *
     * @return \AdminBundle\Entity\Negocio
     */
    public function getNegocio()
    {
        return $this->negocio;
    }

    /**
     * Set canal
     *
     * @param \AdminBundle\Entity\CanalEncuestacion $canal
     * @return EncuestaProcesosNosql
     */
    public function setCanal(\AdminBundle\Entity\CanalEncuestacion $canal = null)
    {
        $this->canal = $canal;

        return $this;
    }

    /**
     * Get canal
     *
     * @return \AdminBundle\Entity\CanalEncuestacion
     */
    public function getCanal()
    {
        return $this->canal;
    }

    /**
     * Set detalle_canal
     *
     * @param \AdminBundle\Entity\DeterminacionCanal $detalleCanal
     * @return EncuestaProcesosNosql
     */
    public function setDetalleCanal(\AdminBundle\Entity\DeterminacionCanal $detalleCanal = null)
    {
        $this->detalle_canal = $detalleCanal;

        return $this;
    }

    /**
     * Get detalle_canal
     *
     * @return \AdminBundle\Entity\DeterminacionCanal
     */
    public function getDetalleCanal()
    {
        return $this->detalle_canal;
    }

    /**
     * Set servicio
     *
     * @param \AdminBundle\Entity\Servicio $servicio
     * @return EncuestaProcesosNosql
     */
    public function setServicio(\AdminBundle\Entity\Servicio $servicio = null)
    {
        $this->servicio = $servicio;

        return $this;
    }

    /**
     * Get servicio
     *
     * @return \AdminBundle\Entity\Servicio
     */
    public function getServicio()
    {
        return $this->servicio;
    }

    /**
     * Set g1
     *
     * @param string $g1
     * @return EncuestaProcesosNosql
     */
    public function setG1($g1)
    {
        $this->g1 = $g1;

        return $this;
    }

    /**
     * Get g1
     *
     * @return string
     */
    public function getG1()
    {
        return $this->g1;
    }

    /**
     * Set g1Valor
     *
     * @param integer $g1Valor
     * @return EncuestaProcesosNosql
     */
    public function setG1Valor($g1Valor)
    {
        $this->g1Valor = $g1Valor;

        return $this;
    }

    /**
     * Get g1Valor
     *
     * @return integer
     */
    public function getG1Valor()
    {
        return $this->g1Valor;
    }

    /**
     * Set g2
     *
     * @param integer $g2
     * @return EncuestaProcesosNosql
     */
    public function setG2($g2)
    {
        $this->g2 = $g2;

        return $this;
    }

    /**
     * Get g2
     *
     * @return integer
     */
    public function getG2()
    {
        return $this->g2;
    }

    /**
     * Set g3
     *
     * @param integer $g3
     * @return EncuestaProcesosNosql
     */
    public function setG3($g3)
    {
        $this->g3 = $g3;

        return $this;
    }

    /**
     * Get g3
     *
     * @return integer
     */
    public function getG3()
    {
        return $this->g3;
    }

    /**
     * Set pais
     *
     * @param \DateTime $pais
     * @return EncuestaProcesosNosql
     */
    public function setPais($pais)
    {
        $this->pais = $pais;

        return $this;
    }

    /**
     * Get pais
     *
     * @return \DateTime
     */
    public function getPais()
    {
        return $this->pais;
    }

    /**
     * Set org
     *
     * @param \AdminBundle\Entity\LineaNegocio $org
     * @return EncuestaProcesosNosql
     */
    public function setOrg(\AdminBundle\Entity\LineaNegocio $org = null)
    {
        $this->org = $org;

        return $this;
    }

    /**
     * Get org
     *
     * @return \AdminBundle\Entity\LineaNegocio
     */
    public function getOrg()
    {
        return $this->org;
    }

    /**
     * Set seg
     *
     * @param \AdminBundle\Entity\Segmento $seg
     * @return EncuestaProcesosNosql
     */
    public function setSeg(\AdminBundle\Entity\Segmento $seg = null)
    {
        $this->seg = $seg;

        return $this;
    }

    /**
     * Get seg
     *
     * @return \AdminBundle\Entity\Segmento
     */
    public function getSeg()
    {
        return $this->seg;
    }

    /**
     * Set neg
     *
     * @param \AdminBundle\Entity\Negocio $neg
     * @return EncuestaProcesosNosql
     */
    public function setNeg(\AdminBundle\Entity\Negocio $neg = null)
    {
        $this->neg = $neg;

        return $this;
    }

    /**
     * Get neg
     *
     * @return \AdminBundle\Entity\Negocio
     */
    public function getNeg()
    {
        return $this->neg;
    }

    /**
     * Set can
     *
     * @param \AdminBundle\Entity\CanalEncuestacion $can
     * @return EncuestaProcesosNosql
     */
    public function setCan(\AdminBundle\Entity\CanalEncuestacion $can = null)
    {
        $this->can = $can;

        return $this;
    }

    /**
     * Get can
     *
     * @return \AdminBundle\Entity\CanalEncuestacion
     */
    public function getCan()
    {
        return $this->can;
    }

    /**
     * Set detCan
     *
     * @param \AdminBundle\Entity\DeterminacionCanal $detCan
     * @return EncuestaProcesosNosql
     */
    public function setDetCan(\AdminBundle\Entity\DeterminacionCanal $detCan = null)
    {
        $this->detCan = $detCan;

        return $this;
    }

    /**
     * Get detCan
     *
     * @return \AdminBundle\Entity\DeterminacionCanal
     */
    public function getDetCan()
    {
        return $this->detCan;
    }

    /**
     * Set ser1
     *
     * @param \AdminBundle\Entity\Servicio $ser1
     * @return EncuestaProcesosNosql
     */
    public function setSer1(\AdminBundle\Entity\Servicio $ser1 = null)
    {
        $this->ser1 = $ser1;

        return $this;
    }

    /**
     * Get ser1
     *
     * @return \AdminBundle\Entity\Servicio
     */
    public function getSer1()
    {
        return $this->ser1;
    }

    /**
     * Set p1
     *
     * @param integer $p1
     * @return EncuestaProcesosNosql
     */
    public function setP1($p1)
    {
        $this->p1 = $p1;

        return $this;
    }

    /**
     * Get p1
     *
     * @return integer
     */
    public function getP1()
    {
        return $this->p1;
    }

    /**
     * Set p2
     *
     * @param integer $p2
     * @return EncuestaProcesosNosql
     */
    public function setP2($p2)
    {
        $this->p2 = $p2;

        return $this;
    }

    /**
     * Get p2
     *
     * @return integer
     */
    public function getP2()
    {
        return $this->p2;
    }

    /**
     * Set p3
     *
     * @param integer $p3
     * @return EncuestaProcesosNosql
     */
    public function setP3($p3)
    {
        $this->p3 = $p3;

        return $this;
    }

    /**
     * Get p3
     *
     * @return integer
     */
    public function getP3()
    {
        return $this->p3;
    }

    /**
     * Set p4
     *
     * @param integer $p4
     * @return EncuestaProcesosNosql
     */
    public function setP4($p4)
    {
        $this->p4 = $p4;

        return $this;
    }

    /**
     * Get p4
     *
     * @return integer
     */
    public function getP4()
    {
        return $this->p4;
    }

    /**
     * Set p1_pregunta
     *
     * @param \SurveyBundle\Entity\EncuestaPregunta $p1Pregunta
     * @return EncuestaProcesosNosql
     */
    public function setP1Pregunta(\SurveyBundle\Entity\EncuestaPregunta $p1Pregunta = null)
    {
        $this->p1_pregunta = $p1Pregunta;

        return $this;
    }

    /**
     * Get p1_pregunta
     *
     * @return \SurveyBundle\Entity\EncuestaPregunta
     */
    public function getP1Pregunta()
    {
        return $this->p1_pregunta;
    }

    /**
     * Set p2_pregunta
     *
     * @param \SurveyBundle\Entity\EncuestaPregunta $p2Pregunta
     * @return EncuestaProcesosNosql
     */
    public function setP2Pregunta(\SurveyBundle\Entity\EncuestaPregunta $p2Pregunta = null)
    {
        $this->p2_pregunta = $p2Pregunta;

        return $this;
    }

    /**
     * Get p2_pregunta
     *
     * @return \SurveyBundle\Entity\EncuestaPregunta
     */
    public function getP2Pregunta()
    {
        return $this->p2_pregunta;
    }

    /**
     * Set p3_pregunta
     *
     * @param \SurveyBundle\Entity\EncuestaPregunta $p3Pregunta
     * @return EncuestaProcesosNosql
     */
    public function setP3Pregunta(\SurveyBundle\Entity\EncuestaPregunta $p3Pregunta = null)
    {
        $this->p3_pregunta = $p3Pregunta;

        return $this;
    }

    /**
     * Get p3_pregunta
     *
     * @return \SurveyBundle\Entity\EncuestaPregunta
     */
    public function getP3Pregunta()
    {
        return $this->p3_pregunta;
    }

    /**
     * Set p4_pregunta
     *
     * @param \SurveyBundle\Entity\EncuestaPregunta $p4Pregunta
     * @return EncuestaProcesosNosql
     */
    public function setP4Pregunta(\SurveyBundle\Entity\EncuestaPregunta $p4Pregunta = null)
    {
        $this->p4_pregunta = $p4Pregunta;

        return $this;
    }

    /**
     * Get p4_pregunta
     *
     * @return \SurveyBundle\Entity\EncuestaPregunta
     */
    public function getP4Pregunta()
    {
        return $this->p4_pregunta;
    }

    /**
     * Set warning
     *
     * @param boolean $warning
     * @return EncuestaProcesosNosql
     */
    public function setWarning($warning)
    {
        $this->warning = $warning;

        return $this;
    }

    /**
     * Get warning
     *
     * @return boolean 
     */
    public function getWarning()
    {
        return $this->warning;
    }

    /**
     * Set opiPregunta
     *
     * @param \SurveyBundle\Entity\EncuestaPreguntaOpi $opiPregunta
     * @return EncuestaProcesosNosql
     */
    public function setOpiPregunta(\SurveyBundle\Entity\EncuestaPreguntaOpi $opiPregunta = null)
    {
        $this->opiPregunta = $opiPregunta;

        return $this;
    }

    /**
     * Get opiPregunta
     *
     * @return \SurveyBundle\Entity\EncuestaPreguntaOpi 
     */
    public function getOpiPregunta()
    {
        return $this->opiPregunta;
    }
}

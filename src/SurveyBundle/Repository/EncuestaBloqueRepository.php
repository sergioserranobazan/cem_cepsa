<?php

namespace SurveyBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * EncuestaBloqueRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class EncuestaBloqueRepository extends EntityRepository
{
}

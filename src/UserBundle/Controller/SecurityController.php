<?php

namespace UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Controller\SecurityController as BaseSecurityController;

class SecurityController extends BaseSecurityController
{

    /**
     * Overriding login to add custom logic.
     */
    public function loginAction(Request $request)
    {
        if( $this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED') ){
            // IS_AUTHENTICATED_FULLY also implies IS_AUTHENTICATED_REMEMBERED,
            // but IS_AUTHENTICATED_ANONYMOUSLY doesn't

            return new RedirectResponse($this->container->get('router')->generate('front', array()));
        }

        return parent::loginAction($request);
    }
}

// Credit: http://stackoverflow.com/a/22169951
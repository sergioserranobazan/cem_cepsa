<?php

namespace CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Contenedor
 *
 * @ORM\Table(name="cms_contenedor")
 * @ORM\Entity(repositoryClass="CmsBundle\Repository\ContenedorRepository")
 */
class Contenedor
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="CmsBundle\Entity\Zona", inversedBy="contenedores")
     */
    private $zona;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255)
     */
    private $descripcion;

    /**
     * Valor entre 0 y 1 que indica si el elemento ha de representarse en la
     * aplicación o no.

     * @ORM\Column(name="activo", type="integer")
     */
    private $activo;

    /**
     * Orden de las zonas en la maqueta
     * @ORM\Column(name="orden", type="integer")
     */
    private $orden;

    /**
     * Nombre de la plantilla de twig asociada a la zona
     * @ORM\Column(name="twig", type="string", length=255)
     */
    private $twig;

    /**
     * @ORM\OneToMany(targetEntity="CmsBundle\Entity\Widget", mappedBy="contenedor")
     * @ORM\OrderBy({"orden" = "ASC"})
     */
    private $widgets;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->widgets = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Contenedor
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Contenedor
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set activo
     *
     * @param integer $activo
     * @return Contenedor
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return integer
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set orden
     *
     * @param integer $orden
     * @return Contenedor
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden
     *
     * @return integer
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * Set twig
     *
     * @param string $twig
     * @return Contenedor
     */
    public function setTwig($twig)
    {
        $this->twig = $twig;

        return $this;
    }

    /**
     * Get twig
     *
     * @return string
     */
    public function getTwig()
    {
        return "FrontBundle:Contenedores:" . $this->twig . ".html.twig";
    }

    /**
     * Set zona
     *
     * @param \CmsBundle\Entity\Zona $zona
     * @return Contenedor
     */
    public function setZona(\CmsBundle\Entity\Zona $zona = null)
    {
        $this->zona = $zona;

        return $this;
    }

    /**
     * Get zona
     *
     * @return \CmsBundle\Entity\Zona
     */
    public function getZona()
    {
        return $this->zona;
    }

    /**
     * Add widgets
     *
     * @param \CmsBundle\Entity\Widget $widgets
     * @return Contenedor
     */
    public function addWidget(\CmsBundle\Entity\Widget $widgets)
    {
        $this->widgets[] = $widgets;

        return $this;
    }

    /**
     * Remove widgets
     *
     * @param \CmsBundle\Entity\Widget $widgets
     */
    public function removeWidget(\CmsBundle\Entity\Widget $widgets)
    {
        $this->widgets->removeElement($widgets);
    }

    /**
     * Get widgets
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWidgets()
    {
        return $this->widgets;
    }
}

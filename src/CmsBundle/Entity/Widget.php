<?php

namespace CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Widget
 *
 * @ORM\Table(name="cms_widget")
 * @ORM\Entity(repositoryClass="CmsBundle\Repository\WidgetRepository")
 */
class Widget
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="CmsBundle\Entity\Contenedor", inversedBy="widgets")
     */
    private $contenedor;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255)
     */
    private $descripcion;

    /**
     * Valor entre 0 y 1 que indica si el elemento ha de representarse en la
     * aplicación o no.

     * @ORM\Column(name="activo", type="integer")
     */
    private $activo;

    /**
     * Orden de las zonas en la maqueta
     * @ORM\Column(name="orden", type="integer")
     */
    private $orden;

    /**
     * Nombre de la plantilla de twig asociada a la zona
     * @ORM\Column(name="twig", type="string", length=255)
     */
    private $twig;

    /**
     * Nombre para el service que alimentará de datos al widget
     * @ORM\Column(name="service", type="string", length=255)
     */
    private $service;

    /**
     * @ORM\Column(name="widget_ajax", type="boolean", options={"default":false})
     */
    private $widgetAjax;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Widget
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Widget
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set activo
     *
     * @param integer $activo
     * @return Widget
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return integer
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set orden
     *
     * @param integer $orden
     * @return Widget
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden
     *
     * @return integer
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * Set twig
     *
     * @param string $twig
     * @return Widget
     */
    public function setTwig($twig)
    {
        $this->twig = $twig;

        return $this;
    }

    /**
     * Get twig
     *
     * @return string
     */
    public function getTwig()
    {
        return "FrontBundle:Widgets:" . $this->twig . ".html.twig";
    }

    /**
     * Set contenedor
     *
     * @param \CmsBundle\Entity\Contenedor $contenedor
     * @return Widget
     */
    public function setContenedor(\CmsBundle\Entity\Contenedor $contenedor = null)
    {
        $this->contenedor = $contenedor;

        return $this;
    }

    /**
     * Get contenedor
     *
     * @return \CmsBundle\Entity\Contenedor
     */
    public function getContenedor()
    {
        return $this->contenedor;
    }

    /**
     * Set service
     *
     * @param string $service
     * @return Widget
     */
    public function setService($service)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return string
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Set widgetAjax
     *
     * @param boolean $widgetAjax
     * @return Widget
     */
    public function setWidgetAjax($widgetAjax)
    {
        $this->widgetAjax = $widgetAjax;

        return $this;
    }

    /**
     * Get widgetAjax
     *
     * @return boolean 
     */
    public function getWidgetAjax()
    {
        return $this->widgetAjax;
    }
}

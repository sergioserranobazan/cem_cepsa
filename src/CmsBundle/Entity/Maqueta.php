<?php

namespace CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Maqueta
 *
 * @ORM\Table(name="cms_maquetas")
 * @ORM\Entity(repositoryClass="CmsBundle\Repository\MaquetaRepository")
 */
class Maqueta
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * Utilizado como texto cuando se referencia a la maqueta (por ejemplo en
     * el menú)
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255)
     */
    private $descripcion;

    /**
     * Valor entre 0 y 1 que indica si el elemento ha de representarse en la
     * aplicación o no.

     * @ORM\Column(name="activo", type="integer")
     */
    private $activo;

    /**
     * @ORM\Column(name="visible_cabecera", type="boolean")
     */
    private $visibleCabecera;

    /**
     * Orden de las maquetas
     * @ORM\Column(name="orden", type="integer")
     */
    private $orden;

    /**
     * Nombre de la plantilla de twig asociada a la maqueta
     * @ORM\Column(name="twig", type="string", length=255)
     */
    private $twig;

    /**
     * Referencia del icono de font-awesome para el menú
     * @ORM\Column(name="icono", type="string", length=255)
     */
    private $icono;

    /**
     * @ORM\OneToMany(targetEntity="CmsBundle\Entity\Zona", mappedBy="maqueta")
     * @ORM\OrderBy({"orden" = "ASC"})
     */
    private $zonas;

    /**
     * @ORM\OneToMany(targetEntity="AdminBundle\Entity\FiltrosMaqueta", mappedBy="maqueta")
     */
    private $filtrosMaqueta;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->zonas = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Maqueta
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Maqueta
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Maqueta
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set activo
     *
     * @param integer $activo
     * @return Maqueta
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return integer
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set orden
     *
     * @param integer $orden
     * @return Maqueta
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden
     *
     * @return integer
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * Set twig
     *
     * @param string $twig
     * @return Maqueta
     */
    public function setTwig($twig)
    {
        $this->twig = $twig;

        return $this;
    }

    /**
     * Get twig
     *
     * @return string
     */
    public function getTwig()
    {
        return $this->twig;
    }

    /**
     * Add zonas
     *
     * @param \CmsBundle\Entity\Zona $zonas
     * @return Maqueta
     */
    public function addZona(\CmsBundle\Entity\Zona $zonas)
    {
        $this->zonas[] = $zonas;

        return $this;
    }

    /**
     * Remove zonas
     *
     * @param \CmsBundle\Entity\Zona $zonas
     */
    public function removeZona(\CmsBundle\Entity\Zona $zonas)
    {
        $this->zonas->removeElement($zonas);
    }

    /**
     * Get zonas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getZonas()
    {
        return $this->zonas;
    }

    /**
     * Set icono
     *
     * @param string $icono
     * @return Maqueta
     */
    public function setIcono($icono)
    {
        $this->icono = $icono;

        return $this;
    }

    /**
     * Get icono
     *
     * @return string 
     */
    public function getIcono()
    {
        return $this->icono;
    }
    
    /**
     * Set visibleCabecera
     *
     * @param boolean $visibleCabecera
     * @return Maqueta
     */
    public function setVisibleCabecera($visibleCabecera)
    {
        $this->visibleCabecera = $visibleCabecera;

        return $this;
    }

    /**
     * Get visibleCabecera
     *
     * @return boolean
     */
    public function getVisibleCabecera()
    {
        return $this->visibleCabecera;
    }

    /**
     * Add filtros
     *
     * @param \AdminBundle\Entity\FiltrosMaqueta $filtros
     * @return Maqueta
     */
    public function addFiltro(\AdminBundle\Entity\FiltrosMaqueta $filtros)
    {
        $this->filtros[] = $filtros;

        return $this;
    }

    /**
     * Remove filtros
     *
     * @param \AdminBundle\Entity\FiltrosMaqueta $filtros
     */
    public function removeFiltro(\AdminBundle\Entity\FiltrosMaqueta $filtros)
    {
        $this->filtros->removeElement($filtros);
    }

    /**
     * Get filtros
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFiltros()
    {
        return $this->filtros;
    }

    /**
     * Add filtrosMaqueta
     *
     * @param \AdminBundle\Entity\FiltrosMaqueta $filtrosMaqueta
     * @return Maqueta
     */
    public function addFiltrosMaquetum(\AdminBundle\Entity\FiltrosMaqueta $filtrosMaqueta)
    {
        $this->filtrosMaqueta[] = $filtrosMaqueta;

        return $this;
    }

    /**
     * Remove filtrosMaqueta
     *
     * @param \AdminBundle\Entity\FiltrosMaqueta $filtrosMaqueta
     */
    public function removeFiltrosMaquetum(\AdminBundle\Entity\FiltrosMaqueta $filtrosMaqueta)
    {
        $this->filtrosMaqueta->removeElement($filtrosMaqueta);
    }

    /**
     * Get filtrosMaqueta
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFiltrosMaqueta()
    {
        return $this->filtrosMaqueta;
    }

}

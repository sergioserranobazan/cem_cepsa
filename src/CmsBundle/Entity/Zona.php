<?php

namespace CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Zona
 *
 * @ORM\Table(name="cms_zona")
 * @ORM\Entity(repositoryClass="CmsBundle\Repository\ZonaRepository")
 */
class Zona
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="CmsBundle\Entity\Maqueta", inversedBy="zonas")
     */
    private $maqueta;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255)
     */
    private $descripcion;

    /**
     * Valor entre 0 y 1 que indica si el elemento ha de representarse en la
     * aplicación o no.

     * @ORM\Column(name="activo", type="integer")
     */
    private $activo;

    /**
     * Orden de las zonas en la maqueta
     * @ORM\Column(name="orden", type="integer")
     */
    private $orden;

    /**
     * Nombre de la plantilla de twig asociada a la zona
     * @ORM\Column(name="twig", type="string", length=255)
     */
    private $twig;

    /**
     * @ORM\OneToMany(targetEntity="CmsBundle\Entity\Contenedor", mappedBy="zona")
     * @ORM\OrderBy({"orden" = "ASC"})
     */
    private $contenedores;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->contenedores = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Zona
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Zona
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set activo
     *
     * @param integer $activo
     * @return Zona
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return integer
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set orden
     *
     * @param integer $orden
     * @return Zona
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden
     *
     * @return integer
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * Set twig
     *
     * @param string $twig
     * @return Zona
     */
    public function setTwig($twig)
    {
        $this->twig = $twig;

        return $this;
    }

    /**
     * Get twig
     *
     * @return string
     */
    public function getTwig()
    {
        return "FrontBundle:Zonas:" . $this->twig . ".html.twig";
    }

    /**
     * Set maqueta
     *
     * @param \CmsBundle\Entity\Maqueta $maqueta
     * @return Zona
     */
    public function setMaqueta(\CmsBundle\Entity\Maqueta $maqueta = null)
    {
        $this->maqueta = $maqueta;

        return $this;
    }

    /**
     * Get maqueta
     *
     * @return \CmsBundle\Entity\Maqueta
     */
    public function getMaqueta()
    {
        return $this->maqueta;
    }

    /**
     * Add contenedores
     *
     * @param \CmsBundle\Entity\Contenedor $contenedores
     * @return Zona
     */
    public function addContenedore(\CmsBundle\Entity\Contenedor $contenedores)
    {
        $this->contenedores[] = $contenedores;

        return $this;
    }

    /**
     * Remove contenedores
     *
     * @param \CmsBundle\Entity\Contenedor $contenedores
     */
    public function removeContenedore(\CmsBundle\Entity\Contenedor $contenedores)
    {
        $this->contenedores->removeElement($contenedores);
    }

    /**
     * Get contenedores
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContenedores()
    {
        return $this->contenedores;
    }
}

<?php

namespace CmsBundle\Service;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use FrontBundle\Service\SwitcherDatosServices;

class CmsService
{
    protected $em;
    protected $filtros;
    protected $container;
    protected $maqueta; // Entidad doctrine de la maqueta actual que se
                        // configura con la función inicializarService
    protected $slug;
    protected $switcherDatosServices;

    /**
     * Inyecta los Services de Doctrine y Container. También inicializa los
     * filtros por defecto a null
     */
    public function __construct(EntityManager $em, Container $c, SwitcherDatosServices $sds){
        $this->em = $em;
        $this->container = $c;
        $this->filtros = NULL;
        $this->switcherDatosServices = $sds;
    }

    public function inicializarService($slug){
            /**
             *  Slug de la request del usuario. Nos viene dada por el controller
             *  al iniciar el service
             */
            $this->slug = $slug;

            /**
             *  Objeto doctrine de la tabla Maquetas
             */
            $this->maqueta =
                    $this->em->getRepository('CmsBundle:Maqueta')->
                    findOneBySlug($slug);

            /**
            * Lanzamiento de un error en caso de que el slug no corresponda con
            * ningún registro en Contenido
            */
            if (!$this->maqueta) {
                // TODO: personalizar error 404
                throw new \Exception("No se ha encontrado ninguna página"
                . " llamada '" . $slug . "'", 1);
            }


            return $this; // Para poder concatenar los métodos del service
    }


    /**
     * Función que retorna al controlador la información que se necesita para
     * devolverle una respuesta al usuario
     */
    public function procesaVista()
    {

        $nombreMaqueta = 'FrontBundle:Default:' . $this->maqueta->getTwig()
            . '.html.twig';

        $estructura = $this->prepararContenido();

        return array(
            "maqueta" => $nombreMaqueta,

            /**
             * Las siguientes variables son necesarias para el correcto renderizado de las plantillas de twig
             */
            "variablesParaRender" => array(
                "estructura" => $estructura
            )
        );
    }

    /**
     * Recorre desde maqueta todos los elementos internos y contruye un array
     * de estructura
     */
    private function prepararContenido()
    {
        // Cargamos la maqueta del archivo de estructura yml
        $contenidoVista = $this->container->getParameter('estructura')[$this->slug]['zonas'];

            foreach ($contenidoVista as $nombreZona => $zona) {
                foreach ($zona['contenedores'] as $nombreContenedor => $contenedor) {
                    $widgets = $contenedor['widgets'] == null ? [] : $contenedor['widgets'];
                    foreach ($widgets as $keyWidget => $widget) {

                        if($widget["widgetAjax"] == true){
                        $contenidoVista[$nombreZona]['contenedores'][$nombreContenedor]['widgets'][$keyWidget]['twig'] =
                                "FrontBundle:Widgets:WidgetAjaxVacio.html.twig";
                        }

                        $contenidoVista[$nombreZona]['contenedores'][$nombreContenedor]['widgets'][$keyWidget]['datos'] =
                            $widget["widgetAjax"] == true
                                ? []
                                : $this->switcherDatosServices
                                       ->getDatosWidget(
                                            $widget['service'],
                                            $datos = ['maqueta' => $this->slug]
                                );
                    }
                }
            }

        return $contenidoVista;
    }

    /** Extraer datos contenido. Usado luego para la obtención de datos en
     * los widgets
     */
    private function getDatosWidget($widget)
    {
        // Los datos que se le van a pasar a todos los widgets
        $datos = array(
            "maqueta"        => $this->maqueta->getSlug(),
        );

        $nombreServiceWidget = $widget->getService();

        $datosWidget = $this->switcherDatosServices
                            ->getDatosWidget($nombreServiceWidget, $datos);

        return $datosWidget;
    }

    private function creaArrayInfoWidgets($arrayWidgetsDoctrine)
    {
        $arrayInfoWidgets = [];

        foreach ($arrayWidgetsDoctrine as $widget) {
            // La información devuelta por el service del widget
            $datosWidget = $this->getDatosWidget($widget);

            // Construcción del array resultado.
            // El array de widgets está basado en índices por si un
            // contenedor va a recibir un número específico de widgets
            // y dependiendo del orden en el que llegue el widget va a
            // un sitio u otro
            $arrayInfoWidgets[] = array(
                "nombre" => $widget->getNombre(),
                "twig"   => $widget->getTwig(),
                // ID permita que podamos usar la descripción de la
                // base de datos para identificar widgets en una maqueta
                // y de esta forma atacarlos mejor con javascript
                "id"     => $widget->getDescripcion(),
                "datos"  => $datosWidget
            );
        }

        return $arrayInfoWidgets;
    }


    /*----------------- FUNCIONES PARA LLAMADAS AJAX -------------------*/

    public function datosRenderWidgetsAjaxPorNombre($descripcionesWidgets)
    {
        $qb = $this->em->createQueryBuilder();

        $qb->select("w")
           ->from("CmsBundle:Widget", "w")
           ->join("w.contenedor", "c")
           ->join("c.zona", "z")
           ->join("z.maqueta", "m")
           ->where("m.slug = :maquetaActual")
           ->andWhere("w.activo = True")
           ->andWhere("w.widgetAjax = True");

        // En caso de que se especifiquen los nombres de los widgets que se
        // quieren renderizar
        if(sizeof($descripcionesWidgets) > 0){
           $qb->andWhere("w.descripcion IN (:descripcionesWidgets)")
               ->setParameter("descripcionesWidgets", $descripcionesWidgets);
        }

        $qb->setParameter("maquetaActual", $this->slug);

        // Obtención de los widgets
        $query = $qb->getQuery();
        $widgets = $query->getResult(Query::HYDRATE_OBJECT);

        // Transformación de los widgets a sus datos de renderizado para el
        // controlador.
        $datosRender = $this->creaArrayInfoWidgets($widgets);

        return $datosRender;
    }

    public function filterWidgetsAjax($arrayWidgetsDoctrine)
    {
        $widgets = $arrayWidgetsDoctrine;

        $widgetsAjax = array_filter($widgets, function($w){
            return $w->getWidgetAjax();
        });

        return $widgetsAjax;
    }
}
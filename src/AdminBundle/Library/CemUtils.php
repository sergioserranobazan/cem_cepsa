<?php

namespace AdminBundle\Library;

/**
*
*/
class CemUtils
{

    function __construct()
    {}

    /**
     * Dado un número, se devuelve este como string redondeado a N decimales
     *  (Por defecto 2) y con "," en lugar de "." para separar las posiciones
     *  decimales
     * @param  [type] $num [el número que se quiere redondear]
     * @param  [integer] $dec [número de decimales para el redondeo]
     * @return [type]      [description]
     */
    public static function formateoNumero($num, $dec=2)
    {
        $num = floatval($num);

        $num = number_format(round($num, $dec), $dec);

        $num = str_replace(".", ",", strval($num));

        return $num;
    }

    /**
     * Devuelve un array con todos sus valores como strings. En caso de que el
     * array original tuviera una posición del array como un subarray, los
     * valores de este se concatenarán en un único string con comas.
     * Las claves originales son mantenidas.
     *
     * @param  array $arrayFiltros [array de filtros con sus valores]
     * @return [array]               [array de filtros con strings como único tipo de valor]
     */
    public static function arrayDeStrings($arrayOriginal){
      $arrayFormateado = [];

      foreach ($arrayOriginal as $clave => $valores) {
        $valoresFormateados = "";

        if(gettype($valores) == "array"){
          // En caso de que el filtro tenga varios valores seleccionados se hace
          // la conversión a un único string.
          $valoresFormateados = implode(", ", $valores);
        }else {
            $valoresFormateados = $valores;
        }

        $arrayFormateado[$clave] = $valoresFormateados;
      }

      return $arrayFormateado;
    }
}
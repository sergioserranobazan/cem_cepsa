<?php
namespace AdminBundle\Library;
use AdminBundle\Library\FechasUtils;
/**
*
*/
class FechasUtils
{

    function __construct()
    {}


    public static function getRangoTemporalidadId(
        $temporalidadId, \DateTime $fecha_ref=null, $periodos=1
    ){
        // Puesto en el código para ahorrar una consulta a base de datos innecesaria
        $temporalidadEquivalencias = [1 => "semana", 2=> "mes", 3 => "trimestre", 4 => "año"];
        $temporalidadString = $temporalidadEquivalencias[$temporalidadId];

        return FechasUtils::getRangoTemporalidad(
            $temporalidadString, $fecha_ref, $periodos
        );
    }

    /**
    *   Devuelve un array con fechas inicio y fin para una temporalidad
    *   @param type $temporalidad -> semana,mes,año,... (para los ids se ha hecho una funcion especifica) ...ID
    *   @param type $fecha_ref -> (OPCIONAL) Datetime (formato YYYY-mm-dd)
    *   @param type $periodos -> (OPCIONAL) Numero de temporalidades a obtener fechas, por defecto un periodo(el actual)
    *   @return array
    */
    public static function getRangoTemporalidad(
        $temporalidadString, $fecha_ref=null, $periodos=1
    ){
        //Si la fecha no existe o no es objeto DateTime, la genero
        if($fecha_ref==null){
            $fecha_ref = new \DateTime();
        }
        if(!($fecha_ref instanceof \DateTime)){
            $fecha_ref = new \DateTime($fecha_ref);
        }

        $fechaActual = $fecha_ref;     
        $anyoActual = intval($fechaActual->format("Y"));       

        $fechaInicio = new \DateTime();
        $fechaFin = new \DateTime();

        switch ($temporalidadString) {
            case 'rango':
                $fechaInicio = clone($fechaActual);
                $fechaFin = clone($fechaActual);
            case 'dia':
                break;
            case 'semana':
                $semanaActual = intval($fechaActual->format("W"));

                $anyoActualSemana = FechasUtils::getFuckingYear($fechaActual); 

                $fechaFin->setISODate($anyoActualSemana, $semanaActual, 7);                   
                $fechaInicio->setISODate($anyoActualSemana, $semanaActual, 1);               
                $diasRestar = abs(7*($periodos-1)); // Dias Atras respecto de la fecha inicio.
                $fechaInicio->sub(new \DateInterval('P'.$diasRestar.'D'));
                break;
            case 'mes':
                $maximoDiaMesActual = intval($fechaActual->format("t"));
                $mesInicio = intval($fechaActual->format("m"))-($periodos-1);
                $fechaInicio->setDate($anyoActual, $mesInicio, 1);
                $mesActual = intval($fechaActual->format("m"));   
                $fechaFin->setDate($anyoActual, $mesActual, $maximoDiaMesActual);
                break;
            case 'año':
                $diciembre = 12;
                $nDiasDiciembre = 31;
                $anyoInicio = intval($fechaActual->format("Y"))-($periodos-1);
                $fechaInicio->setDate($anyoInicio, 1, 1);
                $fechaFin->setDate($anyoActual, $diciembre, $nDiasDiciembre);
                break;
            case 'trimestre':
                $fechaRefTrimestre = new \DateTime();
                $mesActual = intval($fechaActual->format("m"));   
                $mesInicio = $mesActual-($periodos*3)+3;//Mes contenido en el trimestre

                $fechaRefTrimestre->setDate($anyoActual, $mesInicio, 1);

                $rangoTrimestre = FechasUtils::getRangoTrimestre($fechaRefTrimestre);
                $fechaInicio = $rangoTrimestre["fecha_inicio"];
                $rangoTrimestre = FechasUtils::getRangoTrimestre($fechaActual);
                $fechaFin = $rangoTrimestre["fecha_fin"];
                break;
            default:
                break;
        }

        $fechaInicio->setTime(0,0,0);
        $fechaFin->setTime(23,59,59);

        return array(
            "fecha_inicio" => $fechaInicio,
            "fecha_fin"    => $fechaFin
        );
    }

    // Funcion que me devuelve el año perteneciente a una semana.
    public static function getFuckingYear($fecha){
        $dia = intval($fecha->format("d"));
        $semana = intval($fecha->format("W"));
        $mes = intval($fecha->format("m"));
        $anyo= intval($fecha->format("Y"));

        if(($dia==1 || $dia==2 || $dia==3) && $mes==1 && $semana>1){
            $anyo=$anyo-1;
        }

        return $anyo;
    }

    /**
     * Funcion que devuelve un array con el primer y último dia
     * del trimestre de la fecha pasada como parametro.
     * @param type $datetime
     * @return array
     */
    public static function getRangoTrimestre($fecha) {

        $anyo = intval($fecha->format("Y"));
        $trimestre = floor(($fecha->format('m') - 1) / 3 + 1);

        $fechaInicio = new \DateTime();
        $fechaFin = new \DateTime();

        switch ($trimestre) {
            case 1:
                $fechaInicio->setDate($anyo, 1, 1);
                $fechaFin->setDate($anyo, 3, 31);
                break;
            case 2:
                $fechaInicio->setDate($anyo, 4, 1);
                $fechaFin->setDate($anyo, 6, 30);
                break;
            case 3:
                $fechaInicio->setDate($anyo, 7, 1);
                $fechaFin->setDate($anyo, 9, 30);
                break;
            case 4:
                $fechaInicio->setDate($anyo, 10, 1);
                $fechaFin->setDate($anyo, 12, 31);
                break;
        }

        //No seteo las horas, eso lo hace la funcion padre (getRangoTemporalidad)
        //Si se usa esta funcion independientemente, habría que setear esas horas.

        return array(
            "fecha_inicio" => $fechaInicio,
            "fecha_fin"    => $fechaFin
        );
    }

    /**
     * Devuelve el trimestre para una fecha (1,2,3,4)
     * @param type $datetime
     * @return int
     */
    public static function getTrimestre($fecha) {
        $mes = intval($fecha->format("m"));
        $mes = is_null($mes) ? date('m') : $mes;
        $trimestre = floor(($mes - 1) / 3) + 1;

        return $trimestre;
    }

    /**
     * Funcion que devuelve el el periodo de la temporalidad en funcion de la fecha
     * Periodos con dos digitos para dia, semana y mes.
     * @param type $cruce
     * @return int
     */
    public static function getPeriodo($temporalidadString,$fecha=null) {
        if($fecha==null){
            $fecha = new \DateTime();
        }
        if(!($fecha instanceof \DateTime)){
            $fecha = new \DateTime($fecha);
        }

        switch ($temporalidadString) {
                case 'dia':
                    $periodo = $fecha->format('d');
                    break;
                case 'semana':
                    $periodo = $fecha->format('W');
                    break;
                case 'mes':
                    $periodo = $fecha->format('m');
                    break;
                case 'año':
                    $periodo = $fecha->format('Y');
                    break;
                case 'trimestre':
                    $periodo = floor(($fecha->format('m') - 1) / 3 + 1);
                    break;
                default:
                    $periodo = "1";
        }
        return $periodo;
    }

    /**
     * Funcion que devuelve el el periodo de la temporalidad en funcion de la fecha
     * precedido del año correspondiente al periodo
     * Periodos con dos digitos para dia, semana y mes.
     * @param type $cruce
     * @return int
     */
    public static function getAnyoConPeriodo($temporalidadString,$fecha=null) {
        if($fecha==null){
            $fecha = new \DateTime();
        }
        if(!($fecha instanceof \DateTime)){
            $fecha = new \DateTime($fecha);
        }

        switch ($temporalidadString) {
                case 'dia':
                    $periodo = $fecha->format('Ymd');
                    break;
                case 'semana':
                    $anyo = FechasUtils::getFuckingYear($fecha);
                    $periodo = $anyo.$fecha->format('W');
                    break;
                case 'mes':
                    $periodo = $fecha->format('Ym');
                    break;
                case 'año':
                    $periodo = $fecha->format('Y');
                    break;
                case 'trimestre':
                    $anyo = $fecha->format('Y');
                    $periodo = $anyo.floor(($fecha->format('m') - 1) / 3 + 1);
                    break;
                default:
                    $periodo = "1";
        }
        return $periodo;
    }    
}
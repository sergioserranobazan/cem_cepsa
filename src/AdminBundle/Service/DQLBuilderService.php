<?php

namespace AdminBundle\Service;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;
use AdminBundle\Library\FechasUtils;

/**
*
*/
class DQLBuilderService
{

    function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function nuevaDQL()
    {
        $this->qb = $this->em->createQueryBuilder();

        return $this;
    }

    public function contarEncuestas()
    {
        $this->qb
             ->select("count(encuesta.id) as nEncuestas")
             ->from("SurveyBundle:Encuesta", "encuesta");

        return $this;
    }

    public function getQB()
    {
        return $this->qb;
    }

    public function setRangoFechas($fechaInicio, $fechaFinal)
    {
        $this->qb
             ->andWhere("encuesta.fecha BETWEEN :fechaInicio AND :fechaFinal")
             ->setParameter("fechaInicio", $fechaInicio)
             ->setParameter("fechaFinal", $fechaFinal);

        return $this;
    }

    public function sqlAddRangoFechas(
        $aliasTablaEncuesta ,$fechaInicio, $fechaFinal,
        $sql, $checkWhere = true
    ){
        // Aseguramos que se puedan hacer selecciones de 1 día
        $fechaInicio->setTime(0,0,0);
        $fechaFinal->setTime(23,59,59);

        $fechaInicio = "'". $fechaInicio->format("Y-m-d H:i:s")."'";
        $fechaFinal = "'".$fechaFinal->format("Y-m-d H:i:s")."'";

        if(strpos($sql, "WHERE") === false
            && strpos($sql, "where") === false
            && $checkWhere
        ){
            $sql .= " WHERE ";
        }else {
            $sql .= " AND ";
        }

        $sql .= "(" .$aliasTablaEncuesta.".fecha" . " BETWEEN "
                . $fechaInicio . " AND " . $fechaFinal . ")";

        return $sql;
    }

    public function sqlFiltrarPorTemporalidad(
        $aliasTablaEncuesta, $temporalidadId, $fecha_referencia,
        $sql, $checkWhere = true
    ){
        $rango = FechasUtils::getRangoTemporalidadId(
            $temporalidadId, $fecha_referencia);

        $sql = $this->sqlAddRangoFechas(
            $aliasTablaEncuesta, $rango["fecha_inicio"], $rango["fecha_fin"],
            $sql, $checkWhere);

        return $sql;
    }

    public function sqlAddFiltroLike(
        $aliasTablaEncuesta, $nombreCampo, $valores,
        $sql, $checkWhere = true, $porcentajes = false
    ){

        // suponemos que los valores 0 son filtros no seleccionados
        if(empty($valores)) return $sql;

        if($checkWhere && strpos($sql, "WHERE") === false
           && strpos($sql, "where") === false
        ){
            $sql .= " WHERE (";
        }else {
            $sql .= " AND (";
        }

        $valores = gettype($valores) == "array" ? $valores : [$valores];
      
        if($porcentajes){
            $sql .= $aliasTablaEncuesta.".".$nombreCampo. " LIKE '%$valores[0]%'";
        }else{
            $sql .= $aliasTablaEncuesta.".".$nombreCampo. " LIKE '$valores[0]'";
        }

        if(sizeof($valores) > 1){
            $demasValores = array_slice($valores, 1);
            $indiceValor = 1;
            foreach ($demasValores as $valor) {
                $sql .= " OR ";
                if($porcentajes){
                    $sql .= $aliasTablaEncuesta.".".$nombreCampo. " LIKE '%$valores[$indiceValor]%'";
                }else{
                    $sql .= $aliasTablaEncuesta.".".$nombreCampo. " LIKE '$valores[$indiceValor]'";
                }               
                $indiceValor++;
            }
        }

        $sql .= ")";

        return $sql;
    }

    public function sqlAddFiltro(
        $aliasTablaEncuesta, $nombreCampo, $valores,
        $sql, $checkWhere = true
    ){

        // suponemos que los valores 0 son filtros no seleccionados
        if(empty($valores)) return $sql;

        if($checkWhere && strpos($sql, "WHERE") === false
           && strpos($sql, "where") === false
        ){
            $sql .= " WHERE (";
        }else {
            $sql .= " AND (";
        }

        $valores = gettype($valores) == "array" ? $valores : [$valores];

        $valores = array_map(function($valor){
            if(is_numeric($valor)){
                return intval($valor);
            }else{
                return "'" . $valor . "'";
            }
        }, $valores);

        $sql .= $aliasTablaEncuesta.".".$nombreCampo. " = " . $valores[0];

        if(sizeof($valores) > 1){
            $demasValores = array_slice($valores, 1);
            $indiceValor = 1;
            foreach ($demasValores as $valor) {
                $sql .= " OR ";
                $sql .= $aliasTablaEncuesta.".".$nombreCampo. " = " . $valores[$indiceValor];
                $indiceValor++;
            }
        }

        $sql .= ")";

        return $sql;
    }

    /**
     * Esta función recibe un array con toda la información necesaria para
     * procesar de forma automática todos los filtros de golpe para una sentencia
     * sql
     * @param [array] $mapeoFiltros [aliasTabla, nombreCampo, valores]
     * @param [string] $sql [Cadena con una sql para añadirle los filtros]
     */
    public function addFiltros($mapeoFiltros, $sql, $checkWhere = true)
    {
        foreach ($mapeoFiltros as $filtro) {
            $sql = $this->sqlAddFiltro(
                $filtro[0], // Alias tabla
                $filtro[1], // Campo en la tabla
                $filtro[2], // Valores de filtrado
                $sql,       // sql sobre la que añadir el resultado
                $checkWhere // Si `false` siempre pone AND al principio
            );
        }

        return $sql;
    }

    public function filtrarPorTemporalidad($temporalidadId, $fecha_referencia)
    {
        $rango = FechasUtils::getRangoTemporalidadId($temporalidadId, $fecha_referencia);

        $this->setRangoFechas($rango["fecha_inicio"], $rango["fecha_fin"]);

        return $this;
    }

    public function filtradoFecha(
        $temporalidadId, $sql, $fec_ini, $fec_fin = "", $checkWhere=true, $aliasTablaObjetivo = "e"
    ){

        if(empty($fec_ini)){
            $fec_ini = new \DateTime();
        }else {
            $fec_ini = new \DateTime($fec_ini);
        }

        if(empty($fec_fin)){
            $fec_fin = new \DateTime();
        }else {
            $fec_fin = new \DateTime($fec_fin);
        }

        switch ($temporalidadId) {
            case '0':
                $sql = $this->sqlAddRangoFechas(
                    $aliasTablaObjetivo, $fec_ini, $fec_fin, $sql, $checkWhere);
                break;
            case '1': case '2': case '3': case '4':
                $sql = $this->sqlFiltrarPorTemporalidad(
                    $aliasTablaObjetivo, $temporalidadId, $fec_ini, $sql, $checkWhere);
                break;
            default:
                throw new \Exception("Error al procesar la temporalidad", 1);
                break;
        }

        return $sql;
    }

    private function checkQB()
    {
        if(isset($this->qb)){
            return true;
        }else{
            throw new \Exception("No hay una QB válida. Ejecuta nuevaDQL antes", 1);
        }
    }

    public function ejecutarDQL()
    {
        $this->checkQB();

        $q = $this->qb->getQuery();

        $resultado = $q->getResult(Query::HYDRATE_ARRAY);

        $this->qb = NULL;

        return $resultado;
    }

    /**
     * Función generadora (con yield en lugar de return) para la obtención de los
     * resultados de la sql indicada. Este generador irá
     * dando los resultados de 1 en 1 (fila a fila) conforme se vaya llamando.
     * @param  $sql La sql a ejecutar.
     * @return [array] array de longitud 2 con las claves *encuesta* y *proceso*
     */
    public function generadorResultadosSQL($sql)
    {

        $conn = $this->em->getConnection();
        $conn->getConfiguration()->setSQLLogger(null);

        $stmt = $conn->prepare($sql);
        $stmt->execute();

        while($row = $stmt->fetch()){
            yield $row;
        }
    }
}
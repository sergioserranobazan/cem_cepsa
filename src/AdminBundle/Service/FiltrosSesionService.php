<?php

namespace AdminBundle\Service;

use Symfony\Component\HttpFoundation\Session\Session;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Doctrine\ORM\Query;
use AdminBundle\Library\FechasUtils;


/**
*
*/
class FiltrosSesionService
{

    private $session;
    private $filtros;
    private $slug;

    function __construct(
        Session $session, EntityManager $em, TokenStorage $ts
    ){
        $this->session = $session;
        $this->em = $em;

        $this->ts = $ts;

        $this->valoresFiltrosEnBD = [];

        $this->equivalenciasEntidades = [
            "neg" => "admin_negocio",
            "seg" => "admin_segmento",
            "can" => "admin_canal_encuestacion",
            "vista" => "admin_vista",
            "temporalidad" => "admin_temporalidad",
            "ser1" => "admin_servicio",
            "proceso" => "admin_proceso",
            "org" => "admin_linea_negocio",
            "experiencia" => "admin_experiencia",
            "recomendacion" => "admin_recomendacion",
            "tipologia_cliente" => "admin_tipologia_cliente",
            "det_can" => "admin_determinacion_canal",
            "opi_pregunta" => "survey_encuesta_pregunta_opi",
            "pais" => "admin_pais",
            "prov" => "admin_provincia"
        ];

    }

    // Se usa para la obtención de datos que van a ser estáticos tras cada set
    // de filtros. Evitamos así repetición de consultas.
    public function initFiltrosService($slug=null)
    {
        if(!is_null($slug)) $this->setSlug($slug);

        $this->maquetaId = $this->em->getRepository("CmsBundle:Maqueta")
                                ->findOneBySlug($this->slug)->getId();

        // Obtenemos el ID del grupo al que pertenece el usuario loggeado
        $this->groupId = $this->ts->getToken()->getUser()
                                              ->getGroups()
                                              ->toArray()[0]
                                              ->getId();

        // Valores fijos de filtros según el grupo
        $this->valoresFijosGrupoUsuario = $this->calcularValoresFijosGrupoUser(
            $this->groupId, $this->maquetaId
        );

        // No está en el constructor porque necesitamos el $slug de la request
        $this->valoresDefaultFiltros = $this->getValoresDefaultFiltros($this->slug);

        // Resto de valores de filtros
        $this->valoresFiltrosMaquetaXBloque = $this->valoresBloqueMaqueta(
            $this->valoresDefaultFiltros, $this->valoresFiltrosEnBD
        );

        return $this;
    }

    public function getValoresFiltroBloque($bloqueId)
    {
        return $this->valoresFiltrosMaquetaXBloque[$bloqueId];
    }

    public function getValoresEnBDFiltro($nombreEntidad)
    {
        if(is_null($nombreEntidad)){
            throw new \Exception("Llamado un filtro NULL", 1);
        }

        if(isset($this->valoresFiltrosEnBD[$nombreEntidad])){
            $valoresFiltro = $this->valoresFiltrosEnBD[$nombreEntidad];
        }else{
            $valoresFiltro = $this->valoresEnBDFiltro($nombreEntidad);

            $this->valoresFiltrosEnBD[$nombreEntidad] = $valoresFiltro;
        }

        return $valoresFiltro;
    }

    public function setSlug($slug){
        $this->slug = $slug;
        return $this;
    }

    /**
     * Obtiene los datos de los filtros
     */
    public function getValoresDefaultFiltros($slug=null)
    {
        if(!is_null($slug)) $this->setSlug($slug);

        $qb = $this->em->createQueryBuilder();
        $qb->select(
            "fm.nombreId, fm.valorCampoCero, fm.valorDefault as valorDefault,"
            ."f.entidadDoctrine as entidadDoctrine, f.nombreFiltro as nombreFiltro,"
            ."f.tabla as tabla, b.id as bloque, fm.tag as tag");
        $qb->from("AdminBundle:FiltrosMaqueta", "fm")
           ->leftJoin("fm.filtro", "f")
           ->join("fm.maqueta", "m")
           ->join("fm.bloque", "b")
           ->where("m.slug = :slug")
           ->orderBy("fm.orden", "ASC")
           ->setParameter("slug", $this->slug);

        $query = $qb->getQuery();

        $result = $query->getResult(Query::HYDRATE_ARRAY);

        return $result;
    }

    private function calcularValoresFijosGrupoUser($grupoUserFiltroId, $maquetaId){


        $db = $this->em->getConnection();

        $sql = "
            SELECT fmaqueta.nombreId nombre_filtro, valor_fijo as valor
            FROM grupo_filtro gf
            JOIN admin_filtros_maqueta fmaqueta ON gf.filtro_maqueta_id = fmaqueta.id
            WHERE gf.grupo_id = $grupoUserFiltroId
            AND fmaqueta.id_maqueta = $maquetaId
        ";

        $r = $db->executeQuery($sql)->fetchAll();

        return $r;
    }

    public function getValoresFijosGrupoUsuario()
    {
        return $this->valoresFijosGrupoUsuario;
    }


    private function valoresBloqueMaqueta($defaultFiltros, $datosFiltros)
    {

        $result = [];

        foreach ($defaultFiltros as $d) {
            $bloque         = $d["bloque"];
            $nombreFiltro   = $d["nombreFiltro"];
            $nombreId       = $d["nombreId"];
            $valorDefault   = $d["valorDefault"];
            $tag            = $d["tag"];
            $valorCampoCero = $d["valorCampoCero"] ? $d["valorCampoCero"]: "";

            if(!isset($result[$bloque])){
                $result[$bloque] = [];
            }

            $result[$bloque][$nombreId] = array(
                "nombreId" => $nombreId,
                "tag"      => $tag,
                "valores"  => !is_null($d["entidadDoctrine"])
                              ? $this->getValoresEnBDFiltro($nombreFiltro)
                              : [], // Para los filtros que no tengan una tabla asociada
            );
            if(!empty($valorCampoCero)) $result[$bloque][$nombreId]["valores"][0] = $valorCampoCero;
        }

        return $result;
    }



    public function setFiltros($slug=null, $filtrosEnPeticion=null)
    {
        if(!is_null($slug)) $this->setSlug($slug);

        $this->initFiltrosService();
        // Comentar la línea siguiente cuando se quieran usar los filtros de
        // sesión
        // $this->session->set($slug, NULL);
        $this->filtros =    (array)$filtrosEnPeticion +
                            (array)$this->session->get($this->slug) +
                            $this->newArrayFiltros();

        $this->session->set($this->slug, $this->filtros);

        return $this;
    }

    public function getFiltros()
    {
        return $this->filtros;
    }

    /**
     * Devuelve un array con los filtros de la maqueta indicada
     */
    public function getFiltrosMaqueta($slug=null){
        if(!is_null($slug)) $this->setSlug($slug);

        $this->filtros = $this->session->get($this->slug);
        return $this->filtros;
    }

    /**
     * Crea la estructura del array para la maqueta actual con todos
     * los valores en null
     */
    public function newArrayFiltros()
    {

        $arrayEstructuraFiltros = [];

        // Seteo básico de valores a los filtros maqueta
        foreach ($this->valoresDefaultFiltros as $value) {
            if(empty($value["valorDefault"])){
                $arrayEstructuraFiltros[$value["nombreId"]] = "0";
            }else{
                $arrayEstructuraFiltros[$value["nombreId"]] = $value["valorDefault"];
            }
        }

        // Asignación Valores Fijos
        $valoresFijos = $this->valoresFijosGrupoUsuario;

        foreach ($valoresFijos as $filtro) {
            if(!is_null($filtro["valor"]))
                $arrayEstructuraFiltros[$filtro["nombre_filtro"]] = $filtro["valor"];
        }

        // Seteo de fechas por defecto
        if( isset($arrayEstructuraFiltros["temporalidad"]) )
        {
            // Seteo fechas para cuando temporalidad es por Rango
            if($arrayEstructuraFiltros["temporalidad"] == "0")
            {
                $fechasRangoDefault = FechasUtils::getRangoTemporalidad("semana");

                $arrayEstructuraFiltros["fec_ini"] =
                    $fechasRangoDefault["fecha_inicio"]->format("Y-m-d");

                $arrayEstructuraFiltros["fec_fin"] =
                    $fechasRangoDefault["fecha_fin"]->format("Y-m-d");
            }else{
            // Seteo las fechas cuando la temporalidad no es de rango
            // pero estan definidas una o ambas fechas.
                $fechasRangoDefault = FechasUtils::getRangoTemporalidad("dia");
                // Seteo solo si estan definidas
                if( isset($arrayEstructuraFiltros['fec_ini']) )
                    $arrayEstructuraFiltros["fec_ini"] =
                        $fechasRangoDefault["fecha_inicio"]->format("Y-m-d");
                if( isset($arrayEstructuraFiltros['fec_fin']) )
                    $arrayEstructuraFiltros["fec_fin"] =
                        $fechasRangoDefault["fecha_fin"]->format("Y-m-d");
            }
        }

        return $arrayEstructuraFiltros;
    }

    public function valoresEnBDFiltro($nombreFiltro){

        $tabla = $this->equivalenciasEntidades[$nombreFiltro];

        $conn = $this->em->getConnection();

        $sql = "
            SELECT id, nombre FROM $tabla
        ";

        $queryResult = $conn->executeQuery($sql)->fetchAll();

        $functionResult = [];

        foreach ($queryResult as $registroFiltro) {
            $functionResult[$registroFiltro["id"]] = $registroFiltro["nombre"];
        }

        return $functionResult;
    }

    public function setFiltro($nombreFiltro, $nombreMaqueta, $valor)
    {
        $this->filtros[$nombreFiltro] = $valor;

        $this->session->set($nombreMaqueta, $this->filtros);
    }
}

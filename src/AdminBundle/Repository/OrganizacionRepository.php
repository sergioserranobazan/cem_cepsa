<?php

namespace AdminBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * OrganizacionRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class OrganizacionRepository extends EntityRepository
{
}

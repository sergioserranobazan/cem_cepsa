<?php

namespace AdminBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * LineaNegocioRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class LineaNegocioRepository extends EntityRepository
{
}

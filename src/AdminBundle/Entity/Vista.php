<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Vista
 *
 * @ORM\Table(name="admin_vista")
 * @ORM\Entity(repositoryClass="AdminBundle\Repository\VistaRepository")
 */
class Vista
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     *  @ORM\OneToOne(targetEntity="Filtros")
     */
    private $filtro;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Vista
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set filtro
     *
     * @param \AdminBundle\Entity\Filtros $filtro
     * @return Vista
     */
    public function setFiltro(\AdminBundle\Entity\Filtros $filtro = null)
    {
        $this->filtro = $filtro;

        return $this;
    }

    /**
     * Get filtro
     *
     * @return \AdminBundle\Entity\Filtros 
     */
    public function getFiltro()
    {
        return $this->filtro;
    }
}

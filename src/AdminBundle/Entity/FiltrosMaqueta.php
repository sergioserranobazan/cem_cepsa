<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ConfiguracionFiltrosMaqueta
 *
 * @ORM\Table(name="admin_filtros_maqueta")
 * @ORM\Entity
 */
class FiltrosMaqueta
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="\CmsBundle\Entity\Maqueta" , inversedBy="filtrosMaqueta")
     * @ORM\JoinColumn(name="id_maqueta", referencedColumnName="id", nullable=true)
     */
    protected $maqueta;

    /**
     * @ORM\ManyToOne(targetEntity="Filtros" )
     * @ORM\JoinColumn(name="id_filtro", referencedColumnName="id", nullable=true)
     */
    private $filtro;

    /**
     * @ORM\Column(name="orden", type="integer", options={"default" : 1})
     */
    private $orden;

    /**
     * @ORM\Column(name="tipoFiltro", type="integer", options={"default" : 1})
     */
    private $tipoFiltro;

    /**
     * @ORM\Column(name="activo", type="boolean", options={"default" : true})
     */
    private $activo;

    /* Los atributos que aparecen a continuación se colocaron aquí por no tener
    que cambiar profundamente la lógica de los filtros y tener que estructurarlos
    de forma que una maqueta contuviera bloques de filtros que a su vez contuvieran
    instancias de los filtros */

    /**
     * @ORM\ManyToOne(targetEntity="FiltrosBloque", inversedBy="filtrosMaqueta")
     * @ORM\JoinColumn(name="bloque", referencedColumnName="id")
     */
    private $bloque;

    /**
     * @ORM\Column(name="nombreId", type="string", length=255)
     *
     * Nombre único en el bloque para referenciar al filtro. Principalmente
     * usado para poder dar ids a los filtros en el html y trabajar con ellos
     * con js.
     */
    private $nombreId;

    /**
     * @ORM\Column(name="valorDefault", type="string", options={"default": ""})
     */
    private $valorDefault;

    /**
     * @ORM\Column(name="valorCampoCero", type="string", options={"default": ""})
     */
    private $valorCampoCero;


    /**
     * @ORM\Column(name="tag", type="string", length=255)
     */
    private $tag;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set orden
     *
     * @param integer $orden
     * @return ConfiguracionFiltrosMaqueta
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden
     *
     * @return integer
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * Set tipoFiltro
     *
     * @param integer $tipoFiltro
     * @return ConfiguracionFiltrosMaqueta
     */
    public function setTipoFiltro($tipoFiltro)
    {
        $this->tipoFiltro = $tipoFiltro;

        return $this;
    }

    /**
     * Get tipoFiltro
     *
     * @return integer
     */
    public function getTipoFiltro()
    {
        return $this->tipoFiltro;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     * @return ConfiguracionFiltrosMaqueta
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set Maqueta
     *
     * @param \CmsBundle\Entity\Maqueta $maqueta
     * @return ConfiguracionFiltrosMaqueta
     */
    public function setMaqueta(\CmsBundle\Entity\Maqueta $maqueta = null)
    {
        $this->maqueta = $maqueta;

        return $this;
    }

    /**
     * Get Maqueta
     *
     * @return \CmsBundle\Entity\Maqueta
     */
    public function getMaqueta()
    {
        return $this->maqueta;
    }

    /**
     * Set Filtro
     *
     * @param \AdminBundle\Entity\FiltrosMaqueta $filtro
     * @return FiltrosMaqueta
     */
    public function setFiltro(\AdminBundle\Entity\FiltrosMaqueta $filtro = null)
    {
        $this->filtro = $filtro;

        return $this;
    }

    /**
     * Get Filtro
     *
     * @return \AdminBundle\Entity\FiltrosMaqueta
     */
    public function getFiltro()
    {
        return $this->filtro;
    }

    /**
     * Set bloque
     *
     * @param string $bloque
     * @return FiltrosMaqueta
     */
    public function setBloque($bloque)
    {
        $this->bloque = $bloque;

        return $this;
    }

    /**
     * Get bloque
     *
     * @return string
     */
    public function getBloque()
    {
        return $this->bloque;
    }

    /**
     * Set nombreId
     *
     * @param string $nombreId
     * @return FiltrosMaqueta
     */
    public function setNombreId($nombreId)
    {
        $this->nombreId = $nombreId;

        return $this;
    }

    /**
     * Get nombreId
     *
     * @return string
     */
    public function getNombreId()
    {
        return $this->nombreId;
    }

    /**
     * Set valorDefault
     *
     * @param string $valorDefault
     * @return FiltrosMaqueta
     */
    public function setValorDefault($valorDefault)
    {
        $this->valorDefault = $valorDefault;

        return $this;
    }

    /**
     * Get valorDefault
     *
     * @return string
     */
    public function getValorDefault()
    {
        return $this->valorDefault;
    }

    /**
     * Set tag
     *
     * @param string $tag
     * @return FiltrosMaqueta
     */
    public function setTag($tag)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag
     *
     * @return string
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Set valorCampoCero
     *
     * @param string $valorCampoCero
     * @return FiltrosMaqueta
     */
    public function setValorCampoCero($valorCampoCero)
    {
        $this->valorCampoCero = $valorCampoCero;

        return $this;
    }

    /**
     * Get valorCampoCero
     *
     * @return string 
     */
    public function getValorCampoCero()
    {
        return $this->valorCampoCero;
    }
}

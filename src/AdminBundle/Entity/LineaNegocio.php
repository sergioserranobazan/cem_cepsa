<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LineaNegocio
 *
 * @ORM\Table(name="admin_linea_negocio")
 * @ORM\Entity(repositoryClass="AdminBundle\Repository\LineaNegocioRepository")
 */
class LineaNegocio
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\OneToMany(targetEntity="NegocioLineaNegocio", mappedBy="lineaNegocio")
     */
    private $negocioLineaNegocio;

    /**
     * @ORM\OneToMany(targetEntity="SurveyBundle\Entity\Encuesta", mappedBy="org")
     */
    private $encuestas;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->negocioLineaNegocio = new \Doctrine\Common\Collections\ArrayCollection();
        $this->encuestas = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return LineaNegocio
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Add negocioLineaNegocio
     *
     * @param \AdminBundle\Entity\NegocioLineaNegocio $negocioLineaNegocio
     * @return LineaNegocio
     */
    public function addNegocioLineaNegocio(\AdminBundle\Entity\NegocioLineaNegocio $negocioLineaNegocio)
    {
        $this->negocioLineaNegocio[] = $negocioLineaNegocio;

        return $this;
    }

    /**
     * Remove negocioLineaNegocio
     *
     * @param \AdminBundle\Entity\NegocioLineaNegocio $negocioLineaNegocio
     */
    public function removeNegocioLineaNegocio(\AdminBundle\Entity\NegocioLineaNegocio $negocioLineaNegocio)
    {
        $this->negocioLineaNegocio->removeElement($negocioLineaNegocio);
    }

    /**
     * Get negocioLineaNegocio
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNegocioLineaNegocio()
    {
        return $this->negocioLineaNegocio;
    }

    /**
     * Add encuestas
     *
     * @param \SurveyBundle\Entity\Encuesta $encuestas
     * @return LineaNegocio
     */
    public function addEncuesta(\SurveyBundle\Entity\Encuesta $encuestas)
    {
        $this->encuestas[] = $encuestas;

        return $this;
    }

    /**
     * Remove encuestas
     *
     * @param \SurveyBundle\Entity\Encuesta $encuestas
     */
    public function removeEncuesta(\SurveyBundle\Entity\Encuesta $encuestas)
    {
        $this->encuestas->removeElement($encuestas);
    }

    /**
     * Get encuestas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEncuestas()
    {
        return $this->encuestas;
    }
}

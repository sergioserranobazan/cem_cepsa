<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Organizacion
 *
 * @ORM\Table(name="admin_organizacion")
 * @ORM\Entity(repositoryClass="AdminBundle\Repository\OrganizacionRepository")
 */
class Organizacion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\OneToMany(targetEntity="SurveyBundle\Entity\Encuesta", mappedBy="org")
     */
    private $encuestas;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Organizacion
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->encuestas = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add encuestas
     *
     * @param \SurveyBundle\Entity\Encuesta $encuestas
     * @return Organizacion
     */
    public function addEncuesta(\SurveyBundle\Entity\Encuesta $encuestas)
    {
        $this->encuestas[] = $encuestas;

        return $this;
    }

    /**
     * Remove encuestas
     *
     * @param \SurveyBundle\Entity\Encuesta $encuestas
     */
    public function removeEncuesta(\SurveyBundle\Entity\Encuesta $encuestas)
    {
        $this->encuestas->removeElement($encuestas);
    }

    /**
     * Get encuestas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEncuestas()
    {
        return $this->encuestas;
    }
}

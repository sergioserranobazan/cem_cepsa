<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bloque
 *
 * @ORM\Table(name="admin_filtros_bloque")
 * @ORM\Entity(repositoryClass="AdminBundle\Repository\FiltrosBloqueRepository")
 */
class FiltrosBloque
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="nombreId", type="string", length=255, options={"default": ""})
     *
     * Nombre único por maqueta para referenciar al bloque. Principalmente
     * usado para darle id a los bloques de filtros en el html y trabajar con
     * ellos usando js.
     */
    private $nombreId;

    /**
     * @ORM\Column(name="cabecera", type="string", length=255, options={"default": ""})
     */
    private $cabecera;

    /**
     * @ORM\OneToMany(targetEntity="FiltrosMaqueta", mappedBy="bloque")
     */
    private $filtrosMaqueta;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->filtrosMaqueta = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set nombreId
     *
     * @param string $nombreId
     * @return FiltrosBloque
     */
    public function setNombreId($nombreId)
    {
        $this->nombreId = $nombreId;

        return $this;
    }

    /**
     * Get nombreId
     *
     * @return string
     */
    public function getNombreId()
    {
        return $this->nombreId;
    }

    /**
     * Set cabecera
     *
     * @param string $cabecera
     * @return FiltrosBloque
     */
    public function setCabecera($cabecera)
    {
        $this->cabecera = $cabecera;

        return $this;
    }

    /**
     * Get cabecera
     *
     * @return string
     */
    public function getCabecera()
    {
        return $this->cabecera;
    }

    /**
     * Add filtrosMaqueta
     *
     * @param \AdminBundle\Entity\FiltrosMaqueta $filtrosMaqueta
     * @return FiltrosBloque
     */
    public function addFiltrosMaquetum(\AdminBundle\Entity\FiltrosMaqueta $filtrosMaqueta)
    {
        $this->filtrosMaqueta[] = $filtrosMaqueta;

        return $this;
    }

    /**
     * Remove filtrosMaqueta
     *
     * @param \AdminBundle\Entity\FiltrosMaqueta $filtrosMaqueta
     */
    public function removeFiltrosMaquetum(\AdminBundle\Entity\FiltrosMaqueta $filtrosMaqueta)
    {
        $this->filtrosMaqueta->removeElement($filtrosMaqueta);
    }

    /**
     * Get filtrosMaqueta
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFiltrosMaqueta()
    {
        return $this->filtrosMaqueta;
    }

}

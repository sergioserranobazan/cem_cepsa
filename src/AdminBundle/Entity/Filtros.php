<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ConfiguracionFiltros
 *
 * @ORM\Table(name="admin_filtros")
 * @ORM\Entity
 */
class Filtros
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Filtros", inversedBy="hijos")
     * @ORM\JoinColumn(name="padre_id", referencedColumnName="id")
     */
    protected $padre;

    /**
     * @ORM\Column(name="entidadDoctrine", type="string", length=255)
     */
    private $entidadDoctrine;

    /**
     * @ORM\OneToMany(targetEntity="Filtros", mappedBy="padre")
     */
    protected $hijos;

     /**
     * @var string $tabla
     *
     * @ORM\Column(name="tabla", type="string", length=255)
     */
    private $tabla;

    /**
     * @var string $tablaHijo
     *
     * @ORM\Column(name="tablaHijo", type="string", length=255, nullable=true)
     */
    private $tablaHijo;

    /**
     * @var string $campoId
     *
     * @ORM\Column(name="campoId", type="string", length=255, options={"default":"id"})
     */
    private $campoId;


    /**
     * @var string $campoDescripcion
     *
     * @ORM\Column(name="campoDescripcion", type="string", length=255, nullable=true)
     */
    private $campoDescripcion;


    /**
     * @var string $nombreFiltro
     *
     * @ORM\Column(name="nombreFiltro", type="string", length=255, nullable=true)
     */
    private $nombreFiltro;

    /**
     * @var string $campoIdPadre
     *
     * En caso de que el padre gaste un nombre distinto a ID
     *
     * @ORM\Column(name="campoIdPadre", type="string", length=255, nullable=true)
     */
    private $campoIdPadre;

    /**
     * @var $auto
     * @ORM\Column(name="auto", type="boolean", nullable=true)
     */
    private $auto;

    /**
     * @var $etiqueta
     * @ORM\Column(name="etiqueta", type="string", nullable=true)
     */
    private $etiqueta;


    /**
     * @var text $sentencia
     *
     * @ORM\Column(name="sentencia", type="text", nullable=true)
     */
    private $sentencia;



      /**
     * __toString()
     *
     * @return String
     */
    public function __toString(){
    	return $this->getTabla();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tabla
     *
     * @param string $tabla
     * @return ConfiguracionFiltros
     */
    public function setTabla($tabla)
    {
        $this->tabla = $tabla;

        return $this;
    }

    /**
     * Get tabla
     *
     * @return string
     */
    public function getTabla()
    {
        return $this->tabla;
    }

    /**
     * Set tablaHijo
     *
     * @param string $tablaHijo
     * @return ConfiguracionFiltros
     */
    public function setTablaHijo($tablaHijo)
    {
        $this->tablaHijo = $tablaHijo;

        return $this;
    }

    /**
     * Get tablaHijo
     *
     * @return string
     */
    public function getTablaHijo()
    {
        return $this->tablaHijo;
    }

    /**
     * Set campoId
     *
     * @param string $campoId
     * @return ConfiguracionFiltros
     */
    public function setCampoId($campoId)
    {
        $this->campoId = $campoId;

        return $this;
    }

    /**
     * Get campoId
     *
     * @return string
     */
    public function getCampoId()
    {
        return $this->campoId;
    }

    /**
     * Set campoDescripcion
     *
     * @param string $campoDescripcion
     * @return ConfiguracionFiltros
     */
    public function setCampoDescripcion($campoDescripcion)
    {
        $this->campoDescripcion = $campoDescripcion;

        return $this;
    }

    /**
     * Get campoDescripcion
     *
     * @return string
     */
    public function getCampoDescripcion()
    {
        return $this->campoDescripcion;
    }

    /**
     * Set nombreFiltro
     *
     * @param string $nombreFiltro
     * @return ConfiguracionFiltros
     */
    public function setNombreFiltro($nombreFiltro)
    {
        $this->nombreFiltro = $nombreFiltro;

        return $this;
    }

    /**
     * Get nombreFiltro
     *
     * @return string
     */
    public function getNombreFiltro()
    {
        return $this->nombreFiltro;
    }

    /**
     * Set campoIdPadre
     *
     * @param string $campoIdPadre
     * @return ConfiguracionFiltros
     */
    public function setCampoIdPadre($campoIdPadre)
    {
        $this->campoIdPadre = $campoIdPadre;

        return $this;
    }

    /**
     * Get campoIdPadre
     *
     * @return string
     */
    public function getCampoIdPadre()
    {
        return $this->campoIdPadre;
    }

    /**
     * Set auto
     *
     * @param boolean $auto
     * @return ConfiguracionFiltros
     */
    public function setAuto($auto)
    {
        $this->auto = $auto;

        return $this;
    }

    /**
     * Get auto
     *
     * @return boolean
     */
    public function getAuto()
    {
        return $this->auto;
    }

    /**
     * Set etiqueta
     *
     * @param string $etiqueta
     * @return ConfiguracionFiltros
     */
    public function setEtiqueta($etiqueta)
    {
        $this->etiqueta = $etiqueta;

        return $this;
    }

    /**
     * Get etiqueta
     *
     * @return string
     */
    public function getEtiqueta()
    {
        return $this->etiqueta;
    }

    /**
     * Set sentencia
     *
     * @param string $sentencia
     * @return ConfiguracionFiltros
     */
    public function setSentencia($sentencia)
    {
        $this->sentencia = $sentencia;

        return $this;
    }

    /**
     * Get sentencia
     *
     * @return string
     */
    public function getSentencia()
    {
        return $this->sentencia;
    }

    /**
     * Set padre
     *
     * @param \AdminBundle\Entity\ConfiguracionFiltros $padre
     * @return ConfiguracionFiltros
     */
    public function setPadre(\AdminBundle\Entity\Filtros $padre = null)
    {
        $this->padre = $padre;

        return $this;
    }

    /**
     * Get padre
     *
     * @return \AdminBundle\Entity\ConfiguracionFiltros
     */
    public function getPadre()
    {
        return $this->padre;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->hijos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add hijos
     *
     * @param \AdminBundle\Entity\Filtros $hijos
     * @return Filtros
     */
    public function addHijo(\AdminBundle\Entity\Filtros $hijos)
    {
        $this->hijos[] = $hijos;

        return $this;
    }

    /**
     * Remove hijos
     *
     * @param \AdminBundle\Entity\Filtros $hijos
     */
    public function removeHijo(\AdminBundle\Entity\Filtros $hijos)
    {
        $this->hijos->removeElement($hijos);
    }

    /**
     * Get hijos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHijos()
    {
        return $this->hijos;
    }

    /**
     * Set entidadDoctrine
     *
     * @param string $entidadDoctrine
     * @return Filtros
     */
    public function setEntidadDoctrine($entidadDoctrine)
    {
        $this->entidadDoctrine = $entidadDoctrine;

        return $this;
    }

    /**
     * Get entidadDoctrine
     *
     * @return string
     */
    public function getEntidadDoctrine()
    {
        return $this->entidadDoctrine;
    }
}

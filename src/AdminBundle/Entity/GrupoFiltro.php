<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GrupoFiltro
 *
 * @ORM\Table(name="grupo_filtro")
 * @ORM\Entity(repositoryClass="AdminBundle\Repository\GrupoFiltroRepository")
 */
class GrupoFiltro
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="\UserBundle\Entity\Group", inversedBy="grupoFiltros")
     */
    private $grupo;

    /**
     * @ORM\ManyToOne(targetEntity="FiltrosMaqueta", inversedBy="grupoFiltro")
     */
    private $filtroMaqueta;

    /**
     * @ORM\Column(name="valor_fijo", type="string", length=256)
     */
    private $valorFijo;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}

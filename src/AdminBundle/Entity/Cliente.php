<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cliente
 *
 * @ORM\Table(name="admin_cliente")
 * @ORM\Entity(repositoryClass="AdminBundle\Repository\ClienteRepository")
 */
class Cliente
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="SurveyBundle\Entity\Encuesta", mappedBy="cliente")
     */
    private $encuestas;

    /**
     * @var string
     * @ORM\Column(name="cod_pers", type="string", length=255)
     */
    private $codPers;

    /**
     * @ORM\Column(name="telf_BI", type="string", length=255)
     */
    private $telfBI;

    /**
     * @ORM\Column(name="mail_BI", type="string", length=255)
     */
    private $mailBI;

    /**
     * @ORM\Column(name="id_hash", type="string", length=255)
     */
    private $idHash;

    // CAMPOS DEL FINAL DEL FICHERO DE CARGA
    /**
     * @ORM\Column(name="telf", type="string", length=255)
     */
    private $telf;

    /**
     * @ORM\Column(name="mail", type="string", length=255)
     */
    private $mail;

    /**
     * @ORM\Column(name="nif", type="string", length=255)
     */
    private $nif;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Cliente
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->encuesta = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add encuesta
     *
     * @param \SurveyBundle\Entity\Encuesta $encuesta
     * @return Cliente
     */
    public function addEncuestum(\SurveyBundle\Entity\Encuesta $encuesta)
    {
        $this->encuesta[] = $encuesta;

        return $this;
    }

    /**
     * Remove encuesta
     *
     * @param \SurveyBundle\Entity\Encuesta $encuesta
     */
    public function removeEncuestum(\SurveyBundle\Entity\Encuesta $encuesta)
    {
        $this->encuesta->removeElement($encuesta);
    }

    /**
     * Get encuesta
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEncuesta()
    {
        return $this->encuesta;
    }

    /**
     * Set codPers
     *
     * @param string $codPers
     * @return Cliente
     */
    public function setCodPers($codPers)
    {
        $this->codPers = $codPers;

        return $this;
    }

    /**
     * Get codPers
     *
     * @return string
     */
    public function getCodPers()
    {
        return $this->codPers;
    }

    /**
     * Set telfBI
     *
     * @param string $telfBI
     * @return Cliente
     */
    public function setTelfBI($telfBI)
    {
        $this->telfBI = $telfBI;

        return $this;
    }

    /**
     * Get telfBI
     *
     * @return string
     */
    public function getTelfBI()
    {
        return $this->telfBI;
    }

    /**
     * Set mailBI
     *
     * @param string $mailBI
     * @return Cliente
     */
    public function setMailBI($mailBI)
    {
        $this->mailBI = $mailBI;

        return $this;
    }

    /**
     * Get mailBI
     *
     * @return string
     */
    public function getMailBI()
    {
        return $this->mailBI;
    }

    /**
     * Set idHash
     *
     * @param string $idHash
     * @return Cliente
     */
    public function setIdHash($idHash)
    {
        $this->idHash = $idHash;

        return $this;
    }

    /**
     * Get idHash
     *
     * @return string
     */
    public function getIdHash()
    {
        return $this->idHash;
    }

    /**
     * Set telf
     *
     * @param string $telf
     * @return Cliente
     */
    public function setTelf($telf)
    {
        $this->telf = $telf;

        return $this;
    }

    /**
     * Get telf
     *
     * @return string
     */
    public function getTelf()
    {
        return $this->telf;
    }

    /**
     * Set mail
     *
     * @param string $mail
     * @return Cliente
     */
    public function setMail($mail)
    {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get mail
     *
     * @return string
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Set nif
     *
     * @param string $nif
     * @return Cliente
     */
    public function setNif($nif)
    {
        $this->nif = $nif;

        return $this;
    }

    /**
     * Get nif
     *
     * @return string
     */
    public function getNif()
    {
        return $this->nif;
    }

    /**
     * Add encuestas
     *
     * @param \SurveyBundle\Entity\Encuesta $encuestas
     * @return Cliente
     */
    public function addEncuesta(\SurveyBundle\Entity\Encuesta $encuestas)
    {
        $this->encuestas[] = $encuestas;

        return $this;
    }

    /**
     * Remove encuestas
     *
     * @param \SurveyBundle\Entity\Encuesta $encuestas
     */
    public function removeEncuesta(\SurveyBundle\Entity\Encuesta $encuestas)
    {
        $this->encuestas->removeElement($encuestas);
    }

    /**
     * Get encuestas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEncuestas()
    {
        return $this->encuestas;
    }
}

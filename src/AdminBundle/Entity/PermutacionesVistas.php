<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PermutacionesVistas
 *
 * @ORM\Table(name="admin_permutaciones_vistas")
 * @ORM\Entity(repositoryClass="AdminBundle\Repository\PermutacionesVistasRepository")
 */
class PermutacionesVistas
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="SurveyBundle\Entity\EncuestaPregunta")
     */
    private $indicador;

    /**
     * @ORM\ManyToOne(targetEntity="SurveyBundle\Entity\EncuestaSubBloque")
     */
    private $subBloque;

    /**
     * @ORM\ManyToOne(targetEntity="Negocio")
     */
    private $negocio;

    /**
     * @ORM\ManyToOne(targetEntity="CanalEncuestacion")
     */
    private $canal;

    /**
     * @ORM\ManyToOne(targetEntity="SurveyBundle\Entity\EncuestaSubBloque")
     */
    private $proceso;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set indicador
     *
     * @param \SurveyBundle\Entity\EncuestaPregunta $indicador
     * @return PermutacionesVistas
     */
    public function setIndicador(\SurveyBundle\Entity\EncuestaPregunta $indicador = null)
    {
        $this->indicador = $indicador;

        return $this;
    }

    /**
     * Get indicador
     *
     * @return \SurveyBundle\Entity\EncuestaPregunta 
     */
    public function getIndicador()
    {
        return $this->indicador;
    }

    /**
     * Set subBloque
     *
     * @param \SurveyBundle\Entity\EncuestaSubBloque $subBloque
     * @return PermutacionesVistas
     */
    public function setSubBloque(\SurveyBundle\Entity\EncuestaSubBloque $subBloque = null)
    {
        $this->subBloque = $subBloque;

        return $this;
    }

    /**
     * Get subBloque
     *
     * @return \SurveyBundle\Entity\EncuestaSubBloque 
     */
    public function getSubBloque()
    {
        return $this->subBloque;
    }

    /**
     * Set negocio
     *
     * @param \AdminBundle\Entity\Negocio $negocio
     * @return PermutacionesVistas
     */
    public function setNegocio(\AdminBundle\Entity\Negocio $negocio = null)
    {
        $this->negocio = $negocio;

        return $this;
    }

    /**
     * Get negocio
     *
     * @return \AdminBundle\Entity\Negocio 
     */
    public function getNegocio()
    {
        return $this->negocio;
    }

    /**
     * Set canal
     *
     * @param \AdminBundle\Entity\CanalEncuestacion $canal
     * @return PermutacionesVistas
     */
    public function setCanal(\AdminBundle\Entity\CanalEncuestacion $canal = null)
    {
        $this->canal = $canal;

        return $this;
    }

    /**
     * Get canal
     *
     * @return \AdminBundle\Entity\CanalEncuestacion
     */
    public function getCanal()
    {
        return $this->canal;
    }

    /**
     * Set proceso
     *
     * @param \SurveyBundle\Entity\EncuestaSubBloque $proceso
     * @return PermutacionesVistas
     */
    public function setProceso(\SurveyBundle\Entity\EncuestaSubBloque $proceso = null)
    {
        $this->proceso = $proceso;

        return $this;
    }

    /**
     * Get proceso
     *
     * @return \SurveyBundle\Entity\EncuestaSubBloque 
     */
    public function getProceso()
    {
        return $this->proceso;
    }
}

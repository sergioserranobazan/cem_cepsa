<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DeterminacionCanal
 *
 * @ORM\Table(name="admin_determinacion_canal")
 * @ORM\Entity(repositoryClass="AdminBundle\Repository\DeterminacionCanalRepository")
 */
class DeterminacionCanal
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="nombre", type="string", length=256)
     */
    private $nombre;

    /**
     * @ORM\OneToMany(targetEntity="CanalDetCanal", mappedBy="determinacionCanal")
     */
    private $canalDetCanal;

    /**
     * @ORM\OneToMany(targetEntity="SurveyBundle\Entity\Encuesta",
      mappedBy="detCan")
     */
    private $encuestas;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return DeterminacionCanal
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->canalDetCanal = new \Doctrine\Common\Collections\ArrayCollection();
        $this->encuestas = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add canalDetCanal
     *
     * @param \AdminBundle\Entity\CanalDetCanal $canalDetCanal
     * @return DeterminacionCanal
     */
    public function addCanalDetCanal(\AdminBundle\Entity\CanalDetCanal $canalDetCanal)
    {
        $this->canalDetCanal[] = $canalDetCanal;

        return $this;
    }

    /**
     * Remove canalDetCanal
     *
     * @param \AdminBundle\Entity\CanalDetCanal $canalDetCanal
     */
    public function removeCanalDetCanal(\AdminBundle\Entity\CanalDetCanal $canalDetCanal)
    {
        $this->canalDetCanal->removeElement($canalDetCanal);
    }

    /**
     * Get canalDetCanal
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCanalDetCanal()
    {
        return $this->canalDetCanal;
    }

    /**
     * Add encuestas
     *
     * @param \SurveyBundle\Entity\Encuesta $encuestas
     * @return DeterminacionCanal
     */
    public function addEncuesta(\SurveyBundle\Entity\Encuesta $encuestas)
    {
        $this->encuestas[] = $encuestas;

        return $this;
    }

    /**
     * Remove encuestas
     *
     * @param \SurveyBundle\Entity\Encuesta $encuestas
     */
    public function removeEncuesta(\SurveyBundle\Entity\Encuesta $encuestas)
    {
        $this->encuestas->removeElement($encuestas);
    }

    /**
     * Get encuestas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEncuestas()
    {
        return $this->encuestas;
    }
}

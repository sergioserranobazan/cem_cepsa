<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NegocioProducto
 *
 * @ORM\Table(name="negocio_producto")
 * @ORM\Entity(repositoryClass="AdminBundle\Repository\NegocioProductoRepository")
 */
class NegocioProducto
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Negocio" , inversedBy="negocioProductos")
     * @ORM\JoinColumn(name="id_negocio", referencedColumnName="id", nullable=true)
     */
    private $negocio;

    /**
     * @ORM\ManyToOne(targetEntity="Producto", inversedBy="negocioProductos" )
     * @ORM\JoinColumn(name="id_producto", referencedColumnName="id", nullable=true)
     */
    private $producto;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set negocio
     *
     * @param \AdminBundle\Entity\Negocio $negocio
     * @return NegocioProducto
     */
    public function setNegocio(\AdminBundle\Entity\Negocio $negocio = null)
    {
        $this->negocio = $negocio;

        return $this;
    }

    /**
     * Get negocio
     *
     * @return \AdminBundle\Entity\Negocio 
     */
    public function getNegocio()
    {
        return $this->negocio;
    }

    /**
     * Set producto
     *
     * @param \AdminBundle\Entity\Producto $producto
     * @return NegocioProducto
     */
    public function setProducto(\AdminBundle\Entity\Producto $producto = null)
    {
        $this->producto = $producto;

        return $this;
    }

    /**
     * Get producto
     *
     * @return \AdminBundle\Entity\Producto 
     */
    public function getProducto()
    {
        return $this->producto;
    }
}

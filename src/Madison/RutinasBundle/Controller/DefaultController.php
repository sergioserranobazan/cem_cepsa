<?php

namespace Madison\RutinasBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

// Imports para poder usar comandos desde un controlador

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return new Response("Seleccione una rutina para ejecutar");
    }

    /**
     * @Route("/{comando}")
     * @Route("/{comando}/{opcion}")
     */
    public function ejecutarComando($comando, $opcion = "" )
    {
        $rutaBase = $this->get('kernel')->getRootDir() . "/../";

        chdir($rutaBase);

        if(!empty($opcion) && $opcion != "t")
            return new Response("Opción no válida");

        $kernel = $this->get('kernel');
        $application = new Application($kernel);
        $application->setAutoExit(false);

        // --  Creación de la configuración del comando a ejecutar
        $infoInput = array(
            "command" => $this->getComando($comando)
        );

        if(!empty($opcion) && $opcion == "t")
            $infoInput["-t"] = true;

        $input = new ArrayInput($infoInput);

        $output = new BufferedOutput();
        $application->run($input, $output);

        $content = $output->fetch();

        return new Response($content);
    }

    private function getComando($comandoPeticion)
    {
        $cmd = "";

        switch ($comandoPeticion) {
            case 'produccion':
                $cmd = "madison:produccion";
                break;
            default:
                # code...
                break;
        }

        return $cmd;
    }
}

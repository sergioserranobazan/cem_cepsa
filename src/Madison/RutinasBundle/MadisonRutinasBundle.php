<?php

namespace Madison\RutinasBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\Console\Application;
use Madison\RutinasBundle\Command\ComprimeCss;
use Madison\RutinasBundle\Command\PreparacionProduccion;
use Madison\RutinasBundle\Command\CrearEstructura;

class MadisonRutinasBundle extends Bundle
{
    /**
     * Registra de manera automática el comando para que esté accesible desde
     * app/console
     */
    public function registerCommands(Application $application)
    {
        $application->add(new ComprimeCss());
        $application->add(new PreparacionProduccion());
        $application->add(new CrearEstructura());
    }
}

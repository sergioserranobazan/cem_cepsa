<?php
namespace Madison\RutinasBundle\Command;

// Clase de Command en la cual se puede obtener el objeto container usado para
// obtener las rutas de los assets
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
// Formatea la salida por consola
use Symfony\Component\Console\Style\SymfonyStyle;
// Dependencias necesarias para cuando haces un comando en Symfony
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;
// Entity Manager para generar la estructura en funcíón de la BD
use Doctrine\ORM\EntityManager;


class CrearEstructura extends ContainerAwareCommand
{
    /**
     * Configuración del comando
     */
    protected function configure()
    {
        $this
            ->setName('madison:crear-estructura-maquetas') // Nombre para su ejecución
            ->setDescription('Crea la estructura del proyecto')
            ->setHelp("Este comando permite regenerar la estructura
                        del proyecto y guardarla en un yml");
    }

    protected function execute(InputInterface $input, OutputInterface $output){
        // Elementos necesarios para indentación YML
    	$estructura = $this->prepararEstructura();
    	$espacio_tab = 4;
    	$char_tab = " ";
        // Abrimos el archivo donde guardamos la estructura
		$file = fopen("src/Madison/RutinasBundle/Resources/config/estructura.yml", "w+");
        /*
        Ciclamos la estructura para ir escribiendo en el YML
        Es importante comprobar si cada nivel tiene hijos(foreach), en el caso contrario
        seteamos ese nivel a array vacío.
        */     	fwrite($file, "parameters: \n");
    	fwrite($file, str_repeat($char_tab, $espacio_tab)."estructura: \n");
        // Maquetas
    	foreach ($estructura as $nombreMaqueta => $contenidoMaquetas) {
    		fwrite(
                $file,
                str_repeat($char_tab, $espacio_tab*2).
                $nombreMaqueta.": \n");
    		fwrite(
                $file,
                str_repeat($char_tab, $espacio_tab*3)."twig: ".
                $contenidoMaquetas["twig"]." \n");
            // Zonas
            if(empty($contenidoMaquetas["zonas"])){
                fwrite($file, str_repeat($char_tab, $espacio_tab*3)."zonas: [] \n");
            }else{
    		    fwrite($file, str_repeat($char_tab, $espacio_tab*3)."zonas: \n");
                foreach ($contenidoMaquetas["zonas"] as  $nombreZona => $zonas) {
        			fwrite(
                        $file,
                        str_repeat($char_tab, $espacio_tab*4).
                        $nombreZona.": \n");
        			fwrite(
                        $file,
                        str_repeat($char_tab, $espacio_tab*5).
                        "twig: ".$zonas['twig']." \n");
                    // Contenedores
                    if(empty($zonas['contenedores'])){
                        fwrite(
                            $file,
                            str_repeat($char_tab, $espacio_tab*5).
                            "contenedores: [] \n");
                    // Contenedores
                    }else{
                        fwrite(
                            $file,
                            str_repeat($char_tab, $espacio_tab*5).
                            "contenedores: \n");
            			foreach ($zonas["contenedores"] as
                                    $nombreContenedor => $contenedores) {
            				fwrite(
                                    $file,
                                    str_repeat($char_tab, $espacio_tab*6).
                                    $nombreContenedor.": \n"
                            );
        	    			fwrite(
                                $file,
                                str_repeat($char_tab, $espacio_tab*7).
                                "twig: ".$contenedores['twig']." \n"
                            );
                                // Widgets
                                if (empty($contenedores['widgets'])){
                                    fwrite(
                                        $file,
                                        str_repeat($char_tab, $espacio_tab*7).
                                        "widgets: [] \n"
                                );
                                }else{
                                    fwrite(
                                        $file,
                                        str_repeat($char_tab, $espacio_tab*7).
                                        "widgets: \n"
                                    );
                                    foreach ($contenedores["widgets"] as
                                                $nombreWidget => $widgets) {
                                        fwrite(
                                            $file,
                                            str_repeat($char_tab, $espacio_tab*8).
                                            $nombreWidget.": \n"
                                        );
                                        fwrite(
                                            $file,
                                            str_repeat($char_tab, $espacio_tab*9).
                                            "nombre: ".$widgets['nombre']." \n");
                                        fwrite(
                                            $file,
                                            str_repeat($char_tab, $espacio_tab*9).
                                            "twig: ".$widgets['twig']." \n");
                                        fwrite(
                                            $file,
                                            str_repeat($char_tab, $espacio_tab*9).
                                            "service: ".$widgets['service']." \n");
                                        fwrite(
                                            $file,
                                            str_repeat($char_tab, $espacio_tab*9).
                                            "id: ".$widgets['id']." \n");
                                        fwrite(
                                            $file,
                                            str_repeat($char_tab, $espacio_tab*9).
                                            "widgetAjax: ".$widgets['widgetAjax']." \n");
                                    }
                                }
            			}
                    }
        		}
            }
    	}
    	fclose($file);
	}

    /*
     * Recorre desde maqueta todos los elementos internos y contruye un array
     * de estructura
     */
    public function prepararEstructura()
    {
        // Cargamos el Entity Manager
        $em = $this->getContainer()->get('doctrine')->getManager();
        $this->maquetas =
                    $em->getRepository('CmsBundle:Maqueta')->
                    findBy(
                        array('activo' => 1),
                        array('orden' => "ASC")
                        );

        $contenidoVista = [];
        foreach ($this->maquetas as $maqueta) {
            $nombreMaqueta = $maqueta->getSlug();
            $zonas = $this->getActivos($maqueta, "zonas");
            $contenidoVista[$nombreMaqueta] = array(
                    "twig" => $maqueta->getTwig(),
                    "zonas" => []
                );

            foreach ($zonas as $zona) {
                $nombreZona = $zona->getNombre();

                $contenedores = $this->getActivos($zona,"contenedores");

                $contenidoVista[$nombreMaqueta]["zonas"][$nombreZona] = array(
                    "twig" => $zona->getTwig(),
                    "contenedores" => []
                );
                foreach ($contenedores as $contenedor) {
                    $nombreContenedor = $contenedor->getNombre();
                    $contenidoVista[$nombreMaqueta]["zonas"][$nombreZona]["contenedores"][$nombreContenedor] =
                        array(
                            "twig"    => $contenedor->getTwig(),
                            "widgets" => [
]                        );

                    $widgets = $this->getActivos($contenedor,"widgets");
                    foreach ($widgets as $widget) {
                        $nombreWidget = $widget->getNombre();
                        $contenidoVista[$nombreMaqueta]["zonas"][$nombreZona]["contenedores"][$nombreContenedor]["widgets"][] =
                            array(
                                "nombre"     => $nombreWidget,
                                "service"    => $widget->getService(),
                                "id"         => $widget->getDescripcion(),
                                "twig"       => $widget->getTwig(),
                                "widgetAjax" => $widget->getWidgetAjax()
                                                ? "true"
                                                : "false"
                            );
                        }

                }

            }
        }
        return $contenidoVista;
    }

    /**
     * Devuelve una colección doctrine filtrada por el campo "activo"
     */
    private function getActivos($obj, $nombreColeccion)
    {
        $get = "get"
                    // Pone la primera letra del nombre de la colección en
                    // mayúsculas para generar el nombre de la función get que
                    // corresponde.
                    . ucfirst($nombreColeccion);

        $arrayOriginal = $obj->$get()
                            ->toArray(); // Para obtener un array con el que
                                         // pueda trabajar `array_filter`

        return array_filter($arrayOriginal, function($var){
            return $var->getActivo() == 1; // Se quedan en el array sólo los
                                            // que estén activos
        });
    }
}
?>
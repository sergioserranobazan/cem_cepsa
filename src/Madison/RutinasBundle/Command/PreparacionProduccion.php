<?php
namespace Madison\RutinasBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Filesystem\Filesystem;

class PreparacionProduccion extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('madison:produccion')
            ->setDescription('Comando para la preparación para producción');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

            // madison:crear-estructura-maquetas
            $output->writeln("Crea estructura");
            $command = $this->getApplication()
                            ->find('madison:crear-estructura-maquetas');
            $arguments = array();

            $input = new ArrayInput($arguments);
            $returnCode = $command->run($input, $output);

            // assets:install
            $output->writeln("Instala assets: ");
            $command = $this->getApplication()->find('assets:install');
            $arguments = array();

            $input = new ArrayInput($arguments);
            $returnCode = $command->run($input, $output);

            // assetic:dump
            $output->writeln("Instala assets assetic: ");
            $command = $this->getApplication()->find('assetic:dump');
            $arguments = array(
                "--env" =>"prod", "--no-debug" => true
            );
            $input = new ArrayInput($arguments);
            $returnCode = $command->run($input, $output);

            // limpieza cache
            $this->cacheClear($input, $output);

            // Creacion main-min.css
            $command = $this->getApplication()->find('madison:rutina-css');
            $output->writeln("Compila scss y comprime css: ");
            $arguments = array();
            $returnCode = $command->run($input, $output);
    }

    private function cacheClear(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("Borrando Cache...");
        $fs = new Filesystem();
        $dirCache = $this->getContainer()->getParameter('kernel.root_dir');
        $dirCache = $dirCache . "/cache/prod/";

        $output->writeln("Borrando directorios cache: " . $dirCache);
        $fs->remove($dirCache);

        $output->writeln("Cache limpiada con éxito");
    }
}
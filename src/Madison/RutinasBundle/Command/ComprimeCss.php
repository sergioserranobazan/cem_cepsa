<?php
namespace Madison\RutinasBundle\Command;

// Clase de Command en la cual se puede obtener el objeto container usado para
// obtener las rutas de los assets
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
// Formatea la salida por consola
use Symfony\Component\Console\Style\SymfonyStyle;
// Dependencias necesarias para cuando haces un comando en Symfony
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;

// Dependencias Assetic
use Assetic\AssetManager;
use Assetic\Asset\FileAsset; // Este de momento no se usa y se podría borrar
use Assetic\Asset\GlobAsset;
use Assetic\Asset\AssetCollection;
use Assetic\AssetWriter;
// Filtro para compilar-formatear scss con assetic
use Assetic\Filter\ScssphpFilter;

class ComprimeCss extends ContainerAwareCommand
{
    /**
     * Configuración del comando
     */
    protected function configure()
    {
        $this
            ->setName('madison:rutina-css') // Nombre para su ejecución
            ->setDescription('Compila Scss y comprime todos los archivos del
                proyecto');
    }

    protected function execute(InputInterface $input, OutputInterface $output){

        $arrayCss = $this->getRutasCss($output);
        $arrayFileAssets = [];

        foreach ($arrayCss as $asset) {
            $arrayFileAssets[] = new GlobAsset($asset);
        }


        //Configuración del filtro
        $scssFilter = new ScssphpFilter();
        $scssFilter->setFormatter(
            // Formateador que complia y comprime. Sin este filtro sólo se
            // compila el scss
            'scss_formatter_crunched'
        );

        //Creación de la colección de assets con sus filtros
        $css = new AssetCollection( $arrayFileAssets, array($scssFilter));

        // Configuración del nombre para el archivo de salida
        $css->setTargetPath("main-min.css");

        // Creación de asset manager y asignación de la AssetCollection
        // el assetManages es necesario para utilizar AssetWriter
        $am = new AssetManager();
        $am->set('scss', $css);

        // Ruta a la carpeta web
        $webFolderRoute = $this
                            ->getContainer()
                            ->get('kernel')
                            ->getRootDir() . "/../web";

        // Configuración y ejecución del AssetWriter
        $writer = new AssetWriter($webFolderRoute . "/css");
        $writer->writeManagerAssets($am);


        // Mensaje de salida
        $io = new SymfonyStyle($input, $output);
        $io ->success(array(
            'SCSS compilado',
            'CSS comprimido'
        ));
    }

    /**
     * Obtiene el array de rutas desde el parámetro 'madison_rutinas'
     * A continuación le pasa el array resultado a `addRutaDirectorioRaiz`
     * para procesar este array antes de devolverlo
     */
    protected function getRutasCss(OutputInterface $output)
    {
        $arrayCss = [];

        if($this->getContainer()->hasParameter('madison_rutinas')){
            // Obtener el array de rutas que ha sido previamente definido en
            // parameters.yml
            $arrayCss = $this->getContainer()
                        ->getParameter('madison_rutinas')['rutas_scss'];

        }else{
            // Mensaje de error por si 'madison_rutinas' no está definido
            $io->caution(
                "No está definida la variable madison_rutinas en"
                . "parameters.yml");
        }

        return $arrayCss;
    }
}
?>
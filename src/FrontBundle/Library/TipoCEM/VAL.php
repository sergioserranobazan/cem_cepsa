<?php

namespace FrontBundle\Library\TipoCEM;

class VAL
{
    //Preguntas de valoracion
    const INSATISFECHOS_MIN = 0;
    const INSATISFECHOS_MAX = 4;

    const NEUTRALES_MIN = 5;
    const NEUTRALES_MAX = 6;

    const SATISFECHOS_MIN = 7;
    const SATISFECHOS_MAX = 10;

    public static function esSatisfactorio($valor){
        if ($valor > VAL::SATISFECHOS_MAX || $valor < VAL::SATISFECHOS_MIN) {
            return false;
        }
        return true;
    }

    public static function esNeutral($valor){
        if ($valor > VAL::NEUTRALES_MAX || $valor < VAL::NEUTRALES_MIN) {
            return false;
        }
        return true;
    }

    public static function esInsatisfactorio($valor){
        if ($valor > VAL::INSATISFECHOS_MAX || $valor < VAL::INSATISFECHOS_MIN) {
            return false;
        }
        return true;
    }

    public static function getReValor($valor){
        return $valor;
    }

    public static function getNombre($valor){
        $nombre="No Name";
        if(VAL::esInsatisfactorio($valor)){
            $nombre = VAL::getNombres()[0];
        }elseif(VAL::esNeutral($valor)){
            $nombre = VAL::getNombres()[1];
        }elseif(VAL::esSatisfactorio($valor)){
            $nombre = VAL::getNombres()[2];
        }
        return $nombre;
    }

    public static function getArrayValoresTyped(){
        $arrayTyped=[];
        for ($contador = 0; $contador < count(VAL::getNombres()); $contador++) {
            $arrayTyped[$contador]["nombre"] = VAL::getNombres()[$contador];
            $arrayTyped[$contador]["valor"] = "";
            $arrayTyped[$contador]["color"] = VAL::getColores()[$contador];
        }
        return $arrayTyped;
    } 

    public static function getNombres(){
        $nombres = array ("INSATISFECHOS","NEUTRALES","SATISFECHOS");
        return $nombres;
    }  

    public static function getColores(){
        // Prosegur
        $colores = array ("CF002A","DEAE00","009900");
        // Leyenda Encuestas
        //$colores = array ("D9534F","FFA500","5CB85C");
        // Td's Encuestas
        //$colores = array ("F2DEDE","FCF8E3","DFF0D8");
        return $colores;
    }
    
    public static function getRangoLabels(){
        $rango[0] = array(  "min"=>VAL::INSATISFECHOS_MIN,
                            "max"=>VAL::INSATISFECHOS_MAX);
        $rango[1] = array(  "min"=>VAL::NEUTRALES_MIN,
                            "max"=>VAL::NEUTRALES_MAX);
        $rango[2] = array(  "min"=>VAL::SATISFECHOS_MIN,
                            "max"=>VAL::SATISFECHOS_MAX);

        return $rango;
    }

    public static function getSQL(){
        // p es alias de la tabla survey_encuesta_pregunta
        // er alias de survey_encuesta_respuesta
        // e alias de survey_encuesta
        $select=  "(AVG( er.valor )) AS indice,        
        COUNT( CASE WHEN er.valor <=".VAL::INSATISFECHOS_MAX." THEN 1 ELSE NULL END ) AS '".VAL::getNombres()[0]."',
        COUNT( CASE WHEN er.valor >=".VAL::NEUTRALES_MIN." AND er.valor <=".VAL::NEUTRALES_MAX." THEN 1 ELSE NULL END ) AS '".VAL::getNombres()[1]."',
        COUNT( CASE WHEN er.valor >=".VAL::SATISFECHOS_MIN." THEN 1 ELSE NULL END ) AS '".VAL::getNombres()[2]."',
        COUNT( CASE WHEN er.valor <=".VAL::INSATISFECHOS_MAX." THEN 1 ELSE NULL END )/COUNT(e.id)*100 AS 'PORC_".VAL::getNombres()[0]."',
        COUNT( CASE WHEN er.valor >=".VAL::NEUTRALES_MIN." AND er.valor <=".VAL::NEUTRALES_MAX." THEN 1 ELSE NULL END )/COUNT(e.id)*100 AS 'PORC_".VAL::getNombres()[1]."',
        COUNT( CASE WHEN er.valor >=".VAL::SATISFECHOS_MIN." THEN 1 ELSE NULL END )/COUNT(e.id)*100 AS 'PORC_".VAL::getNombres()[2]."' ";
        
        return $select;
    }    

    public static function getNoSQL($campo){

        $select=  "(AVG( e.".$campo." )) AS indice,        
        COUNT( CASE WHEN e.".$campo." <=".VAL::INSATISFECHOS_MAX." THEN 1 ELSE NULL END ) AS '".VAL::getNombres()[0]."',
        COUNT( CASE WHEN e.".$campo." >=".VAL::NEUTRALES_MIN." AND e.".$campo." <=".VAL::NEUTRALES_MAX." THEN 1 ELSE NULL END ) AS '".VAL::getNombres()[1]."',
        COUNT( CASE WHEN e.".$campo." >=".VAL::SATISFECHOS_MIN." THEN 1 ELSE NULL END ) AS '".VAL::getNombres()[2]."',
        COUNT( CASE WHEN e.".$campo." <=".VAL::INSATISFECHOS_MAX." THEN 1 ELSE NULL END )/COUNT(e.id)*100 AS 'PORC_".VAL::getNombres()[0]."',
        COUNT( CASE WHEN e.".$campo." >=".VAL::NEUTRALES_MIN." AND e.".$campo." <=".VAL::NEUTRALES_MAX." THEN 1 ELSE NULL END )/COUNT(e.id)*100 AS 'PORC_".VAL::getNombres()[1]."',
        COUNT( CASE WHEN e.".$campo." >=".VAL::SATISFECHOS_MIN." THEN 1 ELSE NULL END )/COUNT(e.id)*100 AS 'PORC_".VAL::getNombres()[2]."' ";
        
        return $select;
    }   
}

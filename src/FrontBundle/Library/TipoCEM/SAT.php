<?php

namespace FrontBundle\Library\TipoCEM;

class SAT
{
    //Experiencia
    const INSATISFECHOS_MIN = 0;
    const INSATISFECHOS_MAX = 4;

    const NEUTRALES_MIN = 5;
    const NEUTRALES_MAX = 6;

    const SATISFECHOS_MIN = 7;
    const SATISFECHOS_MAX = 10;

    public static function esSatisfactorio($valor){
        if ($valor > SAT::SATISFECHOS_MAX || $valor < SAT::SATISFECHOS_MIN) {
            return false;
        }
        return true;
    }

    public static function esNeutral($valor){
        if ($valor > SAT::NEUTRALES_MAX || $valor < SAT::NEUTRALES_MIN) {
            return false;
        }
        return true;
    }

    public static function esInsatisfactorio($valor){
        if ($valor > SAT::INSATISFECHOS_MAX || $valor < SAT::INSATISFECHOS_MIN) {
            return false;
        }
        return true;
    }

    public static function getReValor($valor){
        return $valor;
    }

    public static function getNombre($valor){
        $nombre="No Name";
        if(SAT::esInsatisfactorio($valor)){
            $nombre = SAT::getNombres()[0];
        }elseif(SAT::esNeutral($valor)){
            $nombre = SAT::getNombres()[1];
        }elseif(SAT::esSatisfactorio($valor)){
            $nombre = SAT::getNombres()[2];
        }
        return $nombre;
    }

    public static function getArrayValoresTyped(){
        $arrayTyped=[];
        for ($contador = 0; $contador < count(SAT::getNombres()); $contador++) {
            $arrayTyped[$contador]["nombre"] = SAT::getNombres()[$contador];
            $arrayTyped[$contador]["valor"] = "";
            $arrayTyped[$contador]["color"] = SAT::getColores()[$contador];
        }
        return $arrayTyped;
    } 

    public static function getNombres(){
        $nombres = array ("INSATISFECHOS","NEUTRALES","SATISFECHOS");
        return $nombres;
    }  

    public static function getColores(){
        // Prosegur
        $colores = array ("CF002A","DEAE00","009900");
        // Leyenda Encuestas
        //$colores = array ("D9534F","FFA500","5CB85C");
        // Td's Encuestas
        //$colores = array ("F2DEDE","FCF8E3","DFF0D8");
        return $colores;
    }

    public static function getRangoLabels(){
        $rango[0] = array(  "min"=>SAT::INSATISFECHOS_MIN,
                            "max"=>SAT::SATISFECHOS_MIN);
        $rango[1] = array(  "min"=>SAT::NEUTRALES_MIN,
                            "max"=>SAT::NEUTRALES_MAX);
        $rango[2] = array(  "min"=>SAT::SATISFECHOS_MIN,
                            "max"=>SAT::SATISFECHOS_MAX);

        return $rango;
    }

    public static function getSQL(){
        // p es alias de la tabla survey_encuesta_pregunta
        // er alias de survey_encuesta_respuesta
        // e alias de survey_encuesta
        $select=  "(AVG( er.valor )) AS indice,        
        COUNT( CASE WHEN er.valor <=".SAT::INSATISFECHOS_MAX." THEN 1 ELSE NULL END ) AS '".SAT::getNombres()[0]."',
        COUNT( CASE WHEN er.valor >=".SAT::NEUTRALES_MIN." AND er.valor <=".SAT::NEUTRALES_MAX." THEN 1 ELSE NULL END ) AS '".SAT::getNombres()[1]."',
        COUNT( CASE WHEN er.valor >=".SAT::SATISFECHOS_MIN." THEN 1 ELSE NULL END ) AS '".SAT::getNombres()[2]."',
        COUNT( CASE WHEN er.valor <=".SAT::INSATISFECHOS_MAX." THEN 1 ELSE NULL END )/COUNT(e.id)*100 AS 'PORC_".SAT::getNombres()[0]."',
        COUNT( CASE WHEN er.valor >=".SAT::NEUTRALES_MIN." AND er.valor <=".SAT::NEUTRALES_MAX." THEN 1 ELSE NULL END )/COUNT(e.id)*100 AS 'PORC_".SAT::getNombres()[1]."',
        COUNT( CASE WHEN er.valor >=".SAT::SATISFECHOS_MIN." THEN 1 ELSE NULL END )/COUNT(e.id)*100 AS 'PORC_".SAT::getNombres()[2]."' ";
        
        return $select;
    }

    public static function getNoSQL(){

        $select=  "(AVG( e.g3 )) AS indice,        
        COUNT( CASE WHEN e.g3 <=".SAT::INSATISFECHOS_MAX." THEN 1 ELSE NULL END ) AS '".SAT::getNombres()[0]."',
        COUNT( CASE WHEN e.g3 >=".SAT::NEUTRALES_MIN." AND e.g3 <=".SAT::NEUTRALES_MAX." THEN 1 ELSE NULL END ) AS '".SAT::getNombres()[1]."',
        COUNT( CASE WHEN e.g3 >=".SAT::SATISFECHOS_MIN." THEN 1 ELSE NULL END ) AS '".SAT::getNombres()[2]."',
        COUNT( CASE WHEN e.g3 <=".SAT::INSATISFECHOS_MAX." THEN 1 ELSE NULL END )/COUNT(e.id)*100 AS 'PORC_".SAT::getNombres()[0]."',
        COUNT( CASE WHEN e.g3 >=".SAT::NEUTRALES_MIN." AND e.g3 <=".SAT::NEUTRALES_MAX." THEN 1 ELSE NULL END )/COUNT(e.id)*100 AS 'PORC_".SAT::getNombres()[1]."',
        COUNT( CASE WHEN e.g3 >=".SAT::SATISFECHOS_MIN." THEN 1 ELSE NULL END )/COUNT(e.id)*100 AS 'PORC_".SAT::getNombres()[2]."' ";
        
        return $select;
    }    
}

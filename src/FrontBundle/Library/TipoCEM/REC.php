<?php

namespace FrontBundle\Library\TipoCEM;

class REC
{

    const NORECOMENDADORES_MIN = 0;
    const NORECOMENDADORES_MAX = 4;

    const NEUTRALES_MIN = 5;
    const NEUTRALES_MAX = 6;

    const RECOMENDADORES_MIN = 7;
    const RECOMENDADORES_MAX = 10;

    public static function esRecomendador($valor){
        if ($valor > REC::RECOMENDADORES_MAX || $valor < REC::RECOMENDADORES_MIN) {
            return false;
        }
        return true;
    }

    public static function esNeutral($valor){
        if ($valor > REC::NEUTRALES_MAX || $valor < REC::NEUTRALES_MIN) {
            return false;
        }
        return true;
    }

    public static function esNoRecomendador($valor){
        if ($valor > REC::NORECOMENDADORES_MAX || $valor < REC::NORECOMENDADORES_MIN) {
            return false;
        }
        return true;
    }

    public static function getReValor($valor){
        return $valor;
    }

    public static function getNombre($valor){
        $nombre="No Name";
        if(REC::esNoRecomendador($valor)){
            $nombre = REC::getNombres()[0];
        }elseif(REC::esNeutral($valor)){
            $nombre = REC::getNombres()[1];
        }elseif(REC::esRecomendador($valor)){
            $nombre = REC::getNombres()[2];
        }
        return $nombre;
    }

    public static function getArrayValoresTyped(){
        $arrayTyped=[];
        for ($contador = 0; $contador < count(REC::getNombres()); $contador++) {
            $arrayTyped[$contador]["nombre"] = REC::getNombres()[$contador];
            $arrayTyped[$contador]["valor"] = "";
            $arrayTyped[$contador]["color"] = REC::getColores()[$contador];
        }
        return $arrayTyped;
    }  

    public static function getNombres(){
        $nombres = array ("NO RECOMENDADORES","NEUTRALES","RECOMENDADORES");
        return $nombres;
    }

    public static function getColores(){
        // Prosegur
        $colores = array ("CF002A","DEAE00","009900");
        // Leyenda Encuestas
        //$colores = array ("D9534F","FFA500","5CB85C");
        // Td's Encuestas
        //$colores = array ("F2DEDE","FCF8E3","DFF0D8");
        return $colores;
    }

    public static function getRangoLabels(){
        $rango[0] = array(  "min"=>REC::NORECOMENDADORES_MIN,
                            "max"=>REC::NORECOMENDADORES_MAX);
        $rango[1] = array(  "min"=>REC::NEUTRALES_MIN,
                            "max"=>REC::NEUTRALES_MAX);
        $rango[2] = array(  "min"=>REC::RECOMENDADORES_MIN,
                            "max"=>REC::RECOMENDADORES_MAX);

        return $rango;
    }
    
    public static function getSQL(){
        // p es alias de la tabla survey_encuesta_pregunta
        // er alias de survey_encuesta_respuesta
        // e alias de survey_encuesta
        $select=  "(AVG( er.valor )) AS indice,           
        COUNT( CASE WHEN er.valor <=".REC::NORECOMENDADORES_MAX." THEN 1 ELSE NULL END ) AS '".REC::getNombres()[0]."',
        COUNT( CASE WHEN er.valor >=".REC::NEUTRALES_MIN." AND er.valor <=".REC::NEUTRALES_MAX." THEN 1 ELSE NULL END ) AS '".REC::getNombres()[1]."',
        COUNT( CASE WHEN er.valor >=".REC::RECOMENDADORES_MIN." THEN 1 ELSE NULL END ) AS '".REC::getNombres()[2]."',
        COUNT( CASE WHEN er.valor <=".REC::NORECOMENDADORES_MAX." THEN 1 ELSE NULL END )/COUNT(e.id)*100 AS 'PORC_".REC::getNombres()[0]."',
        COUNT( CASE WHEN er.valor >=".REC::NEUTRALES_MIN." AND er.valor <=".REC::NEUTRALES_MAX." THEN 1 ELSE NULL END )/COUNT(e.id)*100 AS 'PORC_".REC::getNombres()[1]."',
        COUNT( CASE WHEN er.valor >=".REC::RECOMENDADORES_MIN." THEN 1 ELSE NULL END )/COUNT(e.id)*100 AS 'PORC_".REC::getNombres()[2]."' "; // p es alias de la tabla survey_encuesta_pregunta
        
        return $select;
    }

    public static function getNoSQL(){

        $select=  "(AVG( e.g2 )) AS indice,           
        COUNT( CASE WHEN e.g2 <=".REC::NORECOMENDADORES_MAX." THEN 1 ELSE NULL END ) AS '".REC::getNombres()[0]."',
        COUNT( CASE WHEN e.g2 >=".REC::NEUTRALES_MIN." AND e.g2 <=".REC::NEUTRALES_MAX." THEN 1 ELSE NULL END ) AS '".REC::getNombres()[1]."',
        COUNT( CASE WHEN e.g2 >=".REC::RECOMENDADORES_MIN." THEN 1 ELSE NULL END ) AS '".REC::getNombres()[2]."',
        COUNT( CASE WHEN e.g2 <=".REC::NORECOMENDADORES_MAX." THEN 1 ELSE NULL END )/COUNT(e.id)*100 AS 'PORC_".REC::getNombres()[0]."',
        COUNT( CASE WHEN e.g2 >=".REC::NEUTRALES_MIN." AND e.g2 <=".REC::NEUTRALES_MAX." THEN 1 ELSE NULL END )/COUNT(e.id)*100 AS 'PORC_".REC::getNombres()[1]."',
        COUNT( CASE WHEN e.g2 >=".REC::RECOMENDADORES_MIN." THEN 1 ELSE NULL END )/COUNT(e.id)*100 AS 'PORC_".REC::getNombres()[2]."' "; // p es alias de la tabla survey_encuesta_pregunta
        
        return $select;
    }      
}
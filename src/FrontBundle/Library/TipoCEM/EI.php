<?php

namespace FrontBundle\Library\TipoCEM;

class EI
{
    const NEGATIVOS_MIN = 0;
    const NEGATIVOS_MAX = 5;

    const NEUTROS_MIN = 5;   // No inclusive
    const NEUTROS_MAX = 7;   // No inclusive

    const POSITIVOS_MIN = 7;
    const POSITIVOS_MAX = 10;

    public static function getValor($adjetivo){
        $adjetivo=trim($adjetivo," ​");
        $valor=10;
        switch ($adjetivo) {
            case EI::getAdjetivos()[0]:
                $valor=0;
                break;
            case EI::getAdjetivos()[1]:
                $valor=2;
                break;
            case EI::getAdjetivos()[2]:
                $valor=4;
                break;
            case EI::getAdjetivos()[3]:
            case EI::getAdjetivos()[4]:
                $valor=7;
                break;                
            case EI::getAdjetivos()[5]:
                $valor=10;
                break;
            default:
                $valor=10;
                break;
        }
        return $valor;
    }

    /*public static function getValor($adjetivo){
        $adjetivo=trim($adjetivo," ​");
        $valor=10;
        switch ($adjetivo) {
            case 'Enfadad@':
                $valor=0;
                break;
            case 'Decepcionad@':
                $valor=2;
                break;
            case 'Ignorad@':
                $valor=4;
                break;
            case 'Confiad@':
            case 'Cuidad@':
                $valor=7;
                break;                
            case 'Gratamente Sorprendid@':
                $valor=10;
                break;
            default:
                $valor=10;
                break;
        }
        return $valor;
    }*/

    public static function esPositivo($adjetivo){
        if(!is_numeric($adjetivo)){
            $valor= EI::getValor($adjetivo);
        }else{
            $valor=$adjetivo;
        }
        if ($valor > EI::POSITIVOS_MAX || $valor < EI::POSITIVOS_MIN) {
            return false;
        }
        return true;
    }

    //Ningun valor asociado al adjetivo es neutral (neutro es >5 y <7 )
    public static function esNeutro($adjetivo){
        if(!is_numeric($adjetivo)){
            $valor= EI::getValor($adjetivo);
        }else{
            $valor=$adjetivo;
        }
        if ($valor >= EI::NEUTROS_MAX || $valor <= EI::NEUTROS_MIN) {
            return false;
        }
        return true;
    }

    public static function esNegativo($adjetivo){
        if(!is_numeric($adjetivo)){
            $valor= EI::getValor($adjetivo);
        }else{
            $valor=$adjetivo;
        }
        if ($valor > EI::NEGATIVOS_MAX || $valor < EI::NEGATIVOS_MIN) {
            return false;
        }
        return true;
    }

    public static function getReValor($adjetivo){
        $valor= EI::getValor($adjetivo);
        return $valor;
    }

    public static function getNombre($adjetivo){

        if(!is_numeric($adjetivo)){
            $valor= EI::getValor($adjetivo);
        }else{
            $valor=$adjetivo;
        }
        $nombre="No Name";
        if(EI::esNegativo($valor)){
            $nombre = EI::getNombres()[0];
        }elseif(EI::esNeutro($valor)){
            $nombre = EI::getNombres()[1];
        }elseif(EI::esPositivo($valor)){
            $nombre = EI::getNombres()[2];
        }
        return $nombre;
    }

    public static function getArrayValoresTyped(){
        $arrayTyped=[];
        for ($contador = 0; $contador < count(EI::getNombres()); $contador++) {
            $arrayTyped[$contador]["nombre"] = EI::getNombres()[$contador];
            $arrayTyped[$contador]["valor"] = "";
            $arrayTyped[$contador]["color"] = EI::getColores()[$contador];
        }
        return $arrayTyped;
    }

    public static function getNombres(){
        $nombres = array ("NEGATIVO","NEUTRO","POSITIVO");
        return $nombres;
    } 

    public static function getAdjetivos(){
        $nombres = array ("Enfadad@","Decepcionad@","Ignorad@","Confiad@","Cuidad@","Gratamente Sorprendid@");
        return $nombres;
    }

    public static function getRangoLabels(){
        $rango[0] = array(  "min"=>EI::NEGATIVOS_MIN,
                            "max"=>EI::NEGATIVOS_MAX);
        $rango[1] = array(  "min"=>EI::NEUTROS_MIN+0.01,
                            "max"=>EI::NEUTROS_MAX-0.01);
        $rango[2] = array(  "min"=>EI::POSITIVOS_MIN,
                            "max"=>EI::POSITIVOS_MAX);

        return $rango;
    }
    // Array usado en los service de treeview
    // Este orden ha de ser el mismo que el array de EI::getAdjetivos()
    // para asegurar que con ocurrencias iguales de adjetivos, muestre
    // siempre el mismo adjetivo.
    public static function getCamposAdjetivos(){
        $campos = ["num_enfadados", "num_decepcionados", "num_ignorados",
                    "num_confiados", "num_cuidados", "num_g_sorprendidos",
        ];
        return $campos;
    }

    public static function getColores(){
        // Prosegur
        $colores = array ("CF002A","DEAE00","009900");
        // Leyenda Encuestas
        //$colores = array ("D9534F","FFA500","5CB85C");
        // Td's Encuestas
        //$colores = array ("F2DEDE","FCF8E3","DFF0D8");
        return $colores;
    }  

    public static function getSQL(){
        // p es alias de la tabla survey_encuesta_pregunta
        // er alias de survey_encuesta_respuesta
        // e alias de survey_encuesta
        $arrayAdjetivos = EI::getAdjetivos();
        $select=  "AVG(null) AS indice ";   // el indice se calculara en php, en funcion de las cantidades maximas
                                            // de los adjetivos, y del cruce en el service correspondiente
        for ($i=0; $i <count($arrayAdjetivos) ; $i++) { 
            $select .= ", COUNT( CASE WHEN er.valor LIKE '".$arrayAdjetivos[$i]."%' THEN 1 ELSE NULL END ) AS 'ADJETIVO_".$arrayAdjetivos[$i]."' ";
            //$select .= ", COUNT( CASE WHEN er.valor LIKE '".$arrayAdjetivos[$i]."%' THEN 1 ELSE NULL END )/COUNT(e.id)*100 AS 'PORC_ADJETIVO_".$arrayAdjetivos[$i]."' ";
        }

        return $select;
    }

    public static function getNoSQL(){
        $arrayAdjetivos = EI::getAdjetivos();
        //$select=  "AVG(null) AS indice ";   // el indice se calculara en php, en funcion de las cantidades maximas
                                            // de los adjetivos, y del cruce en el service correspondiente
        $select = " AVG(e.g1_valor) as indice";
        for ($i=0; $i <count($arrayAdjetivos) ; $i++) { 
            $select .= ", COUNT( CASE WHEN e.g1 LIKE '".$arrayAdjetivos[$i]."%' THEN 1 ELSE NULL END ) AS 'ADJETIVO_".$arrayAdjetivos[$i]."' ";
            //$select .= ", COUNT( CASE WHEN er.valor LIKE '".$arrayAdjetivos[$i]."%' THEN 1 ELSE NULL END )/COUNT(e.id)*100 AS 'PORC_ADJETIVO_".$arrayAdjetivos[$i]."' ";
        }

        return $select;
    }    
}

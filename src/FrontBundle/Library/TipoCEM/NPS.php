<?php

namespace FrontBundle\Library\TipoCEM;

class NPS
{

    const DETRACTORES_MIN = 0;
    const DETRACTORES_MAX = 6;

    const NEUTRALES_MIN = 7;
    const NEUTRALES_MAX = 8;

    const PROMOTORES_MIN = 9;
    const PROMOTORES_MAX = 10;

    public static function esPromotor($valor){
        if ($valor > NPS::PROMOTORES_MAX || $valor < NPS::PROMOTORES_MIN) {
            return false;
        }
        return true;
    }

    public static function esNeutral($valor){
        if ($valor > NPS::NEUTRALES_MAX || $valor < NPS::NEUTRALES_MIN) {
            return false;
        }
        return true;
    }

    public static function esDetractor($valor){
        if ($valor > NPS::DETRACTORES_MAX || $valor < NPS::DETRACTORES_MIN) {
            return false;
        }
        return true;
    }

    public static function getReValor($valor){
        return $valor * 10;
    }

    public static function getNombre($valor){
        $nombre="No Name";
        if(NPS::esDetractor($valor)){
            $nombre = NPS::getNombres()[0];
        }elseif(NPS::esNeutral($valor)){
            $nombre = NPS::getNombres()[1];
        }elseif(NPS::esPromotor($valor)){
            $nombre = NPS::getNombres()[2];
        }
        return $nombre;
    }

    public static function getArrayValoresTyped(){
        $arrayTyped=[];
        for ($contador = 0; $contador < count(NPS::getNombres()); $contador++) {
            $arrayTyped[$contador]["nombre"] = NPS::getNombres()[$contador];
            $arrayTyped[$contador]["valor"] = "";
            $arrayTyped[$contador]["color"] = NPS::getColores()[$contador];
        }
        return $arrayTyped;
    }

    public static function getNombres(){
        $nombres = array ("DETRACTORES","NEUTRALES","PROMOTORES");
        return $nombres;
    } 

    public static function getColores(){
        // Prosegur
        $colores = array ("CF002A","DEAE00","009900");
        // Leyenda Encuestas
        //$colores = array ("D9534F","FFA500","5CB85C");
        // Td's Encuestas
        //$colores = array ("F2DEDE","FCF8E3","DFF0D8");
        return $colores;
    }

    public static function getRangoLabels(){
        $rango[0] = array(  "min"=>NPS::DETRACTORES_MIN,
                            "max"=>NPS::DETRACTORES_MAX);
        $rango[1] = array(  "min"=>NPS::NEUTRALES_MIN,
                            "max"=>NPS::NEUTRALES_MAX);
        $rango[2] = array(  "min"=>NPS::PROMOTORES_MIN,
                            "max"=>NPS::PROMOTORES_MAX);

        return $rango;
    }

    public static function getSQL(){
        // p es alias de la tabla survey_encuesta_pregunta
        // er alias de survey_encuesta_respuesta
        // e alias de survey_encuesta
        $select="  (AVG( er.valor >=".NPS::PROMOTORES_MIN." ) - AVG( er.valor <=".NPS::DETRACTORES_MAX." ))*100 AS indice,        
        COUNT( CASE WHEN er.valor <=".NPS::DETRACTORES_MAX." THEN 1 ELSE NULL END ) AS '".NPS::getNombres()[0]."',
        COUNT( CASE WHEN er.valor >=".NPS::NEUTRALES_MIN." AND er.valor <=".NPS::NEUTRALES_MAX." THEN 1 ELSE NULL END ) AS '".NPS::getNombres()[1]."',
        COUNT( CASE WHEN er.valor >=".NPS::PROMOTORES_MIN." THEN 1 ELSE NULL END ) AS '".NPS::getNombres()[2]."',
        COUNT( CASE WHEN er.valor <=".NPS::DETRACTORES_MAX." THEN 1 ELSE NULL END )/COUNT(e.id)*100 AS 'PORC_".NPS::getNombres()[0]."',
        COUNT( CASE WHEN er.valor >=".NPS::NEUTRALES_MIN." AND er.valor <=".NPS::NEUTRALES_MAX." THEN 1 ELSE NULL END )/COUNT(e.id)*100 AS 'PORC_".NPS::getNombres()[1]."',
        COUNT( CASE WHEN er.valor >=".NPS::PROMOTORES_MIN." THEN 1 ELSE NULL END )/COUNT(e.id)*100 AS 'PORC_".NPS::getNombres()[2]."' "; 

        return $select;
    }

    public static function getNoSQL(){

        $select="  (AVG( e.g2 >=".NPS::PROMOTORES_MIN." ) - AVG( e.g2 <=".NPS::DETRACTORES_MAX." ))*100 AS indice,        
        COUNT( CASE WHEN e.g2 <=".NPS::DETRACTORES_MAX." THEN 1 ELSE NULL END ) AS '".NPS::getNombres()[0]."',
        COUNT( CASE WHEN e.g2 >=".NPS::NEUTRALES_MIN." AND e.g2 <=".NPS::NEUTRALES_MAX." THEN 1 ELSE NULL END ) AS '".NPS::getNombres()[1]."',
        COUNT( CASE WHEN e.g2 >=".NPS::PROMOTORES_MIN." THEN 1 ELSE NULL END ) AS '".NPS::getNombres()[2]."',
        COUNT( CASE WHEN e.g2 <=".NPS::DETRACTORES_MAX." THEN 1 ELSE NULL END )/COUNT(e.id)*100 AS 'PORC_".NPS::getNombres()[0]."',
        COUNT( CASE WHEN e.g2 >=".NPS::NEUTRALES_MIN." AND e.g2 <=".NPS::NEUTRALES_MAX." THEN 1 ELSE NULL END )/COUNT(e.id)*100 AS 'PORC_".NPS::getNombres()[1]."',
        COUNT( CASE WHEN e.g2 >=".NPS::PROMOTORES_MIN." THEN 1 ELSE NULL END )/COUNT(e.id)*100 AS 'PORC_".NPS::getNombres()[2]."' "; 

        return $select;
    }

}

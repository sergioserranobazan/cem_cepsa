<?php

namespace FrontBundle\Service;

use AdminBundle\Service\FiltrosSesionService as FiltrosSesion;
use AdminBundle\Library\CemUtils;
/**
*
*/
class WidgetFiltrosSeleccionadosService extends ServiceBase
{

    function __construct(FiltrosSesion $fs, $bloquesFiltros)
    {
        $this->fs = $fs;
        $this->filtros = $fs->getFiltros();
        $this->bloquesFiltros = $bloquesFiltros;
    }

    public function filtrarDatos($datos)
    {
        $this->datos["filtrosSeleccionados"] = 
            array_merge($this->tratamientoDatosTemporalidad($this->filtros),
                        $this->tratamientoArrayFiltros($this->filtros));
        // Transforma los arrays de los distintos filtros a un String
        $this->datos["filtrosFormateados"] = 
            CemUtils::arrayDeStrings($this->datos["filtrosSeleccionados"]);
        // Añade los datos (la maqueta)
        $this->datos = array_merge($this->datos,$datos);

        return $this;
    }

    public function tratamientoArrayFiltros($filtros)
    {

        $arrayResultado = [];

        $datosFiltros = [];

        foreach ($this->bloquesFiltros as $id) {
            $datosFiltros =
                array_merge(
                    $datosFiltros, $this->fs->getValoresFiltroBloque($id));
        }

        foreach ($datosFiltros as $datos) {

            $equivalenciasId = $datos["valores"];

            $valoresFiltroSesion = $filtros[$datos["nombreId"]];

            $nombreId = empty($datos["tag"])
                        ? $datos["nombreId"]
                        : $datos["tag"];

            if(!empty($valoresFiltroSesion)){

                $valoresFiltroSesion = $this->filtroIdToString(
                    $valoresFiltroSesion, $equivalenciasId
                );

                if(gettype($valoresFiltroSesion) != "array"){
                    $valoresFiltroSesion =  [$valoresFiltroSesion];
                }

                $arrayResultado[$nombreId] = $valoresFiltroSesion;
            }
        }

        // Filtros especiales Graficos -> DataTables
        if(isset($filtros["filtrosGraficoDatatable"])){
            $filtroCruce = null;
            if(isset($arrayResultado['cruce'])){ // RadioButton              
                $filtroCruce = $arrayResultado['cruce'][0];
            }
            $filtrosGraficoDatatable = 
                $this->tratamientoFiltrosEspeciales($filtros["filtrosGraficoDatatable"],
                                                    $filtroCruce);
            $arrayResultado = array_merge($arrayResultado,$filtrosGraficoDatatable);
        }

        return $arrayResultado;
    }

    // Criterios de filtrado especiales del datatable del grafico
    public function tratamientoFiltrosEspeciales($filtrosGraficos,$filtroCruce = null){
        $arrayResultado = [];

        $arrayCruce = $filtrosGraficos["cruce"];
        $descripcionCompleta = $filtrosGraficos["pregunta"]["descripcion_completa"];
        $rangoLabel = $filtrosGraficos["rangoValoraciones"];

        $filtrosDefault = $this->fs->getValoresDefaultFiltros();
        $tagCruce = "";
        foreach ($filtrosDefault as $filtro) {
            if($filtro['nombreId'] == $arrayCruce["cruce"]."-filtro"){
                $tagCruce = empty($filtro["tag"])
                        ? $filtro["nombreId"]
                        : $filtro["tag"];
            }
        }
        // Se añaden los nuevos criterios de filtrado
        //1* Cruce
        $arrayResultado[$tagCruce] = $arrayCruce["nombreStack"];
        //2* Pregunta
        $arrayResultado["Pregunta"] = $descripcionCompleta;
        //3* Rango Valoraciones, si exite
        if(!empty($rangoLabel)){
            $arrayResultado["Valoraciones"] = 
                "[".$rangoLabel["min"]."-".$rangoLabel["max"]."]";
        }
        //4* Para la maqueta de cruce de variables, el radioButton.
        if(!is_null($filtroCruce)){
            $arrayResultado['cruce'] = $tagCruce;
        }

        return $arrayResultado;
    }

    /**
     * Extrae y formatea los datos de filtros de temporalidad (temporalidad,
     * fec_ini  y fec_fin) del array de filtros
     * @param  [array] $filtros
     * @return [array]
     */
    public function tratamientoDatosTemporalidad($filtros)
    {
        $datosTemporalidad = [];

        $temporalidad = $filtros["temporalidad"];

        $fec_ini = $filtros["fec_ini"];

        if($temporalidad != "0"){ // Cuando la temporalidad es por rango
            // Transformación del id de temporalidad en su equivalente string
            $datosTemporalidad["temporalidad"] =
                $this->fs
                     ->getValoresEnBDFiltro("temporalidad")
                     [$temporalidad];

            $datosTemporalidad["fecha referencia"] =
                $this->formatoFecha($fec_ini);

        }else{ // Cualquier otro tipo de temporalidad
            // Temporalidad 0 es por rango y no está parametrizada en BD
            $datosTemporalidad["temporalidad"] = "Por rango";

            $datosTemporalidad["fecha inicio"] = $this->formatoFecha($fec_ini);

            $fec_fin = $filtros["fec_fin"];
            $datosTemporalidad["fecha fin"] = $this->formatoFecha($fec_fin);
        }

        return $datosTemporalidad;
    }

    private function formatoFecha($fechaAFormatear)
    {
        $fecha = new \DateTime();

        if(!empty($fechaAFormatear)){
            $fecha = new \DateTime($fechaAFormatear);
        }

        $fecha = $fecha->format("d/m/Y");

        return $fecha;
    }

    private function filtroIdToString($valores, $equivalencias)
    {
        if(gettype($valores) == "string" && !is_numeric($valores))
            return $valores;

        $resultado = [];

        if(!empty($valores)){
            if(is_array($valores)){
                foreach ($valores as $valor) {
                    $resultado[] = $equivalencias[$valor];
                }
            }else{
                $resultado = $equivalencias[$valores];
            }
        }else{
            $resultado = "";
        }

        return $resultado;
    }

    public function getDatos()
    {
        return $this->datos;
    }
}
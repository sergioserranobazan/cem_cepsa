<?php

namespace FrontBundle\Service;

use Doctrine\ORM\EntityManager;

/**
*
*/
class WidgetFiltroOpcionesConsultaService extends ServiceBase
{

    function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->lineasNegocio = $this->getArrayFiltro(
            $em->getRepository('AdminBundle:LineaNegocio')->findAll(),
            "L. Negocio"
        );

        $this->segmentos = $this->getArrayFiltro(
            $em->getRepository('AdminBundle:Segmento')->findAll(),
            "Segmento"
        );

        $this->tipificaciones = $this->getArrayFiltro(
            $em->getRepository('AdminBundle:TipificacionOpinion')->findAll(),
            "V. Tipificaciones"
        );
    }

    public function filtrarDatos($datos)
    {
        $this->datos['cabecera'] = "OPC. CONSULTA";

        $this->datos['filtros']["fSegmento"] = array(
            "filtroDatos" => $this->segmentos
        );

        $this->datos['filtros']["fNegocio"] = array(
            "filtroDatos" => $this->lineasNegocio
        );
        $this->datos['filtros']["fTipificaciones"] = array(
            "filtroDatos" => $this->tipificaciones
        );

        return $this;
    }

    private function getArrayFiltro($filtroRepo, $nombreDefault)
    {
        $arrayResultado= ["default" => $nombreDefault];

        foreach ($filtroRepo as $registro) {
            // Seguimos con la necesidad de que los filtros tengan un campo
            // nombre con sus valores
            $arrayResultado["valores"][] = $registro->getNombre();
        }

        return $arrayResultado;
    }
}
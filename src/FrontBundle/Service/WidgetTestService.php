<?php

namespace FrontBundle\Service;

use AdminBundle\Service\FiltrosSesionService as FiltrosService;

class WidgetTestService extends ServiceBase
{
    private $filtros;

    function __construct($foo, FiltrosService $fs)
    {
        $this->filtros = $fs->getFiltros();
    }

    public function filtrarDatos($datos)
    {
        $this->datos = array(
            "filtros" => $this->filtros,
            "encuestas" => $datos
        );

        return $this;
    }
}

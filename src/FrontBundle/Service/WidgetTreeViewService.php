<?php

namespace FrontBundle\Service;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;
use AdminBundle\Service\DQLBuilderService as DQLBuilder;
use AdminBundle\Service\FiltrosSesionService as FiltrosSesion;

use AdminBundle\Library\CemUtils;

use FrontBundle\Service\MaquetaPrincipal\WidgetTablaEspecificoProcesoService as EspecificoProceso;
use FrontBundle\Library\TipoCEM\EI;

use Mrcem\PerformanceLogger\PerformanceIssuer;

class WidgetTreeViewService extends ServiceBase
{
    private $encuestas;
    private $equivalencias;
    private $estructuraTreeview = [];

    function __construct(
        FiltrosSesion $fs, EntityManager $em, EspecificoProceso $ep,
        DQLBuilder $dql
    ){

        // Performance Logger
        $this->logSQL = new PerformanceIssuer(
            "treeView_principal", PerformanceIssuer::FILE_LOGGER
        );
        $this->logSQL->start();


        // Services inyectados
        $this->fs  = $fs;
        $this->em  = $em;
        $this->ep  = $ep;
        $this->dql = $dql;

        $this->filtros = $fs->getFiltros();

        // VARIABLES DE CONFIGURACIÓN
        $this->equivalencias = array( // Array donde se relacionan los índices
                                        // de cada una de las entidades usadas
                                        // en este service. Esto es porque en
                                        // sesión guardamos el número de id
                                        // de cada filtro y necesitamos sus
                                        // valores.

            // Permite poder saber las vistas que han sido seleccionadas.
            "vista" => $fs->getValoresEnBDFiltro("vista"),
            // Obtención de array con $id => valor de cada uno de los filtros
            // que han podido seleccionarse como vista por el cliente.
            "proceso" => $fs->getValoresEnBDFiltro("proceso"),
            "negocio" => $fs->getValoresEnBDFiltro("neg")
        );
    }

    private function getDatosConGroupByProcess($groupBy)
    {
        $conn = $this->em->getConnection();

        $subSelectIdPreguntasEsuerzo = "
          SELECT ep.id
            FROM survey_encuesta_pregunta ep
            WHERE ep.descripcion LIKE 'Esfuerzo'
        ";

        $sql = "
            SELECT e.neg_id as neg_id,
                e.can_id as can_id,
                e.proceso_id as proceso_id,
                count(DISTINCT(e.encuesta_id)) numEncuestas,
                count(*) numRegistros,
                SUM(e.g2) as SUM_Recomendacion,
                SUM(e.g3) as SUM_SAT,
                SUM(IF(e.g2 >= 9, 1, 0)) - SUM(IF(e.g2 <= 6, 1, 0)) as SUM_NPS,

                /* ------ Cálculos para Emoción y Emotional Index  ---------*/
                SUM(e.g1_valor) as emotional_index,
                SUM(IF(e.g1 = 'Confiad@', 1, 0)) num_confiados,
                SUM(IF(e.g1 = 'Cuidad@', 1, 0)) num_cuidados,
                SUM(IF(e.g1 = 'Decepcionad@', 1,0)) num_decepcionados,
                SUM(IF(e.g1 = 'Enfadad@', 1, 0)) num_enfadados,
                SUM(IF(e.g1 = 'Gratamente sorprendid@', 1,0)) num_g_sorprendidos,
                SUM(IF(e.g1 = 'Ignorad@', 1, 0)) num_ignorados,

                /* ---------- Cálculo Process Index   --------------*/

                SUM(e.suma_valoracion) as process_index,
                SUM(e.num_preguntas_valoracion) as preguntas_valoracion,


                /* ---------- Cálculo Esfuerzo Index   --------------*/
                SUM(IF(e.p1_pregunta_id IN ($subSelectIdPreguntasEsuerzo)
                    , e.p1, 0)) as sum_esfuerzo_p1,

                SUM(IF(e.p1_pregunta_id IN ($subSelectIdPreguntasEsuerzo)
                    , 1, 0)) as num_preguntas_esfuerzo_p1,


                SUM(IF(e.p2_pregunta_id IN ($subSelectIdPreguntasEsuerzo)
                    , e.p2, 0)) as sum_esfuerzo_p2,

                SUM(IF(e.p2_pregunta_id IN ($subSelectIdPreguntasEsuerzo)
                    , 1, 0)) as num_preguntas_esfuerzo_p2,


                SUM(IF(e.p3_pregunta_id IN ($subSelectIdPreguntasEsuerzo)
                    , e.p3, 0)) as sum_esfuerzo_p3,

                SUM(IF(e.p3_pregunta_id IN ($subSelectIdPreguntasEsuerzo)
                    , 1, 0)) as num_preguntas_esfuerzo_p3

                FROM nosql_encuesta_procesos e
                /* Para evitar conflicto con la construcción del resto de la
                query debido a los WHERE dento de subselectIdPreguntasEsfuerzo*/
                WHERE 1 = 1
        ";

        $sql = $this->dql->filtradoFecha(
            $this->filtros["temporalidad"],
            $sql, $this->filtros["fec_ini"], $this->filtros["fec_fin"],
            true, "e"
        );

        $filtrosEncuestas = [];

        foreach ($this->filtros as $nombreFiltro => $valorFiltro) {
            if(strpos($nombreFiltro, "-filtro") > 0){
                $nombreFiltroValido = str_replace("-filtro", "_id", $nombreFiltro);
                $filtrosEncuestas[$nombreFiltroValido] = $valorFiltro;
            }
        }

        foreach ($filtrosEncuestas as $nombreFiltro => $valor) {
            $sql = $this->dql->sqlAddFiltro(
                "e", $nombreFiltro, $valor, $sql
            );
        }

        $sql = $sql . " GROUP BY " . implode(" ,", array_map(function($col){
            return "e." . $col;
        }, $groupBy));


        $rCalculosGenerales = $conn->executeQuery($sql)->fetchAll();

        return $rCalculosGenerales;
    }

    private function getDatosSinGroupByProcess($groupBy)
    {
        $conn = $this->em->getConnection();

        $sql = "
            SELECT e.neg_id as neg_id,
                e.can_id as can_id,
                count(DISTINCT(e.encuesta_id)) numEncuestas,
                count(*) numRegistros,
                SUM(e.g2) as SUM_Recomendacion,
                SUM(e.g3) as SUM_SAT,
                SUM(IF(e.g2 >= 9, 1, 0)) - SUM(IF(e.g2 <= 6, 1, 0)) as SUM_NPS,

                /* ------ Cálculos para Emoción y Emotional Index  ---------*/
                SUM(e.g1_valor) as emotional_index,
                SUM(IF(e.g1 = 'Confiad@', 1, 0)) num_confiados,
                SUM(IF(e.g1 = 'Cuidad@', 1, 0)) num_cuidados,
                SUM(IF(e.g1 = 'Decepcionad@', 1,0)) num_decepcionados,
                SUM(IF(e.g1 = 'Enfadad@', 1, 0)) num_enfadados,
                SUM(IF(e.g1 = 'Gratamente sorprendid@', 1,0)) num_g_sorprendidos,
                SUM(IF(e.g1 = 'Ignorad@', 1, 0)) num_ignorados

                FROM nosql_encuestas e
                WHERE 1 = 1
            ";

        /* ------------ Datos relacionados con procesos --------------*/
        $subSelectIdPreguntasEsuerzo = "
          SELECT ep.id
            FROM survey_encuesta_pregunta ep
            WHERE ep.descripcion LIKE 'Esfuerzo'
        ";

        $sql2 = "
            SELECT
            e.proceso_id as proceso_id,

            /* ---------- Cálculo Process Index   --------------*/
            SUM(e.suma_valoracion) as process_index,
            SUM(e.num_preguntas_valoracion) as preguntas_valoracion,

            /* ---------- Cálculo Esfuerzo Index   --------------*/
            SUM(IF(e.p1_pregunta_id IN ($subSelectIdPreguntasEsuerzo)
                , e.p1, 0)) as sum_esfuerzo_p1,

            SUM(IF(e.p1_pregunta_id IN ($subSelectIdPreguntasEsuerzo)
                , 1, 0)) as num_preguntas_esfuerzo_p1,

            SUM(IF(e.p2_pregunta_id IN ($subSelectIdPreguntasEsuerzo)
                , e.p2, 0)) as sum_esfuerzo_p2,

            SUM(IF(e.p2_pregunta_id IN ($subSelectIdPreguntasEsuerzo)
                , 1, 0)) as num_preguntas_esfuerzo_p2,


            SUM(IF(e.p3_pregunta_id IN ($subSelectIdPreguntasEsuerzo)
                , e.p3, 0)) as sum_esfuerzo_p3,

            SUM(IF(e.p3_pregunta_id IN ($subSelectIdPreguntasEsuerzo)
                , 1, 0)) as num_preguntas_esfuerzo_p3

            FROM nosql_encuesta_procesos e
            WHERE 1 = 1
        ";


        foreach ($this->filtros as $nombreFiltro => $valorFiltro) {
            if(strpos($nombreFiltro, "-filtro") > 0){
                $nombreFiltroValido = str_replace("-filtro", "_id", $nombreFiltro);

                $sql = $this->dql->sqlAddFiltro(
                    "e", $nombreFiltroValido, $valorFiltro, $sql
                );

                $sql2 = $this->dql->sqlAddFiltro(
                    "e", $nombreFiltroValido, $valorFiltro, $sql2
                );
            }
        }

        $sql = $this->dql->filtradoFecha(
            $this->filtros["temporalidad"],
            $sql, $this->filtros["fec_ini"], $this->filtros["fec_fin"],
            true, "e"
        );

        $sql2 = $this->dql->filtradoFecha(
            $this->filtros["temporalidad"],
            $sql2, $this->filtros["fec_ini"], $this->filtros["fec_fin"],
            true, "e"
        );

        $sql = $sql . " GROUP BY " . implode(" ,", array_map(function($col){
            return "e." . $col;
        }, $groupBy));

        $sql2 = $sql2 . " GROUP BY " . implode(" ,", array_map(function($col){
            return "e." . $col;
        }, $groupBy));


        $rCalculosGenerales = $conn->executeQuery($sql)->fetchAll();
        $rCalculosRelacionadosProcesos = $conn->executeQuery($sql2)->fetchAll();

        // Fusión de los arrays
        $rCalculosFinales =
            array_map(function($generales, $procesos){
                return array_merge($generales, $procesos);
            }, $rCalculosGenerales, $rCalculosRelacionadosProcesos);

        return $rCalculosFinales;
    }

    private function obtenerDatosGlobales($groupBy)
    {
        $incluyeProceso = in_array("proceso_id", $groupBy);

        $resultadosDatos;

        if($incluyeProceso){
            $resultadosDatos = $this->getDatosConGroupByProcess($groupBy);
        }else{
            $resultadosDatos = $this->getDatosSinGroupByProcess($groupBy);
        }

        return $resultadosDatos;
    }


    private function generarTreeview($groupBy, $equivalencias)
    {
        $rowsResultado = [];

        /* ------------------- PRIMER NIVEL -------------------*/
        $rowsPrimerNivel = $this->obtenerDatosGlobales([$groupBy[0]]);

        foreach ($rowsPrimerNivel as $row) {
            $campoPrimerNivel = $groupBy[0];
            $primerNivel = $row[$campoPrimerNivel];

            $nuevosCampos = [
                "nodoTreeview"      => $primerNivel,
                "nodoTreeviewPadre" => "",
                "texto"             => $equivalencias[$groupBy[0]][$primerNivel],
            ];

            $rowsResultado[$primerNivel] = array_merge($row, $nuevosCampos);
        }

        $groupBySegundoNivel  = array_slice($groupBy, 0, 2);
        $rowsSegundoNivel = $this->obtenerDatosGlobales($groupBySegundoNivel);

        /* ------------------- SEGUNDO NIVEL -------------------*/
        foreach ($rowsSegundoNivel as $row) {
            $campoSegundoNivel = $groupBy[1];
            $primerNivel = $row[$groupBy[0]];
            $segundoNivel = $row[$campoSegundoNivel];

            $nodoTreeview = $primerNivel . "-" . $segundoNivel;

            $nuevosCampos = [
                "nodoTreeview"      => $nodoTreeview,
                "nodoTreeviewPadre" => $primerNivel,
                "texto"             => $equivalencias[$groupBy[1]][$segundoNivel],
            ];

            $rowsResultado[$nodoTreeview] = array_merge($row, $nuevosCampos);
        }

        /* ------------------- TERCERO NIVEL -------------------*/
        $rowsTercerNivel = $this->obtenerDatosGlobales($groupBy);

        foreach ($rowsTercerNivel as $row) {
            $primerNivel = $row[$groupBy[0]];
            $segundoNivel = $row[$groupBy[1]];
            $tercerNivel = $row[$groupBy[2]];

            $nodoTreeviewPadre = $primerNivel . "-" . $segundoNivel;
            $nodoTreeview = $primerNivel . "-" . $segundoNivel . "-" . $tercerNivel;

            $nuevosCampos = [
                "nodoTreeview"      => $nodoTreeview,
                "nodoTreeviewPadre" => $nodoTreeviewPadre,
                "texto"             => $equivalencias[$groupBy[2]][$tercerNivel],
            ];

            $rowsResultado[$nodoTreeview] = array_merge($row, $nuevosCampos);
        }


        $procesadoFinal = $this->procesadoFinalFilas($rowsResultado);

        return $procesadoFinal;
    }

    private function procesadoFinalFilas($rows)
    {

        $rows = array_map(function($row){
            $row["emotionalIndex"] = $this->estructuraEmotionalIndex(
                    floatval(round($row["emotional_index"]/$row["numRegistros"],2)));

            $row["emotionalIndex"]["valor"] = CemUtils::formateoNumero(
                $row["emotionalIndex"]["valor"]);

            $row["experiencia"] = CemUtils::formateoNumero(
                $row["SUM_SAT"]/$row["numRegistros"]
            );

            $row["nps"] = CemUtils::formateoNumero(
                ($row["SUM_NPS"]/ $row["numRegistros"])*100
            );

            $row["indiceRecomendacion"] =
                CemUtils::formateoNumero($row["SUM_Recomendacion"]/ $row["numRegistros"]);

            $row["nodoTreeview"] = "treegrid-". $row["nodoTreeview"];
            $row["nodoTreeviewPadre"] = !empty($row["nodoTreeviewPadre"])
                                        ? "treegrid-parent-". $row["nodoTreeviewPadre"]
                                        : "";

            $row["emocionPrincipal"] = $this->getEmocionPrincipalString($row);

            $row["processIndex"] =
                CemUtils::formateoNumero($row["process_index"]/$row["preguntas_valoracion"]);

            $numPreguntasEsfuerzo =
                ($row["num_preguntas_esfuerzo_p1"]
                    + $row["num_preguntas_esfuerzo_p2"]
                    + $row["num_preguntas_esfuerzo_p3"]);

            $sumaPreguntasEsfuerzo =
                ($row["sum_esfuerzo_p1"]
                    + $row["sum_esfuerzo_p2"]
                    + $row["sum_esfuerzo_p3"]);

            $row["esfuerzoIndex"] = $numPreguntasEsfuerzo == 0
                ? "Sin datos"
                : CemUtils::formateoNumero($sumaPreguntasEsfuerzo/$numPreguntasEsfuerzo);

            return $row;

        }, $rows);

        ksort($rows, SORT_STRING);

        return $rows;
    }

    private function getEmocionPrincipalString($row)
    {
        $maxCount = 0;
        $adj = "";

        $campos = EI::getCamposAdjetivos();

        $adjetivos = EI::getAdjetivos();

        foreach ($campos as $index => $campoParaComprobar) {
            if($row[$campoParaComprobar] > $maxCount){
                $maxCount = $row[$campoParaComprobar];
                $adj = $adjetivos[$index];
            }
        }

        $porcentaje = CemUtils::formateoNumero(
            $maxCount / $row["numRegistros"] * 100);

        $string = $adj . " " . $porcentaje;


        return $string;
    }

    private function estructuraEmotionalIndex($valor)
    {
        if(gettype($valor) !== 'double'){
            throw new \Exception("$valor ha de ser 'double'", 1);
        }

        if($valor >= 7.0){
            $color = "positivo";
            $icono = "fa fa-smile-o";
        }elseif($valor > 5.0){
            $color = "neutral";
            $icono = "fa fa-meh-o";
        }else{
            $icono = "fa fa-frown-o";
            $color = "negativo";
        }

        $result = [
            "valor" => $valor,
            "icono" => $icono,
            "representacion" => $color
        ];

        return $result;
    }


    public function filtrarDatos($datos)
    {

        $nodoPrincipal = $this->fs->getFiltros()["fPrimeraVista"];
        $nodoSecundario = $this->fs->getFiltros()["fSegundaVista"];
        $nodoTerciario = $this->fs->getFiltros()["fTerceraVista"];

        $nodoAColumnaEncuestas = [
            "1" => "neg_id",
            "2" => "can_id",
            "3" => "proceso_id"
        ];

        $equivalencias = [
            "neg_id" => $this->fs->getValoresEnBDFiltro("neg"),
            "can_id" => $this->fs->getValoresEnBDFiltro("can"),
            "proceso_id" => $this->fs->getValoresEnBDFiltro("proceso"),
            "vistas" => $this->fs->getValoresEnBDFiltro("vista")
        ];

        $jerarquiaNodos = [
            $nodoAColumnaEncuestas[$nodoPrincipal],
            $nodoAColumnaEncuestas[$nodoSecundario],
            $nodoAColumnaEncuestas[$nodoTerciario],
        ];

        $treeview = $this->generarTreeview(
            $jerarquiaNodos, $equivalencias
        );

        $this->datos["nodosSeleccionados"] = [
            $equivalencias["vistas"][$nodoPrincipal],
            $equivalencias["vistas"][$nodoSecundario],
            $equivalencias["vistas"][$nodoTerciario],
        ];

        $this->datos["estructura"] = $treeview;

        $this->datos["estructuraPlana"] = $treeview;

        $this->logSQL->end(); // Performance

        // Añade los datos (la maqueta)
        $this->datos = array_merge($this->datos,$datos);
        return $this;
    }

    public function getDatos()
    {
        return $this->datos;
    }

}

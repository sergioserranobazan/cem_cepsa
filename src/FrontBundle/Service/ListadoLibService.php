<?php
namespace FrontBundle\Service;
use \PDO;
/**
 * Librer�a php para manejar datables
 */
class ListadoLibService {

    // datos conexion ddbb
    var $ddbb_user;
    var $ddbb_pass;
    var $ddbb_db;
    var $ddbb_host;
    // datos bbdd
    var $sql_from;
    var $sql_join;
    var $sql_where;
    var $sql_group;
    // datos datatable
    var $campos;
    var $primary_key;

    function __construct() {
    }

    public function unsetValues() {
        unset($this->ddbb_user);
        unset($this->ddbb_pass);
        unset($this->ddbb_db);
        unset($this->ddbb_host);
        unset($this->sql_from);
        unset($this->sql_join);
        unset($this->sql_where);
        unset($this->sql_group);
        unset($this->campos);
        unset($this->primary_key);
    }

    /**
     * Parametros baes de datos
     * @param type $user
     * @param type $pass
     * @param type $ddbb
     * @param type $host
     */
    public function setDDBBConnections($user, $pass, $ddbb, $host) {
        $this->ddbb_user = $user;
        $this->ddbb_pass = $pass;
        $this->ddbb_db = $ddbb;
        $this->ddbb_host = $host;
    }

    /**
     *
     * @param type $sql_from
     */
    public function setFrom($sql_from) {
        $this->sql_from = $sql_from;
    }

    /**
     *
     * @param type $sql_join
     */
    public function setJoin($sql_join) {
        $this->sql_join = $sql_join;
    }

    /**
     *
     * @param type $sql_where
     */
    public function setWhere($sql_where) {
        $this->sql_where = $sql_where;
    }

    /**
     *
     * @param type $sql_group
     */
    public function setGroup($sql_group) {
        $this->sql_group = $sql_group;
    }

    /**
     *
     * @param type $campos
     */
    public function setCampos($campos) {
        $this->campos = $campos;
    }

    /**
     *
     * @param type $primary_key
     */
    public function setPrimaryKey($primary_key) {
        $this->primary_key = $primary_key;
    }

    /**
     *
     */
    public function getData() {
        $columns = $this->campos;
        /* Array of database columns which should be read and sent back to DataTables. Use a space where
         * you want to insert a non-database field (for example a counter or static image)
         */
//        $cont = 0;
//        foreach ($this->campos as $k => $v) {
//            $columns[$cont]['db'] = $v;
//            $columns[$cont]['dt'] = $k;
//            $cont++;
//        }
//        var_dump($columns);
//        exit;
        // SQL server connection information
        $sql_details = array(
            'user' => $this->ddbb_user,
            'pass' => $this->ddbb_pass,
            'db' => $this->ddbb_db,
            'host' => $this->ddbb_host
        );

        /*         * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
        $resp = $this->simple($_POST, $sql_details, $this->sql_from, $this->primary_key, $columns, $this->sql_join, $this->sql_where, $this->sql_group);
        return $resp;
    }

    /**
     * Create the data output array for the DataTables rows
     *
     *  @param  array $columns Column information array
     *  @param  array $data    Data from the SQL get
     *  @return array          Formatted data in a row based format
     */
    static function data_output($columns, $data) {
        $out = array();

        for ($i = 0, $ien = count($data); $i < $ien; $i++) {
            $row = array();
            for ($j = 0, $jen = count($columns); $j < $jen; $j++) {
                $column = $columns[$j];
                // Is there a formatter?
                if (isset($column['formatter'])) {
                    $row[$column['filtro_buscador']] = $column['formatter']($data[$i][$column['filtro_buscador']], $data[$i]);
                } else {
                    $row[$column['filtro_buscador']] = $data[$i][$columns[$j]['filtro_buscador']];
                }
            }
            $out[] = $row;
        }
        return $out;
    }

    /**
     * Paging
     *
     * Construct the LIMIT clause for server-side processing SQL query
     *
     *  @param  array $request Data sent to server by DataTables
     *  @param  array $columns Column information array
     *  @return string SQL limit clause
     */
    static function limit($request, $columns) {
        $limit = '';
        if (isset($request['start']) && $request['length'] != -1) {
            $limit = "LIMIT " . intval($request['start']) . ", " . intval($request['length']);
        }
        return $limit;
    }

    /**
     * Ordering
     *
     * Construct the ORDER BY clause for server-side processing SQL query
     *
     *  @param  array $request Data sent to server by DataTables
     *  @param  array $columns Column information array
     *  @return string SQL order by clause
     */
    static function order($request, $columns) {
        $order = '';
        if (isset($request['order']) && count($request['order'])) {
            $orderBy = array();
            //$dtColumns = self::pluck($columns, 'dt');
            $dtColumns = self::pluck($columns, 'filtro_buscador');
            //$dtColumns = self::pluck($columns, 'db');
            for ($i = 0, $ien = count($request['order']); $i < $ien; $i++) {
                // Convert the column index into the column data property
                $columnIdx = intval($request['order'][$i]['column']);
                $requestColumn = $request['columns'][$columnIdx];
                $columnIdx = array_search($requestColumn['data'], $dtColumns);
                $column = $columns[$columnIdx];

                if ($requestColumn['orderable'] == 'true') {
                    $dir = $request['order'][$i]['dir'] === 'asc' ? 'ASC ' : 'DESC ';
                    $orderBy[] = '' . $column['filtro_buscador'] . ' ' . $dir;
                    //$orderBy[] = '`' . $column['filtro_buscador'] . '` ' . $dir;
                }
            }
            $order = 'ORDER BY ' . implode(', ', $orderBy);
        }
        return $order;
    }

    /**
     * Searching / Filtering
     *
     * Construct the WHERE clause for server-side processing SQL query.
     *
     * NOTE this does not match the built-in DataTables filtering which does it
     * word by word on any field. It's possible to do here performance on large
     * databases would be very poor
     *
     *  @param  array $request Data sent to server by DataTables
     *  @param  array $columns Column information array
     *  @param  array $bindings Array of values for PDO bindings, used in the
     *    sql_exec() function
     *  @return string SQL where clause
     */
    static function filter($request, $columns, &$bindings) {
        $globalSearch = array();
        $columnSearch = array();
        //$dtColumns = self::pluck($columns, 'dt');
        $dtColumns = self::pluck($columns, 'filtro_buscador');
        //$dtColumns = self::pluck($columns, 'db');

        if (isset($request['search']) && $request['search']['value'] != '') {
            $str = $request['search']['value'];
            $str = utf8_decode($str);
            for ($i = 0, $ien = count($request['columns']); $i < $ien; $i++) {
                $requestColumn = $request['columns'][$i];
                $columnIdx = array_search($requestColumn['data'], $dtColumns);
                $column = $columns[$columnIdx];
                if ($requestColumn['searchable'] == 'true') {
                    $binding = self::bind($bindings, '%' . $str . '%', PDO::PARAM_STR);
                    $globalSearch[] = "" . $column['dt'] . " LIKE " . $binding;
                }
            }
        }

        // Individual column filtering
        for ($i = 0, $ien = count($request['columns']); $i < $ien; $i++) {
            $requestColumn = $request['columns'][$i];
            $columnIdx = array_search($requestColumn['data'], $dtColumns);
            $column = $columns[$columnIdx];
            $str = $requestColumn['search']['value'];
            $str = utf8_decode($str);
            if ($requestColumn['searchable'] == 'true' &&
                    $str != '') {
                $binding = self::bind($bindings, '%' . $str . '%', PDO::PARAM_STR);
                $columnSearch[] = "" . $column['dt'] . " LIKE " . $binding;
            }
        }

        // Combine the filters into a single string
        $where = '';

        if (count($globalSearch)) {
            $where = '(' . implode(' OR ', $globalSearch) . ')';
        }

        if (count($columnSearch)) {
            $where = $where === '' ?
                    implode(' AND ', $columnSearch) :
                    $where . ' AND ' . implode(' AND ', $columnSearch);
        }

        if ($where !== '') {
            $where = 'WHERE ' . $where;
        }

        return $where;
    }

    /**
     * Perform the SQL queries needed for an server-side processing requested,
     * utilising the helper functions of this class, limit(), order() and
     * filter() among others. The returned array is ready to be encoded as JSON
     * in response to an SSP request, or can be modified if needed before
     * sending back to the client.
     *
     *  @param  array $request Data sent to server by DataTables
     *  @param  array $sql_details SQL connection details - see sql_connect()
     *  @param  string $table SQL table to query
     *  @param  string $primaryKey Primary key of the table
     *  @param  array $columns Column information array
     *  @return array          Server-side processing response array
     */
    static function simple($request, $sql_details, $table, $primaryKey, $columns, $inner_join = '', $sql_where = '', $sql_group = '') {
        $bindings = array();
        $db = self::sql_connect($sql_details);
        // Build the SQL query string from the request
        $limit = self::limit($request, $columns);
        $order = self::order($request, $columns);
        $where = self::filter($request, $columns, $bindings);

//        var_dump($bindings);
//        exit;

        if ($where == "" && strpos($sql_where, "AND") > 0) {
            $sql_where = "WHERE 1=1 " . $sql_where;
        }

        // Main query to actually get the data
        $sql = "SELECT SQL_CALC_FOUND_ROWS " . implode(", ", self::pluck($columns, 'db')) . "
			 FROM $table
                         $inner_join
			 $where
                         $sql_where
                         $sql_group
			 $order
			 $limit;";

        $data = self::sql_exec($db, $bindings, $sql);

        // Data set length after filtering
        $resFilterLength = self::sql_exec($db, "SELECT FOUND_ROWS();");
        $recordsFiltered = $resFilterLength[0][0];

        // Total data set length
        if($where != ""){
            $sql_where = "WHERE 1=1 " . $sql_where;
        }

        $sql = "SELECT " . $primaryKey . "
			 FROM  $table
                         $inner_join
                         $sql_where
                         $sql_group
                        ";
       // var_dump($sql);
       // exit;
        $resTotalLength = self::sql_exec($db, $sql);
        //$recordsTotal = $resTotalLength[0][0];
        $recordsTotal = count($resTotalLength);


        // Conversion de datos a utf8
        /*array_walk_recursive($data, function(&$value, $key) {
            if (is_string($value)) {
                $value = utf8_encode($value);
            }
        });*/

//        $campos = self::pluck($columns, 'dt');
//        $datos = null;
//        $cont = 0;
//        foreach ($data as $key => $value) {
//            var_dump($value);
//            foreach ($columns as $campo) {
//                $datos[$cont][$campo['dt']] = $value[$campo['dt']];
//            }
//            $cont++;
//        }
//        exit;
        /*
         * Output
         */
        return array(
            "draw" => intval($request['draw']),
            "recordsTotal" => intval($recordsTotal),
            "recordsFiltered" => intval($recordsFiltered),
            "data" => self::data_output($columns, $data)
                //"data" => $data
        );
    }

    /**
     * Connect to the database
     *
     * @param  array $sql_details SQL server connection details array, with the
     *   properties:
     *     * host - host name
     *     * db   - database name
     *     * user - user name
     *     * pass - user password
     * @return resource Database connection handle
     */
    static function sql_connect($sql_details) {
        try {
            $db = @new PDO(
                    "mysql:host={$sql_details['host']};dbname={$sql_details['db']}", $sql_details['user'], $sql_details['pass'], array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'")
            );
        } catch (PDOException $e) {
            self::fatal(
                    "An error occurred while connecting to the database. " .
                    "The error reported by the server was: " . $e->getMessage()
            );
        }

        return $db;
    }

    /**
     * Execute an SQL query on the database
     *
     * @param  resource $db  Database handler
     * @param  array    $bindings Array of PDO binding values from bind() to be
     *   used for safely escaping strings. Note that this can be given as the
     *   SQL query string if no bindings are required.
     * @param  string   $sql SQL query to execute.
     * @return array         Result from the query (all rows)
     */
    static function sql_exec($db, $bindings, $sql = null) {
        // Argument shifting
        if ($sql === null) {
            $sql = $bindings;
        }


        $stmt = $db->prepare($sql);
        // Bind parameters
        if (is_array($bindings)) {
            for ($i = 0, $ien = count($bindings); $i < $ien; $i++) {
                $binding = $bindings[$i];
                $stmt->bindValue($binding['key'], $binding['val'], $binding['type']);
            }
        }


        // Execute
        try {
            //print_r($sql);
            $stmt->execute();
        } catch (PDOException $e) {
            self::fatal("An SQL error occurred: " . $e->getMessage());
        }

        // Return all
        $results= $stmt->fetchAll();
        return $results;
    }

    /*     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Internal methods
     */

    /**
     * Throw a fatal error.
     *
     * This writes out an error message in a JSON string which DataTables will
     * see and show to the user in the browser.
     *
     * @param  string $msg Message to send to the client
     */
    static function fatal($msg) {
        echo json_encode(array(
            "error" => $msg
        ));

        exit(0);
    }

    /**
     * Create a PDO binding key which can be used for escaping variables safely
     * when executing a query with sql_exec()
     *
     * @param  array &$a    Array of bindings
     * @param  *      $val  Value to bind
     * @param  int    $type PDO field type
     * @return string       Bound key to be used in the SQL where this parameter
     *   would be used.
     */
    static function bind(&$a, $val, $type) {
        $key = ':binding_' . count($a);
        $a[] = array(
            'key' => $key,
            'val' => $val,
            'type' => $type
        );
        return $key;
    }

    /**
     * Pull a particular property from each assoc. array in a numeric array,
     * returning and array of the property values from each item.
     *
     *  @param  array  $a    Array to get data from
     *  @param  string $prop Property to read
     *  @return array        Array of property values
     */
    static function pluck($a, $prop) {
        $out = array();
        for ($i = 0, $len = count($a); $i < $len; $i++) {
            $out[] = $a[$i][$prop];
        }
        return $out;
    }

}

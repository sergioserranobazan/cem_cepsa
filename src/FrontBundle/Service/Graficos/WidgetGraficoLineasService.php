<?php

namespace FrontBundle\Service\Graficos;

use FrontBundle\Service\Graficos\ServiceGraficosBase;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use AdminBundle\Service\FiltrosSesionService as FiltrosSesion;
use AdminBundle\Service\DQLBuilderService as DQLBuilder;

use FrontBundle\Library\TipoCEM\REC;
use FrontBundle\Library\TipoCEM\SAT;
use FrontBundle\Library\TipoCEM\NPS;
use FrontBundle\Library\TipoCEM\EI;
use FrontBundle\Library\TipoCEM\VAL;

use AdminBundle\Library\FechasUtils;
use AdminBundle\Library\CemUtils;

use Mrcem\PerformanceLogger\PerformanceIssuer;
/**
*
*/
class WidgetGraficoLineasService extends ServiceGraficosBase
{

    public $tipo; // El tipo de indice a medir (SAT/NPS/EI/VAL)
    private $cruceTemporal; // true, para cruzar por temporalidad    
    private $listadoSubCruces;

    function __construct(   EntityManager $em, 
                            Container $c, 
                            FiltrosSesion $fs,
                            DQLBuilder $dql)
    {
        // Services inyectados
        parent::__construct($em,$c,$fs,$dql);
        
        // En principio para este grafico siempre va a hacerse una evolucion temporal
        // En caso de querer un grafico de lineas para una evolucion de motivos u otras cosas, habra
        // que realizar algunas modificaciones (cruce, groupby por lo que se quiera agrupar)
        $this->cruceTemporal = true;        

        $cv = $this->initConfiguracionVisualizacion(false); // Paso siempre cruceTemporal false, aunque cruceTemporal sea true
        $this->cruce = $cv["cruce"]; //Cruce: Negocio/Proceso/Canal o bien temporalidad
        $this->cruceVista = $cv["cruceVista"];
        $this->idOpcionVista = $cv["idOpcionVista"];//Vacio cuando no exista la opcion-vista
        $this->idSubBloque = $cv["idSubBloque"];
        $this->idIndicador = $cv["idIndicador"];

        $this->datosPregunta = $this->getDatosPregunta($this->idIndicador);
        $this->tipo = $this->datosPregunta->getCemTipo()->getNombre(); //tipo: SAT/NPS/EI/VAL

        //Periodos de tiempo necesarios para el grafico (para mostrar la evolucion)
        $numPeriodos = $this->getNumPeriodos($this->stringTemporalidad);
        //Nota: Si es rango no le afecta el numero de periodos, solo el groupBy
        $this->rangoFechas = $this->getRangoFechas($numPeriodos);
        //Devuelve true si hay que usar tabla nosql de procesos
        $this->tablaProcesos = $this->checkTablaNoSQL($cv);

    }

    //Obtiene los periodos para el grafico, en el caso del rango
    //Setea la nueva temporalidad asociada.
    private function getPeriodicidadPorRango(){
        $periodicidad = $this->stringTemporalidad;
        if($this->stringTemporalidad == "rango"){
            $dias = $this->getDiferenciaDias();
            $periodicidad = $this->getPeriodicidadPorDias($dias);
        }
        return $periodicidad;
    }

    private function getDiferenciaDias(){
        $diff = (strtotime($this->filtros['fec_fin'])-strtotime($this->filtros['fec_ini']))/86400;
        //Dias reales incluidos
        $numeroDias = $diff + 1;
        return $numeroDias;
    }

    private function getPeriodicidadPorDias($dias){

        if($dias<= 7){
            $temporalidad = "dia";
        }elseif($dias <= 31){
            $temporalidad = "semana";
        }elseif($dias <= 93){
            $temporalidad = "mes";
        }elseif($dias <= 366){
            $temporalidad = "trimestre";
        }else{
            $temporalidad = "año";
        }
        return $temporalidad;
    }

    /*protected function setStringTemporalidad($nuevaTemporalidad){
        $this->stringTemporalidad = $nuevaTemporalidad; 
    }*/

    public function filtrarDatos($datos)
    {       
        //Generacion de la sql
        $sql=$this->getSQL();
//echo "<br>".$sql."<br>";exit;
    // Inicialización del logger de performance          
    $logSQL = new PerformanceIssuer("graficos_lineas", PerformanceIssuer::FILE_LOGGER);
    $logSQL->start();         
        $resultado = $this->ejecutarSQL($this->conexion,$sql);
    $logSQL->breakpoint("ejecucion_consulta_sql, indicador: ".$this->idIndicador);    
        //Obtengo los datos del grafico.
        if($this->cruceTemporal){
            //Array de los datos con el periodo como key.
            $resultadoPorPeriodo=$this->getArrayPorPeriodos($resultado);           
            $datosGrafico=$this->getEstructuraGrafico($resultadoPorPeriodo);
        }else{
            //Array de los datos con el cruce como key.
            $resultadoPorCruce=$this->getArrayPorCruce($resultado);
            ksort($resultadoPorCruce);
            $datosGrafico=$this->getEstructuraGrafico($resultadoPorCruce);
        }

        // Array que contiene los datos necesarios para mostrar los Graficos
        $this->datos["datosGrafico"] = $this->getParametrosGrafico($datosGrafico);
    $logSQL->end(); 
        return $this;
    }

    protected function getSQLGroupBy($cruce){
        $groupBy=" GROUP BY ";

        if($this->cruceTemporal){
            $periodicidad = $this->getPeriodicidadPorRango();
            $groupByTemporal = $this->getSQLCruce($periodicidad)["groupby"];
            $groupBy .=  $groupByTemporal.", ".$this->getSQLCruce($cruce)["groupby"];
        }else{
            $groupBy .= $this->getSQLCruce($cruce)["groupby"];
            // Añadir Agrupacion por subcruces
        }

        return $groupBy;
    }       

/**
* Genera un Array con la estructura del grafico para pasar a la libreria de graficosGoogle
*/
    protected function getEstructuraGrafico($resultado){
        $countDatosGrafico = 0;

        //Todos los posibles cruces (seran las lineas) // Sacar tb las de motivos
        //Si selecciona un subBloque especifico solo saco una leyenda
        $listadoSubCruces = $this->getListadoSubCruces();
        //ordeno el array de la leyenda alfabeticamente, sin tener en cuenta las mayusculas
        //y minusculas, y manteniendo las relaciones del array asociativo
        natcasesort($listadoSubCruces);
        //Array de etiquetas del grafico
        $datosGrafico[$countDatosGrafico]=$this->getlabelsLineChart($listadoSubCruces, $this->cruce, $this->tipo);     
        //Compongo la estructura del grafico(El contador empieza en 1 para cada serie de datos)
        //La serie 0 esta generada para las rows/labels.
        $countDatosGrafico++;
        $titulo="";

        // Condicion para generar el grafico.
        if(count($resultado)==0){
            return array(   "datosGrafico"=>$datosGrafico,
                            "noData"=>true);
        }
        $arrayINFOAdjetivos=$this->getINFOAdjetivos($resultado);

        foreach ($resultado as $nCruce => $subCruces) {
            // 1* Añado el nombre del cruce al grafico
            $datosGrafico[$countDatosGrafico][] = ucfirst($nCruce);

            //Recorro los valores de las leyendas
            $existenDatos=false;
            foreach ($listadoSubCruces as $key => $value) {

                foreach ($subCruces as $nSubCruce => $datos) {

                    //Obtengo el nombre del subcruce para comparar con el listadoDeSubCruces
                    $nombreSubCruce = $this->equivalencias[$this->cruce][$datos[$this->cruce]];
                    //Compruebo si existe dato para esa leyenda                    
                    if($value==$nombreSubCruce){
                        $existenDatos=true;
                        // Tooltip
                        $tooltip = ucfirst($nSubCruce).":\nN.Enc: ". $datos['numEncuestasCruce'];
                        $tooltipAdjetivos="";
                        
                        if($this->tipo=="EI"){
                            $porcentajeAdjetivo = CemUtils::formateoNumero($arrayINFOAdjetivos[$nCruce][$nSubCruce]["cantidad"]/$datos["numEncuestasCruce"]*100)."%";
                            $tooltipAdjetivos ="\n".$arrayINFOAdjetivos[$nCruce][$nSubCruce]["nombre"].": ".$porcentajeAdjetivo."(".$arrayINFOAdjetivos[$nCruce][$nSubCruce]["cantidad"].")";
                        }
                        /*//Tooltip desglosado por porcentajes
                        foreach ($arrayValoresTyped as $typed) {
                            if ($typed['nombre']) {
                                $tooltipAdjetivos .= "" . substr($typed["nombre"], 0, 15) . ": " . $datos[$typed["nombre"]] . " - " . number_format($datos['porc_' . $typed["nombre"]], '2') . "%\n";
                            }
                        }*/

                        // 2* Añado el indice al grafico
                        // El grafico se encarga del formateo del numero.
                        $datosGrafico[$countDatosGrafico][]= $datos['indice'];

                        $tooltipValor = "\nValor: " . CemUtils::formateoNumero($datos['indice']);
                        $tooltip .= $tooltipValor.$tooltipAdjetivos;
                        // 3* Añado el tooltip al grafico
                        $datosGrafico[$countDatosGrafico][] = $tooltip;
                    }

                }//Fin Subcruce

                if($existenDatos){  //Reinicio la bandera.
                    $existenDatos = false;
                }else{  //Si para esa leyenda no se ha encontrado ningun dato anulo indice y tooltip
                    $datosGrafico[$countDatosGrafico][] = null; // 2* indice nulo
                    $datosGrafico[$countDatosGrafico][] = null; // 3* tooltip nulo
                }
            }
            $countDatosGrafico++;
        }//Fin cruce

        return array(   "datosGrafico"=>$datosGrafico,
                        "noData"=>false);
    }

    protected function getINFOAdjetivos($resultado){

        if($this->tipo!="EI") return null;

        $arrayINFOAdjetivos=[];
        foreach ($resultado as $nCruce => $subCruces) {
            foreach ($subCruces as $nSubCruce => $datos) {
                $arrayAdjetivos= [];
                foreach ($datos as $key => $value) {
                    if(strpos($key,'ADJETIVO_')!==FALSE){
                        $arrayAdjetivos[str_replace("ADJETIVO_",'',$key)]=$value;
                    }
                }
                //Guardo los datos en el array a devolver
                //Nombre y Cantidad del adjetivo que mas se repite
                $arrayINFOAdjetivos[$nCruce][$nSubCruce] = $this->doublemax($arrayAdjetivos);
                //Etiqueta del grafico a la que le corresponderia el valor del EmotionalIndex
                $arrayINFOAdjetivos[$nCruce][$nSubCruce]['etiqueta'] = EI::getNombre($datos['indice']);
            }
        }
        return $arrayINFOAdjetivos;
    }

    //Devuelve un array con el periodo como key
    //Aprovecho para cambiar las keys con id por nombre con las equivalencias
    protected function getArrayPorPeriodos($resultado){
        $r=[];
        foreach ($resultado as $key => $value) {
            if(!is_null($value[$this->cruce]) && !is_null($value['indice'])){
                //NOTA: en este grafico uso como key el nombreMostrar
                // mientras en el de barras uso directamente el periodo(anyoConPeriodo).
                if($this->stringTemporalidad){
                    $periodicidad = $this->getPeriodicidadPorRango();
                    $nombreMostrar = $this->getNombreMostrarTemporalidad($periodicidad,$value['fecha_ref']);
                }
                $r[$nombreMostrar][$this->equivalencias[$this->cruce][$value[$this->cruce]]]=$value;
            }        
        }
        return $r;
    }

    private function getArrayPorCruce($resultado){
        $r=[];
        foreach ($resultado as $key => $value) {
            if(!is_null($value[$this->cruce]) && !is_null($value['indice'])){
                $r[$this->equivalencias[$this->cruce][$value[$this->cruce]]][$value['anyoConPeriodo']]=$value;
                //Agrupar por Subcruce
                //$r[$value['cruce']][$value[$this->subcruce]]=$value;
            }
        }
        return $r;
    } 

    // Funcion que devuelve los nombres de todos los subcruces
    // Si selecciona un subbloque especifico para los procesos solo saco una leyenda
    // Este array se utilizara a la hora de generar los labels de los graficos
    private function getListadoSubCruces(){
        $listadoSubCruces = null;
        if($this->idSubBloque>1 && $this->cruce=="proceso"){
            $listadoSubCruces[] = $this->equivalencias[$this->cruce][$this->idSubBloque];
        }else{
            $listadoSubCruces = $this->equivalencias[$this->cruce];
        }
        return $listadoSubCruces;
    }

    /**
     * Funcion que obtiene las rows para el grafico linechart. Tb para generar grafico vacio.
     * @param type $cruces
     * @return string
     */
    private function getlabelsLineChart($cruces, $subCruce, $typed) {

        $labels = [
            ["label" => "Cruce", "type" => "string"]
        ];
        foreach ($cruces as $cruce) {
            $labels[] = ["label" => ucfirst($cruce), "type" => "number"];
            $labels[] = ["type" => "string", "role" => "tooltip"];
        }
        //Caso Grafico Vacio para los typed
        if (count($cruces) == 0 && ($subCruce == $typed)) {
            $labels[] = ["label" => "", "type" => "number"];
            $labels[] = ["type" => "string", "role" => "tooltip"];
        }
        return $labels;
    }

    /**
     * Ristra de colores. Para mostrar todas las posibles leyendas y lineas del grafico de LINEAS
     */
    private function getAllColors($almohadilla = false) {
        if ($almohadilla) {
            $colores = ['#FF9933', '#009900', '#0080FF', '#800080', '#6E6E09', '#E5E500', '#000000', '#99FFFF', '#FF0000', '#00FF00', '#0000FF', '#FFFF00', '#FF00FF', '#00FFFF', '#800000', '#008000', '#FF6600', '#993300', '#99CC00', '#9999FF', '#993366', '#FFFFCC', '#CCFFFF', '#660066', '#FF8080', '#CCCCFF', '#CCFFCC', '#FFCC99'];
        } else {
            $colores = ['FF9933', '009900', '0080FF', '800080', '6E6E09', 'E5E500', '000000', '99FFFF', 'FF0000', '00FF00', '0000FF', 'FFFF00', 'FF00FF', '00FFFF', '800000', '008000', 'FF6600', '993300', '99CC00', '9999FF', '993366', 'FFFFCC', 'CCFFFF', '660066', 'FF8080', 'CCCCFF', 'CCFFCC', 'FFCC99'];
        }
        return $colores;
    }
    /*
        Devuelve un array con todos los datos necesarios para pintar el grafico.
    */
    private function getParametrosGrafico($datosAct,$indices=null){
        $parametros = array(    "datosGrafico"=> $datosAct['datosGrafico'],
                                "titulo" => $this->getTitulo($datosAct['noData']),
                                "colores"=> $this->getAllColors(),
                                "tipoGrafico"=>"LINEAS",                                
                                "tipo"=>$this->tipo);
        return $parametros;
    }

    protected function getTitulo($noData){
        $titulo = "Evolución - ".parent::getTitulo($noData);
        return $titulo;
    }

    public function getDatos()
    {
        return $this->datos;
    }     

}

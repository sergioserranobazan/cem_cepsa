<?php

namespace FrontBundle\Service\Graficos;

use FrontBundle\Service\Graficos\ServiceGraficosBase;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use AdminBundle\Service\FiltrosSesionService as FiltrosSesion;
use AdminBundle\Service\DQLBuilderService as DQLBuilder;

use FrontBundle\Library\TipoCEM\REC;
use FrontBundle\Library\TipoCEM\SAT;
use FrontBundle\Library\TipoCEM\NPS;
use FrontBundle\Library\TipoCEM\EI;
use FrontBundle\Library\TipoCEM\VAL;

use AdminBundle\Library\FechasUtils;
use AdminBundle\Library\CemUtils;

use Mrcem\PerformanceLogger\PerformanceIssuer;
/**
*
*/
class WidgetGraficoBarrasService extends ServiceGraficosBase
{

    public $tipo; // El tipo de indice a medir (SAT/NPS/EI/VAL)
    private $linea; // Si tiene linea el grafico.
    private $stack; // Si esta staqueado o no.
    private $cruceTemporal; // true, para cruzar por temporalidad
    private $periodoActual;
    private $periodoAnterior;


    function __construct(   EntityManager $em, 
                            Container $c, 
                            FiltrosSesion $fs,
                            DQLBuilder $dql)
    {
        // Services inyectados
        parent::__construct($em,$c,$fs,$dql);

        $this->linea = false;
        $this->stack = true;
        $this->cruceTemporal = false; // Preparado para cuando se quiera cruzar por temporalidad.

        $cv = $this->initConfiguracionVisualizacion($this->cruceTemporal);
        $this->cruce = $cv["cruce"]; //Cruce: Negocio/Proceso/Canal o bien temporalidad
        $this->cruceVista = $cv["cruceVista"];
        $this->idOpcionVista = $cv["idOpcionVista"];//Vacio cuando no exista la opcion-vista
        $this->idSubBloque = $cv["idSubBloque"];
        $this->idIndicador = $cv["idIndicador"];

        $this->datosPregunta = $this->getDatosPregunta($this->idIndicador);
        $this->tipo = $this->datosPregunta->getCemTipo()->getNombre(); //tipo: SAT/NPS/EI/VAL

        // 2 es el numero de peridos por defecto para este grafico.
        //Periodos de tiempo necesarios para el grafico (para mostrar la diferencia de indices)
        $numPeriodos = 2;
        $this->setConfiguracionFechas($numPeriodos);
        //Devuelve true si hay que usar tabla nosql de procesos
        $this->tablaProcesos = $this->checkTablaNoSQL($cv);

    }

    public function getEquivalencias(){
        return $this->equivalencias;
    }

    public function setConfiguracionFechas($numPeriodos){

        $this->numPeriodos = $numPeriodos;

        $this->rangoFechas = $this->getRangoFechas($this->numPeriodos);

        //Sacar el primer periodo, que es de donde voy a sacar los datos del grafico
        $this->periodoActual = intval(FechasUtils::getAnyoConPeriodo($this->stringTemporalidad,$this->rangoFechas['fecha_fin']));

        if($this->numPeriodos > 1){
        //Lo puedo hacer así porque solo uso 2 periodos para filtrar:
            $this->periodoAnterior = intval(FechasUtils::getAnyoConPeriodo($this->stringTemporalidad,$this->rangoFechas['fecha_inicio'])); 
        }

        return $this;
    }

    //Redefino Visibilidad del getSQL para usar desde el controlador AJAX
    /*public function getSQL(){
        $sql = parent::getSQL();
        return $sql;
    }
    public function getSQLFrom(){
        $sql = parent::getSQLFrom();
        return $sql;
    }    
    public function getSQLJoin(){
        $sql = parent::getSQLJoin();
        return $sql;
    }
    public function getSQLWhere(){
        $sql = parent::getSQLWhere();
        return $sql;
    }
    public function ejecutarSQL($conexion,$sql){
        $resultado = parent::ejecutarSQL($conexion,$sql);
        return $resultado;
    }
    /*public function getCruce($cruce){
        $sql = parent::getCruce($this->cruce);
        return $sql;
    }*/

    //En datos viene la maqueta
    public function filtrarDatos($datos)
    {       
        //Generacion de la sql
        $sql = $this->getSQL();

    // Inicialización del logger de performance        
    $logSQL = new PerformanceIssuer("graficos_barras", PerformanceIssuer::FILE_LOGGER);
    $logSQL->start();    
        $resultado = $this->ejecutarSQL($this->conexion,$sql);
    $logSQL->breakpoint("ejecucion_consulta_sql, indicador: ".$this->idIndicador);    
        //Array de los datos con el periodo como key.
        $resultadoPorPeriodos=$this->getArrayPorPeriodos($resultado);

        //$resultadoPorCruce=$this->getArrayPorCruce($resultado);    
         
        //Obtengo los datos del grafico para el periodo actual, que son los datos principales a mostrar.
        $datosGrafico=$this->getEstructuraGrafico($resultadoPorPeriodos);
        //Obtengo el array de indices y diferencias si existen.
        $indices = null;
        $indices=$this->getDiferenciaIndices($resultadoPorPeriodos);
    
        //// Array que contiene los datos necesarios para mostrar los Graficos
        $this->datos["datosGrafico"] = $this->getParametrosGrafico($datosGrafico,$indices);

        //DATATABLES
        // Nombres de los stacks para pintar en el titulo del datatable
        $this->datos["datosGrafico"]['arrayLabels'] = call_user_func(array("FrontBundle\Library\TipoCEM\\".$this->tipo,'getNombres'));
        // Cabecera para el datatable del grafico
        $this->datos["datosGraficoDT"]["cabecera"] = $this->getArrayCabeceraTabla();
        //$datosGraficoToDatatable["datosGraficoDT"]=$this->getDatosGraficoToDataTable();
        //$this->datos = array_merge($this->datos,$datos,$datosGraficoToDatatable);

        //Mergeo los resultados con el nombre de la maqueta
        $this->datos = array_merge($this->datos,$datos);
    $logSQL->end(); 
        return $this;
    }
 
    public function getDatos()
    {
        return $this->datos;
    }

    protected function getSQLGroupBy($cruce){
        $groupBy=" GROUP BY ";

        $groupByTemporal = $this->getSQLCruce($this->stringTemporalidad)["groupby"];

        if($this->cruceTemporal){
            $groupBy .= $groupByTemporal;
        }else{
            $groupBy .= $groupByTemporal.", ".$this->getSQLCruce($cruce)["groupby"];
        }

        return $groupBy;
    }
    /**
    * Genera un Array con la estructura del grafico para pasar a la libreria de graficosGoogle
    */
    protected function getEstructuraGrafico($resultado){

        $countDatosGrafico = 0;
        $arrayValoresTyped = call_user_func(array("FrontBundle\Library\TipoCEM\\".$this->tipo,'getArrayValoresTyped'));
        //Array de etiquetas del grafico
        $datosGrafico[$countDatosGrafico]=$this->getLabelsColumChart($arrayValoresTyped,$this->tipo,$this->linea,$this->stack);
        //Compongo la estructura del grafico(El contador empieza en 1 para cada serie de datos)
        $colores=call_user_func(array("FrontBundle\Library\TipoCEM\\".$this->tipo,'getColores'));
        //La serie 0 esta generada para las rows/labels.
        $countDatosGrafico++;

        // Condicion para generar el grafico.
        if(isset($resultado[$this->periodoActual])){
            $resultado=$resultado[$this->periodoActual];
            //Ordeno el array por la key (cruce), alfabeticamente
            ksort($resultado);
        }else{
            return array(   "datosGrafico"=>$datosGrafico,
                            "noData"=>true,
                            "colores"=>$colores);
        }
        //Saco el array de Adjetivos cuando cuando el tipo sea EI
        $arrayINFOAdjetivos=$this->getINFOAdjetivos($resultado);

        foreach ($resultado as $nCruce => $datos) {

        // 1* Nombre del cruce
            if($this->cruceTemporal){
                $datosGrafico[$countDatosGrafico][] = $this->getNombreMostrarTemporalidad($this->stringTemporalidad,$datos['fecha_ref']);
                //$datosGrafico[$countDatosGrafico][] = $datos["nombreMostrar"];
            }else{
                $datosGrafico[$countDatosGrafico][] = ucfirst($nCruce);
            }

            foreach ($arrayValoresTyped as $typed) {
                switch ($this->tipo) {
                    case "EI"://Caso muy particular.
                        // Para la Emocion estaqueo en funcion del indice (Ahora se calcula en la sql)
                        // Adjetivo Maximo(nombre), Cantidad(cantidad) y nombre(etiqueta) de la etiqueta
                        // obtenidos en getINFOAdjetivos()                  
                        if($typed["nombre"]==$arrayINFOAdjetivos[$nCruce]['etiqueta']){
                        // 2* Indice
                            $datosGrafico[$countDatosGrafico][] = $datos["indice"];
                        // 3* Tooltip
                            //$numEncuestasRealesPorEtiqueta = round($datos[$typed["nombre"]]*$datos['PORC_' . $typed["nombre"]]/100);
                            $tooltip = '';
                            if(!$this->cruceTemporal) $tooltip = ucfirst($nCruce).":\n";    
                            $tooltip .="N.Enc: ". $datos["numEncuestasCruce"] ."\nValor: ".CemUtils::formateoNumero($datos["indice"]);                       
                            $porcentajeAdjetivo = CemUtils::formateoNumero($arrayINFOAdjetivos[$nCruce]["cantidad"]/$datos["numEncuestasCruce"]*100)."%";
                            $tooltip .="\n".$arrayINFOAdjetivos[$nCruce]["nombre"].": ".$porcentajeAdjetivo."(".$arrayINFOAdjetivos[$nCruce]["cantidad"].")";
                            $tooltip .="\n".str_replace("\n","",$this->getNombreMostrarTemporalidad($this->stringTemporalidad,$datos['fecha_ref']));
                            $datosGrafico[$countDatosGrafico][] = $tooltip;
                        // 4* Annotations de los stacks.
                            $datosGrafico[$countDatosGrafico][] = CemUtils::formateoNumero($datos["indice"]);
                        }else{
                            //Elimino los otros stacks (los que no son el maximo)
                            $datosGrafico[$countDatosGrafico][] = null;
                            $datosGrafico[$countDatosGrafico][] = null;
                            $datosGrafico[$countDatosGrafico][] = null;
                        }
                        break;
                    default:
                    // 2* Valores porcentajes stacks
                    // NOTA: Al grafico de google hay que pasarle los decimales con punto.
                    // En el caso de stacks (que suman el 100%) hay que pasarle todos los decimales
                    // el se encarga de gestionarlo en sus opciones con dos decimales, sino da problemas
                    // de escalamiento de dichos stacks.
                        $datosGrafico[$countDatosGrafico][] = $datos['PORC_' . $typed["nombre"]];
                    // 3* Tooltip
                        //El conteo de detractores, neutrales, satisfechos,etc no es real
                        //Ya que se multiplican los registros en funcion de los filtros de los procesos
                        //Por lo que hay que sacar ese numero real de encuestas para cada etiqueta.
                        //El porcentaje esta sacado del numero total de registros.
                        //Hago una proporcion con el numero real de encuestas.
                        $numEncuestasRealesPorEtiqueta = round($datos["numEncuestasCruce"]*$datos['PORC_' . $typed["nombre"]]/100);
                        //Se podria usar el nombreMostrar del sql, para mostrar la fecha en vez de llamar a
                        //-<getNombreMostrarTemporalidad
                        $tooltip = '';
                        if(!$this->cruceTemporal) $tooltip = ucfirst($nCruce).":\n";    
                        $tooltip .= "N.Enc: ". $numEncuestasRealesPorEtiqueta;
                        $tooltip .= "\n".ucfirst($typed["nombre"]).": " . CemUtils::formateoNumero($datos['PORC_' . $typed["nombre"]]) . "%";
                        $tooltip .= "\n".str_replace("\n","",$this->getNombreMostrarTemporalidad($this->stringTemporalidad,$datos['fecha_ref']));
                        $datosGrafico[$countDatosGrafico][] = $tooltip;
                    // 3* Annotations de los stacks.
                        if($datos['PORC_' . $typed["nombre"]] == 0){ //Para no mostrar los 0.00%
                            $datosGrafico[$countDatosGrafico][] = null;
                        }else{
                            $datosGrafico[$countDatosGrafico][] = CemUtils::formateoNumero($datos['PORC_' . $typed["nombre"]])."%";
                        }
                        break;
                }
            }
            // Si al grafico se le quisiera pintar una linea con la informacion del valor medio.
            if ($this->linea) {
                // *2 Linea (en funcion del valor medio (indice))
                $datosGrafico[$countDatosGrafico][] = CemUtils::formateoNumero($datos['indice']);
                //Tooltip( dejo el de por defecto)
                //$datosGrafico[$countDatosGrafico][] = $tooltip;
            }

            $countDatosGrafico++;
        }

        return array(   "datosGrafico"=>$datosGrafico,
                        "noData"=>false,
                        "colores"=>$colores);
    }    


    /**
    *   Retorna la estructura (rows) de las etiquetas, tooltips,annotations,... para el grafico,
        en funcion de un array que contiene el nombre de las etiquetas, en funcion del tipo de indice,
        de si tiene o no linea (para mostrar la media del indice) y de si esta estaqueado o no.
    */
    private function getLabelsColumChart($arrayTyped,$tipo=null,$linea,$stack){

        $labels = [
            ["label" => "Cruce", "type" => "string"]
        ];

        if(!$stack){ // Solo para cuando el grafico no este stackeado
            $labels[] = ["label" => "Valor Medio", "type" => "number"];
            $labels[] = ["type" => "string", "role" => "tooltip"];
            $labels[] = ["type" => "string", "role" => "annotation"];
            return $labels;
        }

        foreach ($arrayTyped as $typed) {
            $labels[] = ["label" => $typed["nombre"], "type" => "number"];
            $labels[] = ["label" => $typed["nombre"], "type" => "string", "role" => "tooltip"];
            $labels[] = ["label" => $typed["nombre"], "type" => "string", "role" => "annotation"];
        }
        if (count($arrayTyped) == 0) {
            $labels[] = ["label" => "", "type" => "number"];
            $labels[] = ["label" => "", "type" => "string", "role" => "tooltip"];
            $labels[] = ["label" => "", "type" => "string", "role" => "annotation"];
        }
        //Si el grafico de Barras lleva linea.
        if ($linea) {
            $labels[] = ["label" => $tipo, "type" => "number"];
            //$labels[] = ["label" => $tipo "type" => "string", "role" => "tooltip"];
        }

        return $labels;

    }

    protected function getINFOAdjetivos($resultado){

        if($this->tipo!="EI") return null;

        $arrayINFOAdjetivos=[];
        foreach ($resultado as $nCruce => $datos) {
            $arrayAdjetivos= [];
            foreach ($datos as $key => $value) {
                if(strpos($key,'ADJETIVO_')!==FALSE){
                    $arrayAdjetivos[str_replace("ADJETIVO_",'',$key)]=$value;
                }
            }
            //Guardo los datos en el array a devolver
            $arrayINFOAdjetivos[$nCruce] = $this->doublemax($arrayAdjetivos);
            $arrayINFOAdjetivos[$nCruce]['etiqueta'] = EI::getNombre($datos["indice"]);
        }

        return $arrayINFOAdjetivos;
    }

    private function getDiferenciaIndices($resultadoPorPeriodo){
        // Sacar la diferencia entre el indice actual y el indice del periodo anterior(si existe)
        $indices = [];
        $indicesAct=[];
        $indicesAnt=[];

        //Si no hay datos del periodo actual no saco los del periodo anterior
        if(isset($resultadoPorPeriodo[$this->periodoActual]) && $this->tipo!="EI"){
            $indicesAct=$resultadoPorPeriodo[$this->periodoActual];
            ksort($indicesAct); // Orden de los overlays, alfabeticamente
                                // tiene que estar en corcondancia con el orden de 
                                // de los stacks en getEstructura
        }else{
            return null;
        }
        //Compruebo que existen datos del periodo anterior
        if(isset($resultadoPorPeriodo[$this->periodoAnterior])){
            $indicesAnt=$resultadoPorPeriodo[$this->periodoAnterior];
        }          

        foreach ($indicesAct as $nombreCruce => $datos) {
            $diferencia=null;
            if(!($this->filtros["temporalidad"]==0)){ // Si es distinto de Rango
                if($this->cruceTemporal){//Cruces de temporalidad
                    foreach ($indicesAnt as $pAnterior => $datosAnt) {
                        $diferencia = number_format($datos['indice'] - $datosAnt['indice'], '2');
                    }
                }elseif(isset($indicesAnt[$nombreCruce])){//Cruces Normales
                    $diferencia = number_format($datos['indice'] - $indicesAnt[$nombreCruce]['indice'], '2');
                }
            }
            $indices["actuales"][]=CemUtils::formateoNumero($datos['indice']);
            $indices["diferencias"][]=$diferencia;
        }
        
        return $indices;
    }   

    //Devuelve un array con el periodo como key
    //Aprovecho para cambiar las keys con id por nombre con las equivalencias
    protected function getArrayPorPeriodos($resultado){

        $r=[];
        foreach ($resultado as $key => $value) {
            if(!is_null($value[$this->cruce]) && !is_null($value['indice']) ){
                // $value[$this->cruce] es el cruce
                if($this->cruceTemporal){
                    $r[$value['anyoConPeriodo']][$value[$this->cruce]]=$value;
                }else{
                    $r[$value['anyoConPeriodo']][$this->equivalencias[$this->cruce][$value[$this->cruce]]]=$value;
                }
            }
        }
        return $r;
    }

    //Sin Uso en el de barras
    private function getArrayPorCruce($resultado){
        $r=[];
        foreach ($resultado as $key => $value) {
            if(!is_null($value[$this->cruce])){
                $r[$this->equivalencias[$this->cruce][$value[$this->cruce]]][$value['anyoConPeriodo']]=$value;
                //$r[$value[$this->cruce]][]=$value;
                //$r[$value['cruce']][]=$value;
            }
        }
        return $r;
    }     

    /*
        Devuelve un array con todos los datos necesarios para pintar el grafico.
    */
    public function getParametrosGrafico($datosAct,$indices=null){
        $parametros = array(    "datosGrafico"=> $datosAct['datosGrafico'],
                                "titulo" => $this->getTitulo($datosAct['noData'],$this->idIndicador,$this->tipo),
                                "colores"=> $datosAct['colores'],
                                "tipoGrafico"=>"BARRAS",                                
                                "tipo"=>$this->tipo,
                                "stack"=>$this->stack,
                                "linea"=>$this->linea);                               

        if($indices!=null){
            return array_merge( $parametros,
                                array(  "indices"=>$indices["actuales"],
                                        "diferencias"=>$indices["diferencias"]));
        }
        return $parametros;
    }      

    ######################## PARA EL DATATABLE DEL GRAFICO ################################
    // Function de llamada en Ajax
    // Retorna datos a pasar al Ajax que transforma el grafico en encuestas.
    public function getDatosGraficoToDataTable(){

        $labels = call_user_func(array("FrontBundle\Library\TipoCEM\\".$this->tipo,'getNombres'));
        $rangoLabels = call_user_func(array("FrontBundle\Library\TipoCEM\\".$this->tipo,'getRangoLabels'));
        $cabeceraTabla = $this->getArrayCabeceraTabla();

        return array(   "tipoCEM"=>$this->tipo,
                        "cruceTemporal"=>$this->cruceTemporal,
                        "cruce"=>$this->cruce,
                        "idPregunta"=>$this->idIndicador,
                        "descripcion_completa"=>$this->datosPregunta->getDescripcionCompleta(),
                        "stringLabels"=>implode(',',$labels),
                        "rangoLabels"=>$rangoLabels,
                        "cabecera" =>$cabeceraTabla
                        );
    }

    // Array de las cabeceras del datatable
    private function getArrayCabeceraTabla(){
        return array(
                    "Fecha"      ,
                    "Negocio"    ,
                    "L. neg."    ,
                    "Canal"      ,
                    "Det. Canal" ,
                    "Procesos"   ,
                    "Exp."       ,
                    "Rec."       ,
                    "Sent."      ,
                    "OPI"        ,
                    "Info."     ,
                );
    }

}

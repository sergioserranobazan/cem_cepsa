<?php
namespace FrontBundle\Service\Graficos;

use FrontBundle\Service\ServiceBase;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use AdminBundle\Service\FiltrosSesionService as FiltrosSesion;
use AdminBundle\Service\DQLBuilderService as DQLBuilder;

use FrontBundle\Library\TipoCEM\REC;
use FrontBundle\Library\TipoCEM\SAT;
use FrontBundle\Library\TipoCEM\NPS;
use FrontBundle\Library\TipoCEM\EI;
use FrontBundle\Library\TipoCEM\VAL;

use AdminBundle\Library\FechasUtils;


abstract class ServiceGraficosBase extends ServiceBase {

    protected $equivalencias;
    protected $rango;
    protected $stringTemporalidad;
    protected $rangoFechas;

    function __construct(   EntityManager $em, 
                            Container $c,
                            FiltrosSesion $fs,
                            DQLBuilder $dql
    ){
        parent::__construct($em,$c);

        $this->conexion = $this->em->getConnection();
        $this->fs = $fs;
        $this->dql = $dql;

        $this->filtros = $this->fs->getFiltros();   

        //ALERTA C.G: Cada vez que se añade un filtro nuevo, añadir a este array.
        //Se podria "generalizar" cargando las equivalencias en funcion de los filtros
        $this->setEquivalencias();       
        $this->setStringTemporalidad();
        //$this->filtroProceso = $this->checkFiltroProceso();

    }

#################################### FUNCIONES ABSTRACTAS #############################

    // Devuelve el array con 2 datos asociados a los adjetivos 
    //          etiqueta asociada al ei(Negativo, neutro o positivo),
    //          array que contiene el adjetivo que mas se repite y su nombre)
    // en funcion del array de resultados (resulset)
    abstract protected function getINFOAdjetivos($resultado);

    // Devuelve un array con los periodos (periodoConAnyo) en el primer nivel del array
    abstract protected function getArrayPorPeriodos($resultado);

    // Devuelve el groupBy de la query en funcion del cruce
    abstract protected function getSQLGroupBy($cruce);

    // Retorna la estructura del grafico, como array
    abstract protected function getEstructuraGrafico($resultado);

#################################### VARIABLES DE CONFIGURACION ########################

    protected function setEquivalencias(){
        if(isset($this->equivalencias)) {
            print_r("hola");
            return $this->equivalencias;
        }
        
        $equivalencias = array(
            "vistas" => $this->getNombresDoctrineVistas(),
            // Obtención de array con $id => valor de cada uno de los filtros
            // que han podido seleccionarse
            "temporalidad"  => $this->fs->getValoresEnBDFiltro("temporalidad"),
            "neg"       => $this->fs->getValoresEnBDFiltro("neg"),
            "org"  => $this->fs->getValoresEnBDFiltro("org"),
            "proceso"       => $this->fs->getValoresEnBDFiltro("proceso"),
            "ser1"      => $this->fs->getValoresEnBDFiltro("ser1"),
            "pais"          => $this->fs->getValoresEnBDFiltro("pais"),
            "opi_pregunta"           => $this->fs->getValoresEnBDFiltro("opi_pregunta"),
            "can"         => $this->fs->getValoresEnBDFiltro("can")
        );
        //Añado la key para el rango a la equivalencia de la temporalidad, ya que no esta en la BD.
        $this->rango = "rango";
        $equivalencias["temporalidad"][0]=$this->rango;

        $this->equivalencias = $equivalencias;
    }

    protected function setStringTemporalidad(){
        $this->stringTemporalidad = $this->equivalencias["temporalidad"][$this->filtros["temporalidad"]]; 
    }

    private function getNombresDoctrineVistas(){

        $vistas = $this->em->getRepository("AdminBundle:Vista")->findAll();
        $array = [];
        foreach ($vistas as $value) {
            $array[$value->getId()]=$value->getFiltro()->getNombreFiltro();
        }
        return $array;
    }

    //Obtiene los valores del widget de indicadores.
    protected function initConfiguracionVisualizacion($cruceTemporal = false){

        $r=[];
        $cv=explode("-",$this->filtros["configuracionVisualizacion"]);
        if(count($cv)==3){//DesgloseInformacion
            if($cruceTemporal){
                $r["cruce"]=$this->equivalencias["temporalidad"][$this->filtros['temporalidad']];//Cruces Temporales
            }else{
                $r["cruce"]=$this->equivalencias["vistas"][$cv[0]];//Cruces Filtros
            }
            
            $r["idOpcionVista"] = "";
            $r["idSubBloque"]=$cv[1];           
            $r["cruceVista"] = $r["cruce"]; // El cruce coincide con el cruceVista
        }elseif(count($cv)==4){//Cruce de Variables
            if(isset($this->filtros["cruce"])){
                $r["cruce"]=str_replace("-filtro","",$this->filtros["cruce"]); // Cruces ticados como checkbox
            }else{
                //Caso en el que exista opcion-vista pero no exista cruce (radioButton) en la maqueta
                //De momento no se da el caso
                if($cruceTemporal){
                    $r["cruce"]=$this->equivalencias["temporalidad"][$this->filtros['temporalidad']];//Cruces Temporales
                }else{
                    $r["cruce"]=$this->equivalencias["vistas"][$cv[0]];//Cruces Filtros
                }
            }           
            $r["idOpcionVista"] = $cv[1];
            $r["idSubBloque"]=$cv[2];
            $r["cruceVista"] = $this->equivalencias["vistas"][$cv[0]];
        }
        //datos comunes a las dos configuraciones
        $r["idVista"] = $cv[0];
        $r["idIndicador"]=end($cv);

        return $r;
    } 

    protected function checkTablaNoSQL($cv){
        //Condiciones para usar la tabla nosql_encuesta_procesos
        //teniendo en cuenta las dos configuraciones de visualizacion (desglose y cruce de variables)
        if( $cv['cruce'] == "proceso" || 
            ($this->checkFiltroProceso() && $cv['idSubBloque']!=1) || 
            ($cv['cruceVista'] == "proceso" && !empty($cv['idOpcionVista']) )
        ){
            return true;
        }
        return false;
    }

    protected function checkFiltroProceso(){
        $numProcesos = count($this->filtros["proceso-filtro"]);
        if( $numProcesos > 0 ) return true;

        return false;
    }

    protected function getCampoPregunta($tablaProcesos){
        $campo = '';
        if($tablaProcesos){
            $campo = "p".$this->datosPregunta->getOrdenPreguntaProceso();
        }else{
            $campo = mb_strtolower($this->datosPregunta->getNombre());
        }
        return $campo;
    }    
#################################### FUNCIONES SQL ######################################

    //Devuelve el sql(string) a ejecutar
    protected function getSQL(){
        $sql = "";

        $select = $this->getSQLSelect($this->cruce);

        $from = $this->getSQLFrom();

        //$join = $this->getSQLJoin();

        $where = $this->getSQLWhere();

        $groupBy = $this->getSQLGroupBy($this->cruce);

        $orderBy = "ORDER BY fecha ASC";

        $sql = $select.$from.$where.$groupBy.$orderBy;
//echo "<br><br>".print_r($sql);
        return $sql;

    }    
    //Devuelve los campos SELECT de la query
    protected function getSQLSelect($cruce){
        $select = "";
        //Cruce
        $sqlCruce = $this->getSQLCruce($cruce)["cruce"]. " AS ".$cruce.", ";
        //Fecha de Referencia y NombreMostrar
        $sqlCruceTemporal = "e.fecha AS fecha_ref, "; //Fecha de Referencia de la encuesta o periodo agrupado.
        $sqlCruceTemporal .= $this->getSQLCruce($this->stringTemporalidad)["cruce"]." AS nombreMostrar, ";
        //Periodo precedido del año, para identificarlo unívocamente
        $sqlPeriodo = "".$this->getSQLCruce($this->stringTemporalidad)["anyoConPeriodo"]." AS anyoConPeriodo, ";
        //Saca los campos necesarios en funcion del tipoCem a mostrar
        $campoPregunta = $this->getCampoPregunta($this->tablaProcesos);
        $sqlTipoCem = call_user_func(array("FrontBundle\Library\TipoCEM\\".$this->tipo,'getNoSQL'),$campoPregunta);

        $select = "SELECT ".$sqlCruce.
                            $sqlCruceTemporal.
                            $sqlPeriodo.
                            "COUNT(DISTINCT(e.id)) AS numEncuestasCruce,". //numero de encuestas reales
                            "COUNT(e.id) AS numRegistrosCruce, ". //numero de encuestas (pueden duplicarse en funcion del filtrado)
                            $sqlTipoCem;           
        return $select;
    }

    protected function getSQLFrom(){
        $from = '';
        if($this->tablaProcesos){// Condicion para usar la tabla nosql_encuesta_procesos
            $from .= "FROM nosql_encuesta_procesos e ";
        }else{
            $from .= "FROM nosql_encuestas e ";
        }
        return $from;
    }

    //Version OLD
    protected function _getSQLJoin(){

        $sqlJoin = "";     
        //Join de las respuestas
        $sqlJoin .= "JOIN survey_encuesta_respuesta er ON (e.id=er.encuesta_id) ";
        //Join de las preguntas
        $sqlJoin .= "JOIN survey_encuesta_pregunta p ON (er.encuesta_pregunta_id=p.id) ";

        // Join de los procesos si el cruce es "proceso"
        if( $this->cruce == "proceso"){
            $sqlJoin.=$this->getSQLProcesos()["join"];
        }  

        return $sqlJoin;
    }

    //Devuelve los posibles WHERE's de la query
    protected function getSQLWhere(){

        $where = "WHERE 1=1 ";
        //Filtro por fechas
        $where .="AND e.fecha >= '".$this->rangoFechas['fecha_inicio']->format('Y-m-d H:i:s')."' 
                  AND e.fecha <= '".$this->rangoFechas['fecha_fin']->format('Y-m-d H:i:s')."' ";

        //Filtro por la opcion vista (cruce de variables)
        if(!empty($this->idOpcionVista)){
            $where .="AND e.".$this->cruceVista."_id IN (".$this->idOpcionVista.") ";
        }

        //Filtro por el proceso especifico, si no es de subBloque Global
        //Para eliminar procesos del groupBy de procesos
        //Podria hacerse con PHP ("eliminarProcesos")
        if($this->idSubBloque!=1 && $this->tablaProcesos){
            $where .= " AND e.proceso_id =".$this->idSubBloque." ";
        }

        //Filtro por los filtros de encuesta (exceptuando el que coincida con la vista seleccionada)
        foreach ( $this->filtros as $nombreFiltro => $values ) {
            if( strpos($nombreFiltro, '-filtro') !== FALSE && !empty($values) ){
                if( strpos($nombreFiltro, $this->cruceVista) !== false ) continue; //Por Seguridad. Si el filtro coincide con el seleccionado en la
                                                                            // vista, no hacemos nada ( ya que no estará disponible)
                                                                            // como filtro de encuesta ( no debería estar disponible)
                $valores = implode(",",$values);

                if($nombreFiltro == "proceso-filtro" && !$this->tablaProcesos){
                    //$where .= $this->getSubSelectProcesos($values); 
                    $filtroProceso = $this->filtroIdToString($values,"proceso");
                    $where = $this->dql->sqlAddFiltroLike(
                            "e",
                            "procesos",
                            $filtroProceso,
                            $where,
                            false,
                            true //Filtra con %.....%
                        );
                }else{// Para nosql_encuestas(menos el proceso) y nosql_encuesta_procesos(todos los filtros)
                    $nombreFiltroValido = str_replace("-filtro", "_id", $nombreFiltro);
                    $where .= " AND e.".$nombreFiltroValido." IN (".$valores.") "; // Resto de filtros.
                }
            }
        }

        return $where;
    }

    // Funcion muy bonita que hace cosas muy bonitas.
    protected function getSQLCruce($cruce){

        $sqlCruce=[];
        switch ($cruce) {
            case 'rango':
                $sqlCruce["cruce"] = "'".$this->rangoFechas['fecha_inicio']->format('d/m/Y')."\n".$this->rangoFechas['fecha_fin']->format('d/m/Y')."' ";                
                $sqlCruce["groupby"] = " 1 ";
                $sqlCruce["anyoConPeriodo"]=$sqlCruce["groupby"];
                break;
            case 'dia':
                $sqlCruce["cruce"] = "CONCAT('Día: ',DAY(e.fecha) ";                
                $sqlCruce["groupby"] = "DATE_FORMAT(e.fecha,'%Y%m%d') ";
                $sqlCruce["anyoConPeriodo"] = $sqlCruce["groupby"];
                break;            
            case 'semana':
                $fechaLunes = "IF(
                                DATE_FORMAT(e.fecha,'%w')=0,
                                DATE_FORMAT(DATE_SUB(e.fecha,INTERVAL 6 DAY),'%d/%m/%Y'),
                                DATE_FORMAT(DATE_SUB(e.fecha,INTERVAL DATE_FORMAT(e.fecha,'%w')-1 DAY),'%d/%m/%Y'))";
                                
                $sqlCruce["cruce"] = "CONCAT('Sem',WEEKOFYEAR( e.fecha),':\n ',".$fechaLunes.")";
                $sqlCruce["groupby"] = "YEARWEEK(e.fecha,3) ";
                $sqlCruce["anyoConPeriodo"] = $sqlCruce["groupby"];
                break;            
            case 'mes':
                $sqlCruce["cruce"] = "CONCAT('Mes: \n',DATE_FORMAT(e.fecha,'%m/%Y'))";
                $sqlCruce["groupby"] = "DATE_FORMAT(e.fecha,'%Y%m') ";
                $sqlCruce["anyoConPeriodo"] = $sqlCruce["groupby"];
                break;            
            case 'trimestre':
                $sqlCruce["cruce"] = "CONCAT('Trimestre: \nT',QUARTER( e.fecha),'-',DATE_FORMAT(e.fecha,'%Y'))";
                //$sqlCruce["groupby"]="CONCAT(YEAR(e.fecha),LPAD(QUARTER(e.fecha), 2, '0')) ";
                $sqlCruce["groupby"] = "CONCAT(YEAR(e.fecha),QUARTER(e.fecha)) ";
                $sqlCruce["anyoConPeriodo"] = $sqlCruce["groupby"];
                break;            
            case 'año':
                $sqlCruce["cruce"] = "CONCAT('Año: \n',YEAR(e.fecha))";
                $sqlCruce["groupby"] = "YEAR(e.fecha) ";
                $sqlCruce["anyoConPeriodo"] = $sqlCruce["groupby"];
                break;
            default: // Si el cruce es un filtro
                $sqlCruce["cruce"] = "e.".$cruce."_id ";
                $sqlCruce["groupby"] = $sqlCruce["cruce"]; //El groupby es igual
                
                break;
        }
        return $sqlCruce;
    }

    // Ejecuta una SQL y la retorna
    protected function ejecutarSQL($conexion, $sql){

        $statement = $conexion->prepare($sql);
        $statement->execute();
        $resultado = $statement->fetchAll();

        return $resultado;
    }

#################################### PROCESOS ##########################################
    protected function getSubSelectProcesos($valores){

        //if(empty($valores)) return "";

        $valores=implode(",",$valores);

        return "AND e.id IN (SELECT 
                    ep.encuesta_id
                FROM
                    survey_encuesta_proceso ep
                WHERE
                    ep.proceso_id IN (".$valores.")) ";
    }

    // Callback para eliminar un proceso en el array de $resultadosFinales
    protected function eliminaProcesos($array){
        if(empty($this->filtros["proceso-filtro"])) return true;

        if(!in_array(intval($array["proceso_id"]), $this->filtros["proceso-filtro"])){
            return false;
        }
        return true;
    }


    protected function _getSQLProcesos(){
        $cruce = "e.proceso_id ";
        $join = "JOIN survey_encuesta_proceso ep ON (e.id=ep.encuesta_id) ";           
        $where = "AND e.proceso_id = ".$this->idSubBloque." ";

        return array(   "cruce"=>$cruce,
                        "join"=>$join,
                        "where"=>$where,
                        "groupby"=>$cruce);
    }

    private function filtroIdToString($valores, $nombreFiltro)
    {
        $equivalenciasFiltro = $this->equivalencias[$nombreFiltro];
        $resultado = [];

        if(!empty($valores)){
            if(is_array($valores)){
                foreach ($valores as $valor) {
                    $resultado[] = $equivalenciasFiltro[$valor];
                }
            }else{
                $resultado = $equivalenciasFiltro[$valor];
            }
        }else{
            $resultado = "";
        }

        return $resultado;
    }
#################################### FECHAS ##########################################

    // Devuelve el rango de Fechas para el whereSQL
    protected function getRangoFechas($numPeriodos){
        $this->rangoFechas = [];
        if($this->filtros['temporalidad']==0){//rango
            $inicio = FechasUtils::getRangoTemporalidad($this->stringTemporalidad, $this->filtros["fec_ini"])['fecha_inicio'];
            $fin = FechasUtils::getRangoTemporalidad($this->stringTemporalidad, $this->filtros["fec_fin"])['fecha_fin'];
            $this->rangoFechas['fecha_inicio'] = $inicio;
            $this->rangoFechas['fecha_fin'] = $fin;
        }else{
            $fec_ref = $this->getFecRef('fec_ini');
            $this->rangoFechas=FechasUtils::getRangoTemporalidad($this->stringTemporalidad, $fec_ref,$numPeriodos);
        }
        return $this->rangoFechas;
    }

    protected function getFecRef($nombreFiltro){
        if(isset($this->filtros[$nombreFiltro])){
            return new \DateTime($this->filtros[$nombreFiltro]);
        }else{
            return new \DateTime();
        }
    }

    /*
    * Funcion que devuelve un array de fechas inicio y fin para cada periodo comprendido entre
    * el que se especifique y el actual.
    */
    // Funcionalidad implementada pero no usada actualmente
    protected function getRangosTemporalidad($temporalidadString, \DateTime $fecha_ref, $periodos=1){
        $arrayFechasCompleto=[];
        for ($i=1; $i <= $periodos ; $i++) {
            $arrayFechasCompleto[$i]=FechasUtils::getRangoTemporalidad($temporalidadString, $fecha_ref);
            $fecha_ref = clone($arrayFechasCompleto[$i]["fecha_inicio"]);
            //Para evitar el problema con la primera semana del año
            //Resto 7 dias a la fecha_inicio que se genera (no puedo restar solo 1 dia)
            if($temporalidadString!="dia"){
                $fecha_ref->sub(new \DateInterval('P7D'));
            }else{
                $fecha_ref->sub(new \DateInterval('P1D'));
            }
        }
        return $arrayFechasCompleto;
    }

    /*
        Devuelve el nombre asociado a la temporalidad y a una fecha.
        Si no se especifica fecha (Datetime), usa como referencia la actual
    */
    protected function getNombreMostrarTemporalidad($temporalidad,$fecha=null){
        if($fecha==null){
            $fecha = new \DateTime();
        }
        if(!($fecha instanceof \DateTime)){
            $fecha = new \DateTime($fecha);
        }

        switch ($temporalidad) {
                case 'dia':
                    $nombre = ucfirst(('día')) . ":\n " . $fecha->format("d-m-Y");
                    break;
                case 'semana':
                    $fechaLunes = new \DateTime();
                    $anyoActualSemana = FechasUtils::getFuckingYear($fecha); 
                    $fechaLunes = $fechaLunes->setISODate(intval($anyoActualSemana), intval($fecha->format("W")), 1)->format('d/m/Y');
                    $nombre = "Sem".$fecha->format('W') . ":\n " . $fechaLunes;
                    break;
                case 'mes':
                    $fecha = ucfirst(strftime("%b/%Y", $fecha->getTimeStamp()));
                    $nombre = "Mes" . ":\n " . $fecha;
                    break;
                case 'año':
                    $nombre = "Año" . ":\n " . $fecha->format('Y');
                    break;
                case 'trimestre':
                    $nombre = "Trimestre" . ":\n " . floor(($fecha->format('m') - 1) / 3 + 1) . '/' . $fecha->format('Y');
                    break;
                case 'rango':
                    $nombre = "Rango" . ":\n " . $this->rangoFechas['fecha_inicio']->format('d/m/Y')." \n".$this->rangoFechas['fecha_fin']->format('d/m/Y');
                    break;
                default:
                    $nombre = "";
        }

        return $nombre;
    }

    /**
     * Funcion que devuelve el numero de temporalidades a las que nos vamos a remontar
     * @param type $cruce
     * @return int
     */
    protected function getNumPeriodos($temporalidad) {
        $periodo = 0;
        switch ($temporalidad) {
            case 'dia':
                $periodo = 7;
                break;
            case 'semana':
                $periodo = 8;
                break;
            case 'mes':
                $periodo = 6;
                break;
            case 'año':
                $periodo = 4;
                break;
            case 'trimestre':
                $periodo = 8;
                break;
            default:
                $periodo = 1;
                break;
        }
        return $periodo;
    }


#################################### OTROS ##########################################

    // Retorna los datos de una fila de la pregunta
    protected function getDatosPregunta($idIndicador){
        $rowPregunta = $this->em
            ->getRepository("SurveyBundle:EncuestaPregunta")
            ->findOneById($idIndicador);
        return $rowPregunta;
    }

    /*
    * Para sacar el valor maximo y su key (nombre adjetivo) asociada de un array.
    * Extraigo la primera ocurrencia del array.
    * $array es un array que contiene las repeticiones para cada adjetivo ( o cosa)
    */
    protected function doublemax($array){
        // Ejemplo: se extraería; Confiad@ con cantidad 1
        /*$array =  array('Enfadad@' => 0,
                          'Decepcionad@' => 0,
                          'Ignorad@' => 0,
                          'Confiad@' => 1,
                          'Cuidad@' => 0,
                          'Gratamente Sorprendid@' => 1);*/
        $maxvalue=max($array); // Saco el valor maximo
        $maxindex = "";
        while(list($key,$value)=each($array)){
            if($value==$maxvalue){
                $maxindex = $key;
                break; // Saco la primera ocurrencia del array.
            }
        }

        return array(   "cantidad"=>$maxvalue,
                        "nombre"=>$maxindex);
    }

    // $arrayAdjetivos es un array que contiene las repeticiones para cada adjetivo
    // Sin uso, Actualmente el emotionalIndex se saca calculado de BD
    protected function _getEmotionalIndex($arrayAdjetivos){
        $acumulado=0;
        $numEncuestas = 0;

        foreach ($arrayAdjetivos as $key => $value) {
            $numEncuestas+=$value;
            $acumulado+=intval(EI::getValor($key))*intval($value);
        }
        $ei=$acumulado/$numEncuestas;

        return $ei;
    }

    // El titulo del titleBox para el twig
    protected function getTitulo($noData){
        if($noData){
            return $titulo = "No hay datos para esta consulta.";
        }
        if($this->tipo == "NPS"){
            $titulo = $this->tipo;
        }elseif($this->tipo == "EI"){
            $titulo = "Emotional Index";
        }else{
            $titulo = $this->datosPregunta->getDescripcionCompleta();
        }
        return $titulo;
    } 

    protected function getEquivalencias(){
        return $this->equivalencias;
    }

    public function getDatos()
    {
        return $this->datos;
    }

    /**
     * Esta es la función que principalmente han de redefinir los Services
     */
    public function filtrarDatos($datos){

        return $this;
    }

}
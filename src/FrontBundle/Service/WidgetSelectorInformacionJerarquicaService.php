<?php

namespace FrontBundle\Service;

use AdminBundle\Service\FiltrosSesionService as FiltrosService;
use Doctrine\ORM\EntityManager;

class WidgetSelectorInformacionJerarquicaService extends ServiceBase
{
    private $filtros;

    function __construct(FiltrosService $fs, EntityManager $em)
    {
        $this->filtros = $fs->getFiltros();
        $this->em = $em;
    }

    public function filtrarDatos($datos)
    {
        $this->datos = array(
            $this->filtros,
            "optionsNegocio" => $this->getDatosNegocios(),
            "negocioChecked" => $this->filtros["optionNegocio"]
        );

        $this->getDatosNegocios();

        return $this;
    }

    public function getDatosNegocios()
    {
        $registrosDoctrine = $this->em
                                  ->getRepository("AdminBundle:FiltrosMaqueta")
                                  ->findOneByNombreId("neg-filtro")
                                  ->getFiltro()
                                  ->getEntidadDoctrine();

        $registrosDoctrine = $this->em
                                  ->getRepository($registrosDoctrine)
                                  ->findAll();

        $datosNegocio = [];

        foreach ($registrosDoctrine as $negocio) {
            $datosNegocio[$negocio->getId()] = $negocio->getNombre();
        }

        return $datosNegocio;
    }
}

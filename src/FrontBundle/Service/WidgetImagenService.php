<?php

namespace FrontBundle\Service;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use FrontBundle\Service\ServiceBase;

/**
*
*/
class WidgetImagenService extends ServiceBase
{
    private $rootDir;
    private $nombreImagen;

    function __construct($webDir, $nombreImagen)
    {
        $this->rootDir = $webDir;
        $this->nombreImagen = $nombreImagen;
    }

    public function filtrarDatos($datos)
    {
        $this->datos = array(
            "rutaImagen" => $this->rootDir . "/" . $this->nombreImagen
        );

        return $this;
    }
}
<?php

namespace FrontBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class ServiceBase
{
    protected $em;
    protected $session;
    protected $datos;

    public function __construct(EntityManager $em, Container $container)
    {
        $this->em = $em;
        $this->datos = null;
    }

    protected function setSesionActiva()
    {

        $this->session = $container
                            ->get('request')
                            ->getSession();

        return $this;
    }


    public function getDatos()
    {
        return $this->datos;
    }

    /**
     * Esta es la función que principalmente han de redefinir los Services
     */
    public function filtrarDatos($datos){

        return $this;
    }
}

?>
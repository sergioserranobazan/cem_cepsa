<?php

namespace FrontBundle\Service;

use FrontBundle\Service\ServiceBase;
use AdminBundle\Service\FiltrosSesionService as FiltrosService;
use Doctrine\ORM\EntityManager;

class WidgetFiltrosService extends ServiceBase
{
    function __construct(FiltrosService $fs, EntityManager $em, $filtrosBloqueId)
    {
        $this->fs = $fs;
        $this->em = $em;
        $this->repoFiltros = $em->getRepository("AdminBundle:FiltrosMaqueta");

        $this->valoresFiltrosBloque =
            $fs->getValoresFiltroBloque($filtrosBloqueId);
    }

    /**
     * Se devuelve un array con los datos de los filtros organizados de la
     * siguiente manera:

        "filtro1" => [
            "datosPadre" => [
                "tabla" => "tablaPadre1",
                "valores" => ["valorPadre1", "valorPadre2", "valorPadre3"]
            ],

            "valoresFiltro" => ["valor1", "valor2"]
        ],
        "filtro2" => [
                etc...
        ]

     * En caso de no haber padre la estructura de datosPadre es la misma pero
     * todos los valores son devueltos como 0
     */
    public function filtrarDatos($datos)
    {
        $this->datos["valoresFiltros"] =
            $this->getDatosFiltros($datos["maqueta"]);

        $this->datos["valoresSeleccionados"] =
            $this->fs->getFiltros();

        return $this;
    }

    private function getDatosFiltros($slugMaqueta)
    {

        $filtros = $this->fs->newArrayFiltros();
        $datosFiltros = [];

        foreach(array_keys($filtros) as $nombreFiltro){
            $datos = [];

            $filtroMaqueta = $this->repoFiltros
                                  ->findOneByNombreId($nombreFiltro);

            $filtro = $filtroMaqueta->getFiltro();

            $datos["padre"] = $this->getDatosPadre($filtro);

            $datos["hijo"] = $filtro->getTablaHijo();

            $filtroEntity = $filtro->getEntidadDoctrine();

            $registrosFiltro = $this->getValoresFiltroBloqueActual($nombreFiltro);


            foreach ($registrosFiltro as $idRegistro => $nombreRegistro) {
                // Sólo guardaremos los registros que tengan un nombre.
                // El nombre representa el texto que se mostrará en el `<option>`
                // Importante que los registros de los filtros tengan el campo
                // nombre.
                if((empty($nombreRegistro) && $nombreRegistro !== "0" )
                    || empty($idRegistro)
                ){
                    continue;
                }

                $datos["registros"][] = array(
                    "nombre" => $nombreRegistro,
                    "valor" => $idRegistro,
                    "valoresPadre" => $this->getValoresPadre()
                );
            }

            $datosFiltros[$nombreFiltro] = $datos;
        }

        return $datosFiltros;
    }

    private function getDatosPadre($filtro)
    {
        $padre = $filtro->getPadre();

        if(!$padre) return null;

        $repositorioPadre = $padre->getTabla();

        return $this->em->getRepository($repositorioPadre)
                            ->getNombre();


    }

    /**
     * Extrae del array de filtros los valores del filtro especificado por
     * nombreId
     */
    private function getValoresFiltroBloqueActual($nombreFiltro)
    {
        foreach ($this->valoresFiltrosBloque as $filtro) {
            if($filtro["nombreId"] == $nombreFiltro){
                return $filtro["valores"];
            }
        }

        return [];
    }

    private function getValoresPadre()
    {
        /**
         * Alerta CG: Función por implementar
         */
        return [];
    }
}

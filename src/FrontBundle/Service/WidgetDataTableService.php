<?php

namespace FrontBundle\Service;
use AdminBundle\Service\FiltrosSesionService as FiltrosSesion;


/**
 * Este Service ha perdido importancia porque ahora la lógica de la
 * datatable se realiza completamente en el controlador Ajax. Se tendría que
 * eliminar este service o mover la lógica presente en el ajax controller
 * a este service
 */
class WidgetDataTableService extends ServiceBase
{
    public function __construct(){}

    public function filtrarDatos($datosMaqueta)
    {
        $cabeceraTabla = array(
            "Fecha"      ,
            "Negocio"    ,
            "L. neg."    ,
            "Canal"      ,
            "Det. Canal" ,
            "Procesos"   ,
            "Exp."       ,
            "Rec."       ,
            "Sent."      ,
            "OPI"        ,
            "Info."     ,
        );

        $datosTabla = array(
            "cabecera" => $cabeceraTabla,
        );

        $this->datos = array_merge($datosTabla,$datosMaqueta);

        return $this;
    }
}
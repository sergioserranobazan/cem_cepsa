<?php

namespace FrontBundle\Service;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;
use AdminBundle\Service\DQLBuilderService as DQLBuilder;
use AdminBundle\Service\FiltrosSesionService as FiltrosSesion;

use AdminBundle\Library\CemUtils;
use FrontBundle\Library\TipoCEM\EI;
//use FrontBundle\Service\MaquetaPrincipal\WidgetTablaEspecificoProcesoService as EspecificoProceso;
use Mrcem\PerformanceLogger\PerformanceIssuer;

class WidgetTreeViewEnCMInformacionJerarquicaService extends ServiceBase
{
    private $encuestas;
    private $equivalencias;
    private $estructuraTreeview = [];
    private $rProcessIndex = []; // Array para almacenar el processIndex

    function __construct(
        FiltrosSesion $fs, EntityManager $em,
        DQLBuilder $dql
    ){
    $this->logSQL = new PerformanceIssuer("treeView_CM3", PerformanceIssuer::FILE_LOGGER);
    $this->logSQL->start();  
        // Services inyectados
        $this->fs  = $fs;
        $this->em  = $em;
        //$this->ep  = $ep;
        $this->dql = $dql;

        $this->filtros = $fs->getFiltros();

        // VARIABLES DE CONFIGURACIÓN
        $this->setEquivalencias();

        $this->optionNegocio = $this->filtros["optionNegocio"];
    }

    private function obtenerDatosGlobales($groupBy,$proceso)
    {

        $conn = $this->em->getConnection();

        $subSelectIdPreguntasEsuerzo = "
          SELECT ep.id
            FROM survey_encuesta_pregunta ep
            WHERE ep.orden_pregunta_proceso = 1 and ep.sub_bloque_id!=1
        ";
        //$sql = "RESET QUERY CACHE; ";
        $sql = "
            SELECT e.encuesta_id as encuesta_id,
                e.neg_id as neg_id,
                e.can_id as can_id,
                count(DISTINCT(e.encuesta_id)) numEncuestas,
                count(*) numRegistros,
                SUM(e.g2) as SUM_Recomendacion,
                SUM(e.g3) as SUM_SAT,
                SUM(IF(e.g2 >= 9, 1, 0)) - SUM(IF(e.g2 <= 6, 1, 0)) as SUM_NPS,

                /* ------ Cálculos para Emoción y Emotional Index  ---------*/
                SUM(e.g1_valor) as emotional_index,
                SUM(IF(e.g1 = 'Confiad@', 1, 0)) num_confiados,
                SUM(IF(e.g1 = 'Cuidad@', 1, 0)) num_cuidados,
                SUM(IF(e.g1 = 'Decepcionad@', 1,0)) num_decepcionados,
                SUM(IF(e.g1 = 'Enfadad@', 1, 0)) num_enfadados,
                SUM(IF(e.g1 = 'Gratamente sorprendid@', 1,0)) num_g_sorprendidos,
                SUM(IF(e.g1 = 'Ignorad@', 1, 0)) num_ignorados
        ";

        if($proceso){ // Los campos afectados por procesos los trato de nosql_encuesta_procesos

            $sql .= ",  e.proceso_id as proceso_id ";
            /* ---------- Cálculo Process Index   --------------*/
            /*$sql .= ",  SUM(e.suma_valoracion) as process_index,
                        SUM(e.num_preguntas_valoracion) as preguntas_valoracion ";*/

            /* ---------- Cálculo Esfuerzo Index   --------------*/
            /*$sql .= ",  SUM(IF(e.p1_pregunta_id IN ($subSelectIdPreguntasEsuerzo)
                        , e.p1, 0)) as sum_esfuerzo_p1,

                        SUM(IF(e.p1_pregunta_id IN ($subSelectIdPreguntasEsuerzo)
                            , 1, 0)) as num_preguntas_esfuerzo_p1,


                        SUM(IF(e.p2_pregunta_id IN ($subSelectIdPreguntasEsuerzo)
                            , e.p2, 0)) as sum_esfuerzo_p2,

                        SUM(IF(e.p2_pregunta_id IN ($subSelectIdPreguntasEsuerzo)
                            , 1, 0)) as num_preguntas_esfuerzo_p2,


                        SUM(IF(e.p3_pregunta_id IN ($subSelectIdPreguntasEsuerzo)
                            , e.p3, 0)) as sum_esfuerzo_p3,

                        SUM(IF(e.p3_pregunta_id IN ($subSelectIdPreguntasEsuerzo)
                            , 1, 0)) as num_preguntas_esfuerzo_p3 ";*/

            /* ---------- FROM   --------------*/                        
            $sql .= "   FROM nosql_encuesta_procesos e
                        WHERE 1 = 1 ";
        }else{  // Los campos que no afectan a los procesos los trato de nosql_encuestas
                // Problema: para sacar los datos a procesos se nos llena de SUBSELECTS las querys

            /* ---------- Cálculo Process Index   --------------*/
            /*$sql .= ",  SUM((SELECT SUM(nep.suma_valoracion) FROM nosql_encuesta_procesos nep WHERE nep.encuesta_id = e.encuesta_id)) as process_index,
                        SUM((SELECT SUM(nep.num_preguntas_valoracion) FROM nosql_encuesta_procesos nep WHERE nep.encuesta_id = e.encuesta_id)) as preguntas_valoracion ";*/

            /* ---------- Cálculo Esfuerzo Index   --------------*/
            /* MORTALMENTE LENTAS => USAR 2 QUERYS Y HACER MERGE */
            /*$sql .= ",  SUM((SELECT
                            SUM(IF(nep.p1_pregunta_id IN ($subSelectIdPreguntasEsuerzo)
                        , nep.p1, 0)) as sum_esfuerzo_p1
                            FROM
                                nosql_encuesta_procesos nep  
                            WHERE
                                nep.encuesta_id = e.encuesta_id)) AS sum_esfuerzo_p1,

                        SUM((SELECT
                            SUM(IF(nep.p1_pregunta_id IN ($subSelectIdPreguntasEsuerzo)
                                , 1, 0)) as num_preguntas_esfuerzo_p1
                            FROM
                                nosql_encuesta_procesos nep  
                            WHERE
                                nep.encuesta_id = e.encuesta_id)) AS num_preguntas_esfuerzo_p1,


                        SUM((SELECT
                            SUM(IF(nep.p2_pregunta_id IN ($subSelectIdPreguntasEsuerzo)
                        , nep.p2, 0)) as sum_esfuerzo_p2
                            FROM
                                nosql_encuesta_procesos nep  
                            WHERE
                                nep.encuesta_id = e.encuesta_id)) AS sum_esfuerzo_p2,

                        SUM((SELECT
                            SUM(IF(nep.p2_pregunta_id IN ($subSelectIdPreguntasEsuerzo)
                                , 1, 0)) as num_preguntas_esfuerzo_p2
                            FROM
                                nosql_encuesta_procesos nep  
                            WHERE
                                nep.encuesta_id = e.encuesta_id)) AS num_preguntas_esfuerzo_p2,


                        SUM((SELECT
                            SUM(IF(nep.p3_pregunta_id IN ($subSelectIdPreguntasEsuerzo)
                        , nep.p3, 0)) as sum_esfuerzo_p3
                            FROM
                                nosql_encuesta_procesos nep  
                            WHERE
                                nep.encuesta_id = e.encuesta_id)) AS sum_esfuerzo_p3,

                        SUM((SELECT
                            SUM(IF(nep.p3_pregunta_id IN ($subSelectIdPreguntasEsuerzo)
                                , 1, 0)) as num_preguntas_esfuerzo_p3
                            FROM
                                nosql_encuesta_procesos nep  
                            WHERE
                                nep.encuesta_id = e.encuesta_id)) AS num_preguntas_esfuerzo_p3 ";*/

            /* ---------- FROM   --------------*/ 
            $sql .= "   FROM nosql_encuestas e
                        WHERE 1 = 1 ";
        }


        $sql = $this->dql->filtradoFecha(
            $this->filtros["temporalidad"],
            $sql, $this->filtros["fec_ini"], $this->filtros["fec_fin"],
            true, "e"
        );

        // El negocio de la Vista
        $sql = $this->dql->sqlAddFiltro(
            "e", "neg_id", $this->optionNegocio, $sql
        );

        //Filtro por los filtros de encuesta
        foreach ($this->filtros as $nombreFiltro => $values) {

            if(strpos($nombreFiltro, '-filtro')!==FALSE && !empty($values) ){//Solo filtros de encuesta no vacios
                if($nombreFiltro == "proceso-filtro" && !$proceso){
                    //$sql .= $this->getSubSelectProcesos($values); 
                    $filtroProceso = $this->filtroIdToString($values,"proceso_id");
                    $sql = $this->dql->sqlAddFiltroLike(
                            "e",
                            "procesos",
                            $filtroProceso,
                            $sql,
                            false,
                            true //Filtra con %.....%
                        );               
                }else{ //Caso generico
                    $nombreFiltroValido = str_replace("-filtro", "_id", $nombreFiltro);
                    $sql = $this->dql->sqlAddFiltro(
                        "e",
                         $nombreFiltroValido,
                         $this->filtros[$nombreFiltro], 
                         $sql
                     );
                }
            }
        }

        $sql = $sql . " GROUP BY " . implode(" ,", array_map(function($col){
            return "e." . $col;
        }, $groupBy));

//echo "<br><br>".$sql."<br>";
    $this->logSQL->breakpoint("inicioSQL-".implode(',',$groupBy));
        $rCalculosGenerales = $conn->executeQuery($sql)->fetchAll();
    $this->logSQL->breakpoint("finSQL-".implode(',',$groupBy));

        // Extrae los procesos no filtrados:
        // Elimina los registros que tienen un proceso no filtrado
        // Obligatorio hacerlo ya que se hace el groupBy por proceso_id
        if($proceso){
            $rCalculosGenerales= array_filter($rCalculosGenerales, array($this, 'eliminaProcesos'));
        }

        return $rCalculosGenerales;
    }

    private function generarTreeview($groupBy, $equivalencias)
    {
        //PROCESOS
        $proceso = true;
        $rowsProceso = $this->obtenerDatosGlobales($groupBy,$proceso,false);

        //NO PROCESOS
        $groupBySegundoNivel  = array_slice($groupBy, 0, 2);
        //Añado al principio GroupBy encuesta_id
        //array_unshift($groupBySegundoNivel, "encuesta_id");
        $proceso = false;
        //Comprobacion, si hay que filtrar por mas de un proceso
        //$filtroProceso = $this->checkFiltroProceso();
        $rowsNegCanal = $this->obtenerDatosGlobales($groupBySegundoNivel,$proceso);

        $rowsResultado = [];

        foreach ($rowsNegCanal as $row) {
            $primerNivel = $row[$groupBy[0]];
            $segundoNivel = $row[$groupBy[0]] . "-" . $row[$groupBy[1]];

            // -------------  Negocio Node -----------------------
            if(isset($rowsResultado[$primerNivel])){
                $rowsResultado[$primerNivel] =
                    $this->acumularDatos(
                        $rowsResultado[$primerNivel], $row
                    );

                $rowsResultado[$primerNivel]["numEncuestas"] += $row["numEncuestas"];

            }else{
                $nuevosCamposNeg = [
                    "nodoTreeview"      => $primerNivel,
                    "nodoTreeviewPadre" => "",
                    "texto"             => $equivalencias[$groupBy[0]][$row[$groupBy[0]]],
                ];

                $rowsResultado[$primerNivel] = array_merge($row, $nuevosCamposNeg);
            }

            // --------------- Canal Node -----------------
            $nuevosCamposCanal = [
                "nodoTreeview" => $segundoNivel,
                "nodoTreeviewPadre" => $primerNivel,
                "texto" => $equivalencias[$groupBy[1]][$row[$groupBy[1]]],
            ];

            $rowsResultado[$segundoNivel] = array_merge($row, $nuevosCamposCanal);
        }

        foreach ($rowsProceso as $row) {
            $segundoNivel =
                $row[$groupBy[0]] . "-" . $row[$groupBy[1]];
            $tercerNivel =
                $row[$groupBy[0]] . "-" . $row[$groupBy[1]]. "-" . $row[$groupBy[2]];

            $nuevosCampos = [
                "nodoTreeview" => $tercerNivel,
                "nodoTreeviewPadre" => $segundoNivel,
                // "numEncuestas" => $row["numEncuestas"],
                "texto" => $equivalencias[$groupBy[2]][$row[$groupBy[2]]],
            ];

            $rowsResultado[$tercerNivel] = array_merge($row, $nuevosCampos);
        }

        return $rowsResultado;
    }

    private function filtroIdToString($valores, $nombreFiltro)
    {
        $equivalenciasFiltro = $this->equivalencias[$nombreFiltro];
        $resultado = [];

        if(!empty($valores)){
            if(is_array($valores)){
                foreach ($valores as $valor) {
                    $resultado[] = $equivalenciasFiltro[$valor];
                }
            }else{
                $resultado = $equivalenciasFiltro[$valor];
            }
        }else{
            $resultado = "";
        }

        return $resultado;
    }

    private function procesoFirstTreeview($groupBy, $equivalencias)
    {

        $rowProceso = $this->obtenerDatosGlobales($groupBy);

        $rowsResultado = [];

        foreach ($rowProceso as $row) {
            $primerNivel = $row[$groupBy[2]];

            $segundoNivel =
                $primerNivel . "-" . $row[$groupBy[0]];

            $tercerNivel =
                $segundoNivel . "-" . $row[$groupBy[1]];


            // Nodo Proceso
            if(isset($rowsResultado[$primerNivel])){
                $rowsResultado[$primerNivel] =
                    $this->acumularDatos(
                        $rowsResultado[$primerNivel], $row
                    );
                $rowsResultado[$primerNivel]["numEncuestas"] += $row["numEncuestas"];

            }else{
                $rowProceso = [
                    "nodoTreeview" => $primerNivel,
                    "nodoTreeviewPadre" => "",
                    "texto" => $equivalencias[$groupBy[2]][$row[$groupBy[2]]],
                ];

                $rowsResultado[$primerNivel] = array_merge($row, $rowProceso);
            }

            // Nodo Negocio
            if(isset($rowsResultado[$segundoNivel])){
                $rowsResultado[$segundoNivel] =
                    $this->acumularDatos(
                        $rowsResultado[$segundoNivel], $row
                    );

                $rowsResultado[$segundoNivel]["numEncuestas"] += $row["numEncuestas"];


            }else{
                $rowNegocio = [
                    "nodoTreeview" => $segundoNivel,
                    "nodoTreeviewPadre" => $primerNivel,
                    "texto" => $equivalencias[$groupBy[0]][$row[$groupBy[0]]],
                ];

                $rowsResultado[$segundoNivel] = array_merge($row, $rowNegocio);
            }

            // Nodo Canal
            $rowCanal = [
                "nodoTreeview" => $tercerNivel,
                "nodoTreeviewPadre" => $segundoNivel,
                "texto" => $equivalencias[$groupBy[1]][$row[$groupBy[1]]],
            ];

            $rowsResultado[$tercerNivel] = array_merge($row, $rowCanal);
        }

        return $rowsResultado;
    }

    private function procesadoFinalFilas($rows)
    {

        $rows = array_map(function($row){
            $row["emotionalIndex"] = $this->estructuraEmotionalIndex(
                    floatval(round($row["emotional_index"]/$row["numRegistros"],2)));

            $row["emotionalIndex"]["valor"] = CemUtils::formateoNumero(
                $row["emotionalIndex"]["valor"]);

            $row["experiencia"] = CemUtils::formateoNumero(
                $row["SUM_SAT"]/$row["numRegistros"]
            );

            $row["nps"] = CemUtils::formateoNumero(
                ($row["SUM_NPS"]/ $row["numRegistros"])*100
            );

            /*$row["indiceRecomendacion"] =
                CemUtils::formateoNumero($row["SUM_Recomendacion"]/ $row["numRegistros"]);*/

            $row["nodoTreeview"] = "treegrid-". $row["nodoTreeview"];
            $row["nodoTreeviewPadre"] = !empty($row["nodoTreeviewPadre"])
                                        ? "treegrid-parent-". $row["nodoTreeviewPadre"]
                                        : "";

            /*$row["emocionPrincipal"] = $this->getEmocionPrincipalString($row);*/
            
            $row["emociones"] = $this->getEmocionesDesagregadas($row);

            /*$row["processIndex"] = CemUtils::formateoNumero($row["process_index"]/$row["preguntas_valoracion"]);*/

            /* EsfuerzoIndex */
            /*$numPreguntasEsfuerzo =
                ($row["num_preguntas_esfuerzo_p1"]
                    + $row["num_preguntas_esfuerzo_p2"]
                    + $row["num_preguntas_esfuerzo_p3"]);

            $sumaPreguntasEsfuerzo =
                ($row["sum_esfuerzo_p1"]
                    + $row["sum_esfuerzo_p2"]
                    + $row["sum_esfuerzo_p3"]);

            $row["esfuerzoIndex"] = $numPreguntasEsfuerzo == 0
                ? "Sin datos"
                : CemUtils::formateoNumero($sumaPreguntasEsfuerzo/$numPreguntasEsfuerzo);*/

            return $row;

        }, $rows);

        ksort($rows, SORT_STRING);

        return $rows;
    }

    private function getEmocionesDesagregadas($row){

        $emociones = [];
        $campos = EI::getCamposAdjetivos();

        $adjetivos = EI::getAdjetivos();

        $i=0;
        foreach ($campos as $index => $campoParaComprobar) {
            $emociones[$adjetivos[$i]] = CemUtils::formateoNumero(
                                            $row[$campoParaComprobar] / $row["numRegistros"] *100);
            $i++;
        }

        return $emociones;
    }

    private function getEmocionPrincipalString($row)
    {
        $maxCount = 0;
        $adj = "";

        $campos = EI::getCamposAdjetivos();

        $adjetivos = EI::getAdjetivos();

        foreach ($campos as $index => $campoParaComprobar) {
            if($row[$campoParaComprobar] > $maxCount){
                $maxCount = $row[$campoParaComprobar];
                $adj = $adjetivos[$index];
            }
        }

        $porcentaje = CemUtils::formateoNumero(
            $maxCount / $row["numRegistros"] * 100);

        $string = $adj . " " . $porcentaje;


        return $string;
    }

    private function acumularDatos($rowOriginal, $rowPorAcumular)
    {


        $camposParaAcumular = [
            "SUM_SAT", "SUM_NPS",
            "SUM_Recomendacion", "emotional_index",
            "num_confiados", "num_cuidados", "num_g_sorprendidos",
            "num_ignorados", "num_decepcionados", "num_enfadados"/*,
            "process_index", "preguntas_valoracion"*/
        ];

        foreach ($camposParaAcumular as $campo) {
            $rowOriginal[$campo] += floatval($rowPorAcumular[$campo]);
        }

        return $rowOriginal;
    }

    private function estructuraEmotionalIndex($valor)
    {
        if(gettype($valor) !== 'double'){
            throw new \Exception("$valor ha de ser 'double'", 1);
        }

        if($valor >= 7.0){
            $color = "positivo";
            $icono = "fa fa-smile-o";
        }elseif($valor > 5.0){
            $color = "neutral";
            $icono = "fa fa-meh-o";
        }else{
            $icono = "fa fa-frown-o";
            $color = "negativo";
        }

        $result = [
            "valor" => $valor,
            "icono" => $icono,
            "representacion" => $color
        ];

        return $result;
    }


    public function filtrarDatos($datos)
    {

        $nodoPrincipal = 1;
        $nodoSecundario = 2;
        $nodoTerciario = 3;

        $nodoAColumnaEncuestas = [
            "1" => "neg_id",
            "2" => "can_id",
            "3" => "proceso_id"
        ];

        $equivalencias = $this->getEquivalencias();

        $jerarquiaNodos = [
            $nodoAColumnaEncuestas[$nodoPrincipal],
            $nodoAColumnaEncuestas[$nodoSecundario],
            $nodoAColumnaEncuestas[$nodoTerciario],
        ];

        $rows = [];
        if($nodoPrincipal == 1){
            $rows = $this->generarTreeview(
                $jerarquiaNodos, $equivalencias
            );
        }elseif($nodoPrincipal == 3){

            $rows = $this->procesoFirstTreeview(
                $jerarquiaNodos, $equivalencias
            );
        }

        $treeview = $this->procesadoFinalFilas($rows);

        //Jerarquía
        $this->datos["nodosSeleccionados"] = [
            $equivalencias["vistas"][$nodoPrincipal],
            $equivalencias["vistas"][$nodoSecundario],
            $equivalencias["vistas"][$nodoTerciario],
        ];

        $this->datos["estructura"] = $treeview;

        $this->datos["estructuraPlana"] = $treeview;
    $this->logSQL->end();
        return $this;
    }

    public function getEquivalencias(){
        return $this->equivalencias;
    }

    protected function setEquivalencias(){
        //if(isset($this->equivalencias)) return $this->equivalencias;
        // Nota estas equivalencias llevan _id
        // Se prodrian gestionar sin el id, e ir introduciendo el id donde
        // fuese necesario
        $equivalencias = [
            "neg_id"    => $this->fs->getValoresEnBDFiltro("neg"),
            "can_id"    => $this->fs->getValoresEnBDFiltro("can"),
            "proceso_id"=> $this->fs->getValoresEnBDFiltro("proceso"),
            "vistas"    => $this->fs->getValoresEnBDFiltro("vista")
            //"vistas_doctrine"    => $this->getNombresDoctrineVistas()
            
        ];

        $this->equivalencias = $equivalencias;
    }   

    private function getNombresDoctrineVistas(){

        $vistas = $this->em->getRepository("AdminBundle:Vista")->findAll();
        $array = [];
        foreach ($vistas as $value) {
            $array[$value->getId()]=$value->getFiltro()->getNombreFiltro();
        }
        return $array;
    }

    public function getSubSelectProcesos($valores){

        //if(empty($valores)) return "";

        $valores=implode(",",$valores);

        return "AND e.id IN (SELECT 
                    ep.encuesta_id
                FROM
                    survey_encuesta_proceso ep
                WHERE
                    ep.proceso_id IN (".$valores.")) ";
    }

    // Callback para eliminar un proceso en el array de $resultadosFinales
    private function eliminaProcesos($array){
        if(empty($this->filtros["proceso-filtro"])) return true;

        if(!in_array(intval($array["proceso_id"]), $this->filtros["proceso-filtro"])){
            return false;
        }
        return true;
    }

    public function checkFiltroProceso(){
        $numProcesos=count($this->filtros["proceso-filtro"]);
        if($numProcesos >1) return true;

        return false;
    }

    public function getDatos()
    {
        return $this->datos;
    }

}

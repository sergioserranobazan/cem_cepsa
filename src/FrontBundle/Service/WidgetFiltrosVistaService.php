<?php

namespace FrontBundle\Service;

use AdminBundle\Service\FiltrosSesionService as FiltrosSesion;

/**
*
*/
class WidgetFiltrosVistaService extends ServiceBase
{

    function __construct(FiltrosSesion $fs)
    {
        $this->valorEnSesion = $fs->getFiltros()["fPrimeraVista"];
    }

    public function filtrarDatos($datos)
    {
        $this->datos["cabecera"] = "VISTAS";

        $this->datos["filtros"]["fPrimeraVista"] = array(
                1 => "Negocio / Canal / Proceso",
                2 => "Proceso / Negocio / Canal"
        );

        $this->datos["valorEnSesion"] = $this->valorEnSesion;
        // Añade los datos (la maqueta)
        $this->datos = array_merge($this->datos,$datos);
        return $this;
    }
}
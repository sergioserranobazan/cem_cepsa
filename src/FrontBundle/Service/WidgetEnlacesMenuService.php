<?php
namespace FrontBundle\Service;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use FrontBundle\Service\ServiceBase;

/**
*
*/
class WidgetEnlacesMenuService extends ServiceBase
{
    private $maquetas;

    function __construct(Container $c)
    {
        $repoMaquetas = $c->get('doctrine')
                            ->getRepository('CmsBundle:Maqueta');

        // Consigue todas las maquetas ordenadas y que tengan que aparecer en
        // el menú de la cabecera.
        $maquetas = $repoMaquetas->findBy(array(), array('orden' => 'asc'));
        $this->maquetas = $this->maquetasEnCabecera($maquetas);
    }

    private function maquetasEnCabecera($arrayMaquetas)
    {
        $maquetasEnCabecera = array_filter($arrayMaquetas, function($maqueta){
            return $maqueta->getVisibleCabecera() === true;
        });

        return $maquetasEnCabecera;
    }

    public function filtrarDatos($datos=null)
    {

        $slugRequest = $datos["maqueta"];

        $datosWidget = [];

        foreach ($this->maquetas as $maqueta) {
            $slugMaqueta = $maqueta->getSlug();
            $tituloMaqueta = $maqueta->getNombre();
            $datosWidget[] = array(
                // Para saber qué link ha de aparecer como seleccionado
                "activo" => $this->esMaquetaActiva($slugRequest, $slugMaqueta),
                // Icono de font-awesome que aparecerá en el menú 
                "icono" => $maqueta->getIcono(),
                // Enlace para el link
                "referencia" => $slugMaqueta,
                // Nombre que aparecerá para el link
                "titulo" => $tituloMaqueta
            );
        }

        $this->datos = $datosWidget;

        return $this;
    }

    /**
     * Realiza una comprobación de que la primera parte del slug del request
     * coincida con la primera parte del slug del elemento que aparece en
     * el menú de navegación
     */
    private function esMaquetaActiva($slugRequest, $slugMaqueta)
    {
        return explode("/", $slugRequest)[0] === explode("/", $slugMaqueta)[0];
    }
}
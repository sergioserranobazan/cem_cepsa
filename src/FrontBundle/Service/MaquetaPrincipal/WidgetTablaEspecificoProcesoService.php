<?php

namespace FrontBundle\Service\MaquetaPrincipal;

use AdminBundle\Service\FiltrosSesionService as FiltrosService;
use AdminBundle\Service\DQLBuilderService as DQLBuilder;
use FrontBundle\Service\ServiceBase;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;

use AdminBundle\Library\CemUtils;

class WidgetTablaEspecificoProcesoService extends ServiceBase
{
    private $filtros;

    function __construct(EntityManager $em, FiltrosService $fs, DQLBuilder $dql)
    {
        $this->filtros = $fs->getFiltros();

        $this->dql = $dql;

        $this->em = $em;

        $this->equivalencias = $fs->getValoresFiltroBloque(7);

        $this->arrayProcesos = $fs->getValoresEnBDFiltro("proceso");
    }

    public function filtrarDatos($datos)
    {

        $sqlResultados = $this->getDatosDeBD($this->filtros);

        $sqlResultados = $this->formateoResultados($sqlResultados);

        $this->datos = array(
            "calculosVal" => $sqlResultados,
            "filtros" => $this->filtros
        );
        // Añade los datos (la maqueta)
        $this->datos = array_merge($this->datos,$datos);
        return $this;
    }

    private function getDatosDeBD($filtros)
    {
        if($this->acumuladosIsEmpty($filtros)){
            return [];
        }

        $db = $this->em->getConnection();

        $sql1 =
        "
            SELECT
                'Total' as pregunta,
                count(DISTINCT e.id) as nEncuestas,
                AVG(IF(epregunta.cem_tipo_id = 4, er.valor, NULL)) as media
                FROM survey_encuesta_respuesta er
                    JOIN  `survey_encuesta_pregunta` epregunta ON er.encuesta_pregunta_id = epregunta.id
                    JOIN nosql_encuestas e ON e.encuesta_id = er.encuesta_id
                    JOIN  `admin_proceso` proceso ON epregunta.sub_bloque_id = proceso.id
                    JOIN `survey_encuesta_proceso` eproceso
                        ON eproceso.encuesta_id = e.encuesta_id
                        AND eproceso.proceso_id = proceso.id
        ";


        /**
         * ALERTA CG: Se está igualando el id del proceso con el id de subbloque
         * Esto sólo funciona porque en la base de datos los id coinciden, pero
         * no hay una relación real entre tablas
         */
        $sql2 =
        "
            SELECT CONCAT_WS(' -- ', proceso.nombre, epregunta.descripcion_completa) as pregunta,
                count(*) as nEncuestas,
                SUM(er.valor)/count(*) media
            FROM `survey_encuesta_respuesta` er
                JOIN `survey_encuesta_pregunta` epregunta ON er.encuesta_pregunta_id = epregunta.id
                JOIN `nosql_encuestas` e ON er.encuesta_id = e.encuesta_id
                JOIN `admin_proceso` proceso ON epregunta.sub_bloque_id = proceso.id
                JOIN `survey_encuesta_proceso` eproceso
                    ON eproceso.encuesta_id = e.encuesta_id
                    AND eproceso.proceso_id = proceso.id
            WHERE epregunta.cem_tipo_id = 4
        ";


        $sql1 = $this->dql->filtradoFecha(
            $this->filtros["temporalidad"],
            $sql1, $this->filtros["fec_ini"], $this->filtros["fec_fin"],
            true, "e"
        );

        $sql2 = $this->dql->filtradoFecha(
            $this->filtros["temporalidad"],
            $sql2, $this->filtros["fec_ini"], $this->filtros["fec_fin"],
            true, "e"
        );


        /* -------------  Filtros de encuestas generales ------------*/
        $filtrosEncuestas = [];

        foreach ($this->filtros as $nombreFiltro => $valorFiltro) {
            if(strpos($nombreFiltro, "-filtro") > 0){
                $nombreColumnaValido = str_replace("-filtro", "_id", $nombreFiltro);
                $filtrosEncuestas[$nombreColumnaValido] = $valorFiltro;
            }
        }

        foreach ($filtrosEncuestas as $nombreFiltro => $valor) {

            $tablaObjetivo = $nombreFiltro === "proceso_id"
                             ? "eproceso" // Excepción porque los procesos están
                                          // en otra tabla distinta a encuestas
                             : "e";

            $sql1 = $this->dql->sqlAddFiltro(
                $tablaObjetivo, $nombreFiltro, $valor, $sql1
            );

            $sql2 = $this->dql->sqlAddFiltro(
                $tablaObjetivo, $nombreFiltro, $valor, $sql2
            );
        }

        /* ----- Filtros de acumulados ----- */

        $filtrosAcumulados = [];

        foreach ($this->filtros as $nombreFiltro => $valorFiltro) {
            if(strpos($nombreFiltro, "-acumulados") > 0){
                $nombreColumnaValido = str_replace("-acumulados", "_id", $nombreFiltro);
                $filtrosAcumulados[$nombreColumnaValido] = $valorFiltro;
            }
        }

        foreach ($filtrosAcumulados as $nombreFiltro => $valor) {
            $tablaObjetivo = $nombreFiltro === "proceso_id"
                             ? "eproceso" // Excepción porque los procesos están
                                          // en otra tabla distinta a encuestas
                             : "e";

            $sql1 = $this->dql->sqlAddFiltro(
                $tablaObjetivo, $nombreFiltro, $valor, $sql1
            );

            $sql2 = $this->dql->sqlAddFiltro(
                $tablaObjetivo, $nombreFiltro, $valor, $sql2
            );
        }

        // Group By por el Nombre de la pregunta
        $sql2 .= " GROUP BY epregunta.nombre";

        // Union de las 2 query
        $sql = $sql1 . " UNION " . $sql2;

        return $db->executeQuery($sql);
    }

    public function formateoResultados($tablaResultados)
    {
        $resultadosFormateados = [];

        foreach ($tablaResultados as $row) {
            $row["media"] = CemUtils::formateoNumero($row["media"]);

            $resultadosFormateados[] = $row;
        }

        return $resultadosFormateados;
    }

    /**
     * Devuelve true si todos los filtros que acaban en `_acumulados` están
     * vacios
     * @param  array $filtros filtros de sesión de la maqueta
     * @return boolean
     */
    private function acumuladosIsEmpty($filtros)
    {
        $isEmpty = true;

        foreach ($filtros as $nombre => $valor) {
            if(strpos($nombre, "-acumulados") > 0 && !empty($valor)){
                $isEmpty = false;
            }
        }

        return $isEmpty;
    }

}

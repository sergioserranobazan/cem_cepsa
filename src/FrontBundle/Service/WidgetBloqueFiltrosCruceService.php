<?php

namespace FrontBundle\Service;

use AdminBundle\Service\FiltrosSesionService as FiltrosSesion;
use Doctrine\ORM\EntityManager;
/**
*
*/
class WidgetBloqueFiltrosCruceService extends WidgetBloqueFiltrosService
{

    function __construct(FiltrosSesion $fs, EntityManager $em, $bloqueId)
    {
        parent::__construct($fs,$em,$bloqueId);  

    }

    protected function getInformacionFiltros($coleccionFiltros)
    {
        $r = [];

        $cruceATicar=$this->fs->getFiltros()["cruce"];

        foreach ($coleccionFiltros as $filtroMaqueta) {

            $filtroNombreId = $filtroMaqueta["nombreId"];

            $tagFiltro = $filtroMaqueta["tag"];

            $r[$filtroNombreId] = array(
                "filtroDatos" =>
                    $this->getArrayValoresFiltro($filtroMaqueta)
            );

            if(!empty($tagFiltro))
                $r[$filtroNombreId]["tag"] = $tagFiltro;

            //Cruce
            if($cruceATicar==$filtroNombreId){
                $r[$filtroNombreId]["cruceChecked"] = 'checked="checked"';
            }
            
        }

        unset($r["cruce"]);

        return $r;
    }

}
<?php

namespace FrontBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class SwitcherDatosServices
{
    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /*public function getDatosMaqueta($slugMaqueta)
    {

        // Modificación realizada porque el nombre de los services en el yml no
        // puede contener el caracter "/".
        $slugMaqueta = str_replace(["/"], "_", $slugMaqueta);

        $nombreService = 'front.maqueta.'.$slugMaqueta.'Service';

        if($this->container->has($nombreService)){
            $service = $this->container->get($nombreService);
            return $service->filtrarEncuestas()->getEncuestasFiltradas();
        }

        $this->mensajeAlerta("Estás accediendo a una maqueta sin service"
            . " asociado para que obtenga datos de encuestas");
        return NULL;

    }*/

    public function getDatosWidget($nombreWidgetService, $datos)
    {
        $nombreService = 'front.widget.'. $nombreWidgetService . 'Service';

        $service = $this->container->get($nombreService);

        $datosFiltrados = $service->filtrarDatos($datos)->getDatos();

        return $datosFiltrados;
    }

    private function mensajeAlerta($mensaje)
    {
        echo("<h4 style='color:red'> Atención!: ". $mensaje . "</h2>");
    }
}

?>
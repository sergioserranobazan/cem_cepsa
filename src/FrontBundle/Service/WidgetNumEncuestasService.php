<?php

namespace FrontBundle\Service;

use AdminBundle\Service\FiltrosSesionService;
use Doctrine\ORM\EntityManager;
use AdminBundle\Service\DQLBuilderService;
use AdminBundle\Library\FechasUtils;

/**
*
*/
class WidgetNumEncuestasService extends ServiceBase
{
    function __construct(
        FiltrosSesionService $fs, EntityManager $em,
        DQLBuilderService $dqlBuilder
    ){
        $this->fs = $fs;
        $this->em = $em;
        $this->dql = $dqlBuilder;

        $this->filtros = $fs->getFiltros();

        $this->numEncuestas =
            $this->calcularNumEncuestas(
                $dqlBuilder,
                $this->filtros["temporalidad"]
            );
    }

    public function calcularNumEncuestas($dqlBuilder, $temporalidadId)
    {

        $conn = $this->em->getConnection();

        $sql = "
            SELECT count(*) nEncuestas
            FROM `nosql_encuestas` e
        ";

        if(empty($this->filtros["fec_ini"])){
            $fec_ini = new \DateTime();
        }else{
            $fec_ini = new \DateTime($this->filtros["fec_ini"]);
        }

        if(empty($this->filtros["fec_fin"])){
            $fec_fin = new \DateTime();
        }else{
            $fec_fin = new \DateTime($this->filtros["fec_fin"]);
        }

        $temporalidad = $this->filtros["temporalidad"];
        switch ($temporalidad) {
            case '0':
                $sql = $this->dql->sqlAddRangoFechas(
                    "e", $fec_ini, $fec_fin, $sql
                );
                break;
            case '1': case '2': case '3': case '4':
                $sql = $this->dql->sqlFiltrarPorTemporalidad(
                    "e", $temporalidad, $fec_ini, $sql
                );
                break;
            default:
                throw new \Exception(
                    "Error al procesar la temporalidad con valor $temporalidad"
                    , 1);

                break;
        }

        $filtrosEncuestas = [];

        foreach ($this->filtros as $nombreFiltro => $valorFiltro) {
            if(strpos($nombreFiltro, "-filtro") > 0){
                $nombreColumnaValido = str_replace("-filtro", "_id", $nombreFiltro);
                $filtrosEncuestas[$nombreColumnaValido] = $valorFiltro;
            }
        }

        foreach ($filtrosEncuestas as $nombreFiltro => $valor) {
            $sql = $this->dql->sqlAddFiltro(
                "e", $nombreFiltro, $valor, $sql
            );
        }

        $result = $conn->executeQuery($sql)
                       ->fetchAll();

        return $result[0]['nEncuestas'];
    }

    public function filtrarDatos($datos)
    {
        $maqueta = $datos["maqueta"];

        $this->datos["numEncuestas"] = $this->numEncuestas;
        $this->datos["temporalidad"] = $this->getTemporalidad($maqueta);
        $this->datos["periodo"] =
            $this->getPeriodo($maqueta, $this->datos["temporalidad"]);

        return $this;
    }

    private function getPeriodo($maqueta, $temporalidad){
        $filtrosMaqueta = $this->fs->getFiltrosMaqueta($maqueta);

        if(isset($filtrosMaqueta["fecha_activa"]))
            return $filtrosMaqueta["fecha_activa"];

        $fecha = new \DateTime($this->filtros["fec_ini"]);
        $formatoFecha = "";

        switch (mb_strtolower($temporalidad, 'UTF-8')) {
            case 'semana':
                return $fecha->format('W')."/".FechasUtils::getFuckingYear($fecha);
                break;
            case 'mes':
                $formatoFecha = "%b/%Y";
                break;
            case 'año':
                $formatoFecha = "%Y";
                break;
            case 'trimestre':
                return floor(($fecha->format('m') - 1) / 3 + 1)."/".$fecha->format('Y');
            default:
                return "";
                break;
        }

        $fecha = $fecha->getTimestamp();

        return ucfirst(strftime($formatoFecha, $fecha));
    }

    private function getTemporalidad($maqueta)
    {
        $filtrosMaqueta = $this->fs->getFiltrosMaqueta($maqueta);
        $temporalidadId = $filtrosMaqueta["temporalidad"];

        $nombreTemp =
            $this->fs
                 ->getValoresEnBDFiltro("temporalidad");

        if(isset($nombreTemp[$temporalidadId])){
            $nombreTemp = $nombreTemp[$temporalidadId];
        }else{
            $nombreTemp = "Periodo personalizado";
        }

        return ucfirst($nombreTemp);
    }
}
<?php

namespace FrontBundle\Service;

use AdminBundle\Service\FiltrosSesionService as FiltrosService;
use Doctrine\ORM\EntityManager as EntityManager;

class WidgetConfiguracionVistaService extends ServiceBase
{
    protected $filtros;

    function __construct(
        EntityManager $em
        ,FiltrosService $fs
        ,$estructuraPermutaciones=[
            "opcion_vista",
            "sub-bloque",
            "indicador"
        ]
    ){
        $this->em = $em;

        array_unshift($estructuraPermutaciones, "vista");

        $this->estructuraPermutaciones = $estructuraPermutaciones;

        $this->fs = $fs;

        $this->repoPermutaciones =
            $em->getRepository("AdminBundle:PermutacionesVistas");

        $this->repoVista = $em->getRepository("AdminBundle:Vista");

        $this->equivalencias["vistas"] = $this->fs->getValoresEnBDFiltro("vista");

        $this->permutacionesArray = $this->arrayPermutaciones($this->equivalencias["vistas"]);
    }

    /**
     * $configuraciones -> Array. Contiene los nombres de los "filtros" a usar
     * para la visualización de gráficos.
     * Los valores admitidos son: "opcionesVistas, indicadores y bloques"
     */
    public function filtrarDatos($datos)
    {
        $this->datos = array(
            "vistas"         =>  array(
                "optionDefault" => "Vista",
                "restoOptions"  => $this->equivalencias["vistas"],
                "restoOptionsDoctrine" => $this->getVistasDoctrine()
            ),

            "filtros" => $this->fs->getFiltros(),

            "dependientes" => array(),

            "maqueta" => $datos["maqueta"]
        );

        foreach ($this->estructuraPermutaciones as $filtroDependiente) {
            if($filtroDependiente == "vista") continue;

            $this->datos["dependientes"][$filtroDependiente] =
                $this->getFiltroDependiente($filtroDependiente);
        }

        return $this;
    }

    private function getFiltroDependiente($nombreFiltro)
    {
        $filtroDependiente = [];

        switch ($nombreFiltro) {
            case 'opcion_vista':
                $filtroDependiente = array(
                    "optionDefault" => "Valor Vista",
                    "valorCombinado" => true,
                    "restoOptions" => $this->getOpcionesVista(),
                );
                break;
            case 'sub-bloque':
                $filtroDependiente = array(
                    "optionDefault" => "Bloque Preguntas",
                    "valorCombinado" => false,
                    "restoOptions"  => $this->getOptions(
                        "SurveyBundle:EncuestaSubBloque",
                        array_search(
                            "sub-bloque",
                            $this->estructuraPermutaciones
                        )
                    )
                );
                break;
            case 'indicador':
                $filtroDependiente = array(
                    "optionDefault" => "Indicador",
                    "valorCombinado" => false,
                    "restoOptions"  => $this->getOptions(
                        "SurveyBundle:EncuestaPregunta",
                        array_search(
                            "indicador",
                            $this->estructuraPermutaciones
                        )
                    )
                );
                break;
            default:
                # code...
                break;
        }

        return $filtroDependiente;

    }

    protected function getVistas()
    {
        $doctrineVistas = $this->repoVista->findAll();

        $vistas = [];

        foreach ($this->equivalencias["vistas"] as $key => $nombre) {
            $vistas[$key] = $nombre;
        }

        return $vistas;
    }

    protected function arrayPermutaciones($arrayVistas)
    {
        $permutacionesDoctrine = $this->repoPermutaciones->findAll();

        $arrayPermutaciones = [];

        foreach ($permutacionesDoctrine as $permutacion) {

            foreach ($arrayVistas as $vista) {
                $arrayPermutacion = [];
                foreach ($this->estructuraPermutaciones as $elemento) {
                    $arrayPermutacion[$elemento] =
                        $this->getIdParametroPermutacion($vista,
                                                         $elemento,
                                                         $permutacion);
                }

                $arrayPermutaciones[] = $arrayPermutacion;
            }
        }

        return $arrayPermutaciones;
    }

    private function getIdParametroPermutacion($vista, $nombreCampo, $permutacion)
    {
        $idParametro = "";

        switch ($nombreCampo) {
            case 'vista':
                $idParametro = array_keys($this->equivalencias["vistas"], $vista)[0];
                break;
            case 'opcion_vista':
                // Sirve para obtener la función get para opciones vista
                $funcionGet = "get" . ucfirst($vista);

                $idParametro = $permutacion->$funcionGet()
                                           ->getId();
                break;
            case 'sub-bloque':
                $idParametro =
                    $permutacion->getSubBloque()->getId();
                break;
            case 'indicador':
                $idParametro =
                    $permutacion->getIndicador()->getId();
            default:
                # code...
                break;
        }

        return $idParametro;
    }


    protected function getOpcionesVista()
    {
        $opcionesVistas = [];

        foreach ($this->permutacionesArray as $permutacion) {
            $indicePermutacion = $permutacion["vista"] . "-"
                                 . $permutacion["opcion_vista"];

            if(!isset($opcionesVistas[$indicePermutacion])){
                $opcionesVistas[$indicePermutacion] = array(
                    "valoresPrevios" => $permutacion["vista"],
                    "texto" => $this->getTextoOpcionVista(
                        $permutacion["vista"] ,$permutacion["opcion_vista"]
                    )
                );
            }
        }

        return $opcionesVistas;
    }


    protected function getOptions($nombreRepositorio, $profundidad)
    {

        $options = [];

        /**
         * El orden refleja la dependencia descendiente que tienen los filtros.
         * es utilizado para poder automáticamente coger el valor que corresponda
         * al filtro sobre el que se está ejecutando la función.
         */
        $estructuraPermutaciones = $this->estructuraPermutaciones;


        foreach ($this->permutacionesArray as $permutacion) {

            $indiceOption = $permutacion[
                /**
                 * Se utiliza la profundidad para obtener el valor usando de
                 * ayuda la variable $estructuraPermutaciones.
                 */
                $estructuraPermutaciones[$profundidad]
            ];

            /*
             * Variable donde se establece cuales son las combinaciones de los
             * filtros anteriores que corresponde a esta permutación. Por
             * ejemplo: 1-1-1 significaría que el valor actual ($indiceOption)
             * está con la permutación de vita=1, opc_vista=1 y subbloque=1.
             * Esto está hecho así porque era necesario que cada filtro se
             * guardara la información de todas las combinaciones que permitían
             * la selección del mismo. De esta forma se puede habilitar el
             * filtrado de options en el lado del cliente
            */
            $valorAcumulado = array_slice($permutacion, 0, $profundidad);
            $valorAcumulado = implode("-", $valorAcumulado);


            /**
             * Si el valor del elemento no ha sido guardado en el array, creamos
             * un nuevo array para dicho índice inicializando los valoresPrevios
             * como un array de 1 sólo elemento.
             */
            if(!isset($options[$indiceOption])){
                $options[$indiceOption] = array(
                    "valoresPrevios" => [$valorAcumulado],
                    // El texto que aparecerá de cara al usuario para el option
                    "texto" => $this->getNombreOption($nombreRepositorio,$indiceOption)
                );

            /**
             * En caso de que ya se hubiera instanciado un elemento en esa
             * posición.
             */
            }else
            /**
             * Se comprueba que el valorAcumulado no haya sido insertado para
             * ese elemento en una iteración previa y se inserta en el array
             * de valores previos
             */
            if(!in_array($valorAcumulado,
                              $options[$indiceOption]["valoresPrevios"])
            ){

                $options[$indiceOption]["valoresPrevios"][] =
                    $valorAcumulado;
            } // Si ya está introducido, no hacemos nada
        }

        return $options;
    }

    private function getNombreOption($nombreRepositorio,$indiceOption){
        $nombre="";
        $repo = $this->em
                     ->getRepository($nombreRepositorio);

        switch ($nombreRepositorio) {
            case 'SurveyBundle:EncuestaPregunta':
                if($indiceOption == 1){ $nombre = "Emotional Index";break; }//Sentimiento
                if($indiceOption == 2){ $nombre = "NPS";break; }//Recomendación
                $nombre = $repo->findOneById($indiceOption)
                            ->getDescripcion();
                break;

            default:
                $nombre = $repo->findOneById($indiceOption)
                                    ->getNombre();
                break;
        }
        return $nombre;
    }

    protected function getTextoOpcionVista($idVista, $idOpcionVista){

        $entidadDoctrine = $this->repoVista
                                ->findOneById($idVista)
                                ->getFiltro()
                                ->getEntidadDoctrine();

        return $this->em->getRepository($entidadDoctrine)
                    ->findOneById($idOpcionVista)
                    ->getNombre();
    }

    private function getVistasDoctrine(){

        $vistas = $this->em->getRepository("AdminBundle:Vista")->findAll();
        $array = [];
        foreach ($vistas as $value) {
            $array[$value->getId()]['nombre']=$value->getNombre();
            $array[$value->getId()]['nombreDoctrine']=$value->getFiltro()->getNombreFiltro();
        }
        return $array;
    }
}
<?php

namespace FrontBundle\Service;

use AdminBundle\Service\FiltrosSesionService as FiltrosSesion;
use Doctrine\ORM\EntityManager;
/**
*
*/
class WidgetBloqueFiltrosService extends ServiceBase
{

    function __construct(FiltrosSesion $fs, EntityManager $em, $bloqueId)
    {
        $this->fs = $fs;

        $this->em = $em;

        $this->bloqueRepo = $em->getRepository("AdminBundle:FiltrosBloque");

        $this->bloque = $this->bloqueRepo
                             ->findOneById($bloqueId);

        $this->filtrosBloque = $fs->getValoresFiltroBloque($bloqueId);

    }

    public function filtrarDatos($datos)
    {
        $this->datos["cabecera"] = $this->bloque->getCabecera();


        $this->datos["filtros"] =
            $this->getInformacionFiltros($this->filtrosBloque);

        $this->datos["valoresSeleccionados"] =
            $this->fs->getFiltros();

        $this->datos["filtrosADeshabilitar"] =
            $this->fs->getValoresFijosGrupoUsuario();

        return $this;
    }

    protected function getInformacionFiltros($coleccionFiltros)
    {
        $r = [];

        foreach ($coleccionFiltros as $filtroMaqueta) {
            $filtroNombreId = $filtroMaqueta["nombreId"];
            $tagFiltro = $filtroMaqueta["tag"];

            $r[$filtroNombreId] = array(
                "filtroDatos" =>
                    $this->getArrayValoresFiltro($filtroMaqueta)
            );

            if(!empty($tagFiltro))
                $r[$filtroNombreId]["tag"] = $tagFiltro;
        }

        return $r;
    }

    protected function getArrayValoresFiltro($filtroMaqueta)
    {

        $registrosFiltro = $filtroMaqueta["valores"];

        // Obtención del valor que ha de aparecer como seleccionado al cargar
        // la página
        $nombreFiltro = $filtroMaqueta["nombreId"];

        $valorSeleccionado = $this->fs->getFiltros()[$nombreFiltro];

        //Convierto el valor o valores seleccionados a un array
        $valoresSeleccionados = [];
        if(is_array($valorSeleccionado)){
            $valoresSeleccionados=$valorSeleccionado;
        }else{
            $valoresSeleccionados[]=$valorSeleccionado;
        }

        // Creación del array de valores para el filtro
        $arrayValoresFiltro = [];
        foreach ($registrosFiltro as $registroId => $registroValor) {

            $arrayValoresFiltro["valores"][$registroId] =
                array(
                    "texto" => $registroValor,
                    "selected" => in_array($registroId,$valoresSeleccionados)
                );
        }

        return $arrayValoresFiltro;
    }
}

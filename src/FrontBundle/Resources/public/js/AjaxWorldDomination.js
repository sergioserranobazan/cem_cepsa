var ajaxUtils = (function(){

  /**
   * Devuelve un objeto con todos los id y valores de los filtros presentes
   * en la maqueta.
   * Para poder ejecutar esta función, es necesario que los input, select,
   * etc. tengan una clase en común (se propone filtro-maqueta).
   * Adicionalmente, los input de fecha deberán tener el atributo
   * data-type="fecha".
   * @param  {string} claseComunFiltros
   *         [string con la clase que comparten los filtros]
   * @return {object} [objeto con la información de los filtros]
   */
  function getFiltros(claseComunFiltros){
    var datos = {};
    $("." + claseComunFiltros).each(function(){
        $filtro = $(this);
        nombreFiltro = $filtro.attr("id");

        datoFiltro = $filtro.val() || "";

        if($filtro.attr("data-type") === "fecha"){
            datoFiltro = datoFiltro
                                .split("/")
                                .reverse()
                                .join("-");
        }

        datos[nombreFiltro] = datoFiltro;
    });

    return datos;
  }


  /**
   * Devuelve el slug de la maqueta en la que estamos
   * @return {string} El Slug de front hacia delante incluyendo `/`
   */
  function getSlug(){
    var reSlug = new RegExp("front(/.*)");

    var slug = reSlug.exec(window.location.href)[1];

    if(slug.length == 1) slug = "/principal"; // Default slug

    return slug;
  }

  // API pública
  return {
    getFiltros: getFiltros,
    getSlug: getSlug
  }
})();

function ajaxWorldDomination(datos, url, targets){
    /**
     * Objeto con toda la información que va a ser
     * mandada por POST al servidor
     */
    var datosPeticion = datos || {};

    // Url al controlador AJAX
    var postUrl = url || "";

    /**
     * Cuales son los widget que van a ser recargados
     * ahora mismo se usa sólo para saber a qué divs hay
     * que ponerles la animación de spinning
     */
    var targets = targets || [];

    /**
     * Inicialización de todas las variables del módulo (hace lo mismo que el
     * constructor)
     */
    function init(datos, url, arrayTargets){
        datosPeticion = datos;
        postUrl = url;
        targets = arrayTargets || [""];
    }

    /**
     * Setea los datos que se van a mandar en la petición ajax
     * @param {object} datos
     */
    function setDatosPeticion(datos){
        if(datos === undefined)
            throw "Has de pasar un objeto";

        datosPeticion = datos;
    }

    /**
     * URL para el envío de la petición AJAX
     * @param {object} url
     */
    function setPostUrl(url){
        postUrl = url;
    }

    /**
     * Setea los widgets que tienen que ser recargados
     * @param {array} arrayTargets array de strings con el id "sin `#`" de los
     * widgets que van a ser recargados.
     */
    function setTargets(arrayTargets){
        if(typeof arrayTargets !== "object") throw "setTargets espera un array"
        targets = arrayTargets;
    }

    /**
     * Devuelve los targets que han sido seteados.
     * @return {array}
     */
    function getTargets(){
        return targets;
    }

    /**
     * Función privada que comprueba que todos los valores necesarios han sido
     * seteados correctamente
     * @return {boolean} True si se puede proceder con la llamada AJAX y False
     *                   en caso contrario
     */
    function configuracionOk(){
        var datosOk = datosPeticion !== undefined
                      && datosPeticion !== null;
                      //&& Object.keys(datosPeticion).length > 0;

        var postUrlOk = postUrl !== null
                      && postUrl !== undefined
                      && postUrl.length !== 0;

        var targetsOk = targets.length !== 0 && typeof targets === "object";

        // Logs de errores
        if(!datosOk){
            console.error("Los datos en petición no son correctos");
        }

        if(!postUrlOk){
            console.error("La url no es válida");
        }

        if(!targetsOk){
            console.error("Los targets no son correctos")
        }


        return datosOk && postUrlOk && targetsOk;
    }

    /**
     * CallBack para llamada AJAX. Espera recibir un array de HTMLs que a través
     * de su id los colocará en el DOM.
     * @param  {[type]} data [description]
     * @return {[type]}      [description]
     */
    function tratarHTML(data){
        try{
            data.forEach(function(widgetHtmlString){
                $nodoDom = $(widgetHtmlString);
                idNodo = $nodoDom.attr("id");

                $("#" + idNodo).replaceWith($nodoDom);
            });
        }catch(err){
            // Información de debug
            console.info("Información mandada al servidor: ");
            console.info(datosPeticion);

            console.info("Recibida información de tipo: " + typeof data)
            console.info(data);

            console.debug(err);
        }
    }

    /**
     * Ejecuta la llamada ajax para cada uno de los target de manera paralela.
     * (Siempre que se hayan seteado los target previamente)
     */
    function recargaAjax(){
        // ------- Comprobacion de premisas -----------
        if(!configuracionOk()){
            console.info("Datos en peticion: ");
            console.info(datosPeticion);
            console.info("PostUrl: ");
            console.info(postUrl);
            console.info("Targets: ")
            console.info(targets)
            throw "Configuracion de ajaxWorldDomination incorrecta.";
        }

        // ------ Lógica de recarga Ajax -----

        // Spinners, a rodar!
        targets.forEach(function(target){
            var selector = target !== ""
                           ? "#" + target + " " + ".has-spinner"
                           : ".has-spinner";

            $(selector).addClass("spinning");
        })

        // Envío de la petición POST
        targets.forEach(function(target){
            var data = datosPeticion;
            data.target = [target];
            $.ajax({
                type: "POST",
                url: postUrl,
                async: true,
                data: data,
            }).done(tratarHTML);
        });
    }

    return {
        init: init,
        setDatosPeticion: setDatosPeticion,
        setTargets: setTargets,
        setPostUrl: setPostUrl,
        recargaAjax: recargaAjax,
        getTargets: getTargets
    }
};
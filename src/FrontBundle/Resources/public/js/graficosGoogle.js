var graficosGoogle = (function () {

    /**
     * Funcion que devuelve el array con las opciones de los graficos en funcion de los parametros pasados
     */
    function getOpcionesPorTipo(tipo, colores, linea, stack) {
        if (linea && colores.length >1) {//Color de la linea si existe
            colores.push("1764FF");
        }
        opciones = [];
        //opciones['colores'] = null;
        opciones['anchoGrafico'] = "100%";
        opciones['annotationsOutside'] = false;
        if(!stack){opciones['annotationsOutside'] = true;}
        
        switch (tipo) {
            case 'SAT_BARRAS':
            case 'VAL_BARRAS':
            case 'REC_BARRAS':
                opciones['stacked'] = stack;
                opciones['minValue'] = [0,0];
                opciones['maxValue'] = [100,10];
                if (!stack) {
                    opciones['anchoGrafico'] = "92%";
                    opciones['colores'] = ['FED102'];
                } else {
                    opciones['colores'] = colores;
                }
                opciones['ticks'] = [[0, 25, 50, 75, 100], [0, 2.5, 5.0, 7.5, 10]];
                opciones['textoLeyenda'] = 11;
                break;
            case 'NPS_BARRAS':
                opciones['stacked'] = stack;
                if (!stack) {
                    opciones['anchoGrafico'] = "92%";
                    opciones['minValue'] = [-100];
                    opciones['maxValue'] = [100];
                    opciones['ticks'] = [[-100, -50, 0, 50, 100]];
                    opciones['colores'] = ['FED102'];
                } else {
                    opciones['minValue'] = [0, -100];
                    opciones['maxValue'] = [100, 100];
                    opciones['ticks'] = [[0, 25, 50, 75, 100], [0, 2.5, 5.0, 7.5, 10]];
                    opciones['colores'] = colores;
                }
                opciones['textoLeyenda'] = 11;
                break;
            case 'EI_BARRAS':
                opciones['stacked'] = stack;
                opciones['minValue'] = [0,0];
                opciones['maxValue'] = [10,10];
                if (!stack) {
                    opciones['anchoGrafico'] = "92%";
                    opciones['colores'] = ['FED102'];
                } else {
                    opciones['colores'] = colores;
                }
                opciones['ticks'] = [[0, 2.5, 5, 7.5, 10], [0, 2.5, 5.0, 7.5, 10]];
                opciones['textoLeyenda'] = 11;
                break;            
            case 'DIC_BARRAS':
                opciones['stacked'] = stack;
                opciones['minValue'] = [0];
                opciones['maxValue'] = [100];
                opciones['colores'] = colores;
                opciones['ticks'] = [[0, 25, 50, 75, 100]];
                opciones['textoLeyenda'] = 10;
                break;
            case 'PRO_BARRAS':
                opciones['stacked'] = stack;
                opciones['minValue'] = [0];
                opciones['maxValue'] = [100];
                opciones['colores'] = colores;
                opciones['ticks'] = [[0, 25, 50, 75, 100]];
                opciones['textoLeyenda'] = 10;
                break;
            case 'MOT_BARRAS':
                opciones['stacked'] = stack;
                opciones['minValue'] = [0];
                opciones['maxValue'] = [100];
                opciones['colores'] = colores;
                opciones['ticks'] = [[0, 25, 50, 75, 100]];
                opciones['textoLeyenda'] = 10;
                break;

            //lineas
            case 'SAT_LINEAS':
            case 'VAL_LINEAS':
            case 'REC_LINEAS':
                opciones['stacked'] = false;
                opciones['anchoGrafico'] = "92%";
                opciones['minValue'] = [0];
                opciones['maxValue'] = [10];
                opciones['colores'] = colores;
                opciones['ticks'] = [[0, 2.5, 5, 7.5, 10]];
                opciones['textoLeyenda'] = 11;
                break;
            case 'NPS_LINEAS':
                opciones['stacked'] = false;
                opciones['anchoGrafico'] = "92%";
                opciones['minValue'] = [-100];
                opciones['maxValue'] = [100];
                opciones['colores'] = colores;
                opciones['ticks'] = [[-100, -50, 0, 50, 100]];
                opciones['textoLeyenda'] = 11;
                break;
            case 'EI_LINEAS':
                opciones['stacked'] = false;
                opciones['anchoGrafico'] = "92%";
                opciones['minValue'] = [0];
                opciones['maxValue'] = [10];
                opciones['colores'] = colores;
                opciones['ticks'] = [[0, 2.5, 5, 7.5, 10]];
                opciones['textoLeyenda'] = 11;
                break; 
            case 'MOT_LINEAS':
                opciones['stacked'] = false;
                //opciones['anchoGrafico'] = "92%";
                opciones['minValue'] = [0];
                //opciones['vAxes'] = logScale: false };
                opciones['maxValue'] = [100];
                opciones['ticks'] = [[0, 25, 50, 75, 100]];
                opciones['colores'] = colores;
                opciones['textoLeyenda'] = 11;
                break;
            default:
                break;
        }
        if(opciones["colores"].length==0){//Colores por defecto
            opciones['colores']= ["red","blue","green"];
        }
        return opciones;
    }

    /**
     * Datos -> los datos que recoge del controlador
     * targetId -> La capa donde se va a mostrar el grafico
     * modalidad -> Los ejes del grafico
     * Función closure para la lógica de generación de un gráfico tipo Barras
     */
    function graficoBarras(datos, targetId, tipo, colores, linea, stack) {

        return function logicaGraficoBarras() {
            var datosJSONTotal = datos;
            var datosJSONGrafico = datosJSONTotal['datosGrafico'];
            
            var opcionesPorTipo = getOpcionesPorTipo(tipo + '_BARRAS', colores, linea, stack);
        //console.log(datosJSONGrafico);
            var datosGrafico = google.visualization.arrayToDataTable(datosJSONGrafico);
        //console.log(datosGrafico);
            seriesGrafico = [];
            for (var i = 0; i < Math.floor((datosJSONGrafico[0].length - 1) / 2); i++) {
                seriesGrafico[i] = {'type': 'bars', 'targetAxisIndex': 0};
            }
            if (linea) {
                seriesGrafico.push({'type': 'line', 'targetAxisIndex': 1/*,'curveType': 'function'*/});
            }

            var opciones = {
                isStacked: opcionesPorTipo['stacked'],
                bar: {
                    groupWidth: '45%'
                },
                tooltip: {trigger: 'selection' /*,isHtml: true*/},
                backgroundColor: {fill: 'none'},
                /*curveType: 'function',*/
                chartArea: {width: opcionesPorTipo['anchoGrafico'], left: "5%",right:"2%", height: "65%", top: 25},
                seriesType: "bars",
                //series: seriesGrafico,
                hAxis: {
                    allowContainerBoundaryTextCufoff: true, // false las recorta, true las esconde ?????
                    textPosition: 'out', // out por defecto
                    slantedText: false, // true, muestra el texto inclinado
                    maxAlternation: 1,
                    showTextEvery: 1,
                    textStyle: {
                        fontSize: 11
                    }
                },
                vAxes: {
                    0: {logScale: false, /*format: 'percent',*/ minValue: opcionesPorTipo['minValue'][0], maxValue: opcionesPorTipo['maxValue'][0], ticks: opcionesPorTipo['ticks'][0]},
                    1: {logScale: false, /*format: 'percent',*/ minValue: opcionesPorTipo['minValue'][1], maxValue: opcionesPorTipo['maxValue'][1], ticks: opcionesPorTipo['ticks'][1], title: tipo}
                },
                //vAxis: {format:'decimal'},
                legend: {position: 'bottom', textStyle: {fontSize: opcionesPorTipo['textoLeyenda']}},
                pointSize: 5,
                height: 260, //div_alto,FA9F16#FA9F16
                //COLORES: rojo,naranja,verde,verde oscuro, azul,color amarillo prosegur '#FED102'
                colors: opcionesPorTipo['colores'],
                displayAnnotations: true,
                annotations: {
                    textStyle: {fontSize: 12},
                    alwaysOutside: opcionesPorTipo['annotationsOutside'],
                    boxStyle: {
                        // Color of the box outline.
                        stroke: '#ccc',
                        // Thickness of the box outline.
                        strokeWidth: 1,
                        // x-radius of the corner curvature.
                        rx: 4,
                        // y-radius of the corner curvature.
                        ry: 5,
                        // Attributes for linear gradient fill.
                        gradient: {
                            // Start color for gradient.
                            color1: '#000000',
                            // Finish color for gradient.
                            color2: '#808080',
                            // Where on the boundary to start and
                            // end the color1/color2 gradient,
                            // relative to the upper left corner
                            // of the boundary.
                            x1: '0%', y1: '0%',
                            x2: '0%', y2: '100%',
                            // If true, the boundary for x1,
                            // y1, x2, and y2 is the box. If
                            // false, it's the entire chart.
                            useObjectBoundingBoxUnits: true
                        }
                    }
                },
                animation: {
                    'duration': 500,
                    'easing': 'Out',
                    'startup': true
                }
            };

            var targetIdGrafico = targetId+"-grafico";
            var chart = new google.visualization.ColumnChart(document.getElementById(targetIdGrafico));

            if(typeof datosJSONTotal.indices!="undefined"){
                // Evento para pintar los datos sobre los stacks
                google.visualization.events.addListener(chart, 'ready',function(){                
                    if(datosGrafico.getNumberOfRows()>0){
                        var cli = chart.getChartLayoutInterface();
                        var anchocolumna = cli.getBoundingBox('bar#0#0').width;
                        anchocolumna /= 2;
                        for (var i=0; i<datosGrafico.getNumberOfRows(); i++) {
                            $("#"+targetIdGrafico).siblings("#overlay"+tipo+i).remove(); //Los elimino antes de repintarlos (por el callback)
                            $("#"+targetIdGrafico).prepend( '<div id="overlay'+tipo+i+'" class="overlay-marker"><span class="overlay-indicador">'+datosJSONTotal.indices[i]+'&nbsp;</span></div>' );
                            var diferencia = datosJSONTotal.diferencias[i];
                            if(diferencia != null){
                                //Pinto el valor de la diferencia
                                $('#overlay'+tipo+i).append(diferencia.toString().replace('.',','));
                            }
                            //Pinto las flechitas               
                            if (diferencia < 0) {
                                $('#overlay'+tipo+i).append('<span class="glyphicon glyphicon-arrow-down"></span>');
                                document.querySelector('#overlay'+tipo+i).style.color = "#CF002A";
                            } else if(diferencia >= 0 && diferencia!=null) {
                                $('#overlay'+tipo+i).append('<span class="glyphicon glyphicon-arrow-up"></span>');
                                document.querySelector('#overlay'+tipo+i).style.color = "#80D50E";
                            }
                            document.querySelector('#overlay'+tipo+i).style.position = "absolute";
                            //document.querySelector('#overlay'+tipo+i).style.top = "0px";
                            var anchoDIV=$('#overlay'+tipo+i).width();
                            document.querySelector('#overlay'+tipo+i).style.left = Math.floor(cli.getXLocation(i)-anchoDIV/2) + "px";
                        }
                    }
                });
            }

            // ATENCION: Para hacer funcionar el evento doble click sobre el grafico
            // descomentar el include en el twig  -> WidgetGraficoBarras.html.twig
            // y poner a true la siguiente variable

            var datatable = false;

            if(datatable){
                var firstClick = 0;
                var secondClick = 0;
                //Evento click/dobleClick
                google.visualization.events.addListener(chart, 'click', function (e) {
                    if (!stack) { 
                        getTooltip(chart,e); //Muestra el tooltip clickeando en un nombre del 
                                                //eje X para un grafico stack = false
                    }
                    var date = new Date();
                    var millis = date.getTime();
                    if (millis - firstClick < 250) {// Evento DobleClick      
                        firstClick = 0;
                        secondClick = millis;
                        // Comprueba el evento y si es valido lanza la llamada AJAX
                        compruebaEventoGetModal(e,datos,targetId,tipo,colores);
                    } else {
                        firstClick = millis;
                        secondClick = 0;
                    }
                });
            }

            //PINTO
            chart.draw(datosGrafico, opciones);
            //Elimino el spinning
            //removeSpinner(targetId);
        };

    }

    //Muestra el tooltip clicando en el eje de abcisas (eje X)
    //Valido para un grafico con stack = false;
    function getTooltip(chart,e){
        var parts = e.targetID.split('#');
        var axisX = e.targetID.substring(0, 1);
        if (parts.indexOf('label') >= 0 && axisX == 'h') {
            var idx = parts[3];
            var selected = chart.getSelection();
            row = -1;
            if (typeof selected[0] !== 'undefined')
            {
                row = selected[0].row;
            }
            if (row == -1 || row != idx)
            {
                chart.setSelection([{"row": idx, "column": 1}]);
            } else {
                chart.setSelection([]);
                row = -1;
            }
        }
    }

    // Comprueba el evento y si es valido lanza la llamada AJAX
    function compruebaEventoGetModal(e,datos,targetId,tipo,colores){
        var datosJSONGrafico = datos['datosGrafico'];
        var arrayLabels = datos['arrayLabels'];
        var parts = e.targetID.split('#');
        //console.log(parts);
        var axisX = e.targetID.substring(0, 1);
        var nombreStack = "";
        //var idSubStack = "-1";
        var stack = "";//id del stack(columna) empezando en 0.
        var subStack = -1;//id del subStack empezando en 0;
        var arrayDatosBeforeAjax = new Array();
        
        // Si el doble click es sobre el cruce o sobre el stack.
        if ( (parts.indexOf('label') >= 0 && axisX == 'h')  //Etiqueta Eje X
            || parts.indexOf('bar') >= 0                    //Stack
            || parts.indexOf('tooltip') >= 0                //Tooltip
            || parts.indexOf('annotationtext') >= 0 )       //Annotation   
        {
            if(parts.indexOf('label') >= 0){//Caso stack
                stack = parts[3];
                //subStack = -1;
            }else{//Caso subStack (tb tooltip y annotation)
                stack = parts[2];
                if(tipo!="EI"){//Para este grafico no se puede filtrar por el subStack
                    subStack = parts[1];
                    arrayDatosBeforeAjax['nombreLabel'] = arrayLabels[subStack];
                    arrayDatosBeforeAjax['colorLabel'] = colores[subStack];
                }
            }
            var keyArrayJSON = parseInt(stack)+1;   //Sumo 1 por compatibilidad con
                                                    //los datos del JSON 

            //DATOS BEFORE AJAX (label,colorLabel,tituloGrafico)
            //var nombreSubStack = datosJSONGrafico[0][(keyArrayJSON*3)+1].label; // Nombre del label (DETRACTORES,...)        
            arrayDatosBeforeAjax['tituloGrafico'] = $('#'+targetId+'-cabecera').attr('title');           

            //PARAMETROS AJAX
            var tipoCEM = tipo;
            //var cruce = $('#'+targetId).attr('data-cruce');
            //var idPregunta = $("#indicador option:selected").attr("value");
            var nombreStack = datosJSONGrafico[keyArrayJSON][0];
            // El slug se podria coger por javascript, ahora lo extraigo de los datos.maqueta
            var slugM = $('#'+targetId).attr('data-slugM');
            // Llamada Ajax                      
            getAjaxDataModal(arrayDatosBeforeAjax,tipoCEM,nombreStack,subStack,slugM);
        }
    }

    function getAjaxDataModal(arrayDatosBeforeAjax,tipoCEM,nombreStack,subStack,slugM) {
        // Llamada a una funcion en el twig del grafico
        // Para configurar las configuraciones del datatable y realizar la llamada AJAX.
        // TipoCEM y slugM no serían estrictamente necesarios
        configDataTable(arrayDatosBeforeAjax,tipoCEM,nombreStack,subStack,slugM);

    }

    /**
     * Función closure para la lógica de generación de un gráfico tipo Lineas
     */
    function graficoLineas(datos, targetId, tipo, colores) {

        var opcionesPorTipo = getOpcionesPorTipo(tipo + '_LINEAS', colores, false, false);
        return function logicaGraficoLineas() {
            var datosJSON = datos;
            var datosJSONGrafico = datos["datosGrafico"];
            var datosGrafico = google.visualization.arrayToDataTable(datosJSONGrafico);

            var options1 = {
                tooltip: {trigger: 'selection'},
                backgroundColor: {fill: 'none'},
                //curveType: 'function',
                chartArea: {width: opcionesPorTipo['anchoGrafico'], left: "5%",right:"2%", height: "65%", top: 25},
                seriesType: "line",
                vAxes: {
                    0: {logScale: false, /*format: 'percent',*/ minValue: opcionesPorTipo['minValue'][0], maxValue: opcionesPorTipo['maxValue'][0], ticks: opcionesPorTipo['ticks'][0]}
                },
                //vAxis: {format:'decimal'},
                height: 260,
                hAxis: {
                    allowContainerBoundaryTextCufoff: true, // false las recorta, true las esconde ?????
                    textPosition: 'out', // out por defecto
                    slantedText: false, // true, muestra el texto inclinado
                    maxAlternation: 1,
                    showTextEvery: 1,
                    textStyle: {
                        fontSize: 11
                    }
                },
                legend: {position: 'bottom', textStyle: {fontSize: opcionesPorTipo['textoLeyenda']}},
                pointSize: 5,
                //height: 200, //div_alto,
                colors: opcionesPorTipo['colores']
                /*,lineWidth: 1*/
                /*,trendlines: {
                      lineWidth: 1,
                      opacity: 0.3,
                      showR2: true,
                      visibleInLegend: true
                }*/
            };
            var targetIdGrafico = targetId+"-grafico";
            var chart = new google.visualization.LineChart(document.getElementById(targetIdGrafico));
            chart.draw(datosGrafico, options1);

            //Elimino el spinning
            //removeSpinner(targetId);
        };
    }

    // Para eliminar el spinner del grafico una vez dibujado.
    // Sin uso actualmente
    function removeSpinner(targetId){
        var selector = targetId !== ""
                       ? "#" + targetId + " " + ".has-spinner"
                       : ".has-spinner";
        $(selector).removeClass("spinning");
    }
    // Para clonar un objeto javascript
    // Sin uso actualmente
    function clone(obj) {
        if (null == obj || "object" != typeof obj) return obj;
        var copy = obj.constructor();
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
        }
        return copy;
    }
    /**
     * Función genérica para registrar la función de google chars que le llega
     * como call y el evento para cuando se redimensiona la ventana
     */
    function registrarGrafico(call) {
        google.charts.setOnLoadCallback(call);
        $(window).on("resize", call);
    }
    //BARRAS
    function registrarGraficoBarras(datos, targetId, tipo, colores, linea, stack) {
        funcionGrafico = graficoBarras(datos, targetId, tipo, colores, linea, stack);
        registrarGrafico(funcionGrafico);
    }
    //LINEAS
    function registrarGraficoLineas(datos, targetId, tipo, colores) {
        funcionGrafico = graficoLineas(datos, targetId, tipo, colores);
        registrarGrafico(funcionGrafico);
    }

    function registrarGG(datos, targetId){

        if(datos["datosGrafico"].length==0){
            throw "Metele unos datos al grafico.";
            return false;
        }
        switch(datos["tipoGrafico"]){
            case "BARRAS":
                registrarGraficoBarras(datos, targetId, datos["tipo"], datos["colores"], datos["linea"], datos["stack"]);
                break;
            case "LINEAS":
                registrarGraficoLineas(datos, targetId, datos["tipo"], datos["colores"]);
                break;
        }
    }

    // API y BLAS
    // !!!!!!!!!!!!!!!!!!!!!!!!
    return {
        registrarGG: registrarGG
    };

})();

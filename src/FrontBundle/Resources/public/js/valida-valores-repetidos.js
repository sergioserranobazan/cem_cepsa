var validaValoresRepetidos = (function(){

    var $bloqueObjetivo = $('');

    var selectorSelects = ""; // Variable helper para coger más fácilmente los
                                // elementos select que hay dentro del bloque

    var mensajeError = ""; // HtmlString que indica el mensaje que se mostrará
                            // al usuario cuando no se cumpla la condición de
                            // que todos los select del bloque configurado tengan
                            // valores distintos.

    var $coleccionSelects;

    /**
     * Inicialización del módulo.
     * El argumento datos es un objeto con 2 posibles parámetros:
     *      -- bloqueId: ID del div principal dentro del cual estará la
                            estructura que necesita el módulo para su
                            funcionamiento.
     *      -- mensajeError: Valor que se guardará en la variable del módulo
                                del mismo nombre.
     */
    function inicializa(datos){

        // Inicialización de las variables del módulo
        $bloqueObjetivo = $(datos.bloqueId);
        selectorSelects = datos.bloqueId + " select";
        mensajeError = datos.mensajeError;

        // Creación del label que contendrá el error
        creaErrorLabel($bloqueObjetivo);

        $coleccionSelects = $(selectorSelects);
        registrarOnChangeListeners($coleccionSelects);

        // Comprobación inicial de que los filtros tengan valores distintos
        compruebaFiltros();
        toogleMensajeError();
    }

    function registrarOnChangeListeners($coleccionSelectsJquery)
    {
        $coleccionSelectsJquery.each(function(){
            $(this).change(function(e){
                compruebaFiltros();
                toogleMensajeError();
            });
        })
    }

    function compruebaFiltros(){
        $(selectorSelects).each(function(index, el){
            var $elemento = $(this);
            var valorElemento = $elemento.val();
            var valorDuplicado = false;

            $coleccionSelects.each(function(){
                if($(this).val() == valorElemento
                   && $(this).attr("id") !== $elemento.attr("id"))
                {
                    valorDuplicado = true;
                }
            })

            if(valorDuplicado){
                $(this).parent().addClass("has-error");
            }

            if(!valorDuplicado){
                $(this).parent().removeClass("has-error");
            }
        });

    };

    function toogleMensajeError()
    {

        if($bloqueObjetivo.find(".has-error").length > 0){
            $bloqueObjetivo.find(".alert").show();
        }else{ // Si los errores son 0. Se presupone que `find` de jQuery no
                // devuelve nunca un número negativo
            $bloqueObjetivo.find(".alert").hide();
        }

    }


    function creaErrorLabel($bloque){
        $bloque.find(".cuerpo").append(
            $("<div class='alert alert-danger col-xs-12' hidden>" +
                mensajeError + "</div>")
        );
    };

    return {
        inicializa: inicializa
    }
})();


//Devuelve Formato YYYY/MM/DD (Formato ISO 8601 extendido con '/')
//Se le puede especificar el numero de dias a sumar o restar	
function getFechaActual(numDias=0) {
    var f = new Date();
    f.setDate(f.getDate()+numDias);
    return f.getFullYear() +"/"
    		+ ("0" + (f.getMonth() + 1)).slice(-2) +"/"
    		+ ("0" + f.getDate()).slice(-2);
}

<?php

namespace FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{

    /**
     * @Route("/")
     * @Route("/{slug}/{subslug}", name="front")
     */
    public function indexAction(Request $request, $slug="principal",$subslug="")
    {
        // Establecimiento del localismo para las fechas (strftime)
        setlocale(LC_TIME,$request->getLocale());

        $slug = $subslug ? $slug . "/" . $subslug : $slug; // No pongas if, pon
                                                            // un ternario
                                                            // Fdo. Sergio

        // Obtenemos los POST parámetros donde estarán los filtros necesarios para
        // generar la información del render de la plantilla
        //
        $filtrosEnPost = $request->request->all();

        $this->get('admin.filtros')->setFiltros($slug, $filtrosEnPost);


        // Pasamos el slug y los parámetros obteniendo un array con la
        // información necesaria para el render de la plantilla
        $informacionRender = $this->get("cms.service")
                                  ->inicializarService($slug)
                                  ->procesaVista();

        // Se pasa la información obtenida en el paso previo al render.
        return $this->render(
            $informacionRender["maqueta"],
            $informacionRender["variablesParaRender"]
        );
    }
}

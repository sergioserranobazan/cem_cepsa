<?php

namespace FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager as em;
use Mrcem\PerformanceLogger\PerformanceIssuer;

class AjaxController extends Controller
{
    /**
     * @Route("/datatable", name="ajax_datatable", options={"expose"=true})
     */
    public function ajaxDataTable(Request $request)
    {
        // Establecimiento del localismo para las fechas (strftime)
        setlocale(LC_TIME,$request->getLocale());

        // Inicialización del logger de performance
        $logSQL = new PerformanceIssuer("encuestas_datatable", PerformanceIssuer::FILE_LOGGER);
        $logSQL->start();

        $valoresEnRequest = $request->request->all();

        // Nota: Si no existen filtros en el request al setearse
        // en sesion un array vacio, obtendremos los valores
        // que ya estaban en sesion.
        $filtrosEnRequest = [];
        if(!empty($valoresEnRequest["filtros"])){
            $filtrosEnRequest = $valoresEnRequest["filtros"];
        }

        $fs = $this->get("admin.filtros")
                   ->setFiltros("encuestas", $filtrosEnRequest);

        $logSQL->breakpoint("seteo_filtros");

        $dqlBuilder = $this->container->get("admin.dql");
        $listadoLib = $this->container
                           ->get("front.ListadoLibService");

        $listadoLib->unsetValues();
        $listadoLib->setDDBBConnections(
            $this->container->getParameter("database_user"),
            $this->container->getParameter("database_password"),
            $this->container->getParameter("database_name"),
            $this->container->getParameter("database_host")
        );

        $listadoLib->setFrom('nosql_encuestas_datatable as dt');
        $listadoLib->setPrimaryKey("dt.encuesta_id");
        $listadoLib->setJoin("");

        $logSQL->breakpoint("configuracion_listadoLib");

        // Se obtienen los filtros seteados en sesion
        $filtrosEnRequest = $fs->getFiltros();

        
        // Arrays de String de todos los filtros
        // (con keys secuenciales empezando en 0)
        // (menos recomendacion y experiencia)
        // y menos tipologiaCliente aún por definir

        //$excepcionesToString = ['recomendacion-filtro','experiencia-filtro','tipologia_cliente-filtro'];
        /*foreach ($filtrosEnRequest as $nombreFiltro => $values) {
            if( strpos($nombreFiltro, '-filtro') !== FALSE 
                && !empty($values) && !in_array($excepcionesToString)){
                $nombreFiltroCorto = str_replace("-filtro", "", $nombreFiltro);
                $filtrosString[] = $filtrosEnRequest[$nombreFiltroCorto."_id"], $nombreFiltroCorto);
            }
        }*/

        $filtroNegocio = $this->filtroIdToString(
            $filtrosEnRequest["neg-filtro"], "neg");

        $filtroLNegocio = $this->filtroIdToString(
            $filtrosEnRequest["org-filtro"], "org");

        $filtroCanal = $this->filtroIdToString(
            $filtrosEnRequest["can-filtro"], "can");

        $filtroProceso = $this->filtroIdToString(
            $filtrosEnRequest["proceso-filtro"], "proceso"
        );

        $filtroSegmento = $this->filtroIdToString(
            $filtrosEnRequest["seg-filtro"], "seg"
        );

        $filtroServicio = $this->filtroIdToString(
            $filtrosEnRequest["ser1-filtro"], "ser1"
        );

        $filtroOpi = $this->filtroIdToString(
            $filtrosEnRequest["opi_pregunta-filtro"], "opi_pregunta"
        );

        // Trujismo en recomendación y experiencia porque son valores numéricos
        // que realmente no se relacionan con ningún filtro "oficial". entonces
        // el índice que nos llega es 1 superior al valor del número:
        // 1 => 0
        // 2 => 1
        // ect...
        //
        // Por eso se resta 1 a los valores (además de convertirlos en número)
        if(!empty($filtrosEnRequest["recomendacion-filtro"])){
            $filtroRecomendacion =
                gettype($filtrosEnRequest["recomendacion-filtro"]) == "array"
                ? array_map(function($el){
                        return intval($el) - 1;
                }, $filtrosEnRequest["recomendacion-filtro"])
                : intval($filtrosEnRequest["recomendacion-filtro"]) - 1;
        }else {
            $filtroRecomendacion = "";
        }

        if(!empty($filtrosEnRequest["experiencia-filtro"])){
            $filtroExperiencia =
                gettype($filtrosEnRequest["experiencia-filtro"]) == "array"
                ? array_map(function($el){
                        return intval($el) - 1;
                }, $filtrosEnRequest["experiencia-filtro"])
                : intval($filtrosEnRequest["experiencia-filtro"]) - 1;
        }else {
            $filtroExperiencia = "";
        }

        // Todos los filtros menos los procesos
        // Campos String en nosql_encuestas_datatable
        $mapeoFiltros = [
            ["dt", "negocio", $filtroNegocio],
            ["dt", "linea_negocio", $filtroLNegocio],
            ["dt", "canal",    $filtroCanal],
            ["dt", "recomendacion", $filtroRecomendacion],
            ["dt", "experiencia", $filtroExperiencia],
            ["dt", "segmento", $filtroSegmento],
            ["dt", "servicio", $filtroServicio],
            ["dt", "opi", $filtroOpi]
        ];

        $sql_where = "";
        $sql_where = $dqlBuilder->addFiltros(
            $mapeoFiltros,
            $sql_where,
            false,
            "dt"
        );

        // Procesos
        $sql_where = $dqlBuilder->sqlAddFiltroLike(
            "dt",
            "procesos",
            $filtroProceso,
            $sql_where,
            false,
            true // Usa los porcentajes para el LIKE
        );

        $sql_where = $dqlBuilder->filtradoFecha(
            $filtrosEnRequest["temporalidad"], $sql_where,
            $filtrosEnRequest["fec_ini"],
            $filtrosEnRequest["fec_fin"],
            false,
            "dt"
        );

        $logSQL->breakpoint("seteo_filtros");

        $listadoLib->setWhere($sql_where);

        $listadoLib->setGroup("");

        $campos_select = Array(
            "dt.fecha as fecha",
            "dt.negocio as negocio",
            "dt.linea_negocio as linea_negocio",
            "dt.canal as canal",
            "dt.detalle_canal as detalle_canal",
            "dt.procesos as procesos",
            "dt.experiencia as experiencia",
            "dt.recomendacion as recomendacion",
            "dt.sentimiento as sentimiento",
            "dt.opi as opi",
            "dt.warning as warning"
        );

        $campos_busqueda_db = Array(
            "dt.fecha",
            "dt.negocio COLLATE UTF8_GENERAL_CI",
            "dt.linea_negocio",
            "dt.canal",
            "dt.detalle_canal",
            "dt.procesos COLLATE UTF8_GENERAL_CI",
            "dt.experiencia",
            "dt.recomendacion",
            "dt.sentimiento",
            "dt.opi",
            "dt.warning"
        );

        $nombre_campos_dt = Array(
            "fecha", "negocio", "linea_negocio", "canal", "detalle_canal",
            "procesos", "experiencia", "recomendacion", "sentimiento", "opi", "warning"
        );

        $cont = 0;
        $colums = null;
        foreach ($nombre_campos_dt as $valor) {
            $colums[$cont]['db'] = $campos_select[$cont];
            $colums[$cont]['dt'] = $campos_busqueda_db[$cont];
            $colums[$cont]['filtro_buscador'] = $nombre_campos_dt[$cont];
            $cont++;
        }

        $listadoLib->setCampos($colums);
        $logSQL->breakpoint("configuracion_final_listadoLib");

        $data = $listadoLib->getData();

        $logSQL->end();

        $response = new JsonResponse();
        $response->setData($data);
        $response->prepare($request);

        return $response;
    }

    // Devuelve un array secuencial (empezando en 0) que contiene
    // los nombres (string) correspondientes a los ids de los filtros
    // recibidos en el request.
    // Nota: Puede devolver un String (vacio en el caso de que no haya valores)
    private function filtroIdToString($valores, $nombreEquivalencia)
    {
        $fs = $this->get("admin.filtros");
        $equivalenciasFiltro = $fs->getValoresEnBDFiltro($nombreEquivalencia);
        $resultado = [];

        if(!empty($valores)){
            if(is_array($valores)){
                foreach ($valores as $valor) {
                    $resultado[] = $equivalenciasFiltro[$valor];
                }
            }else{
                $resultado = $equivalenciasFiltro[$valor];
            }
        }else{
            $resultado = "";
        }

        return $resultado;
    }

    /**
     * @Route("/datatable/{slug}/{subslug}", name="ajax_grafico_datatable", options={"expose"=true})
     * NOTA: El subslug tiene que ir como parametro final porque a veces lo interpreta como null (cuadro de mando) y revienta el route.
     */
    public function ajaxGraficoDataTable(Request $request, 
                                                $slug, 
                                                $subslug=null)
    {       
        $slug = $subslug ? $slug . "/" . $subslug : $slug;

        $valoresEnRequest = $request->request->all();

        // Establecimiento del localismo para las fechas (strftime)
        setlocale(LC_TIME,$request->getLocale());

        $conexion = $this->getDoctrine()->getManager()->getConnection();

        // Inicialización del logger de performance
        $logSQL = new PerformanceIssuer("grafico_encuestas_datatable", PerformanceIssuer::FILE_LOGGER);
        $logSQL->start();

        // Service de filtros Sesion
        $fs = $this->get("admin.filtros");       
        $fs->setSlug($slug);
        $filtros = $fs->getFiltrosMaqueta();

        // Service de construccion de queries
        $dqlBuilder = $this->container->get("admin.dql");

        //Service graficos que contiene la logica de los graficos
        $serviceGraficos = $this->get("front.widget.WidgetGraficoBarrasService");

        $equivalencias = $serviceGraficos->getEquivalencias();
        //$datosGraficoDT = $serviceGraficos->getDatosGraficoToDataTable();

        $logSQL->breakpoint("seteo_filtros");
        
        // Service libreria datatables
        $listadoLib = $this->container
                           ->get("front.ListadoLibService");

        $listadoLib->unsetValues();
        $listadoLib->setDDBBConnections(
            $this->container->getParameter("database_user"),
            $this->container->getParameter("database_password"),
            $this->container->getParameter("database_name"),
            $this->container->getParameter("database_host")
        );

        $listadoLib->setFrom('nosql_encuestas as epdt');
        $listadoLib->setPrimaryKey("epdt.id");
        $listadoLib->setJoin("");

        $logSQL->breakpoint("configuracion_listadoLib");

        // Preparacion Filtros Especiales
        $filtrosEspeciales = ['proceso-filtro'];

        $filtroProceso = $this->filtroIdToString(
            $filtros["proceso-filtro"], "proceso"
        );

        $sql_where = "";
        //Nota: el checkeo del where se hace en la libreria listadoLibService
        $sql_where = $dqlBuilder->filtradoFecha(
            $filtros["temporalidad"], 
            $sql_where,
            $filtros["fec_ini"],
            $filtros["fec_fin"],
            false,
            "epdt"
        );

        //Filtros Normales
        foreach ($filtros as $nombreFiltro => $values) {
            if(strpos($nombreFiltro, '-filtro')!==FALSE && 
                !empty($values) &&
                !in_array($nombreFiltro,$filtrosEspeciales)
            ){
                $nombreFiltroValido = str_replace("-filtro", "_id", $nombreFiltro);  
                $sql_where = $dqlBuilder->sqlAddFiltro(
                    "epdt", 
                    $nombreFiltroValido,
                    $values,
                    $sql_where,
                    false
                );
            }
        }

        //Proceso
        $sql_where = $dqlBuilder->sqlAddFiltroLike(
            "epdt", "procesos", $filtroProceso,
            $sql_where, false, 
            true // Usa los Porcentajes en el LIKE
        );

        //Where correspondiente al filtrado de los graficos (pregunta,cruce,stack,substack)
        $sql_where .= $this->getWhereGraficos(  $valoresEnRequest,
                                                $serviceGraficos,
                                                $equivalencias
                                            );

        $logSQL->breakpoint("seteo_filtros");

        $listadoLib->setWhere($sql_where);

        $listadoLib->setGroup("");

        $campos_select = Array(
            "epdt.fecha as fecha",
            "epdt.neg_id as negocio",
            "epdt.org_id as linea_negocio",
            "epdt.can_id as canal",
            "epdt.det_can_id as detalle_canal",
            "epdt.procesos as procesos",
            "epdt.g3 as experiencia",
            "epdt.g2 as recomendacion",
            "epdt.g1_valor as sentimiento",
            "epdt.opi_pregunta_id as opi",
            "epdt.warning as warning"
        );

        //Mergeo las equivalencias que no estan en el serviceGraficosBase
        //Estas equivalencias se utilizaran para transformar los ids de la query
        //en string, por eso necesito a mayores alguna equivalencia más.
        $det_can_equivalencias = array("det_can" => $fs->getValoresEnBDFiltro("det_can"));
        $equivalencias = array_merge($equivalencias,$det_can_equivalencias);

        $campos_busqueda_db = Array(
            "epdt.fecha",
            $this->getCases("epdt","neg",$equivalencias["neg"]),
            $this->getCases("epdt","org",$equivalencias["org"]),
            $this->getCases("epdt","can",$equivalencias["can"]),
            $this->getCases("epdt","det_can",$equivalencias["det_can"]),
            "epdt.procesos COLLATE UTF8_GENERAL_CI",
            "epdt.g3",
            "epdt.g2",
            "epdt.g1_valor",
            $this->getCases("epdt","det_can",$equivalencias["opi_pregunta"]),
            "epdt.warning"
        );

        $nombre_campos_dt = Array(
            "fecha", "negocio", "linea_negocio", "canal", "detalle_canal",
            "procesos", "experiencia", "recomendacion", "sentimiento","opi", "warning"
        );

        $cont = 0;
        $colums = null;
        foreach ($nombre_campos_dt as $valor) {
            $colums[$cont]['db'] = $campos_select[$cont];
            $colums[$cont]['dt'] = $campos_busqueda_db[$cont];
            $colums[$cont]['filtro_buscador'] = $nombre_campos_dt[$cont];
            $cont++;
        }

        $listadoLib->setCampos($colums);
        $logSQL->breakpoint("configuracion_final_listadoLib");

        $data = $listadoLib->getData();

        //Cambio IDs por string
        //Nombre de campos a pasar a string,
        //Son los nombres de los alias de los campos select
        $nombre_campos_dt_to_string = Array(
            "negocio"=>"neg",
            "linea_negocio"=>"org",
            "canal"=>"can",
            "detalle_canal"=>"det_can",
            "opi"=>"opi_pregunta"
        );

        //Recorro los campos y sobreescribo por su valor string
        foreach ($data["data"] as $key => $rows) {
            foreach ($rows as $nombreCampoToString => $value) {
                if(array_key_exists($nombreCampoToString,$nombre_campos_dt_to_string)){
                    //Extrae el string correspondiente al id del data
                    $data["data"][$key][$nombreCampoToString] = 
                        $equivalencias[$nombre_campos_dt_to_string[$nombreCampoToString]][$value];
                }
            }
        }

        $logSQL->end();

        $response = new JsonResponse();
        $response->setData($data);
        $response->prepare($request);

        return $response;
    }    

// Devuelve el where procedente del filtrado de graficos
    // Y setea nuevos filtros en sesion
    private function getWhereGraficos(  $valoresEnRequest,
                                        $serviceGraficos,
                                        $equivalencias)
    {
        $sql = "";
        // Datos obtenidos del Request
        $tipoCEM = $valoresEnRequest["tipoCEM"];
        $nombreStack = $valoresEnRequest["nombreStack"];
        $idSubStack = $valoresEnRequest["idSubStack"];
        // Datos obtenidos a traves del service del grafico
        $datosGraficoDT = $serviceGraficos->getDatosGraficoToDataTable();      
        $cruce = $datosGraficoDT["cruce"];
        $idPregunta = $datosGraficoDT["idPregunta"];
        $cruceTemporal = $datosGraficoDT["cruceTemporal"];
        $rangoLabels = $datosGraficoDT["rangoLabels"];

        //Extraigo el id del cruce/stack
        //NOTA: El nombre del stack ha podido subrir modificaciones
        $equivalenciasCruce = [];
        foreach ($equivalencias[$cruce] as $key => $value) {
            $equivalenciasCruce[mb_strtolower($value)] = $key;
        }
        $idCruce = $equivalenciasCruce[mb_strtolower($nombreStack)];
    
        //1* Filtra por el cruce seleccionado
        if($cruce != "proceso"){
            $sql = " AND (epdt.".$cruce."_id = ".$idCruce." ) ";
        }else{
            $sql = " AND (epdt.procesos LIKE '%".$nombreStack."%' ) ";
        }
        
        // Campo de la pregunta correspondiente en BD.
        $em = $this->getDoctrine()->getManager();
        $pregunta = $em->getRepository("SurveyBundle:EncuestaPregunta")
                        ->findOneById($idPregunta);
                        //var_dump($pregunta);exit;
        $nombrePregunta = $pregunta->getNombre();
        $ordenPregunta = $pregunta->getOrdenPreguntaProceso();
        $subBloqueId = intval($pregunta->getSubBloque()->getId());
        
        //Campos BD preguntas:
        //p si es de proceso (tipo VAL o subbloqueId!=1)
        //g1_valor, g2, g3 de sub_bloque_id = 1
        $campo = "";
        if($subBloqueId == 1){ //Globales
            // No necesario filtrar ya que todas las encuestas tienen valor
            $campo = "g".$ordenPregunta;
            if($tipoCEM == 'EI'){
                $campo .= "_valor";
            }           
        }else{ //Procesos
            //2* Filtra por la pregunta del proceso (indicador) 
            $campo = mb_strtolower($nombrePregunta);
            $sql .= " AND (epdt.".$campo." is not null )";
            //Casos si se usara la tabla nosql_encuesta_procesos
            //$campo = "p".$ordenPregunta;          
            //$sql .= " AND (epdt.".$campo."_pregunta_id=".$idPregunta." )";

        }  
        //3* Filtra por el rango de Valoraciones, si lo tuviese
        $rangoLabel = [];

        if($idSubStack!=-1){ //Filtro por el subStack
            $rangoLabel = $rangoLabels[$idSubStack];
            $sql .= " AND (epdt.".$campo.">=".$rangoLabel['min'].
                    " AND epdt.".$campo."<=".$rangoLabel['max'].") ";
        }

        // PARA LA EXPORTACION A ENCUESTAS
        //Setea datos necesarios para el filtrado y el criterio de filtrado
        //en la exportacion de encuestas a excel
        $arrayCruce = [ "cruce"=>$cruce, //Nombre del filtro que actua de cruce (sin -filtro)
                        "idCruce"=>$idCruce, // Id del valor del filtro del cruce seleccionado
                        "nombreStack"=>$nombreStack]; // Nombre asociado del id Anterior
        $arrayPregunta = [  "campoPregunta" => $campo, // El nombre del campo de la pregunta en la BD
                            "descripcion_completa" => $datosGraficoDT['descripcion_completa']];

        $nuevosFiltros['filtrosGraficoDatatable'] = array(  "cruce" =>$arrayCruce,
                                                            "rangoValoraciones" => $rangoLabel,
                                                            "pregunta" => $arrayPregunta); 
        //Seteo de los "nuevosFiltros"
        $fs = $this->get("admin.filtros")->setFiltros(null, $nuevosFiltros);

        return $sql;
    }

    // Devuelve los casos posibles para tener en cuenta en el buscador de la datatable
    public function getCases($aliasTabla,$nombreFiltro,$equivalencias){
        
        //$equivalenciaCampo = $equivalencias[$nombreFiltro];

        $case = "(CASE ";
        foreach ($equivalencias as $id => $string) {
            $case .= "WHEN ".$aliasTabla.".".$nombreFiltro."_id = ".$id." THEN '".$string."' ";
        }
        $case .= "end )";

        return $case;
    }
    /**
     * @Route("/{slug}/{subslug}", name="widgets_recargables", options={"expose"=true})
     */
    public function consultaAjax(Request $request, $slug="principal", $subslug=null )
    {
        // Establecimiento del localismo para las fechas (strftime)
        setlocale(LC_TIME,$request->getLocale());
        // Composición del slug completo. Esto es necesario para las maquetas de
        // cuadro de mando
        $slug = $subslug ? $slug . "/" . $subslug : $slug; // No pongas if, pon
                                                            // un ternario
                                                            // Fdo. Sergio

        // Obtención de los valores de los filtros desde la request y asignación
        // en el service de filtros
        $valoresEnRequest = $request->request->all();

        if(isset($valoresEnRequest["filtros"])){
            $valoresFiltrosEnRequest = $valoresEnRequest["filtros"];
        }else{
            $valoresFiltrosEnRequest = $valoresEnRequest;
        }


        $this->get("admin.filtros")->setFiltros($slug, $valoresFiltrosEnRequest);

        $request->getSession()->save();

        // Obtención de la información de todos los widgets marcados como AJAX
        // en la Base de Datos

        if(isset($valoresEnRequest["target"])){
            $targets = $valoresEnRequest["target"];
        }else{
            $targets = [];
        }

        $datosWidgets = $this->get("cms.service")
                                ->inicializarService($slug)
                                // Obtenemos las encuestas como array de datos
                                // en plano en lugar de array de colección
                                // doctrine
                                ->datosRenderWidgetsAjaxPorNombre($targets);


        $htmlWidgets = []; // Acumulador para el envío de los html de los widget
                           // al usuario
        foreach ($datosWidgets as $dw) {
            $htmlWidgets[] =
                $this
                    // Renderizado de la plantilla del widget
                    ->render(
                        $dw["twig"],
                        array("datos" => $dw["datos"], "id" => $dw["id"])
                    // Obtención sólo del html resultado del renderizado
                    )->getContent();
        }

        // Caso en el que no hayan widgets AJAX en la maqueta
        if(sizeof($htmlWidgets) == 0)
            $htmlWidgets = "No hay widgets AJAX para esta maqueta";

        // Envío de la respuesta
        $response = new JsonResponse();
        $response->setData($htmlWidgets);
        $response->prepare($request);
        return $response;
    }

}

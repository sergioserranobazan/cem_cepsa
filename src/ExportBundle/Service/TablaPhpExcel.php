<?php

namespace ExportBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;

Class TablaPhpExcel {

    protected $phpExcel;
    protected $objPHPExcel;
    protected $notaConteo;

    public function __construct(ContainerInterface $container){
        $this->phpExcel = $container->get('phpexcel');
        $this->notaConteo = null;
        // Create new PHPExcel object
        $this->objPHPExcel = $this->phpExcel->createPHPExcelObject();
    }

    /**
     * Imprime en una variable una cadena de texto con el contenido del fichero excel que ya
     * haya sido generado. Es necesario añadirle las cabeceras propias del excel para poder
     * generar un fichero al vuelo que se pueda guardar desde el navegador.
     *
     * @return Devuelve una cadena de texto con el fichero
     */
    public function imprimePhpExcel() {
        $objWriter = $this->phpExcel->createWriter($this->objPHPExcel, 'Excel5');
        ob_start(); // Inicia buffer

        $objWriter->save('php://output'); // Escritura en buffer de la hoja de excel

        $cosas_nazis = ob_get_contents(); // Obtiene lo que se ha escrito en el
                                          // buffer

        ob_end_clean();

        return $cosas_nazis;
    }

    /**
     * Crea una hoja PHPExcel_Worksheet que añade al objeto PHPExcelObject en el lugar indicado
     * por el $indice y que contendrá una parte donde se muestran los $filtros que se han aplicado para
     * la consulta, y otra parte con una tabla conteniendo todos los $datos que se quieren exportar.
     *
     * @param $datos - contiene los datos de las cabeceras y el contenido de la tabla
     * @param $filtros - contiene los datos de los filtros aplicados a la consulta
     * @param $indice - la posición de la hoja en el documento excel
     *
     */
    function creaPaginaExcel($datos, $filtros, $indice){

        /******************************************************************************************
        ******************************* CREACIÓN DE LA HOJA *******************************
        ******************************************************************************************/
        // El nombre será el título
        $hoja = new \PHPExcel_Worksheet($this->objPHPExcel, $datos['nombre']);
        // La agregamos al objeto con su índice y la ponemos como activa
        $this->objPHPExcel->addSheet($hoja, $indice);
        $this->objPHPExcel->setActiveSheetIndex($indice);
        // Le ponemos el título
        $hoja->setTitle($datos['nombre']);

        /******************************************************************************************
        ************************** RELLENADO CON LOS DATOS ***************************
        ******************************************************************************************/
        $hoja->setCellValue('A1', $datos['titulo']);
        $hoja->mergeCells('A1:H1');
        $hoja->mergeCells('A2:H2');

        $hoja->setCellValue('A3', 'Criterios de Filtrado:');
        $hoja->mergeCells('A3:H3');

        // FILTROS USADOS
        $filaFiltros = 4;
        foreach ($filtros as $key => $filtro) {
            $hoja->setCellValue('A'.$filaFiltros, $key);
            $hoja->setCellValue('B'.$filaFiltros, $filtro);
            $hoja->mergeCells('B'.$filaFiltros.':H'.$filaFiltros);
            $filaFiltros++;
        }
        $hoja->mergeCells('B'.$filaFiltros.':H'.$filaFiltros);
        $filaFiltros++;
        $hoja->setCellValue('A'.$filaFiltros, 'Número de registros');
        $hoja->mergeCells('B'.$filaFiltros.':H'.$filaFiltros);
        if(!is_null($this->notaConteo)){
            $filaFiltros++;
            $hoja->setCellValue('B'.$filaFiltros, $this->notaConteo);
        }
        // CABECERA DE LA TABLA
        $colCabecera = $this->rangoColumnas(count($datos['columnas']));
        $filaCabecera = $filaFiltros+1;
        $pregunta = [];
        $contadorPregunta = 0;
        foreach ($datos['columnas'] as $key => $columna) {
            $hoja->setCellValue($colCabecera[$key].$filaCabecera, $columna);
        }

        $colCabecera = $hoja->getHighestColumn();

        // Ponemos los títulos de las preguntas
        foreach ($pregunta as $key => $value) {
            $hoja->setCellValue($value['ini'].$filaFiltros, $value['texto']);
            $hoja->mergeCells($value['ini'].$filaFiltros.':'.$value['fin'].$filaFiltros);
            $hoja->getStyle($value['ini'].$filaFiltros.':'.$value['fin'].$filaFiltros)->applyFromArray($this->estilos('tabla'));
        }

        // CUERPO DE LA TABLA
        $filaIniTabla = $filaCabecera+1;
        $filaFinTabla = $filaIniTabla;
        if (isset($datos['filas'])) {
            $registros = count($datos['filas']);
            foreach ($datos['filas'] as $k1 => $fila) {
                $colTabla = 'A';
                foreach ($fila as $k2 => $columna) {
                    $hoja->setCellValue($colTabla.$filaFinTabla, $columna);
                    $colTabla++;
                }
                $filaFinTabla++;
            }
        } else {
            $registros = 0;
        }
        $filaFinTabla--;
        // El conteo de los registros
        if(!is_null($this->notaConteo)){
            $filaFiltros--;
            $registros .= "*";
        }
        $hoja->setCellValue('B'.$filaFiltros, $registros);

        /******************************************************************************************
        *************************************** ESTILOS ****************************************
        ******************************************************************************************/
        // Alineación por defecto
        $this->objPHPExcel->getDefaultStyle()
                                        ->getAlignment()
                                        ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        // Estilos específicos
        $hoja->getStyle('A1:H1')->applyFromArray($this->estilos('titulo'));
        $hoja->getStyle('A3:H3')->applyFromArray($this->estilos('subtitulo'));
        $hoja->getStyle('A4:A'.$filaFiltros)->applyFromArray($this->estilos('filtros'));
        $hoja->getStyle('A'.$filaCabecera.':'.$colCabecera.$filaCabecera)->applyFromArray($this->estilos('cabecera'));
        $hoja->getStyle('A'.$filaIniTabla.':'.$colCabecera.$filaFinTabla)->applyFromArray($this->estilos('tabla'));

        /******************************************************************************************
        **************************** DIMENSIÓN DE LAS CELDAS ****************************
        ******************************************************************************************/
        // La primera columna con los nombres de los filtros tiene un ancho automático
        $hoja->getColumnDimension('A')->setAutoSize(true);
        // El resto de columnas tienen un ancho fijo de 20
        $ultimaCol = $hoja->getHighestColumn();
        $c = 'A';
        while ($c != $ultimaCol) {
            $c++;
            $hoja->getColumnDimension($c)->setWidth(20);
        }
        // Alto automático de las filas de la tabla (cabecera incluida)
        for ($f=$filaCabecera; $f < $filaFinTabla; $f++) {
            $hoja->getRowDimension($f)->setRowHeight(-1);
        }

    }

    function estilos($grupo){
        switch ($grupo) {
            case 'titulo':
                return  array(
                      'alignment' => array(
                        'horizontal' => 'left',
                      ),
                      'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'd9d9d9')
                      ),
                      'font' => array(
                        'bold'  => true,
                        'size' => 16
                      ),
                      'borders' => array(
                        'bottom' => array(
                          'style' => 'medium',
                          'color' => array('rgb' => 000000)
                        )
                      )
                    );
                break;
            case 'subtitulo':
                return array(
                      'alignment' => array(
                        'horizontal' => 'left',
                      ),
                      'font' => array(
                        'color' => array(
                            'rgb' => 999999
                        ),
                        'bold'  => true
                      ),
                      'borders' => array(
                        'bottom' => array(
                          'style' => 'medium',
                          'color' => array('rgb' => 000000)
                        )
                      )
                    );
                break;
            case 'filtros':
                return array(
                      'alignment' => array(
                        'horizontal'  => 'left'
                      ),
                      'font' => array(
                        'bold'  => true
                      )
                    );
                break;
            case 'cabecera':
                return  array(
                      'alignment' => array(
                        'horizontal' => 'center',
                        'vertical' => 'center',
                        'wrap' => true,
                      ),
                      'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'd9d9d9')
                      ),
                      'font' => array(
                        'bold'  => true
                      ),
                      'borders' => array(
                        'allborders' => array(
                          'style' => 'medium',
                          'color' => array('rgb' => 000000)
                        )
                      )
                    );
                break;
            case 'tabla':
                return  array(
                      'alignment' => array(
                        'horizontal' => 'center',
                        'vertical' => 'center',
                        'wrap' => true,
                      ),
                      'borders' => array(
                        'allborders' => array(
                          'style' => 'medium',
                          'color' => array('rgb' => 000000)
                        )
                      )
                    );
                break;
            case 'negrita':
                return array(
                      'font' => array(
                        'bold'  => true
                      )
                    );
                break;
        }
    }


    /**
    * Borra una hoja del objeto PHPExcelObject indicada por el $indice. Si no se indica ninguna
    * borrará la que está activa en el momento.
    *
    * @param $indice - el índice de la hoja que se quiere borrar
    *
    */
    public function borraHoja($indice = null) {
        if ($indice == null) {
            $indice = $this->objPHPExcel->getIndex($this->objPHPExcel->getActiveSheet());
        }
        $this->objPHPExcel->removeSheetByIndex($indice);
    }

    public function seleccionaHojaActiva($numero){
        $this->objPHPExcel->setActiveSheetIndex($numero);
    }

    /**
     * Devuelve un array con todas las letras incluídas en el rango especificado
     */
    function rangoColumnas($numCol, $letraInicio="A"){
        $array = [];
        $letraActual = $letraInicio;

        for ($i=0; $i < $numCol; $i++) {
            $array[] = $letraActual;
            $letraActual++;
        }

        return $array;
    }

    function setNotaConteo($nota){
        $this->notaConteo = $nota;
    }

} // End of class
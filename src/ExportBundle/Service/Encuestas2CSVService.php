<?php

namespace ExportBundle\Service;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;

use AdminBundle\Service\DQLBuilderService as DQLBuilder;
use AdminBundle\Service\FiltrosSesionService as FiltrosSesion;

use Mrcem\PerformanceLogger\PerformanceIssuer;
/**
*
*/
class Encuestas2CSVService
{
    private $numEncuestas;

    function __construct(EntityManager $em, DQLBuilder $dql, FiltrosSesion $fs)
    {
        $this->em = $em;
        $this->dql = $dql;
        $this->fs = $fs;
        $this->numEncuestas = 0; //Numero de encuestas filtradas (Reales)
    }

    public function crearCSV($filtros,$maxRegistros = false)
    {
    $logSQL = new PerformanceIssuer("exportacion_encuestas_datatable_crearCSV", PerformanceIssuer::FILE_LOGGER);
    $logSQL->start();

        $encuestas = $this->getEncuestas($filtros,$maxRegistros);

    $logSQL->breakpoint("ejecutar_sql_getEncuestas");
        $arrayIdsEncuestas = implode(
            ",", $this->getEncuestasSeleccionadasIds($encuestas));
        $preguntasRegistradas = $this->getPreguntasRegistradas($encuestas,$arrayIdsEncuestas);
    $logSQL->breakpoint("ejecutar_sql_getPreguntasRegistradas");
        $cabeceraCodigos = $this->getCabecera($preguntasRegistradas["codigos"]);
        $preguntas = $this->getPreguntas($filtros,$arrayIdsEncuestas);
    $logSQL->breakpoint("ejecutar_sql_getPreguntas");
        $filas = $this->getFilas($encuestas, $preguntas, $cabeceraCodigos);
        $cabeceraExcel =  $this->getCabecera($preguntasRegistradas["descripciones"]);
    $logSQL->end();

        return [
            "nombre" => "Listado de Encuestas",
            "titulo" => "Listado de Encuestas",
            "columnas" => $cabeceraExcel,
            "filas" => $filas
        ];
    }

    public function getEncuestas($filtros, $maxRegistros)
    {
        $db = $this->em->getConnection();

        //Campos no relevantes (aun)
        /*      e.zon_id as zona,
              e.loc_id as localizacion,
              e.prov_id as provincia,             
              e.lat as latitud,
              e.lon as longitud,
              e.nconcn as nconcn,*/
        $sql = "
            SELECT
              e.encuesta_id as id,
              DATE_FORMAT(e.fecha, '%d/%m/%Y') as fecha,
              e.opi_id as opi_id,
              e.det_can_id as determinacion_canal,
              e.g1 as sentimiento,
              IF(e.warning = 1, 'Sí', 'No') as alerta,

              e.opi_pregunta_id as opi_pregunta_id,
              e.seg_id as segmento,
              e.neg_id as negocio,
              e.org_id as linea_negocio,
              e.can_id as canal,            
              e.ser1_id as servicio,
              e.pais_id as pais,
              e.g2 as recomendacion,
              e.g3 as experiencia
              
            FROM nosql_encuestas e
        ";

        // Preparacion Filtros Especiales
        $filtrosEspeciales = [  'experiencia-filtro',
                                'recomendacion-filtro',
                                'proceso-filtro',
                                'tipologia_cliente-filtro'
                                ];

        if(!empty($filtros["recomendacion-filtro"])){
            $filtroRecomendacion =
                gettype($filtros["recomendacion-filtro"]) == "array"
                ? array_map(function($el){
                        return intval($el) - 1;
                }, $filtros["recomendacion-filtro"])
                : intval($filtros["recomendacion-filtro"]) - 1;
        }else {
            $filtroRecomendacion = "";
        }

        if(!empty($filtros["experiencia-filtro"])){
            $filtroExperiencia =
                gettype($filtros["experiencia-filtro"]) == "array"
                ? array_map(function($el){
                        return intval($el) - 1;
                }, $filtros["experiencia-filtro"])
                : intval($filtros["experiencia-filtro"]) - 1;
        }else {
            $filtroExperiencia = "";
        }

        $filtroProceso = $this->filtroIdToString(
            $filtros["proceso-filtro"], "proceso"
        );

        //FILTRADO:
        //Fecha
        $sql = $this->dql->filtradoFecha(
            $filtros["temporalidad"],
            $sql,
            $filtros["fec_ini"],
            $filtros["fec_fin"],
            true, "e"
        );

        //Filtros Normales
        foreach ($filtros as $nombreFiltro => $values) {
            if( strpos($nombreFiltro, '-filtro') !== FALSE 
                && !empty($values)
                && !in_array($nombreFiltro,$filtrosEspeciales)
            ){
                $nombreFiltroValido = str_replace("-filtro", "_id", $nombreFiltro);
                $sql = $this->dql->sqlAddFiltro(
                    'e', $nombreFiltroValido, $values,
                    $sql, false);
            }
        }

        //Filtros Especiales
        $mapeoFiltrosEspeciales = [
            ["e", "g2", $filtroRecomendacion],
            ["e", "g3", $filtroExperiencia],
        ];

        $sql = $this->dql->addFiltros(
            $mapeoFiltrosEspeciales,
            $sql, true
        );

        //Proceso
        $sql = $this->dql->sqlAddFiltroLike(
            "e", "procesos", $filtroProceso,
            $sql, false, true // Usa los Porcentajes en el LIKE
        );

        //Filtrado del evento dobleClick sobre los graficos
        if(isset($filtros["filtrosGraficoDatatable"])){
            $sql .= $this->getWhereGraficoDatatable($filtros["filtrosGraficoDatatable"]);
        }        

        $sql .= " ORDER BY e.fecha DESC ";

        $r = $db->executeQuery($sql)->fetchAll();

        $this->setNumEncuestas(count($r)); //Encuestas Reales

        if(is_int($maxRegistros)){ //Encuestas Limitadas
            $r = array_slice($r, 0, $maxRegistros);
        }

        return $r;
    }

    public function getPreguntas($filtros,$arrayIdsEncuestas){

        $db = $this->em->getConnection();

        $sql = "
            SELECT  e.encuesta_id as encuesta, 
                    epregunta.nombre as pregunta,
                    er.valor as valor
            FROM `survey_encuesta_respuesta` er
            JOIN survey_encuesta_pregunta epregunta
                ON er.encuesta_pregunta_id = epregunta.id
            JOIN nosql_encuestas e 
                ON er.encuesta_id = e.encuesta_id
            WHERE e.encuesta_id IN ($arrayIdsEncuestas)
        ";

        // Preparacion Filtros Especiales
        $filtrosEspeciales = [  'experiencia-filtro',
                                'recomendacion-filtro',
                                'proceso-filtro',
                                'tipologia_cliente-filtro'
                                ];

        if(!empty($filtros["recomendacion-filtro"])){
            $filtroRecomendacion =
                gettype($filtros["recomendacion-filtro"]) == "array"
                ? array_map(function($el){
                        return intval($el) - 1;
                }, $filtros["recomendacion-filtro"])
                : intval($filtros["recomendacion-filtro"]) - 1;
        }else {
            $filtroRecomendacion = "";
        }

        if(!empty($filtros["experiencia-filtro"])){
            $filtroExperiencia =
                gettype($filtros["experiencia-filtro"]) == "array"
                ? array_map(function($el){
                        return intval($el) - 1;
                }, $filtros["experiencia-filtro"])
                : intval($filtros["experiencia-filtro"]) - 1;
        }else {
            $filtroExperiencia = "";
        }

        $filtroProceso = $this->filtroIdToString(
            $filtros["proceso-filtro"], "proceso"
        );

        //FILTRADO:
        //Fecha
        $sql = $this->dql->filtradoFecha(
            $filtros["temporalidad"],
            $sql,
            $filtros["fec_ini"],
            $filtros["fec_fin"],
            true, "e"
        );

        //Filtros Normales
        foreach ($filtros as $nombreFiltro => $values) {
            if( strpos($nombreFiltro, '-filtro') !== FALSE 
                && !empty($values)
                && !in_array($nombreFiltro,$filtrosEspeciales)
            ){
                $nombreFiltroValido = str_replace("-filtro", "_id", $nombreFiltro);
                $sql = $this->dql->sqlAddFiltro(
                    'e', $nombreFiltroValido, $values,
                    $sql, false);
            }
        }

        //Filtros Especiales
        $mapeoFiltrosEspeciales = [
            ["e", "g2", $filtroRecomendacion],
            ["e", "g3", $filtroExperiencia],
        ];

        $sql = $this->dql->addFiltros(
            $mapeoFiltrosEspeciales,
            $sql, true
        );

        //Proceso
        $sql = $this->dql->sqlAddFiltroLike(
            "e", "procesos", $filtroProceso,
            $sql, false, true // Usa los Porcentajes en el LIKE
        );

        //Filtrado del evento dobleClick sobre los graficos
        if(isset($filtros["filtrosGraficoDatatable"])){
            $sql .= $this->getWhereGraficoDatatable($filtros["filtrosGraficoDatatable"]);
        }         

        $r = strlen($arrayIdsEncuestas) > 0
            ? $db->executeQuery($sql)->fetchAll()
            : [];

        return $r;
    }

    private function getCabecera($preguntasRegistradas)
    {

        $cabecera = [
            "Opinion Unique Id",
            "Id",
            "Date",            
            "OPI",    
            "Segmento",
            "Negocio",
            "L. Negocio",
            "Pais",
            "Canal",
            "Determinacion Canal",
            "Servicio",
            /**
             * Campos comentados porque de momento no contienen información
             * Son mantenidos en el código por si hubiera que activarlos más
             * adelante
             */
            // "nconcn"    ,
            // "zon"       ,
            // "loc"       ,
            // "prov"      ,
            // "lat"       ,
            // "lon"       ,
        ];

        $cabecera = array_merge($cabecera, $preguntasRegistradas);

        $cabecera = array_merge($cabecera, array("Alerta"));

        return $cabecera;

    }

    private function getPreguntasRegistradas($encuestasArray,$arrayIdsEncuestas)
    {
        $result = [];

        $conn = $this->em->getConnection();

        $sql = "
                SELECT CONCAT_WS(' -- ', esub_bloque.nombre, epregunta.descripcion) as pregunta,
                    epregunta.nombre as codigo_pregunta
                    FROM `survey_encuesta_respuesta` er
                    JOIN `survey_encuesta_pregunta` epregunta 
                        ON er.encuesta_pregunta_id = epregunta.id
                    JOIN `survey_encuesta_sub_bloque` esub_bloque 
                        ON epregunta.sub_bloque_id = esub_bloque.id
                    WHERE er.encuesta_id IN ($arrayIdsEncuestas) and epregunta.descripcion IS NOT NULL

                GROUP BY 1
                ORDER BY epregunta.sub_bloque_id, epregunta.nombre
                ";

        $result = strlen($arrayIdsEncuestas) > 0
                    ? $conn->executeQuery($sql)->fetchAll()
                    : array();

        // Separación de los resultados de la query en los 2 array de resultado
        foreach ($result as $value) {
            // Nota: en la tabla nosql_encuestas las preguntas estan en minusculas
            $codigos[] = $value["codigo_pregunta"]; 
            $descripciones[] = $value["pregunta"];
        }

        return array(
            "codigos"       => $codigos,
            "descripciones" => $descripciones
        );
    }

    private function getEncuestasSeleccionadasIds($encuestas)
    {
        $idEncuestas = array_map(function($encuesta){
            return $encuesta["id"];
        }, $encuestas);

        return $idEncuestas;
    }

    private function creaFilaVacia($cabecera)
    {
        $r = [];

        foreach ($cabecera as $columna) {
            $r[$columna] = "";
        }

        return $r;
    }

    /**
     * Obtiene un array de arrays
     * @param  [type] $encuestas [description]
     * @param  [type] $preguntas [description]
     * @param  [type] $cabecera  [description]
     * @return [type]            [description]
     */
    private function getFilas($encuestas, $preguntas, $cabecera)
    {
        $filas = [];

        $filaVacia = $this->creaFilaVacia($cabecera); // Actúa de plantilla

        foreach ($encuestas as $encuesta) {
            $fila = $filaVacia;
            $fila["Id"]                = $encuesta["id"];
            $fila["Opinion Unique Id"] = $encuesta["opi_id"];
            $fila["Date"]              = $encuesta["fecha"];
            //Campos no relevantes (aun)
            //$fila["lat"]               = $encuesta["latitud"];
            //$fila["lon"]               = $encuesta["longitud"];
            //$fila["nconcn"]            = $encuesta["nconcn"];
            //$fila["zon"]               = $encuesta["zona"];
            //$fila["loc"]               = $encuesta["localizacion"];           
            /*$fila["prov"]    =
                $this->fs->getValoresEnBDFiltro(
                                "prov"
                            )[$encuesta["provincia"]];*/

            //Transformar ids a Strings
            $fila["OPI"]     = 
                $this->fs->getValoresEnBDFiltro(
                                "opi_pregunta"
                            )[$encuesta["opi_pregunta_id"]];
            $fila["Segmento"]     =
                $this->fs->getValoresEnBDFiltro(
                                "seg"
                            )[$encuesta["segmento"]];

            $fila["Negocio"]     =
                $this->fs->getValoresEnBDFiltro(
                                "neg"
                            )[$encuesta["negocio"]];
            $fila["L. Negocio"]  =
                $this->fs->getValoresEnBDFiltro(
                                "org"
                            )[$encuesta["linea_negocio"]];

            $fila["Pais"]    =
                $this->fs->getValoresEnBDFiltro(
                                "pais"
                            )[$encuesta["pais"]];

            $fila["Canal"]     =
                $this->fs->getValoresEnBDFiltro(
                                "can"
                            )[$encuesta["canal"]];

            $fila["Determinacion Canal"]  =
                $this->fs->getValoresEnBDFiltro(
                                "det_can"
                            )[$encuesta["determinacion_canal"]];

            $fila["Servicio"]    =
                $this->fs->getValoresEnBDFiltro(
                                "ser1"
                            )[$encuesta["servicio"]];

            $fila["Alerta"] = $encuesta["alerta"];

            $filas[$encuesta["id"]] = $fila;
        }

        //Edgar
        foreach ($preguntas as $pregunta) {
            if(array_key_exists($pregunta["pregunta"], $filaVacia))
                $filas[$pregunta["encuesta"]][$pregunta["pregunta"]] =
                    $pregunta["valor"];
        }

        return $filas;
    }

    private function getArrayRespuestas($encuesta)
    {
        $respuestas = [];

        foreach ($encuesta["preguntas"] as $bloque) {
            foreach ($bloque as $subBloque) {
                foreach ($subBloque as $pregunta) {
                    $respuestas[$pregunta["codigo"]] = $pregunta;
                }
            }
        }

        return $respuestas;
    }


    private function filtroIdToString($valores, $nombreEntidad)
    {
        $equivalenciasFiltro = $this->fs->getValoresEnBDFiltro($nombreEntidad);
        $resultado = [];

        if(!empty($valores)){
            if(is_array($valores)){
                foreach ($valores as $valor) {
                    $resultado[] = $equivalenciasFiltro[$valor];
                }
            }else{
                $resultado = $equivalenciasFiltro[$valor];
            }
        }else{
            $resultado = "";
        }

        return $resultado;
    }

    private function getWhereGraficoDatatable($filtrosGraficos){
        $sql = " ";
        $arrayCruce = $filtrosGraficos["cruce"];
        $rangoLabel = $filtrosGraficos["rangoValoraciones"];
        $campo = $filtrosGraficos["pregunta"]["campoPregunta"];

        //1* Filtra por el cruce seleccionado
        if($arrayCruce["cruce"] != "proceso"){
            $sql = " AND (e.".$arrayCruce["cruce"]."_id = ".$arrayCruce["idCruce"]." ) ";
        }else{
            $sql = " AND (e.procesos LIKE '%".$arrayCruce["nombreStack"]."%' ) ";
        }
        //2* Filtro por la pregunta (indicador)
        // No seria necesario en caso de preguntas transversales(g1,g2,g3)
        $sql .= " AND (e.".$campo." is not null ) ";

        //3* Filtro por el rango de Valoraciones, si lo tuviese
        if(!empty($rangoLabel)){
            $sql .= " AND (e.".$campo.">=".$rangoLabel['min'].
                    " AND e.".$campo."<=".$rangoLabel['max'].") ";
        }

        return $sql;
    }

    public function setNumEncuestas($numEncuestas){
        $this->numEncuestas = $numEncuestas;
    }

    public function getNumEncuestas(){
        return $this->numEncuestas;
    }
}
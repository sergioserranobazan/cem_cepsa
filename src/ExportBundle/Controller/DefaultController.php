<?php

namespace ExportBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use AdminBundle\Library\CemUtils;

use Mrcem\PerformanceLogger\PerformanceIssuer;

class DefaultController extends Controller
{

    /**
     * @Route("/test")
     */
    public function testing()
    {
        $filtros = $this->get("admin.filtros");

        $filtrosOriginales = $filtros->setFiltros("encuestas", [])
                           ->getFiltros("encuestas");

        $excel = $this->generarFicheroExcel($filtrosOriginales);

        $respuesta = new Response();
        $respuesta->setContent($excel);

        return $respuesta;
    }

    /**
     * @Route("/encuestas-format/{slug}/{subslug}", name="exportar_maqueta_encuestas_formateada",
     options={"expose": true})
     */
    public function exportarExcelAction($slug="encuestas",$subslug=null)
    {

        $slug = $subslug ? $slug . "/" . $subslug : $slug;

        $fs = $this->get("admin.filtros");
        $fs->setSlug($slug)->setFiltros(null, null);   // Para inicializar el service pero
                                                       // sólo usar los filtros de sesión.
                                                       // por eso no se cojen filtros por
                                                       // petición HTTP.
        $filtros = $fs->getFiltros();
       
        $response = new StreamedResponse();

        $response->setCallback(function() use ($filtros,$slug){
            $excel = $this->generarFicheroExcel($filtros,$slug);
            echo $excel;
        });

        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'text/csv;charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename="export.xls"');

        // echo "\xEF\xBB\xBF"; // UTF-8 bom

        return $response;
    }

    /**
     * @Route("/encuestas/{slug}/{subslug}", name="exportar_maqueta_encuestas",
     options={"expose": true})
     */
    public function indexAction()
    {
        $slug = $subslug ? $slug . "/" . $subslug : $slug;
        $fs = $this->get("admin.filtros");
        $fs->setSlug($slug)->setFiltros(null, null);   // Para inicializar el service pero
                                                       // sólo usar los filtros de sesión.
                                                       // por eso no se cojen filtros por
                                                       // petición HTTP.
        $filtros = $fs->getFiltros();

        $response = new StreamedResponse();

        $response->setCallback(function() use ($filtros){

            $arrayCSV = $this->get("export.csv")->crearCSV($filtros);

            $handle = fopen('php://output', 'w+');
            fputcsv($handle, $arrayCSV["cabecera"], ";");

            foreach ($arrayCSV["filas"] as $fila) {
                fputcsv($handle, $fila, ";");
            }

            fclose($handle);

        });

        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'text/csv;charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename="export.xls"');

        echo "\xEF\xBB\xBF"; // UTF-8 bom

        return $response;
    }

    private function generarFicheroExcel($filtros,$slug)
    {
        //Para exportar 5000 encuestas necesitamos más de los 128mb que hay por defecto
        ini_set('memory_limit','256M'); 
        // Inicialización del logger de performance
        $logSQL = new PerformanceIssuer(
            "exportacion_encuestas_datatable_callback",
            PerformanceIssuer::FILE_LOGGER
        );

        $logSQL->start();

        $encuestas2CSV = $this->get("export.csv"); //Service csv

        $arrayCSV = $encuestas2CSV->crearCSV($filtros);

        $numEncuestas = $encuestas2CSV->getNumEncuestas(); //Muestro el nº. de encuestas en el logger
        $logSQL->breakpoint("crearCSV-".$numEncuestas,array("numEncuestas" => $numEncuestas));

        /*
         * Lógica para formación del excel
         */
        $tablaExcel = $this->get("export.tabla_phpexcel");
        $tablaExcel->borraHoja(); // Acción necesaria por la librería
        

        $service = $this->getFiltrosSeleccionadosService($slug);
        // Filtros Formateados para la salida excel

        $serviceFiltrosSeleccionados = $this->get($service);

        $datos = $serviceFiltrosSeleccionados
                    ->filtrarDatos(["maqueta"=>$slug])
                    ->getDatos();
        $filtrosFormateados = $datos["filtrosFormateados"];

        $tablaExcel->creaPaginaExcel($arrayCSV, $filtrosFormateados, 0);

        $logSQL->breakpoint("crear_pagina_excel");

        $excel = $tablaExcel->imprimePhpExcel();
        $logSQL->end();

        return $excel;
    }

    private function getFiltrosSeleccionadosService($slug){
        $service = '';
        switch ($slug) {
            case 'cuadro_mando/desglose_informacion':
                $service = 'front.widget.WidgetFiltrosSeleccionadosDesgloseInformacionService';
                break;
            case 'cuadro_mando/cruce_variables':
                $service = 'front.widget.WidgetFiltrosSeleccionadosCruceVariablesService';
                break;
            default:
                $service = 'front.widget.WidgetFiltrosSeleccionadosEncuestasService';
                break;
        }
        return $service;
    }
}

<?php
/**
 * Use this file as an example for configuration
 */
use Monolog\Logger;

return array(
    "output_dir" => __DIR__ . '/../../../app/logs/',
    "log_level"  => Logger::DEBUG,
    "proyect"    => "PerformanceLogger",
    // ElasticSearchLogger specifications
    "es_index"   => "performance",
    "es_type"    => "log",
    "es_file_sufix" => "_elastic",
);
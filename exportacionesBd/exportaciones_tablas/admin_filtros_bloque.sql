-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 24-01-2017 a las 16:47:59
-- Versión del servidor: 5.6.21
-- Versión de PHP: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `cemcepsadb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_filtros_bloque`
--

CREATE TABLE IF NOT EXISTS `admin_filtros_bloque` (
`id` int(11) NOT NULL,
  `nombreId` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cabecera` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `admin_filtros_bloque`
--

INSERT INTO `admin_filtros_bloque` (`id`, `nombreId`, `cabecera`) VALUES
(1, 'temporalidad', 'temporalidad'),
(2, 'vistas', 'vistas'),
(3, 'opc_consulta', 'filtros'),
(4, 'filtros_encuestas', ''),
(5, 'bloque_general', ''),
(6, 'variables_filtro', 'Variables Filtro'),
(7, 'informacion_especifica_proceso', 'información específica de proceso'),
(8, 'temporalidad', 'temporalidad'),
(9, 'variables_filtro_cruce', 'Variables Filtro/Cruce'),
(99, 'filtros_grupo_usuario', '');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `admin_filtros_bloque`
--
ALTER TABLE `admin_filtros_bloque`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `admin_filtros_bloque`
--
ALTER TABLE `admin_filtros_bloque`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=100;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

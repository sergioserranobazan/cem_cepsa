-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 17-01-2017 a las 15:58:59
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cemcepsadb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_maquetas`
--

CREATE TABLE `cms_maquetas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `activo` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `twig` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `icono` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `visible_cabecera` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `cms_maquetas`
--

INSERT INTO `cms_maquetas` (`id`, `nombre`, `slug`, `descripcion`, `activo`, `orden`, `twig`, `icono`, `visible_cabecera`) VALUES
(1, 'Principal', 'principal', '', 1, 1, 'MaquetaPrincipal', 'fa-home', 1),
(2, 'Alertas', 'alertas', '', 1, 2, 'MaquetaPrincipal', 'fa-bell', 1),
(3, 'Cuadro de Mando', 'cuadro_mando/desglose_informacion', '', 1, 3, 'MaquetaCuadroMandoDesgloseInformacion', 'fa-desktop', 1),
(5, 'Reportes', 'reportes', 'Maqueta de reportes', 1, 4, 'MaquetaPrincipal', 'fa-file-text-o', 1),
(7, 'Cuadro de Mando', 'cuadro_mando/cruce_variables', '', 1, 5, 'MaquetaCuadroMandoCruceVariables', 'fa-desktop', 0),
(8, 'Cuadro de Mando Informacion Jerarquica', 'cuadro_mando/informacion_jerarquica', '', 1, 6, 'MaquetaCuadroMandoInformacionJerarquica', 'fa-desktop', 0),
(9, 'Encuestas', 'encuestas', '', 1, 5, 'MaquetaPrincipal', 'fa-pie-chart', 1),
(10, 'Dashboard Estratégico', 'd_estrategico', '', 1, 6, 'MaquetaPrincipal', 'fa-desktop', 1),
(11, 'Informe Estratégico', 'i_estrategico', '', 1, 7, 'MaquetaPrincipal', 'fa-file-text-o', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cms_maquetas`
--
ALTER TABLE `cms_maquetas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cms_maquetas`
--
ALTER TABLE `cms_maquetas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

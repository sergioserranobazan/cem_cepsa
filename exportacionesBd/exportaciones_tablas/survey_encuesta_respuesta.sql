-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Servidor: mysqldev01.madisonmk.local
-- Tiempo de generación: 29-12-2016 a las 12:01:53
-- Versión del servidor: 5.5.31
-- Versión de PHP: 5.5.17-2+deb.sury.org~precise+1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `cemCepsaDb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `survey_encuesta_respuesta`
--

CREATE TABLE IF NOT EXISTS `survey_encuesta_respuesta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `encuesta_id` int(11) DEFAULT NULL,
  `encuesta_pregunta_id` int(11) DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `valor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ev` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_1945667C46844BA6` (`encuesta_id`),
  KEY `IDX_1945667C9AF2BA6` (`encuesta_pregunta_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `survey_encuesta_respuesta`
--
ALTER TABLE `survey_encuesta_respuesta`
  ADD CONSTRAINT `FK_1945667C46844BA6` FOREIGN KEY (`encuesta_id`) REFERENCES `survey_encuesta` (`id`),
  ADD CONSTRAINT `FK_1945667C9AF2BA6` FOREIGN KEY (`encuesta_pregunta_id`) REFERENCES `survey_encuesta_pregunta` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

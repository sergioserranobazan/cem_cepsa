-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 08-02-2017 a las 14:53:46
-- Versión del servidor: 5.6.21
-- Versión de PHP: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `cemcepsadb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_vista`
--

CREATE TABLE IF NOT EXISTS `admin_vista` (
`id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `filtro_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `admin_vista`
--

INSERT INTO `admin_vista` (`id`, `nombre`, `filtro_id`) VALUES
(1, 'negocio', 1),
(2, 'canal', 6),
(3, 'proceso', 10);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `admin_vista`
--
ALTER TABLE `admin_vista`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_71E59A34A326768E` (`filtro_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `admin_vista`
--
ALTER TABLE `admin_vista`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `admin_vista`
--
ALTER TABLE `admin_vista`
ADD CONSTRAINT `FK_71E59A34A326768E` FOREIGN KEY (`filtro_id`) REFERENCES `admin_filtros` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

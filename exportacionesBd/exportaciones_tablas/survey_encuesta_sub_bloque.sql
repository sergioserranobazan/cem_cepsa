-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 17-01-2017 a las 15:56:33
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cemcepsadb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `survey_encuesta_sub_bloque`
--

CREATE TABLE `survey_encuesta_sub_bloque` (
  `id` int(11) NOT NULL,
  `bloque_id` int(11) DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `survey_encuesta_sub_bloque`
--

INSERT INTO `survey_encuesta_sub_bloque` (`id`, `bloque_id`, `nombre`) VALUES
(1, 1, 'Global'),
(2, 2, 'Alta/Contratación'),
(3, 2, 'Oferta/Fidelización'),
(4, 2, 'Pedido/Compra'),
(5, 2, 'Entrega/Suministro'),
(6, 2, 'Pagos/Cobros'),
(7, 2, 'Facturación'),
(8, 2, 'Gestión Cliente'),
(9, 2, 'Asistencia Técnica'),
(10, 2, 'Bajas'),
(11, 2, 'Seguridad'),
(12, 2, 'Producto/Servicio'),
(13, 3, 'Especifico Negocio'),
(14, 3, 'Modulo Opcional'),
(16, 4, 'Preguntas Abiertas');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `survey_encuesta_sub_bloque`
--
ALTER TABLE `survey_encuesta_sub_bloque`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_5DFFFE6FA929EBC` (`bloque_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `survey_encuesta_sub_bloque`
--
ALTER TABLE `survey_encuesta_sub_bloque`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `survey_encuesta_sub_bloque`
--
ALTER TABLE `survey_encuesta_sub_bloque`
  ADD CONSTRAINT `FK_91E25B06A929EBC` FOREIGN KEY (`bloque_id`) REFERENCES `survey_encuesta_bloque` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

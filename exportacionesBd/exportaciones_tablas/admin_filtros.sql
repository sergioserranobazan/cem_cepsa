-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 23-02-2017 a las 17:47:50
-- Versión del servidor: 5.6.21
-- Versión de PHP: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `cemcepsadb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_filtros`
--

CREATE TABLE IF NOT EXISTS `admin_filtros` (
`id` int(11) NOT NULL,
  `padre_id` int(11) DEFAULT NULL,
  `tabla` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tablaHijo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `campoId` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'id',
  `campoDescripcion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nombreFiltro` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `campoIdPadre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auto` tinyint(1) DEFAULT NULL,
  `etiqueta` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sentencia` longtext COLLATE utf8_unicode_ci,
  `entidadDoctrine` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `admin_filtros`
--

INSERT INTO `admin_filtros` (`id`, `padre_id`, `tabla`, `tablaHijo`, `campoId`, `campoDescripcion`, `nombreFiltro`, `campoIdPadre`, `auto`, `etiqueta`, `sentencia`, `entidadDoctrine`) VALUES
(1, NULL, 'admin_negocio', 'admin_segmento', '', NULL, 'neg', NULL, NULL, NULL, NULL, 'AdminBundle:Negocio'),
(5, NULL, 'admin_segmento', NULL, '', NULL, 'seg', NULL, NULL, NULL, NULL, 'AdminBundle:Segmento'),
(6, NULL, 'admin_canal_encuestacion', NULL, '', NULL, 'can', NULL, NULL, NULL, NULL, 'AdminBundle:CanalEncuestacion'),
(7, NULL, 'admin_vista', NULL, 'id', NULL, 'vista', NULL, NULL, NULL, NULL, 'AdminBundle:Vista'),
(8, NULL, 'admin_temporalidad', NULL, 'id', NULL, 'temporalidad', NULL, NULL, NULL, NULL, 'AdminBundle:Temporalidad'),
(9, NULL, 'admin_servicio', NULL, 'id', NULL, 'ser1', NULL, NULL, NULL, NULL, 'AdminBundle:Servicio'),
(10, NULL, 'admin_proceso', NULL, 'id', NULL, 'proceso', NULL, NULL, NULL, NULL, 'AdminBundle:Proceso'),
(11, NULL, 'admin_linea_negocio', NULL, 'id', NULL, 'org', NULL, NULL, NULL, NULL, 'AdminBundle:LineaNegocio'),
(12, NULL, 'admin_experiencia', NULL, 'id', NULL, 'experiencia', NULL, NULL, NULL, NULL, 'AdminBundle:Experiencia'),
(13, NULL, 'admin_recomendacion', NULL, 'id', NULL, 'recomendacion', NULL, NULL, NULL, NULL, 'AdminBundle:Recomendacion'),
(14, NULL, 'admin_tipologia_cliente', NULL, 'id', NULL, 'tipologia_cliente', NULL, NULL, NULL, NULL, 'AdminBundle:TipologiaCliente'),
(15, NULL, 'admin_determinacion_canal', NULL, '', NULL, 'det_can', NULL, NULL, NULL, NULL, 'AdminBundle:DeterminacionCanal'),
(16, NULL, 'survey_encuesta_pregunta_opi', NULL, 'id', NULL, 'opi_pregunta', NULL, NULL, NULL, NULL, 'SurveyBundle:EncuestaPreguntaOpi'),
(17, NULL, 'admin_pais', NULL, 'id', NULL, 'pais', NULL, NULL, NULL, NULL, 'AdminBundle:Pais');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `admin_filtros`
--
ALTER TABLE `admin_filtros`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_5A4E3CED613CEC58` (`padre_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `admin_filtros`
--
ALTER TABLE `admin_filtros`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `admin_filtros`
--
ALTER TABLE `admin_filtros`
ADD CONSTRAINT `FK_5A4E3CED613CEC58` FOREIGN KEY (`padre_id`) REFERENCES `admin_filtros` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

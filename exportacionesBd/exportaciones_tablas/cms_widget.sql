-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 08-02-2017 a las 15:59:13
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cemcepsadb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_widget`
--

CREATE TABLE `cms_widget` (
  `id` int(11) NOT NULL,
  `contenedor_id` int(11) DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `activo` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `twig` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `service` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `widget_ajax` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `cms_widget`
--

INSERT INTO `cms_widget` (`id`, `contenedor_id`, `nombre`, `descripcion`, `activo`, `orden`, `twig`, `service`, `widget_ajax`) VALUES
(1, 1, 'WidgetMenu', 'WidgetLinksMenuEnPrincipal', 1, 3, 'WidgetEnlacesMenu', 'WidgetEnlacesMenu', 0),
(4, 1, 'WidgetImagenCliente', '', 1, 2, 'WidgetImagen', 'WidgetImagenCliente', 0),
(5, 1, 'WidgetImagenCem', '', 1, 1, 'WidgetImagen', 'WidgetImagenCem', 0),
(6, 2, 'WidgetImagenCliente', '', 1, 2, 'WidgetImagen', 'WidgetImagenCliente', 0),
(7, 2, 'WidgetImagenCem', '', 1, 1, 'WidgetImagen', 'WidgetImagenCem', 0),
(8, 2, 'WidgetMenu', 'WidgetLinksMenuEnAlertas', 1, 3, 'WidgetEnlacesMenu', 'WidgetEnlacesMenu', 0),
(9, 3, 'WidgetImagenClienteEnCuadroMandoDesgloseInformacion', '', 1, 2, 'WidgetImagen', 'WidgetImagenCliente', 0),
(10, 3, 'WidgetImagenCemEnCuadroMandoDesgloseInformacion', '', 1, 1, 'WidgetImagen', 'WidgetImagenCem', 0),
(11, 3, 'WidgetMenu', 'WidgetLinksMenuEnCuadroMandoDesgloseInformacion', 1, 3, 'WidgetEnlacesMenu', 'WidgetEnlacesMenu', 0),
(12, 4, 'WidgetTablaAcumulados', 'WidgetTablaAcumuladosEnZonzaContenidoConZonaLateralEnPrincipal', 1, 5, 'Especificos/WidgetTablaAcumulados', 'WidgetTablaEspecificoProceso', 1),
(14, 5, 'WidgetNumEncuestas', 'WidgetNumEncuestasEnPrincipal', 1, 1, 'WidgetNumEncuestas', 'WidgetNumEncuestas', 1),
(17, 6, 'WidgetMenu', 'WidgetLinksCuadroMandoCruceVariables', 1, 3, 'WidgetEnlacesMenu', 'WidgetEnlacesMenu', 0),
(18, 6, 'WidgetImagenClienteCuadroMandoCruceVariables', '', 1, 2, 'WidgetImagen', 'WidgetImagenCliente', 0),
(19, 6, 'WidgetImagenCemCuadroMandoCruceVariables', '', 1, 1, 'WidgetImagen', 'WidgetImagenCem', 0),
(20, 7, 'WidgetMenu', 'WidgetLinksCuadroMandoInformacionJerarquica', 1, 3, 'WidgetEnlacesMenu', 'WidgetEnlacesMenu', 0),
(21, 7, 'WidgetImagenClienteCuadroMandoInformacionJerarquica', '', 1, 2, 'WidgetImagen', 'WidgetImagenCliente', 0),
(22, 7, 'WidgetImagenCemCuadroMandoInformacionJerarquica', '', 1, 1, 'WidgetImagen', 'WidgetImagenCem', 0),
(23, 8, 'WidgetPrueba23', 'WidgetConfigGraficosEnContenedor1ZonaIndicadoresDesgloseInformacion', 1, 1, 'Especificos/WidgetConfigGraficosDesgloseInformacion', 'WidgetConfiguracionVistaDesgloseInformacion', 0),
(25, 10, 'WidgetFechasTemporalidad', 'WidgetFiltroFechasTemporalidadEnContenedor1ZonaLateralCuadroMandoDesgloseInformacion', 1, 1, 'WidgetFechasTemporalidad', 'WidgetFiltroTemporalidad', 0),
(28, 11, 'WidgetPrueba26', 'WidgetPruebaEnContenedor1ZonaIndicadoresEnCuadroMandoCruceVariables', 1, 1, 'Especificos/WidgetConfigGraficosCruceVariables', 'WidgetConfiguracionVistaCruceVariables', 0),
(36, 15, 'WidgetConfiguracionVisualizacionInformacionJerarquica', 'WidgetConfiguracionVistaEnContenedor1ZonaLateralCuadroMandoCruceVariables', 1, 1, 'Especificos/WidgetSelectorInformacionJerarquica', 'WidgetSelectorInformacionJerarquica', 0),
(37, 17, 'WidgetMenu', 'WidgetLinksMenuEnReportes', 1, 3, 'WidgetEnlacesMenu', 'WidgetEnlacesMenu', 0),
(38, 17, 'WidgetImagenCliente', 'WidgetImagenClienteEnReportes', 1, 2, 'WidgetImagen', 'WidgetImagenCliente', 0),
(39, 17, 'WidgetImagenCem', 'WidgetImagenCemEnReportes', 1, 1, 'WidgetImagen', 'WidgetImagenCem', 0),
(40, 18, 'WidgetPrueba', 'WidgetPruebaEnContenedorFiltrosEnReportes', 0, 1, 'WidgetTest', 'WidgetTest', 0),
(41, 19, 'WidgetPrueba', 'WidgetPruebaEnContenedorTablaEnReportes', 0, 1, 'WidgetTest', 'WidgetTest', 0),
(42, 20, 'WidgetMenu', 'WidgetLinksMenuEnAlertas', 1, 3, 'WidgetEnlacesMenu', 'WidgetEnlacesMenu', 0),
(43, 20, 'WidgetImagenCliente', 'WidgetImagenClienteEnAlertas', 1, 2, 'WidgetImagen', 'WidgetImagenCliente', 0),
(44, 20, 'WidgetImagenCem', 'WidgetImagenCemEnAlertas', 1, 1, 'WidgetImagen', 'WidgetImagenCem', 0),
(45, 21, 'WidgetPrueba', 'WidgetPruebaEnContenedorFiltrosEnAlertas', 0, 1, 'WidgetTest', 'WidgetTest', 0),
(46, 22, 'WidgetPrueba', 'WidgetPruebaEnContenedorTablaEnAlertas', 0, 2, 'WidgetTest', 'WidgetTest', 0),
(47, 23, 'WidgetMenu', 'WidgetLinksMenuEnEncuestas', 1, 3, 'WidgetEnlacesMenu', 'WidgetEnlacesMenu', 0),
(48, 23, 'WidgetImagenCliente', 'WidgetImagenClienteEnEncuestas', 1, 2, 'WidgetImagen', 'WidgetImagenCliente', 0),
(49, 23, 'WidgetImagenCem', 'WidgetImagenCemEnEncuestas', 1, 1, 'WidgetImagen', 'WidgetImagenCem', 0),
(50, 24, 'WidgetPruebaFiltros', 'WidgetPruebaEnContenedorFiltrosEnEncuestas', 1, 1, 'WidgetTest', 'WidgetFiltros', 0),
(51, 25, 'WidgetPrueba', 'WidgetDataTableEnContenedorTablaEnEncuestas', 1, 2, 'WidgetDataTable', 'WidgetDataTable', 1),
(52, 26, 'WidgetMenu', 'WidgetLinksMenuEnD_Estrategico', 1, 3, 'WidgetEnlacesMenu', 'WidgetEnlacesMenu', 0),
(53, 26, 'WidgetImagenCliente', 'WidgetImagenClienteEnD_Estrategico', 1, 2, 'WidgetImagen', 'WidgetImagenCliente', 0),
(54, 26, 'WidgetImagenCem', 'WidgetImagenCemEnD_Estrategico', 1, 1, 'WidgetImagen', 'WidgetImagenCem', 0),
(55, 27, 'WidgetPrueba', 'WidgetPruebaEnContenedorTablaEnD_Estrategico', 0, 2, 'WidgetTest', 'WidgetTest', 0),
(56, 29, 'WidgetMenu', 'WidgetLinksMenuEnI_Estrategico', 1, 3, 'WidgetEnlacesMenu', 'WidgetEnlacesMenu', 0),
(57, 29, 'WidgetImagenCliente', 'WidgetImagenClienteEnI_Estrategico', 1, 2, 'WidgetImagen', 'WidgetImagenCliente', 0),
(58, 29, 'WidgetImagenCem', 'WidgetImagenCemEnI_Estrategico', 1, 1, 'WidgetImagen', 'WidgetImagenCem', 0),
(59, 30, 'WidgetPrueba', 'WidgetPruebaEnContenedorTablaEnI_Estrategico', 0, 2, 'WidgetTest', 'WidgetTest', 0),
(60, 32, 'WidgetPruebaP1', 'WidgetFiltroTemporalidadEnContenedorZonaLateralContenedorWidgetsFiltrosEnPRincipal', 1, 1, 'WidgetFechasTemporalidad', 'WidgetFiltroTemporalidad', 0),
(61, 4, 'WidgetFiltroVistas', 'WidgetFiltroVistasEnContenedorZonaLateralContenedorWidgetsFiltrosEnPrincipal', 1, 2, 'Especificos/WidgetFiltrosVistasTreeview', 'WidgetFiltrosVista', 0),
(62, 32, 'WidgetPrueba32', 'WidgetFiltroEncuestasEnContenedorZonaLateralContenedorWidgetsFiltrosEnPrincipal', 1, 3, 'Especificos/WidgetFiltrosEspecializadosMultiselect', 'WidgetFiltroOpcionesConsulta', 0),
(63, 4, 'WidgetTreeView', 'WidgetTreeViewEnContenedorConZonaLateralEnPrincipal', 1, 3, 'WidgetTreeView', 'WidgetTreeView', 1),
(64, 10, 'WidgetBloqueVariablesFiltro', 'WidgeBloqueVariablesFiltroEnContenedorZonaLateralContenedorWidgetsFiltrosEnCMDesgloseInformacion', 1, 1, 'Especificos/WidgetFiltrosEspecializadosMultiselect', 'WidgetBloqueVariablesFiltro', 0),
(65, 4, 'WidgetFiltrosTablaAcumulados', 'WidgetFiltrosTablaAcumuladosEnZonzaContenidoConZonaLateralEnPrincipal', 1, 4, 'Especificos/WidgetFiltrosTablaAcumulados', 'WidgetFiltrosTablaAcumulados', 0),
(66, 9, 'WidgetGraficoBarras', 'WidgetGraficoBarrasEnContenedorZonaContenidosConZonaLateralEnCuadroDeMandoDesglose', 1, 1, 'Especificos/WidgetGraficoBarras', 'WidgetGraficoBarras', 1),
(67, 9, 'WidgetGraficoLineas', 'WidgetGraficoLineasEnContenedorZonaContenidosConZonaLateralEnCuadroDeMandoDesglose', 1, 2, 'Especificos/WidgetGraficoLineas', 'WidgetGraficoLineas', 1),
(68, 24, 'WidgetFechasEncuestas', 'WidgetFiltroTemporalidadFechasEnEncuestas', 1, 1, 'WidgetTest', 'WidgetFiltroTemporalidadEncuestas', 0),
(69, 12, 'WidgetGraficoBarras', 'WidgetGraficoBarrasEnContenedorZonaContenidosConZonaLateralEnCMCruceVariables', 1, 1, 'Especificos/WidgetGraficoBarras', 'WidgetGraficoBarras', 1),
(70, 12, 'WidgetGraficoLineas', 'WidgetGraficoLineasEnContenedorZonaContenidosConZonaLateralEnCMCruceVariables', 1, 2, 'Especificos/WidgetGraficoLineas', 'WidgetGraficoLineas', 1),
(71, 13, 'WidgetFechasTemporalidad', 'WidgetFiltroFechasTemporalidadEnContenedor1ZonaLateralCMCruceVariables', 1, 1, 'WidgetFechasTemporalidad', 'WidgetFiltroTemporalidad', 0),
(72, 13, 'WidgetBloqueVariablesFiltroCruce', 'WidgeBloqueVariablesFiltroEnContenedorZonaLateralContenedorWidgetsFiltrosEnCMCruceVariables', 1, 1, 'Especificos/WidgetFiltrosEspecializadosConCruceMultiselect', 'WidgetBloqueVariablesFiltroCruce', 0),
(73, 4, 'WidgetFiltrosActivosPrincipal', 'WidgetFiltrosActivosPrincipal', 1, 1, 'WidgetFiltrosActivos', 'WidgetFiltrosSeleccionadosPrincipal', 1),
(74, 25, 'WidgetFiltrosActivosEncuestas', 'WidgetFiltrosActivos', 1, 1, 'WidgetFiltrosActivos', 'WidgetFiltrosSeleccionadosEncuestas', 1),
(75, 34, 'WidgetFechasTemporalidad', 'WidgetFiltroFechasTemporalidadEnContenedor1ZonaLateralCuadroMandoInformacionJerarquica', 1, 1, 'WidgetFechasTemporalidad', 'WidgetFiltroTemporalidad', 0),
(76, 34, 'WidgetBloqueVariablesFiltro', 'WidgeBloqueVariablesFiltroEnContenedorZonaLateralContenedorWidgetsFiltrosEnCMInformacionJerarquica', 1, 1, 'Especificos/WidgetFiltrosEspecializadosMultiselect', 'WidgetBloqueVariablesFiltro', 0),
(77, 33, 'WidgetTreeView', 'WidgetTreeViewEnContenedorConZonaLateralEnCMInformacionJerarquica', 1, 1, 'WidgetTreeViewEnCMInformacionJerarquica', 'WidgetTreeViewEnCMInformacionJerarquica', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cms_widget`
--
ALTER TABLE `cms_widget`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_B70F01A8C1A15772` (`contenedor_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cms_widget`
--
ALTER TABLE `cms_widget`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cms_widget`
--
ALTER TABLE `cms_widget`
  ADD CONSTRAINT `FK_B70F01A8C1A15772` FOREIGN KEY (`contenedor_id`) REFERENCES `cms_contenedor` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

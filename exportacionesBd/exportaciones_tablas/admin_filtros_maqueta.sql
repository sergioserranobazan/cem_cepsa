-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-03-2017 a las 13:35:25
-- Versión del servidor: 5.6.21
-- Versión de PHP: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `cemcepsadb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_filtros_maqueta`
--

CREATE TABLE IF NOT EXISTS `admin_filtros_maqueta` (
`id` int(11) NOT NULL,
  `id_maqueta` int(11) DEFAULT NULL,
  `id_filtro` int(11) DEFAULT NULL,
  `orden` int(11) NOT NULL DEFAULT '1',
  `tipoFiltro` int(11) NOT NULL DEFAULT '1',
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `bloque` int(11) DEFAULT NULL,
  `nombreId` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `valorDefault` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tag` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `valorCampoCero` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `admin_filtros_maqueta`
--

INSERT INTO `admin_filtros_maqueta` (`id`, `id_maqueta`, `id_filtro`, `orden`, `tipoFiltro`, `activo`, `bloque`, `nombreId`, `valorDefault`, `tag`, `valorCampoCero`) VALUES
(1, 9, 1, 1, 0, 1, 4, 'neg-filtro', '', 'negocio', ''),
(5, 9, 5, 7, 0, 1, 4, 'seg-filtro', '', 'segmento', ''),
(6, 9, 6, 6, 0, 1, 4, 'can-filtro', '', 'canal', ''),
(7, 1, 8, 1, 1, 1, 1, 'temporalidad', '0', '', 'por rango'),
(8, 1, 7, 1, 1, 1, 2, 'fPrimeraVista', '1', '', ''),
(11, 1, 11, 1, 1, 1, 3, 'org-filtro', '0', 'l. negocio', ''),
(12, 1, 9, 3, 1, 1, 3, 'ser1-filtro', '0', 'servicio', ''),
(13, 7, NULL, 1, 1, 1, 5, 'configuracionVisualizacion', '1-1-1-1', '', ''),
(14, 3, NULL, 1, 1, 1, 5, 'configuracionVisualizacion', '1-1-1', '', ''),
(15, 3, 8, 1, 1, 1, 1, 'temporalidad', '0', '', 'por rango'),
(16, 3, 1, 1, 1, 1, 6, 'neg-filtro', '', 'negocio', ''),
(17, 3, 6, 7, 1, 1, 6, 'can-filtro', '', 'canal', ''),
(18, 3, 10, 3, 1, 1, 6, 'proceso-filtro', '', 'proceso', ''),
(19, 3, 9, 4, 1, 1, 6, 'ser1-filtro', '', 'servicio', ''),
(20, 9, 11, 2, 0, 1, 4, 'org-filtro', '', 'l. negocio', ''),
(21, 9, 10, 3, 0, 1, 4, 'proceso-filtro', '', 'proceso', ''),
(22, 9, 9, 4, 0, 1, 4, 'ser1-filtro', '', 'servicio', ''),
(23, 9, 12, 8, 0, 1, 4, 'experiencia-filtro', '', 'experiencia', ''),
(24, 9, 13, 9, 0, 1, 4, 'recomendacion-filtro', '', 'recomendación', ''),
(25, 9, 14, 10, 0, 1, 4, 'tipologia_cliente-filtro', '', 'tip. cliente', ''),
(26, 1, 1, 1, 1, 1, 7, 'neg-acumulados', '0', '', 'negocio'),
(27, 1, 6, 2, 0, 1, 7, 'can-acumulados', '0', '', 'canal'),
(28, 1, 10, 3, 0, 1, 7, 'proceso-acumulados', '0', '', 'proceso'),
(30, 1, NULL, 1, 1, 1, 1, 'fec_ini', '', '', ''),
(31, 1, NULL, 1, 1, 1, 1, 'fec_fin', '', '', ''),
(32, 1, 15, 2, 1, 1, 3, 'det_can-filtro', '0', 'det. canal', ''),
(33, 9, 8, 1, 1, 1, 8, 'temporalidad', '0', '', 'por rango'),
(34, 9, NULL, 1, 1, 1, 8, 'fec_ini', '', '', ''),
(35, 9, NULL, 1, 1, 1, 8, 'fec_fin', '', '', ''),
(36, 7, 1, 1, 1, 1, 9, 'neg-filtro', '', 'negocio', ''),
(37, 7, 6, 7, 1, 1, 9, 'can-filtro', '', 'canal', ''),
(38, 7, 10, 3, 1, 1, 9, 'proceso-filtro', '', 'proceso', ''),
(39, 7, 9, 4, 1, 1, 9, 'ser1-filtro', '', 'servicio', ''),
(40, 7, 8, 1, 1, 1, 1, 'temporalidad', '0', '', 'por rango'),
(41, 7, NULL, 1, 1, 1, 9, 'cruce', 'org-filtro', '', ''),
(42, 7, 16, 6, 1, 1, 9, 'opi_pregunta-filtro', '', 'opi', ''),
(43, 3, 16, 6, 1, 1, 6, 'opi_pregunta-filtro', '', 'opi', ''),
(44, 1, 1, 1, 1, 1, 99, 'neg-filtro', '0', '', ''),
(45, 3, 11, 2, 1, 1, 6, 'org-filtro', '', 'l. negocio', ''),
(46, 7, 11, 2, 1, 1, 9, 'org-filtro', '', 'l. negocio', ''),
(47, 3, 17, 5, 1, 1, 6, 'pais-filtro', '', 'país', ''),
(48, 7, 17, 5, 1, 1, 9, 'pais-filtro', '', 'país', ''),
(49, 3, NULL, 1, 1, 1, 1, 'fec_ini', '', '', ''),
(50, 3, NULL, 1, 1, 1, 1, 'fec_fin', '', '', ''),
(51, 7, NULL, 1, 1, 1, 1, 'fec_ini', '', '', ''),
(52, 7, NULL, 1, 1, 1, 1, 'fec_fin', '', '', ''),
(53, 8, 8, 1, 1, 1, 1, 'temporalidad', '0', '', 'por rango'),
(55, 8, 6, 7, 1, 1, 6, 'can-filtro', '', 'canal', ''),
(56, 8, 10, 3, 1, 1, 6, 'proceso-filtro', '', 'proceso', ''),
(57, 8, 9, 4, 1, 1, 6, 'ser1-filtro', '', 'servicio', ''),
(58, 8, 16, 6, 1, 1, 6, 'opi_pregunta-filtro', '', 'opi', ''),
(59, 8, 11, 2, 1, 1, 6, 'org-filtro', '', 'l. negocio', ''),
(60, 8, 17, 5, 1, 1, 6, 'pais-filtro', '', 'país', ''),
(61, 8, NULL, 1, 1, 1, 1, 'fec_ini', '', '', ''),
(62, 8, NULL, 1, 1, 1, 1, 'fec_fin', '', '', ''),
(70, 8, NULL, 1, 1, 1, 5, 'optionNegocio', '1', '', ''),
(71, 9, 16, 5, 1, 1, 4, 'opi_pregunta-filtro', '', 'opi', ''),
(72, 1, 7, 2, 1, 1, 2, 'fSegundaVista', '2', '', ''),
(73, 1, 7, 3, 1, 1, 2, 'fTerceraVista', '3', '', '');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `admin_filtros_maqueta`
--
ALTER TABLE `admin_filtros_maqueta`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_57A3F11FCD91FAF2` (`id_maqueta`), ADD KEY `IDX_57A3F11F3F7602BF` (`id_filtro`), ADD KEY `IDX_57A3F11FF1DA68E8` (`bloque`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `admin_filtros_maqueta`
--
ALTER TABLE `admin_filtros_maqueta`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=74;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `admin_filtros_maqueta`
--
ALTER TABLE `admin_filtros_maqueta`
ADD CONSTRAINT `FK_57A3F11F3F7602BF` FOREIGN KEY (`id_filtro`) REFERENCES `admin_filtros` (`id`),
ADD CONSTRAINT `FK_57A3F11FCD91FAF2` FOREIGN KEY (`id_maqueta`) REFERENCES `cms_maquetas` (`id`),
ADD CONSTRAINT `FK_57A3F11FF1DA68E8` FOREIGN KEY (`bloque`) REFERENCES `admin_filtros_bloque` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-01-2017 a las 12:58:03
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cemcepsadb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupo_filtro`
--

CREATE TABLE `grupo_filtro` (
  `id` int(11) NOT NULL,
  `grupo_id` int(11) DEFAULT NULL,
  `filtro_maqueta_id` int(11) DEFAULT NULL,
  `valor_fijo` varchar(256) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `grupo_filtro`
--

INSERT INTO `grupo_filtro` (`id`, `grupo_id`, `filtro_maqueta_id`, `valor_fijo`) VALUES
(2, 3, 44, '4'),
(3, 3, 1, '0');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `grupo_filtro`
--
ALTER TABLE `grupo_filtro`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_1547186B9C833003` (`grupo_id`),
  ADD KEY `IDX_1547186B997E71E8` (`filtro_maqueta_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `grupo_filtro`
--
ALTER TABLE `grupo_filtro`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `grupo_filtro`
--
ALTER TABLE `grupo_filtro`
  ADD CONSTRAINT `FK_1547186B997E71E8` FOREIGN KEY (`filtro_maqueta_id`) REFERENCES `admin_filtros_maqueta` (`id`),
  ADD CONSTRAINT `FK_1547186B9C833003` FOREIGN KEY (`grupo_id`) REFERENCES `fos_group` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-01-2017 a las 12:52:31
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cemcepsadb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_proceso`
--

CREATE TABLE `admin_proceso` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `admin_proceso`
--

INSERT INTO `admin_proceso` (`id`, `nombre`) VALUES
(2, 'Alta/Contratación'),
(3, 'Oferta/Fidelización'),
(4, 'Pedido/Compra'),
(5, 'Entrega/Suministro'),
(6, 'Pagos/Cobros'),
(7, 'Facturación'),
(8, 'Gestión Cliente'),
(9, 'Asistencia Técnica'),
(10, 'Bajas'),
(11, 'Seguridad'),
(12, 'Producto/Servicio');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `admin_proceso`
--
ALTER TABLE `admin_proceso`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `admin_proceso`
--
ALTER TABLE `admin_proceso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

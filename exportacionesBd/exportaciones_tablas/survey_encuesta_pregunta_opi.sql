-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-01-2017 a las 12:58:38
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cemcepsadb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `survey_encuesta_pregunta_opi`
--

CREATE TABLE `survey_encuesta_pregunta_opi` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `survey_encuesta_pregunta_opi`
--

INSERT INTO `survey_encuesta_pregunta_opi` (`id`, `nombre`) VALUES
(1, 'web_ptv_priv'),
(2, 'SAC_P_MP_Red'),
(3, 'UX_Tienda_online'),
(4, 'gasenvasado_rascaygana'),
(5, 'UX_Web_General'),
(6, 'SAC_R_Gas'),
(7, 'optima'),
(8, 'SAC_R_Fidelidad'),
(9, 'optima_mail'),
(10, 'pto_venta_prt_ticket'),
(11, 'es_habitual'),
(12, 'Regalo_Liga_Orange'),
(13, 'promocionPorqueTUVuelves_mai'),
(14, 'Regalo_PTV'),
(15, 'CX_Gas_RyG'),
(16, 'SAC_P_D_Red'),
(18, 'SAC_P_Lubricantes'),
(19, 'SAC_R_VVDD'),
(20, 'calefaccion_sms'),
(21, 'calefaccion-mail'),
(22, 'Gas_Oferta1_BajoC_25-5_mail'),
(23, 'Gas_Oferta1_Generica_10-10_mail'),
(24, 'Gas_Oferta1_GranC_5-12_mail'),
(25, 'Gas_Oferta1_BajoC_25-5_telf'),
(26, 'Gas_Oferta1_Generica_10-10_telf'),
(27, 'Gas_Oferta1_GranC_5-12_telf'),
(28, 'Gas_Oferta2_BajoC_25-5_telf'),
(29, 'Gas_Oferta2_Generica_10-10_telf'),
(30, 'Gas_Oferta2_GranC_5-12_telf'),
(31, 'pto_venta_prt_cartel'),
(32, 'SAC_P_D_Red'),
(33, 'Lub_Xtar'),
(34, 'Gas_Oferta2_BajoC_25-5_telf'),
(35, 'Gas_Oferta2_Generica_10-10_telf'),
(36, 'Gas_Oferta2_GranC_5-12_telf');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `survey_encuesta_pregunta_opi`
--
ALTER TABLE `survey_encuesta_pregunta_opi`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `survey_encuesta_pregunta_opi`
--
ALTER TABLE `survey_encuesta_pregunta_opi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-01-2017 a las 12:57:37
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cemcepsadb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_zona`
--

CREATE TABLE `cms_zona` (
  `id` int(11) NOT NULL,
  `maqueta_id` int(11) DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `activo` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `twig` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `cms_zona`
--

INSERT INTO `cms_zona` (`id`, `maqueta_id`, `nombre`, `descripcion`, `activo`, `orden`, `twig`) VALUES
(1, 1, 'ZonaBienvenidos', 'ZonaBienvenidosEnPrincipal', 1, 1, 'ZonaSimple'),
(2, 2, 'ZonaBienvenidos', 'ZonaBienvenidosEnAlertas', 0, 1, 'ZonaSimple'),
(3, 3, 'ZonaBienvenidos', 'ZonaBienvenidosEnCuadroMandoDesgloseInformacion', 1, 1, 'ZonaSimple'),
(4, 1, 'ZonaContenidosConZonaLateral', 'ZonaContenidosConZonaLateralEnPrincipal', 1, 3, 'ZonaContenidosConZonaLateral'),
(5, 1, 'ZonaLateral', 'ZonaLateralEnPrincipal', 1, 2, 'ZonaLateral'),
(6, 7, 'ZonaBienvenidos', 'ZonaBienvenidosEnCuadroMandoCruceVariables', 1, 1, 'ZonaSimple'),
(7, 8, 'ZonaBienvenidos', 'ZonaBienvenidosEnCuadroMandoInformacionJerarquica', 1, 1, 'ZonaSimple'),
(8, 7, 'ZonaIndicadores', 'ZonaIndicadoresEnCuadroMandoCruceVariables', 1, 2, 'ZonaSimple'),
(9, 8, 'ZonaIndicadores', 'ZonaIndicadoresEnCuadroMandoInformacionJerarquica', 1, 2, 'ZonaSimple'),
(10, 7, 'ZonaContenidosConZonaLateral', 'ZonaContenidosConZonaLateralEnCuadroMandoCruceVariables', 1, 4, 'ZonaContenidosConZonaLateral'),
(11, 7, 'ZonaLateral', 'ZonaLateralEnCuadroMandoCruceVariables', 1, 3, 'ZonaLateral'),
(12, 3, 'ZonaContenidosConZonaLateral', 'ZonaContenidosConZonaLateralEnCuadroMandoDesgloseInformacion', 1, 4, 'ZonaContenidosConZonaLateral'),
(13, 3, 'ZonaLateral', 'ZonaLateralEnCuadroMandoDesgloseInformacion', 1, 3, 'ZonaLateral'),
(14, 8, 'ZonaContenidosConZonaLateral', 'ZonaContenidosConZonaLateralEnCuadroMandoInformacionJerarquica', 1, 4, 'ZonaContenidosConZonaLateral'),
(15, 8, 'ZonaLateral', 'ZonaLateralEnCuadroMandoInformacionJerarquica', 1, 3, 'ZonaLateral'),
(16, 3, 'ZonaIndicadores', 'ZonaIndicadoresEnCuadroMandoDesgloseInformacion', 1, 2, 'ZonaSimple'),
(17, 5, 'ZonaBienvenidos', 'ZonaBienvenidosEnReportes', 1, 1, 'ZonaSimple'),
(18, 5, 'ZonaContenidos', 'ZonaContenidosEnReportes', 1, 1, 'ZonaSimple'),
(19, 2, 'ZonaBienvenidos', 'ZonaBienvenidosEnAlertas', 1, 1, 'ZonaSimple'),
(20, 2, 'ZonaContenidos', 'ZonaContenidosEnAlertas', 1, 1, 'ZonaSimple'),
(21, 9, 'ZonaBienvenidos', 'ZonaBienvenidosEnEncuestas', 1, 1, 'ZonaSimple'),
(22, 9, 'ZonaContenidos', 'ZonaContenidosEnEncuestas', 1, 1, 'ZonaPaddingLateral'),
(23, 10, 'ZonaBienvenidos', 'ZonaBienvenidosEnD_Estrategico', 1, 1, 'ZonaSimple'),
(24, 10, 'ZonaContenidos', 'ZonaContenidosEnD_Estrategico', 1, 1, 'ZonaSimple'),
(25, 11, 'ZonaBienvenidos', 'ZonaBienvenidosEnI_Estrategico', 1, 1, 'ZonaSimple'),
(26, 11, 'ZonaContenidos', 'ZonaContenidosEnI_Estrategico', 1, 1, 'ZonaSimple');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cms_zona`
--
ALTER TABLE `cms_zona`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_6010A9631CD04871` (`maqueta_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cms_zona`
--
ALTER TABLE `cms_zona`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cms_zona`
--
ALTER TABLE `cms_zona`
  ADD CONSTRAINT `FK_6010A9631CD04871` FOREIGN KEY (`maqueta_id`) REFERENCES `cms_maquetas` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

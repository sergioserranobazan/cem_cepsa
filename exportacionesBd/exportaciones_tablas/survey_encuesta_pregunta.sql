-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 17-01-2017 a las 15:55:52
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cemcepsadb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `survey_encuesta_pregunta`
--

CREATE TABLE `survey_encuesta_pregunta` (
  `id` int(11) NOT NULL,
  `sub_bloque_id` int(11) DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cem_tipo_id` int(11) DEFAULT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcion_completa` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `survey_encuesta_pregunta`
--

INSERT INTO `survey_encuesta_pregunta` (`id`, `sub_bloque_id`, `nombre`, `cem_tipo_id`, `descripcion`, `descripcion_completa`) VALUES
(1, 1, 'G1', 1, 'Sentimiento', 'Sentimiento del cliente con Cepsa'),
(2, 1, 'G2', 2, 'Recomendación', 'Si recomendaria a otros clientes'),
(3, 1, 'G3', 3, 'Experiencia', 'Indicador de satisfaccion con la Experiencia'),
(4, 3, 'OF3', 4, 'Fidelización', 'Preguntas relativas a fidelidad'),
(5, 3, 'OF1', 4, 'Esfuerzo', 'Facilidad/Esfuerzo solicitado el cliente en el proceso realizado'),
(6, 3, 'OF2', 4, 'Promociones', 'Satisfaccion con la promoción'),
(7, 6, 'P2', 4, 'Rapidez de pago', 'Rapidez en el pago'),
(8, 8, 'GC4', 4, 'Amabilidad', 'Amabilidad en la atención al cliente'),
(9, 12, 'PS1', 4, 'Productos consumidos', 'Productos que consume'),
(10, 13, 'N1', NULL, NULL, NULL),
(11, 13, 'N2', NULL, NULL, NULL),
(12, 13, 'N3', NULL, NULL, NULL),
(13, 13, 'N4', NULL, NULL, NULL),
(14, 16, 'PA1', NULL, NULL, NULL),
(15, 2, 'A1', 4, 'Esfuerzo', 'Facilidad/Esfuerzo solicitado el cliente en el proceso realizado'),
(16, 16, 'PA2', NULL, NULL, NULL),
(17, 16, 'PA3', NULL, NULL, NULL),
(20, 4, 'PC1', 4, 'Esfuerzo', 'Facilidad/Esfuerzo solicitado el cliente en el proceso realizado'),
(21, 12, 'PS2', 4, 'Variedad de productos', 'Satisfaccion con la variedad de productos'),
(22, 3, 'OF4', 4, 'Regalo incentivo', 'Satisfacción con el regalo recibido'),
(23, 8, 'GC1', 4, 'Esfuerzo', 'Facilidad/Esfuerzo solicitado el cliente en el proceso realizado'),
(24, 4, 'PC2', 4, 'Rapidez', 'Rapidez en el proceso'),
(25, 8, 'GC2', 4, 'Rapidez', 'Rapidez en el proceso'),
(26, 8, 'GC3', 4, 'Resolución', 'Satisfacción con la resolución de recibida'),
(100, NULL, 'E3', NULL, NULL, NULL),
(101, NULL, 'E4', NULL, NULL, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `survey_encuesta_pregunta`
--
ALTER TABLE `survey_encuesta_pregunta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_EFE0EF35189FD802` (`sub_bloque_id`),
  ADD KEY `IDX_EFE0EF35801EB86A` (`cem_tipo_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `survey_encuesta_pregunta`
--
ALTER TABLE `survey_encuesta_pregunta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `survey_encuesta_pregunta`
--
ALTER TABLE `survey_encuesta_pregunta`
  ADD CONSTRAINT `FK_EFE0EF35189FD802` FOREIGN KEY (`sub_bloque_id`) REFERENCES `survey_encuesta_sub_bloque` (`id`),
  ADD CONSTRAINT `FK_EFE0EF35801EB86A` FOREIGN KEY (`cem_tipo_id`) REFERENCES `admin_tipo_cem` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

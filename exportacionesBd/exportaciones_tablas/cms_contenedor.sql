-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-01-2017 a las 12:57:11
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cemcepsadb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_contenedor`
--

CREATE TABLE `cms_contenedor` (
  `id` int(11) NOT NULL,
  `zona_id` int(11) DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `activo` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `twig` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `cms_contenedor`
--

INSERT INTO `cms_contenedor` (`id`, `zona_id`, `nombre`, `descripcion`, `activo`, `orden`, `twig`) VALUES
(1, 1, 'ContenedorMenuEnPrincipal', 'Contenedor para el menú', 1, 2, 'contenedorMenu'),
(2, 2, 'Contenedor para Menú', 'ContenedorMenuEnAlertas', 1, 1, 'contenedorMenu'),
(3, 3, 'Contenedor para Menú', 'ContenedorMenuEnCuadroMando', 1, 1, 'contenedorMenu'),
(4, 4, 'ContenedorSimpleEnZonaContenidosConZonaLateralEnPrincipal', '', 1, 1, 'ContenedorSimple'),
(5, 5, 'ContenedorSimpleEnZonaLateralEnPrincipal', '', 1, 2, 'ContenedorSimple'),
(6, 6, 'Contenedor para Menú', 'ContenedorMenuEnCuadroMandoCruceVariables', 1, 1, 'contenedorMenu'),
(7, 7, 'Contenedor para Menú', 'ContenedorMenuEnCuadroMandoInformacionJerarquica', 1, 1, 'contenedorMenu'),
(8, 16, 'ContenedorIndicadoresCMDesgloseInformacion', 'ContenedorSimpleEnZonaIndicadoresEnCuadroMandoDesgloseInformacion', 1, 1, 'ContenedorSimple'),
(9, 12, 'ContenedorGraficosCMDesgloseInformacion', 'ContenedorSimpleEnZonaContenidosConZonaLateralEnCuadroMandoDesgloseInformacion', 1, 2, 'ContenedorSimple'),
(10, 13, 'BloqueFiltrosCMDesgloseInformacion', 'ContenedorWidgetsFiltrosEnZonaLateralEnCuadroMandoDesgloseInformacion', 1, 2, 'Especificos/ContenedorWidgetsFiltrosCMDesgloseInformacion'),
(11, 8, 'ContenedorIndicadoresCMCruceVariables', 'ContenedorSimpleEnZonaIndicadoresEnCuadroMandoCruceVariables', 1, 1, 'ContenedorSimple'),
(12, 10, 'ContenedorGraficosCMCruceVariables', 'ContenedorSimpleEnZonaContenidosConZonaLateralEnCuadroMandoCruceVariables', 1, 2, 'ContenedorSimple'),
(13, 11, 'BloqueFiltrosCMCruceVariables', 'ContenedorSimpleEnZonaLateralEnCuadroMandoCruceVariables', 1, 2, 'Especificos/ContenedorWidgetsFiltrosCMCruceVariables'),
(15, 9, 'ContenedorSimple10', 'ContenedorSimpleEnZonaLateralEnEnCMInformacionJerarquica', 1, 2, 'ContenedorSimple'),
(16, 15, 'ContenedorIndicadoresCMInformacionJerarquica', 'ContenedorSimpleEnZonaIndicadoresEnEnCMInformacionJerarquica', 1, 1, 'ContenedorSimple'),
(17, 17, 'Contenedor para Menú', 'ContenedorMenuEnReportes', 1, 1, 'contenedorMenu'),
(18, 18, 'ContenedorFiltros', 'ContenedorFiltrosEnReportes', 1, 1, 'ContenedorSimple'),
(19, 18, 'ContenedorTabla', 'ContenedorTablaEnReportes', 1, 2, 'ContenedorSimple'),
(20, 19, 'Contenedor para Menú', 'ContenedorMenuEnAlertas', 1, 1, 'contenedorMenu'),
(21, 20, 'ContenedorFiltros', 'ContenedorFiltrosEnAlertas', 1, 1, 'ContenedorSimple'),
(22, 20, 'ContenedorTabla', 'ContenedorTablaEnAlertas', 1, 2, 'ContenedorSimple'),
(23, 21, 'Contenedor para Menú', 'ContenedorMenuEnEncuestas', 1, 1, 'contenedorMenu'),
(24, 22, 'ContenedorFiltros', 'ContenedorFiltrosEnEncuestas', 1, 1, 'ContenedorFiltros4Columnas'),
(25, 22, 'ContenedorTabla', 'ContenedorTablaEnEncuestas', 1, 2, 'ContenedorSimple'),
(26, 23, 'Contenedor para Menú', 'ContenedorMenuEnD_Estrategico', 1, 1, 'contenedorMenu'),
(27, 24, 'ContenedorTabla', 'ContenedorTablaEnD_Estrategico', 1, 2, 'ContenedorSimple'),
(29, 25, 'Contenedor para Menú', 'ContenedorMenuEnI_Estrategico', 1, 1, 'contenedorMenu'),
(30, 26, 'ContenedorTabla', 'ContenedorTablaEnI_Estrategico', 1, 2, 'ContenedorSimple'),
(32, 5, 'ContenedorWidgetsFiltros', '', 1, 3, 'ContenedorWidgetsFiltros'),
(33, 14, 'ContenedorTreeGreedCMInformacionJerarquica', 'ContenedorSimpleEnZonaContenidosConZonaLateralEnCMInformacionJerarquica', 1, 2, 'ContenedorSimple'),
(34, 15, 'BloqueFiltrosCMInformacionJerarquica', 'ContenedorSimpleEnZonaLateralEnCMInformacionJerarquica', 1, 2, 'Especificos/ContenedorWidgetsFiltrosCMInformacionJerarquica');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cms_contenedor`
--
ALTER TABLE `cms_contenedor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_D575F034104EA8FC` (`zona_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cms_contenedor`
--
ALTER TABLE `cms_contenedor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cms_contenedor`
--
ALTER TABLE `cms_contenedor`
  ADD CONSTRAINT `FK_D575F034104EA8FC` FOREIGN KEY (`zona_id`) REFERENCES `cms_zona` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

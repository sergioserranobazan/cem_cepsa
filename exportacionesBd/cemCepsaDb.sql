-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 23-02-2017 a las 17:47:34
-- Versión del servidor: 5.6.21
-- Versión de PHP: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `cemcepsadb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_canal_det_canal`
--

CREATE TABLE IF NOT EXISTS `admin_canal_det_canal` (
`id` int(11) NOT NULL,
  `canal_id` int(11) DEFAULT NULL,
  `determinacion_canal_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_canal_encuestacion`
--

CREATE TABLE IF NOT EXISTS `admin_canal_encuestacion` (
`id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `admin_canal_encuestacion`
--

INSERT INTO `admin_canal_encuestacion` (`id`, `nombre`) VALUES
(1, 'sms'),
(2, 'mail'),
(3, 'factura'),
(4, 'flyer'),
(5, 'web'),
(6, 'ivr'),
(7, 'telefono'),
(8, 'ticket'),
(9, 'cartel');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_cliente`
--

CREATE TABLE IF NOT EXISTS `admin_cliente` (
`id` int(11) NOT NULL,
  `cod_pers` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telf_BI` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mail_BI` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telf` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nif` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `admin_cliente`
--

INSERT INTO `admin_cliente` (`id`, `cod_pers`, `telf_BI`, `mail_BI`, `id_hash`, `telf`, `mail`, `nif`) VALUES
(1, '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_determinacion_canal`
--

CREATE TABLE IF NOT EXISTS `admin_determinacion_canal` (
`id` int(11) NOT NULL,
  `nombre` varchar(256) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `admin_determinacion_canal`
--

INSERT INTO `admin_determinacion_canal` (`id`, `nombre`) VALUES
(1, 'sin especificar'),
(2, 'ptv_apriv_popup'),
(3, 'ptv_publ'),
(4, 'Optima'),
(5, 'tienda'),
(6, 'resp_sac'),
(7, 'ptv_apriv_banner'),
(8, 'Regalo_Optima'),
(9, 'Regalo_PTV');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_experiencia`
--

CREATE TABLE IF NOT EXISTS `admin_experiencia` (
`id` int(11) NOT NULL,
  `nombre` varchar(256) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `admin_experiencia`
--

INSERT INTO `admin_experiencia` (`id`, `nombre`) VALUES
(1, '0'),
(2, '1'),
(3, '2'),
(4, '3'),
(5, '4'),
(6, '5'),
(7, '6'),
(8, '7'),
(9, '8'),
(10, '9'),
(11, '10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_filtros`
--

CREATE TABLE IF NOT EXISTS `admin_filtros` (
`id` int(11) NOT NULL,
  `padre_id` int(11) DEFAULT NULL,
  `tabla` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tablaHijo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `campoId` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'id',
  `campoDescripcion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nombreFiltro` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `campoIdPadre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auto` tinyint(1) DEFAULT NULL,
  `etiqueta` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sentencia` longtext COLLATE utf8_unicode_ci,
  `entidadDoctrine` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `admin_filtros`
--

INSERT INTO `admin_filtros` (`id`, `padre_id`, `tabla`, `tablaHijo`, `campoId`, `campoDescripcion`, `nombreFiltro`, `campoIdPadre`, `auto`, `etiqueta`, `sentencia`, `entidadDoctrine`) VALUES
(1, NULL, 'admin_negocio', 'admin_segmento', '', NULL, 'neg', NULL, NULL, NULL, NULL, 'AdminBundle:Negocio'),
(5, NULL, 'admin_segmento', NULL, '', NULL, 'seg', NULL, NULL, NULL, NULL, 'AdminBundle:Segmento'),
(6, NULL, 'admin_canal_encuestacion', NULL, '', NULL, 'can', NULL, NULL, NULL, NULL, 'AdminBundle:CanalEncuestacion'),
(7, NULL, 'admin_vista', NULL, 'id', NULL, 'vista', NULL, NULL, NULL, NULL, 'AdminBundle:Vista'),
(8, NULL, 'admin_temporalidad', NULL, 'id', NULL, 'temporalidad', NULL, NULL, NULL, NULL, 'AdminBundle:Temporalidad'),
(9, NULL, 'admin_servicio', NULL, 'id', NULL, 'ser1', NULL, NULL, NULL, NULL, 'AdminBundle:Servicio'),
(10, NULL, 'admin_proceso', NULL, 'id', NULL, 'proceso', NULL, NULL, NULL, NULL, 'AdminBundle:Proceso'),
(11, NULL, 'admin_linea_negocio', NULL, 'id', NULL, 'org', NULL, NULL, NULL, NULL, 'AdminBundle:LineaNegocio'),
(12, NULL, 'admin_experiencia', NULL, 'id', NULL, 'experiencia', NULL, NULL, NULL, NULL, 'AdminBundle:Experiencia'),
(13, NULL, 'admin_recomendacion', NULL, 'id', NULL, 'recomendacion', NULL, NULL, NULL, NULL, 'AdminBundle:Recomendacion'),
(14, NULL, 'admin_tipologia_cliente', NULL, 'id', NULL, 'tipologia_cliente', NULL, NULL, NULL, NULL, 'AdminBundle:TipologiaCliente'),
(15, NULL, 'admin_determinacion_canal', NULL, '', NULL, 'det_can', NULL, NULL, NULL, NULL, 'AdminBundle:DeterminacionCanal'),
(16, NULL, 'survey_encuesta_pregunta_opi', NULL, 'id', NULL, 'opi_pregunta', NULL, NULL, NULL, NULL, 'SurveyBundle:EncuestaPreguntaOpi'),
(17, NULL, 'admin_pais', NULL, 'id', NULL, 'pais', NULL, NULL, NULL, NULL, 'AdminBundle:Pais');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_filtros_bloque`
--

CREATE TABLE IF NOT EXISTS `admin_filtros_bloque` (
`id` int(11) NOT NULL,
  `nombreId` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cabecera` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `admin_filtros_bloque`
--

INSERT INTO `admin_filtros_bloque` (`id`, `nombreId`, `cabecera`) VALUES
(1, 'temporalidad', 'temporalidad'),
(2, 'vistas', 'vistas'),
(3, 'opc_consulta', 'filtros'),
(4, 'filtros_encuestas', ''),
(5, 'bloque_general', ''),
(6, 'variables_filtro', 'Variables Filtro'),
(7, 'informacion_especifica_proceso', 'información específica de proceso'),
(8, 'temporalidad', 'temporalidad'),
(9, 'variables_filtro_cruce', 'Variables Filtro/Cruce'),
(99, 'filtros_grupo_usuario', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_filtros_maqueta`
--

CREATE TABLE IF NOT EXISTS `admin_filtros_maqueta` (
`id` int(11) NOT NULL,
  `id_maqueta` int(11) DEFAULT NULL,
  `id_filtro` int(11) DEFAULT NULL,
  `orden` int(11) NOT NULL DEFAULT '1',
  `tipoFiltro` int(11) NOT NULL DEFAULT '1',
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `bloque` int(11) DEFAULT NULL,
  `nombreId` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `valorDefault` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tag` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `valorCampoCero` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `admin_filtros_maqueta`
--

INSERT INTO `admin_filtros_maqueta` (`id`, `id_maqueta`, `id_filtro`, `orden`, `tipoFiltro`, `activo`, `bloque`, `nombreId`, `valorDefault`, `tag`, `valorCampoCero`) VALUES
(1, 9, 1, 1, 0, 1, 4, 'neg-filtro', '', 'negocio', ''),
(5, 9, 5, 7, 0, 1, 4, 'seg-filtro', '', 'segmento', ''),
(6, 9, 6, 6, 0, 1, 4, 'can-filtro', '', 'canal', ''),
(7, 1, 8, 1, 1, 1, 1, 'temporalidad', '0', '', 'por rango'),
(8, 1, 7, 1, 1, 1, 2, 'fPrimeraVista', '1', '', ''),
(11, 1, 11, 1, 1, 1, 3, 'org-filtro', '0', 'l. negocio', ''),
(12, 1, 9, 3, 1, 1, 3, 'ser1-filtro', '0', 'servicio', ''),
(13, 7, NULL, 1, 1, 1, 5, 'configuracionVisualizacion', '1-1-1-1', '', ''),
(14, 3, NULL, 1, 1, 1, 5, 'configuracionVisualizacion', '1-1-1', '', ''),
(15, 3, 8, 1, 1, 1, 1, 'temporalidad', '0', '', 'por rango'),
(16, 3, 1, 1, 1, 1, 6, 'neg-filtro', '', 'negocio', ''),
(17, 3, 6, 7, 1, 1, 6, 'can-filtro', '', 'canal', ''),
(18, 3, 10, 3, 1, 1, 6, 'proceso-filtro', '', 'proceso', ''),
(19, 3, 9, 4, 1, 1, 6, 'ser1-filtro', '', 'servicio', ''),
(20, 9, 11, 2, 0, 1, 4, 'org-filtro', '', 'l. negocio', ''),
(21, 9, 10, 3, 0, 1, 4, 'proceso-filtro', '', 'proceso', ''),
(22, 9, 9, 4, 0, 1, 4, 'ser1-filtro', '', 'servicio', ''),
(23, 9, 12, 8, 0, 1, 4, 'experiencia-filtro', '', 'experiencia', ''),
(24, 9, 13, 9, 0, 1, 4, 'recomendacion-filtro', '', 'recomendación', ''),
(25, 9, 14, 10, 0, 1, 4, 'tipologia_cliente-filtro', '', 'tip. cliente', ''),
(26, 1, 1, 1, 1, 1, 7, 'neg-acumulados', '0', '', 'negocio'),
(27, 1, 6, 2, 0, 1, 7, 'can-acumulados', '0', '', 'canal'),
(28, 1, 10, 3, 0, 1, 7, 'proceso-acumulados', '0', '', 'proceso'),
(30, 1, NULL, 1, 1, 1, 1, 'fec_ini', '', '', ''),
(31, 1, NULL, 1, 1, 1, 1, 'fec_fin', '', '', ''),
(32, 1, 15, 2, 1, 1, 3, 'det_can-filtro', '0', 'det. canal', ''),
(33, 9, 8, 1, 1, 1, 8, 'temporalidad', '0', '', 'por rango'),
(34, 9, NULL, 1, 1, 1, 8, 'fec_ini', '', '', ''),
(35, 9, NULL, 1, 1, 1, 8, 'fec_fin', '', '', ''),
(36, 7, 1, 1, 1, 1, 9, 'neg-filtro', '', 'negocio', ''),
(37, 7, 6, 7, 1, 1, 9, 'can-filtro', '', 'canal', ''),
(38, 7, 10, 3, 1, 1, 9, 'proceso-filtro', '', 'proceso', ''),
(39, 7, 9, 4, 1, 1, 9, 'ser1-filtro', '', 'servicio', ''),
(40, 7, 8, 1, 1, 1, 1, 'temporalidad', '0', '', 'por rango'),
(41, 7, NULL, 1, 1, 1, 9, 'cruce', 'org-filtro', '', ''),
(42, 7, 16, 6, 1, 1, 9, 'opi_pregunta-filtro', '', 'opi', ''),
(43, 3, 16, 6, 1, 1, 6, 'opi_pregunta-filtro', '', 'opi', ''),
(44, 1, 1, 1, 1, 1, 99, 'neg-filtro', '0', '', ''),
(45, 3, 11, 2, 1, 1, 6, 'org-filtro', '', 'l. negocio', ''),
(46, 7, 11, 2, 1, 1, 9, 'org-filtro', '', 'l. negocio', ''),
(47, 3, 17, 5, 1, 1, 6, 'pais-filtro', '', 'país', ''),
(48, 7, 17, 5, 1, 1, 9, 'pais-filtro', '', 'país', ''),
(49, 3, NULL, 1, 1, 1, 1, 'fec_ini', '', '', ''),
(50, 3, NULL, 1, 1, 1, 1, 'fec_fin', '', '', ''),
(51, 7, NULL, 1, 1, 1, 1, 'fec_ini', '', '', ''),
(52, 7, NULL, 1, 1, 1, 1, 'fec_fin', '', '', ''),
(53, 8, 8, 1, 1, 1, 1, 'temporalidad', '0', '', 'por rango'),
(55, 8, 6, 7, 1, 1, 6, 'can-filtro', '', 'canal', ''),
(56, 8, 10, 3, 1, 1, 6, 'proceso-filtro', '', 'proceso', ''),
(57, 8, 9, 4, 1, 1, 6, 'ser1-filtro', '', 'servicio', ''),
(58, 8, 16, 6, 1, 1, 6, 'opi_pregunta-filtro', '', 'opi', ''),
(59, 8, 11, 2, 1, 1, 6, 'org-filtro', '', 'l. negocio', ''),
(60, 8, 17, 5, 1, 1, 6, 'pais-filtro', '', 'país', ''),
(61, 8, NULL, 1, 1, 1, 1, 'fec_ini', '', '', ''),
(62, 8, NULL, 1, 1, 1, 1, 'fec_fin', '', '', ''),
(70, 8, NULL, 1, 1, 1, 5, 'optionNegocio', '1', '', ''),
(71, 9, 16, 5, 1, 1, 4, 'opi_pregunta-filtro', '', 'opi', ''),
(72, 1, 7, 2, 1, 1, 2, 'fSegundaVista', '2', '', ''),
(73, 1, 7, 3, 1, 1, 2, 'fTerceraVista', '3', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_linea_negocio`
--

CREATE TABLE IF NOT EXISTS `admin_linea_negocio` (
`id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `admin_linea_negocio`
--

INSERT INTO `admin_linea_negocio` (`id`, `nombre`) VALUES
(1, 'sin especificar'),
(2, 'Particular'),
(3, 'Envasado'),
(4, 'Abanderado'),
(5, 'Profesional del Transporte'),
(6, 'Automocion'),
(7, 'Retail'),
(8, 'canalizado'),
(9, 'gas_natural');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_localidad`
--

CREATE TABLE IF NOT EXISTS `admin_localidad` (
`id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `admin_localidad`
--

INSERT INTO `admin_localidad` (`id`, `nombre`) VALUES
(1, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_negocio`
--

CREATE TABLE IF NOT EXISTS `admin_negocio` (
`id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `admin_negocio`
--

INSERT INTO `admin_negocio` (`id`, `nombre`) VALUES
(1, 'red'),
(2, 'gas'),
(3, 'electricidad'),
(4, 'ecommerce'),
(5, 'ventas directas'),
(6, 'lubricantes'),
(7, 'asfaltos'),
(8, 'aviacion'),
(9, 'bunker'),
(10, 'proveedores'),
(11, 'empleados');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_organizacion`
--

CREATE TABLE IF NOT EXISTS `admin_organizacion` (
`id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `admin_organizacion`
--

INSERT INTO `admin_organizacion` (`id`, `nombre`) VALUES
(1, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_pais`
--

CREATE TABLE IF NOT EXISTS `admin_pais` (
`id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `admin_pais`
--

INSERT INTO `admin_pais` (`id`, `nombre`) VALUES
(1, 'España'),
(2, 'Portugal');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_permutaciones_vistas`
--

CREATE TABLE IF NOT EXISTS `admin_permutaciones_vistas` (
`id` int(11) NOT NULL,
  `indicador_id` int(11) DEFAULT NULL,
  `sub_bloque_id` int(11) DEFAULT NULL,
  `negocio_id` int(11) DEFAULT NULL,
  `canal_id` int(11) DEFAULT NULL,
  `proceso_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_proceso`
--

CREATE TABLE IF NOT EXISTS `admin_proceso` (
`id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `admin_proceso`
--

INSERT INTO `admin_proceso` (`id`, `nombre`) VALUES
(2, 'Alta/Contratación'),
(3, 'Oferta/Fidelización'),
(4, 'Pedido/Compra'),
(5, 'Entrega/Suministro'),
(6, 'Pagos/Cobros'),
(7, 'Facturación'),
(8, 'Gestión Cliente'),
(9, 'Asistencia Técnica'),
(10, 'Bajas'),
(11, 'Seguridad'),
(12, 'Producto/Servicio');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_producto`
--

CREATE TABLE IF NOT EXISTS `admin_producto` (
`id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `admin_producto`
--

INSERT INTO `admin_producto` (`id`, `nombre`) VALUES
(1, 'sin especificar'),
(2, 'Combustible');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_provincia`
--

CREATE TABLE IF NOT EXISTS `admin_provincia` (
`id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `admin_provincia`
--

INSERT INTO `admin_provincia` (`id`, `nombre`) VALUES
(1, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_recomendacion`
--

CREATE TABLE IF NOT EXISTS `admin_recomendacion` (
`id` int(11) NOT NULL,
  `nombre` varchar(256) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `admin_recomendacion`
--

INSERT INTO `admin_recomendacion` (`id`, `nombre`) VALUES
(1, '0'),
(2, '1'),
(3, '2'),
(4, '3'),
(5, '4'),
(6, '5'),
(7, '6'),
(8, '7'),
(9, '8'),
(10, '9'),
(11, '10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_segmento`
--

CREATE TABLE IF NOT EXISTS `admin_segmento` (
`id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `admin_segmento`
--

INSERT INTO `admin_segmento` (`id`, `nombre`) VALUES
(1, 'sin especificar');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_servicio`
--

CREATE TABLE IF NOT EXISTS `admin_servicio` (
`id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `admin_servicio`
--

INSERT INTO `admin_servicio` (`id`, `nombre`) VALUES
(1, 'sin especificar');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_temporalidad`
--

CREATE TABLE IF NOT EXISTS `admin_temporalidad` (
`id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `admin_temporalidad`
--

INSERT INTO `admin_temporalidad` (`id`, `nombre`) VALUES
(1, 'semana'),
(2, 'mes'),
(3, 'trimestre'),
(4, 'año');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_tipificacion_opinion`
--

CREATE TABLE IF NOT EXISTS `admin_tipificacion_opinion` (
`id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `admin_tipificacion_opinion`
--

INSERT INTO `admin_tipificacion_opinion` (`id`, `nombre`) VALUES
(1, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_tipologia_cliente`
--

CREATE TABLE IF NOT EXISTS `admin_tipologia_cliente` (
`id` int(11) NOT NULL,
  `nombre` varchar(256) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `admin_tipologia_cliente`
--

INSERT INTO `admin_tipologia_cliente` (`id`, `nombre`) VALUES
(1, 'sin especificar');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_tipo_cem`
--

CREATE TABLE IF NOT EXISTS `admin_tipo_cem` (
`id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `admin_tipo_cem`
--

INSERT INTO `admin_tipo_cem` (`id`, `nombre`) VALUES
(1, 'EI'),
(2, 'NPS'),
(3, 'SAT'),
(4, 'VAL'),
(5, 'TXT');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_variables_organizacion_jerarquica`
--

CREATE TABLE IF NOT EXISTS `admin_variables_organizacion_jerarquica` (
`id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_vista`
--

CREATE TABLE IF NOT EXISTS `admin_vista` (
`id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `filtro_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `admin_vista`
--

INSERT INTO `admin_vista` (`id`, `nombre`, `filtro_id`) VALUES
(1, 'negocio', 1),
(2, 'canal', 6),
(3, 'proceso', 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_zona`
--

CREATE TABLE IF NOT EXISTS `admin_zona` (
`id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `admin_zona`
--

INSERT INTO `admin_zona` (`id`, `nombre`) VALUES
(1, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_contenedor`
--

CREATE TABLE IF NOT EXISTS `cms_contenedor` (
`id` int(11) NOT NULL,
  `zona_id` int(11) DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `activo` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `twig` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `cms_contenedor`
--

INSERT INTO `cms_contenedor` (`id`, `zona_id`, `nombre`, `descripcion`, `activo`, `orden`, `twig`) VALUES
(1, 1, 'ContenedorMenuEnPrincipal', 'Contenedor para el menú', 1, 2, 'contenedorMenu'),
(2, 2, 'Contenedor para Menú', 'ContenedorMenuEnAlertas', 1, 1, 'contenedorMenu'),
(3, 3, 'Contenedor para Menú', 'ContenedorMenuEnCuadroMando', 1, 1, 'contenedorMenu'),
(4, 4, 'ContenedorSimpleEnZonaContenidosConZonaLateralEnPrincipal', '', 1, 1, 'ContenedorSimple'),
(5, 5, 'ContenedorSimpleEnZonaLateralEnPrincipal', '', 1, 2, 'ContenedorSimple'),
(6, 6, 'Contenedor para Menú', 'ContenedorMenuEnCuadroMandoCruceVariables', 1, 1, 'contenedorMenu'),
(7, 7, 'Contenedor para Menú', 'ContenedorMenuEnCuadroMandoInformacionJerarquica', 1, 1, 'contenedorMenu'),
(8, 16, 'ContenedorIndicadoresCMDesgloseInformacion', 'ContenedorSimpleEnZonaIndicadoresEnCuadroMandoDesgloseInformacion', 1, 1, 'ContenedorSimple'),
(9, 12, 'ContenedorGraficosCMDesgloseInformacion', 'ContenedorSimpleEnZonaContenidosConZonaLateralEnCuadroMandoDesgloseInformacion', 1, 2, 'ContenedorSimple'),
(10, 13, 'BloqueFiltrosCMDesgloseInformacion', 'ContenedorWidgetsFiltrosEnZonaLateralEnCuadroMandoDesgloseInformacion', 1, 2, 'Especificos/ContenedorWidgetsFiltrosCMDesgloseInformacion'),
(11, 8, 'ContenedorIndicadoresCMCruceVariables', 'ContenedorSimpleEnZonaIndicadoresEnCuadroMandoCruceVariables', 1, 1, 'ContenedorSimple'),
(12, 10, 'ContenedorGraficosCMCruceVariables', 'ContenedorSimpleEnZonaContenidosConZonaLateralEnCuadroMandoCruceVariables', 1, 2, 'ContenedorSimple'),
(13, 11, 'BloqueFiltrosCMCruceVariables', 'ContenedorSimpleEnZonaLateralEnCuadroMandoCruceVariables', 1, 2, 'Especificos/ContenedorWidgetsFiltrosCMCruceVariables'),
(15, 9, 'ContenedorSimple10', 'ContenedorSimpleEnZonaLateralEnEnCMInformacionJerarquica', 1, 2, 'ContenedorSimple'),
(16, 15, 'ContenedorIndicadoresCMInformacionJerarquica', 'ContenedorSimpleEnZonaIndicadoresEnEnCMInformacionJerarquica', 1, 1, 'ContenedorSimple'),
(17, 17, 'Contenedor para Menú', 'ContenedorMenuEnReportes', 1, 1, 'contenedorMenu'),
(18, 18, 'ContenedorFiltros', 'ContenedorFiltrosEnReportes', 1, 1, 'ContenedorSimple'),
(19, 18, 'ContenedorTabla', 'ContenedorTablaEnReportes', 1, 2, 'ContenedorSimple'),
(20, 19, 'Contenedor para Menú', 'ContenedorMenuEnAlertas', 1, 1, 'contenedorMenu'),
(21, 20, 'ContenedorFiltros', 'ContenedorFiltrosEnAlertas', 1, 1, 'ContenedorSimple'),
(22, 20, 'ContenedorTabla', 'ContenedorTablaEnAlertas', 1, 2, 'ContenedorSimple'),
(23, 21, 'Contenedor para Menú', 'ContenedorMenuEnEncuestas', 1, 1, 'contenedorMenu'),
(24, 22, 'ContenedorFiltros', 'ContenedorFiltrosEnEncuestas', 1, 1, 'ContenedorFiltros4Columnas'),
(25, 22, 'ContenedorTabla', 'ContenedorTablaEnEncuestas', 1, 2, 'ContenedorSimple'),
(26, 23, 'Contenedor para Menú', 'ContenedorMenuEnD_Estrategico', 1, 1, 'contenedorMenu'),
(27, 24, 'ContenedorTabla', 'ContenedorTablaEnD_Estrategico', 1, 2, 'ContenedorSimple'),
(29, 25, 'Contenedor para Menú', 'ContenedorMenuEnI_Estrategico', 1, 1, 'contenedorMenu'),
(30, 26, 'ContenedorTabla', 'ContenedorTablaEnI_Estrategico', 1, 2, 'ContenedorSimple'),
(32, 5, 'ContenedorWidgetsFiltros', '', 1, 3, 'ContenedorWidgetsFiltros'),
(33, 14, 'ContenedorTreeGreedCMInformacionJerarquica', 'ContenedorSimpleEnZonaContenidosConZonaLateralEnCMInformacionJerarquica', 1, 2, 'ContenedorSimple'),
(34, 15, 'BloqueFiltrosCMInformacionJerarquica', 'ContenedorSimpleEnZonaLateralEnCMInformacionJerarquica', 1, 2, 'Especificos/ContenedorWidgetsFiltrosCMInformacionJerarquica');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_maquetas`
--

CREATE TABLE IF NOT EXISTS `cms_maquetas` (
`id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `activo` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `twig` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `icono` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `visible_cabecera` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `cms_maquetas`
--

INSERT INTO `cms_maquetas` (`id`, `nombre`, `slug`, `descripcion`, `activo`, `orden`, `twig`, `icono`, `visible_cabecera`) VALUES
(1, 'Principal', 'principal', '', 1, 1, 'MaquetaPrincipal', 'fa-home', 1),
(2, 'Alertas', 'alertas', '', 1, 2, 'MaquetaPrincipal', 'fa-bell', 1),
(3, 'Cuadro de Mando', 'cuadro_mando/desglose_informacion', '', 1, 3, 'MaquetaCuadroMandoDesgloseInformacion', 'fa-desktop', 1),
(5, 'Reportes', 'reportes', 'Maqueta de reportes', 1, 4, 'MaquetaPrincipal', 'fa-file-text-o', 1),
(7, 'Cuadro de Mando', 'cuadro_mando/cruce_variables', '', 1, 5, 'MaquetaCuadroMandoCruceVariables', 'fa-desktop', 0),
(8, 'Cuadro de Mando Informacion Jerarquica', 'cuadro_mando/informacion_jerarquica', '', 1, 6, 'MaquetaCuadroMandoInformacionJerarquica', 'fa-desktop', 0),
(9, 'Encuestas', 'encuestas', '', 1, 5, 'MaquetaPrincipal', 'fa-pie-chart', 1),
(10, 'Dashboard Estratégico', 'd_estrategico', '', 1, 6, 'MaquetaPrincipal', 'fa-desktop', 1),
(11, 'Informe Estratégico', 'i_estrategico', '', 1, 7, 'MaquetaPrincipal', 'fa-file-text-o', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_widget`
--

CREATE TABLE IF NOT EXISTS `cms_widget` (
`id` int(11) NOT NULL,
  `contenedor_id` int(11) DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `activo` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `twig` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `service` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `widget_ajax` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `cms_widget`
--

INSERT INTO `cms_widget` (`id`, `contenedor_id`, `nombre`, `descripcion`, `activo`, `orden`, `twig`, `service`, `widget_ajax`) VALUES
(1, 1, 'WidgetMenu', 'WidgetLinksMenuEnPrincipal', 1, 3, 'WidgetEnlacesMenu', 'WidgetEnlacesMenu', 0),
(4, 1, 'WidgetImagenCliente', '', 1, 2, 'WidgetImagen', 'WidgetImagenCliente', 0),
(5, 1, 'WidgetImagenCem', '', 1, 1, 'WidgetImagen', 'WidgetImagenCem', 0),
(6, 2, 'WidgetImagenCliente', '', 1, 2, 'WidgetImagen', 'WidgetImagenCliente', 0),
(7, 2, 'WidgetImagenCem', '', 1, 1, 'WidgetImagen', 'WidgetImagenCem', 0),
(8, 2, 'WidgetMenu', 'WidgetLinksMenuEnAlertas', 1, 3, 'WidgetEnlacesMenu', 'WidgetEnlacesMenu', 0),
(9, 3, 'WidgetImagenClienteEnCuadroMandoDesgloseInformacion', '', 1, 2, 'WidgetImagen', 'WidgetImagenCliente', 0),
(10, 3, 'WidgetImagenCemEnCuadroMandoDesgloseInformacion', '', 1, 1, 'WidgetImagen', 'WidgetImagenCem', 0),
(11, 3, 'WidgetMenu', 'WidgetLinksMenuEnCuadroMandoDesgloseInformacion', 1, 3, 'WidgetEnlacesMenu', 'WidgetEnlacesMenu', 0),
(12, 4, 'WidgetTablaAcumulados', 'WidgetTablaAcumuladosEnZonzaContenidoConZonaLateralEnPrincipal', 1, 5, 'Especificos/WidgetTablaAcumulados', 'WidgetTablaEspecificoProceso', 1),
(14, 5, 'WidgetNumEncuestas', 'WidgetNumEncuestasEnPrincipal', 1, 1, 'WidgetNumEncuestas', 'WidgetNumEncuestas', 1),
(17, 6, 'WidgetMenu', 'WidgetLinksCuadroMandoCruceVariables', 1, 3, 'WidgetEnlacesMenu', 'WidgetEnlacesMenu', 0),
(18, 6, 'WidgetImagenClienteCuadroMandoCruceVariables', '', 1, 2, 'WidgetImagen', 'WidgetImagenCliente', 0),
(19, 6, 'WidgetImagenCemCuadroMandoCruceVariables', '', 1, 1, 'WidgetImagen', 'WidgetImagenCem', 0),
(20, 7, 'WidgetMenu', 'WidgetLinksCuadroMandoInformacionJerarquica', 1, 3, 'WidgetEnlacesMenu', 'WidgetEnlacesMenu', 0),
(21, 7, 'WidgetImagenClienteCuadroMandoInformacionJerarquica', '', 1, 2, 'WidgetImagen', 'WidgetImagenCliente', 0),
(22, 7, 'WidgetImagenCemCuadroMandoInformacionJerarquica', '', 1, 1, 'WidgetImagen', 'WidgetImagenCem', 0),
(23, 8, 'WidgetPrueba23', 'WidgetConfigGraficosEnContenedor1ZonaIndicadoresDesgloseInformacion', 1, 1, 'Especificos/WidgetConfigGraficosDesgloseInformacion', 'WidgetConfiguracionVistaDesgloseInformacion', 0),
(25, 10, 'WidgetFechasTemporalidad', 'WidgetFiltroFechasTemporalidadEnContenedor1ZonaLateralCuadroMandoDesgloseInformacion', 1, 1, 'WidgetFechasTemporalidad', 'WidgetFiltroTemporalidad', 0),
(28, 11, 'WidgetPrueba26', 'WidgetPruebaEnContenedor1ZonaIndicadoresEnCuadroMandoCruceVariables', 1, 1, 'Especificos/WidgetConfigGraficosCruceVariables', 'WidgetConfiguracionVistaCruceVariables', 0),
(36, 15, 'WidgetConfiguracionVisualizacionInformacionJerarquica', 'WidgetConfiguracionVistaEnContenedor1ZonaLateralCuadroMandoCruceVariables', 1, 1, 'Especificos/WidgetSelectorInformacionJerarquica', 'WidgetSelectorInformacionJerarquica', 0),
(37, 17, 'WidgetMenu', 'WidgetLinksMenuEnReportes', 1, 3, 'WidgetEnlacesMenu', 'WidgetEnlacesMenu', 0),
(38, 17, 'WidgetImagenCliente', 'WidgetImagenClienteEnReportes', 1, 2, 'WidgetImagen', 'WidgetImagenCliente', 0),
(39, 17, 'WidgetImagenCem', 'WidgetImagenCemEnReportes', 1, 1, 'WidgetImagen', 'WidgetImagenCem', 0),
(40, 18, 'WidgetPrueba', 'WidgetPruebaEnContenedorFiltrosEnReportes', 0, 1, 'WidgetTest', 'WidgetTest', 0),
(41, 19, 'WidgetPrueba', 'WidgetPruebaEnContenedorTablaEnReportes', 0, 1, 'WidgetTest', 'WidgetTest', 0),
(42, 20, 'WidgetMenu', 'WidgetLinksMenuEnAlertas', 1, 3, 'WidgetEnlacesMenu', 'WidgetEnlacesMenu', 0),
(43, 20, 'WidgetImagenCliente', 'WidgetImagenClienteEnAlertas', 1, 2, 'WidgetImagen', 'WidgetImagenCliente', 0),
(44, 20, 'WidgetImagenCem', 'WidgetImagenCemEnAlertas', 1, 1, 'WidgetImagen', 'WidgetImagenCem', 0),
(45, 21, 'WidgetPrueba', 'WidgetPruebaEnContenedorFiltrosEnAlertas', 0, 1, 'WidgetTest', 'WidgetTest', 0),
(46, 22, 'WidgetPrueba', 'WidgetPruebaEnContenedorTablaEnAlertas', 0, 2, 'WidgetTest', 'WidgetTest', 0),
(47, 23, 'WidgetMenu', 'WidgetLinksMenuEnEncuestas', 1, 3, 'WidgetEnlacesMenu', 'WidgetEnlacesMenu', 0),
(48, 23, 'WidgetImagenCliente', 'WidgetImagenClienteEnEncuestas', 1, 2, 'WidgetImagen', 'WidgetImagenCliente', 0),
(49, 23, 'WidgetImagenCem', 'WidgetImagenCemEnEncuestas', 1, 1, 'WidgetImagen', 'WidgetImagenCem', 0),
(50, 24, 'WidgetPruebaFiltros', 'WidgetPruebaEnContenedorFiltrosEnEncuestas', 1, 1, 'WidgetTest', 'WidgetFiltros', 0),
(51, 25, 'WidgetPrueba', 'WidgetDataTableEnContenedorTablaEnEncuestas', 1, 2, 'WidgetDataTable', 'WidgetDataTable', 0),
(52, 26, 'WidgetMenu', 'WidgetLinksMenuEnD_Estrategico', 1, 3, 'WidgetEnlacesMenu', 'WidgetEnlacesMenu', 0),
(53, 26, 'WidgetImagenCliente', 'WidgetImagenClienteEnD_Estrategico', 1, 2, 'WidgetImagen', 'WidgetImagenCliente', 0),
(54, 26, 'WidgetImagenCem', 'WidgetImagenCemEnD_Estrategico', 1, 1, 'WidgetImagen', 'WidgetImagenCem', 0),
(55, 27, 'WidgetPrueba', 'WidgetPruebaEnContenedorTablaEnD_Estrategico', 0, 2, 'WidgetTest', 'WidgetTest', 0),
(56, 29, 'WidgetMenu', 'WidgetLinksMenuEnI_Estrategico', 1, 3, 'WidgetEnlacesMenu', 'WidgetEnlacesMenu', 0),
(57, 29, 'WidgetImagenCliente', 'WidgetImagenClienteEnI_Estrategico', 1, 2, 'WidgetImagen', 'WidgetImagenCliente', 0),
(58, 29, 'WidgetImagenCem', 'WidgetImagenCemEnI_Estrategico', 1, 1, 'WidgetImagen', 'WidgetImagenCem', 0),
(59, 30, 'WidgetPrueba', 'WidgetPruebaEnContenedorTablaEnI_Estrategico', 0, 2, 'WidgetTest', 'WidgetTest', 0),
(60, 32, 'WidgetPruebaP1', 'WidgetFiltroTemporalidadEnContenedorZonaLateralContenedorWidgetsFiltrosEnPRincipal', 1, 1, 'WidgetFechasTemporalidad', 'WidgetFiltroTemporalidad', 0),
(61, 4, 'WidgetFiltroVistas', 'WidgetFiltroVistasEnContenedorZonaLateralContenedorWidgetsFiltrosEnPrincipal', 1, 2, 'Especificos/WidgetFiltrosVistasTreeview', 'WidgetFiltrosVista', 0),
(62, 32, 'WidgetPrueba32', 'WidgetFiltroEncuestasEnContenedorZonaLateralContenedorWidgetsFiltrosEnPrincipal', 1, 3, 'Especificos/WidgetFiltrosEspecializadosMultiselect', 'WidgetFiltroOpcionesConsulta', 0),
(63, 4, 'WidgetTreeView', 'WidgetTreeViewEnContenedorConZonaLateralEnPrincipal', 1, 3, 'WidgetTreeView', 'WidgetTreeView', 1),
(64, 10, 'WidgetBloqueVariablesFiltro', 'WidgeBloqueVariablesFiltroEnContenedorZonaLateralContenedorWidgetsFiltrosEnCMDesgloseInformacion', 1, 1, 'Especificos/WidgetFiltrosEspecializadosMultiselect', 'WidgetBloqueVariablesFiltro', 0),
(65, 4, 'WidgetFiltrosTablaAcumulados', 'WidgetFiltrosTablaAcumuladosEnZonzaContenidoConZonaLateralEnPrincipal', 1, 4, 'Especificos/WidgetFiltrosTablaAcumulados', 'WidgetFiltrosTablaAcumulados', 0),
(66, 9, 'WidgetGraficoBarras', 'WidgetGraficoBarrasEnContenedorZonaContenidosConZonaLateralEnCuadroDeMandoDesglose', 1, 1, 'Especificos/WidgetGraficoBarras', 'WidgetGraficoBarras', 1),
(67, 9, 'WidgetGraficoLineas', 'WidgetGraficoLineasEnContenedorZonaContenidosConZonaLateralEnCuadroDeMandoDesglose', 1, 2, 'Especificos/WidgetGraficoLineas', 'WidgetGraficoLineas', 1),
(68, 24, 'WidgetFechasEncuestas', 'WidgetFiltroTemporalidadFechasEnEncuestas', 1, 1, 'WidgetTest', 'WidgetFiltroTemporalidadEncuestas', 0),
(69, 12, 'WidgetGraficoBarras', 'WidgetGraficoBarrasEnContenedorZonaContenidosConZonaLateralEnCMCruceVariables', 1, 1, 'Especificos/WidgetGraficoBarras', 'WidgetGraficoBarras', 1),
(70, 12, 'WidgetGraficoLineas', 'WidgetGraficoLineasEnContenedorZonaContenidosConZonaLateralEnCMCruceVariables', 1, 2, 'Especificos/WidgetGraficoLineas', 'WidgetGraficoLineas', 1),
(71, 13, 'WidgetFechasTemporalidad', 'WidgetFiltroFechasTemporalidadEnContenedor1ZonaLateralCMCruceVariables', 1, 1, 'WidgetFechasTemporalidad', 'WidgetFiltroTemporalidad', 0),
(72, 13, 'WidgetBloqueVariablesFiltroCruce', 'WidgeBloqueVariablesFiltroEnContenedorZonaLateralContenedorWidgetsFiltrosEnCMCruceVariables', 1, 1, 'Especificos/WidgetFiltrosEspecializadosConCruceMultiselect', 'WidgetBloqueVariablesFiltroCruce', 0),
(73, 4, 'WidgetFiltrosActivosPrincipal', 'WidgetFiltrosActivosPrincipal', 1, 1, 'WidgetFiltrosActivos', 'WidgetFiltrosSeleccionadosPrincipal', 1),
(74, 25, 'WidgetFiltrosActivosEncuestas', 'WidgetFiltrosActivos', 1, 1, 'WidgetFiltrosActivos', 'WidgetFiltrosSeleccionadosEncuestas', 1),
(75, 34, 'WidgetFechasTemporalidad', 'WidgetFiltroFechasTemporalidadEnContenedor1ZonaLateralCuadroMandoInformacionJerarquica', 1, 1, 'WidgetFechasTemporalidad', 'WidgetFiltroTemporalidad', 0),
(76, 34, 'WidgetBloqueVariablesFiltro', 'WidgeBloqueVariablesFiltroEnContenedorZonaLateralContenedorWidgetsFiltrosEnCMInformacionJerarquica', 1, 1, 'Especificos/WidgetFiltrosEspecializadosMultiselect', 'WidgetBloqueVariablesFiltro', 0),
(77, 33, 'WidgetTreeView', 'WidgetTreeViewEnContenedorConZonaLateralEnCMInformacionJerarquica', 1, 1, 'WidgetTreeViewEnCMInformacionJerarquica', 'WidgetTreeViewEnCMInformacionJerarquica', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_zona`
--

CREATE TABLE IF NOT EXISTS `cms_zona` (
`id` int(11) NOT NULL,
  `maqueta_id` int(11) DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `activo` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `twig` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `cms_zona`
--

INSERT INTO `cms_zona` (`id`, `maqueta_id`, `nombre`, `descripcion`, `activo`, `orden`, `twig`) VALUES
(1, 1, 'ZonaBienvenidos', 'ZonaBienvenidosEnPrincipal', 1, 1, 'ZonaSimple'),
(2, 2, 'ZonaBienvenidos', 'ZonaBienvenidosEnAlertas', 0, 1, 'ZonaSimple'),
(3, 3, 'ZonaBienvenidos', 'ZonaBienvenidosEnCuadroMandoDesgloseInformacion', 1, 1, 'ZonaSimple'),
(4, 1, 'ZonaContenidosConZonaLateral', 'ZonaContenidosConZonaLateralEnPrincipal', 1, 3, 'ZonaContenidosConZonaLateral'),
(5, 1, 'ZonaLateral', 'ZonaLateralEnPrincipal', 1, 2, 'ZonaLateral'),
(6, 7, 'ZonaBienvenidos', 'ZonaBienvenidosEnCuadroMandoCruceVariables', 1, 1, 'ZonaSimple'),
(7, 8, 'ZonaBienvenidos', 'ZonaBienvenidosEnCuadroMandoInformacionJerarquica', 1, 1, 'ZonaSimple'),
(8, 7, 'ZonaIndicadores', 'ZonaIndicadoresEnCuadroMandoCruceVariables', 1, 2, 'ZonaSimple'),
(9, 8, 'ZonaIndicadores', 'ZonaIndicadoresEnCuadroMandoInformacionJerarquica', 1, 2, 'ZonaSimple'),
(10, 7, 'ZonaContenidosConZonaLateral', 'ZonaContenidosConZonaLateralEnCuadroMandoCruceVariables', 1, 4, 'ZonaContenidosConZonaLateral'),
(11, 7, 'ZonaLateral', 'ZonaLateralEnCuadroMandoCruceVariables', 1, 3, 'ZonaLateral'),
(12, 3, 'ZonaContenidosConZonaLateral', 'ZonaContenidosConZonaLateralEnCuadroMandoDesgloseInformacion', 1, 4, 'ZonaContenidosConZonaLateral'),
(13, 3, 'ZonaLateral', 'ZonaLateralEnCuadroMandoDesgloseInformacion', 1, 3, 'ZonaLateral'),
(14, 8, 'ZonaContenidosConZonaLateral', 'ZonaContenidosConZonaLateralEnCuadroMandoInformacionJerarquica', 1, 4, 'ZonaContenidosConZonaLateral'),
(15, 8, 'ZonaLateral', 'ZonaLateralEnCuadroMandoInformacionJerarquica', 1, 3, 'ZonaLateral'),
(16, 3, 'ZonaIndicadores', 'ZonaIndicadoresEnCuadroMandoDesgloseInformacion', 1, 2, 'ZonaSimple'),
(17, 5, 'ZonaBienvenidos', 'ZonaBienvenidosEnReportes', 1, 1, 'ZonaSimple'),
(18, 5, 'ZonaContenidos', 'ZonaContenidosEnReportes', 1, 1, 'ZonaSimple'),
(19, 2, 'ZonaBienvenidos', 'ZonaBienvenidosEnAlertas', 1, 1, 'ZonaSimple'),
(20, 2, 'ZonaContenidos', 'ZonaContenidosEnAlertas', 1, 1, 'ZonaSimple'),
(21, 9, 'ZonaBienvenidos', 'ZonaBienvenidosEnEncuestas', 1, 1, 'ZonaSimple'),
(22, 9, 'ZonaContenidos', 'ZonaContenidosEnEncuestas', 1, 1, 'ZonaPaddingLateral'),
(23, 10, 'ZonaBienvenidos', 'ZonaBienvenidosEnD_Estrategico', 1, 1, 'ZonaSimple'),
(24, 10, 'ZonaContenidos', 'ZonaContenidosEnD_Estrategico', 1, 1, 'ZonaSimple'),
(25, 11, 'ZonaBienvenidos', 'ZonaBienvenidosEnI_Estrategico', 1, 1, 'ZonaSimple'),
(26, 11, 'ZonaContenidos', 'ZonaContenidosEnI_Estrategico', 1, 1, 'ZonaSimple');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `encuestas_datatable`
--
CREATE TABLE IF NOT EXISTS `encuestas_datatable` (
`encuesta_id` int(11)
,`segmento` varchar(255)
,`servicio` varchar(255)
,`fecha` datetime
,`negocio` varchar(255)
,`linea_negocio` varchar(255)
,`canal` varchar(255)
,`detalle_canal` varchar(256)
,`procesos` varchar(341)
,`sat` double
,`recomendacion` double
,`sentimiento` double
,`warning` int(1)
,`opi` varchar(255)
);
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fos_group`
--

CREATE TABLE IF NOT EXISTS `fos_group` (
`id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `fos_group`
--

INSERT INTO `fos_group` (`id`, `name`, `roles`) VALUES
(1, 'admin', 'a:0:{}'),
(2, 'user', 'a:0:{}'),
(3, 'group_can_e-commerce', 'a:0:{}');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fos_user`
--

CREATE TABLE IF NOT EXISTS `fos_user` (
`id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `fos_user`
--

INSERT INTO `fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`) VALUES
(2, 'serser', 'serser', 'serser@serser.com', 'serser@serser.com', 1, 'qe780gkg09wwsg0sog4o8k00wgog0s', '$2y$13$qe780gkg09wwsg0sog4o8e1d2/FssN.Lrq5oVgVaPRCKtTXJT56qG', '2017-02-23 14:39:06', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:10:"ROLE_ADMIN";}', 0, NULL),
(3, 'user_ecommerce', 'user_ecommerce', 'foo@foo.com', 'foo@foo.com', 1, 'ek5uu4kczfsogko4g0kwg8wogccc0kg', '$2y$13$ek5uu4kczfsogko4g0kwgu38vY77Pl.tyQ1PJJ3UWYegbK0vW7ir6', '2017-01-05 10:19:29', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:15:"ROLE_CEPSA_USER";}', 0, NULL),
(4, 'admin', 'admin', 'cepsa@cepsa.com', 'cepsa@cepsa.com', 1, 'bmlgrkvigco444ws84w8osso00gws8c', '$2y$13$bmlgrkvigco444ws84w8oebkbZT9rTMPMtxSfOgciogxH7Oe1woMG', '2016-12-21 11:44:45', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:15:"ROLE_CEPSA_USER";}', 0, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fos_user_user_group`
--

CREATE TABLE IF NOT EXISTS `fos_user_user_group` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `fos_user_user_group`
--

INSERT INTO `fos_user_user_group` (`user_id`, `group_id`) VALUES
(2, 1),
(3, 3),
(4, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupo_filtro`
--

CREATE TABLE IF NOT EXISTS `grupo_filtro` (
`id` int(11) NOT NULL,
  `grupo_id` int(11) DEFAULT NULL,
  `filtro_maqueta_id` int(11) DEFAULT NULL,
  `valor_fijo` varchar(256) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `grupo_filtro`
--

INSERT INTO `grupo_filtro` (`id`, `grupo_id`, `filtro_maqueta_id`, `valor_fijo`) VALUES
(2, 3, 44, '4'),
(3, 3, 1, '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `negocio_linea_negocio`
--

CREATE TABLE IF NOT EXISTS `negocio_linea_negocio` (
`id` int(11) NOT NULL,
  `negocio_id` int(11) DEFAULT NULL,
  `linea_negocio_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `negocio_producto`
--

CREATE TABLE IF NOT EXISTS `negocio_producto` (
`id` int(11) NOT NULL,
  `id_negocio` int(11) DEFAULT NULL,
  `id_producto` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nosql_encuestas`
--

CREATE TABLE IF NOT EXISTS `nosql_encuestas` (
`id` int(11) NOT NULL,
  `encuesta_id` int(11) DEFAULT NULL,
  `opi_pregunta_id` int(11) DEFAULT NULL,
  `fecha` datetime NOT NULL,
  `opi_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `g1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `g1_valor` int(11) NOT NULL,
  `g2` int(11) NOT NULL,
  `g3` int(11) NOT NULL,
  `a1` int(11) DEFAULT NULL,
  `gc1` int(11) DEFAULT NULL,
  `gc2` int(11) DEFAULT NULL,
  `gc3` int(11) DEFAULT NULL,
  `gc4` int(11) DEFAULT NULL,
  `of1` int(11) DEFAULT NULL,
  `of2` int(11) DEFAULT NULL,
  `of3` int(11) DEFAULT NULL,
  `of4` int(11) DEFAULT NULL,
  `p2` int(11) DEFAULT NULL,
  `pc1` int(11) DEFAULT NULL,
  `pc2` int(11) DEFAULT NULL,
  `ps1` int(11) DEFAULT NULL,
  `ps2` int(11) DEFAULT NULL,
  `pais_id` int(11) DEFAULT NULL,
  `seg_id` int(11) DEFAULT NULL,
  `neg_id` int(11) DEFAULT NULL,
  `org_id` int(11) DEFAULT NULL,
  `can_id` int(11) DEFAULT NULL,
  `det_can_id` int(11) DEFAULT NULL,
  `ser1_id` int(11) DEFAULT NULL,
  `lat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nconcn` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `e1` int(11) DEFAULT NULL,
  `e2` int(11) DEFAULT NULL,
  `e3` int(11) DEFAULT NULL,
  `e4` int(11) DEFAULT NULL,
  `procesos` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `warning` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nosql_encuestas_datatable`
--

CREATE TABLE IF NOT EXISTS `nosql_encuestas_datatable` (
`id` int(11) NOT NULL,
  `encuesta_id` int(11) DEFAULT NULL,
  `fecha` datetime NOT NULL,
  `negocio` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `linea_negocio` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `canal` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `detalle_canal` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `procesos` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `experiencia` int(11) NOT NULL,
  `recomendacion` int(11) NOT NULL,
  `sentimiento` int(11) NOT NULL,
  `segmento` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `servicio` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `opi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `warning` tinyint(1) NOT NULL,
  `seg_id` int(11) DEFAULT NULL,
  `neg_id` int(11) DEFAULT NULL,
  `org_id` int(11) DEFAULT NULL,
  `zon_id` int(11) DEFAULT NULL,
  `loc_id` int(11) DEFAULT NULL,
  `prov_id` int(11) DEFAULT NULL,
  `pais_id` int(11) DEFAULT NULL,
  `ser1_id` int(11) DEFAULT NULL,
  `det_can_id` int(11) DEFAULT NULL,
  `nconcn` varchar(256) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nosql_encuesta_procesos`
--

CREATE TABLE IF NOT EXISTS `nosql_encuesta_procesos` (
`id` int(11) NOT NULL,
  `suma_valoracion` int(11) NOT NULL,
  `num_preguntas_valoracion` int(11) NOT NULL,
  `encuesta_id` int(11) DEFAULT NULL,
  `proceso_id` int(11) DEFAULT NULL,
  `fecha` datetime NOT NULL,
  `opi_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `g1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `g1_valor` int(11) NOT NULL,
  `g2` int(11) NOT NULL,
  `g3` int(11) NOT NULL,
  `org_id` int(11) DEFAULT NULL,
  `seg_id` int(11) DEFAULT NULL,
  `neg_id` int(11) DEFAULT NULL,
  `can_id` int(11) DEFAULT NULL,
  `det_can_id` int(11) DEFAULT NULL,
  `ser1_id` int(11) DEFAULT NULL,
  `pais_id` int(11) DEFAULT NULL,
  `p1` int(11) DEFAULT NULL,
  `p2` int(11) DEFAULT NULL,
  `p3` int(11) DEFAULT NULL,
  `p1_pregunta_id` int(11) DEFAULT NULL,
  `p2_pregunta_id` int(11) DEFAULT NULL,
  `p3_pregunta_id` int(11) DEFAULT NULL,
  `p4_pregunta_id` int(11) DEFAULT NULL,
  `p4` int(11) DEFAULT NULL,
  `opi_pregunta_id` int(11) DEFAULT NULL,
  `warning` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `survey_encuesta`
--

CREATE TABLE IF NOT EXISTS `survey_encuesta` (
`id` int(11) NOT NULL,
  `cliente_id` int(11) DEFAULT NULL,
  `seg_id` int(11) DEFAULT NULL,
  `neg_id` int(11) DEFAULT NULL,
  `org_id` int(11) DEFAULT NULL,
  `zon_id` int(11) DEFAULT NULL,
  `loc_id` int(11) DEFAULT NULL,
  `prov_id` int(11) DEFAULT NULL,
  `pais_id` int(11) DEFAULT NULL,
  `can_id` int(11) DEFAULT NULL,
  `tip1_id` int(11) DEFAULT NULL,
  `tip2_id` int(11) DEFAULT NULL,
  `tip3_id` int(11) DEFAULT NULL,
  `tip4_id` int(11) DEFAULT NULL,
  `ser1_id` int(11) DEFAULT NULL,
  `ser2_id` int(11) DEFAULT NULL,
  `ser3_id` int(11) DEFAULT NULL,
  `ser4_id` int(11) DEFAULT NULL,
  `lat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lon` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `det_can` int(11) DEFAULT NULL,
  `opi_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fecha` datetime NOT NULL,
  `opi_pregunta_id` int(11) DEFAULT NULL,
  `nconcn` varchar(256) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `survey_encuesta_bloque`
--

CREATE TABLE IF NOT EXISTS `survey_encuesta_bloque` (
`id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `survey_encuesta_bloque`
--

INSERT INTO `survey_encuesta_bloque` (`id`, `nombre`) VALUES
(1, 'Preguntas Transversales a todos los negocios'),
(2, 'Procesos Cliente'),
(3, 'Otras Preguntas'),
(4, 'Preguntas Abiertas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `survey_encuesta_pregunta`
--

CREATE TABLE IF NOT EXISTS `survey_encuesta_pregunta` (
`id` int(11) NOT NULL,
  `sub_bloque_id` int(11) DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cem_tipo_id` int(11) DEFAULT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcion_completa` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `orden_pregunta_proceso` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `survey_encuesta_pregunta`
--

INSERT INTO `survey_encuesta_pregunta` (`id`, `sub_bloque_id`, `nombre`, `cem_tipo_id`, `descripcion`, `descripcion_completa`, `orden_pregunta_proceso`) VALUES
(1, 1, 'G1', 1, 'Sentimiento', 'Sentimiento del cliente con Cepsa', 1),
(2, 1, 'G2', 2, 'Recomendación', 'Si recomendaria a otros clientes', 2),
(3, 1, 'G3', 3, 'Experiencia', 'Indicador de satisfaccion con la Experiencia', 3),
(4, 3, 'OF3', 4, 'Fidelización', 'Preguntas relativas a fidelidad', 3),
(5, 3, 'OF1', 4, 'Esfuerzo', 'Facilidad/Esfuerzo solicitado el cliente en el proceso realizado', 1),
(6, 3, 'OF2', 4, 'Promociones', 'Satisfaccion con la promoción', 2),
(7, 6, 'P2', 4, 'Rapidez de pago', 'Rapidez en el pago', 2),
(8, 8, 'GC4', 4, 'Amabilidad', 'Amabilidad en la atención al cliente', 4),
(9, 12, 'PS1', 4, 'Productos consumidos', 'Productos que consume', 1),
(10, 13, 'N1', NULL, 'Esfuerzo', 'Facilidad/Esfuerzo solicitado el cliente en el proceso realizado', 1),
(11, 13, 'N2', NULL, 'Motivo', 'Motivos por el cual da una valoración', 2),
(12, 13, 'N3', NULL, 'Canal', NULL, 3),
(13, 13, 'N4', NULL, 'Util', NULL, 4),
(14, 16, 'PA1', 5, 'Motivo', 'Motivos por el cual da una valoración', 1),
(15, 2, 'A1', 4, 'Esfuerzo', 'Facilidad/Esfuerzo solicitado el cliente en el proceso realizado', 1),
(16, 16, 'PA2', 5, 'Sugerencia', 'Sugerencias que deseen dejar', 2),
(17, 16, 'PA3', 5, 'No contratación', NULL, 3),
(20, 4, 'PC1', 4, 'Esfuerzo', 'Facilidad/Esfuerzo solicitado el cliente en el proceso realizado', 1),
(21, 12, 'PS2', 4, 'Variedad de productos', 'Satisfaccion con la variedad de productos', 2),
(22, 3, 'OF4', 4, 'Regalo incentivo', 'Satisfacción con el regalo recibido', 4),
(23, 8, 'GC1', 4, 'Esfuerzo', 'Facilidad/Esfuerzo solicitado el cliente en el proceso realizado', 1),
(24, 4, 'PC2', 4, 'Rapidez', 'Rapidez en el proceso', 2),
(25, 8, 'GC2', 4, 'Rapidez', 'Rapidez en el proceso', 2),
(26, 8, 'GC3', 4, 'Resolución', 'Satisfacción con la resolución de recibida', 3),
(100, 5, 'E3', 4, 'Cumplimento_Plazos', 'Cumplimiento plazos de entrega', 3),
(101, 5, 'E4', 4, 'Estado_Producto', 'Estado del producto en la Entrega', 4),
(102, 5, 'E1', 4, 'Esfuerzo', 'Facilidad/Esfuerzo solicitado el cliente en el proceso realizado', 1),
(103, 5, 'E2', 4, 'Aviso retraso', 'Aviso retraso', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `survey_encuesta_pregunta_opi`
--

CREATE TABLE IF NOT EXISTS `survey_encuesta_pregunta_opi` (
`id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `survey_encuesta_pregunta_opi`
--

INSERT INTO `survey_encuesta_pregunta_opi` (`id`, `nombre`) VALUES
(1, 'web_ptv_priv'),
(2, 'SAC_P_MP_Red'),
(3, 'UX_Tienda_online'),
(4, 'gasenvasado_rascaygana'),
(5, 'UX_Web_General'),
(6, 'SAC_R_Gas'),
(7, 'optima'),
(8, 'SAC_R_Fidelidad'),
(9, 'optima_mail'),
(10, 'pto_venta_prt_ticket'),
(11, 'es_habitual'),
(12, 'Regalo_Liga_Orange'),
(13, 'promocionPorqueTUVuelves_mai'),
(14, 'Regalo_PTV'),
(15, 'CX_Gas_RyG'),
(16, 'SAC_P_D_Red'),
(18, 'SAC_P_Lubricantes'),
(19, 'SAC_R_VVDD'),
(20, 'calefaccion_sms'),
(21, 'calefaccion-mail'),
(22, 'Gas_Oferta1_BajoC_25-5_mail'),
(23, 'Gas_Oferta1_Generica_10-10_mail'),
(24, 'Gas_Oferta1_GranC_5-12_mail'),
(25, 'Gas_Oferta1_BajoC_25-5_telf'),
(26, 'Gas_Oferta1_Generica_10-10_telf'),
(27, 'Gas_Oferta1_GranC_5-12_telf'),
(28, 'Gas_Oferta2_BajoC_25-5_telf'),
(29, 'Gas_Oferta2_Generica_10-10_telf'),
(30, 'Gas_Oferta2_GranC_5-12_telf'),
(31, 'pto_venta_prt_cartel'),
(32, 'SAC_P_D_Red'),
(33, 'Lub_Xtar'),
(34, 'Gas_Oferta2_BajoC_25-5_telf'),
(35, 'Gas_Oferta2_Generica_10-10_telf'),
(36, 'Gas_Oferta2_GranC_5-12_telf'),
(37, 'banner_ptv_priv'),
(38, 'SAC_R_ecommerce'),
(39, 'SAC_P_Gas'),
(40, 'promocionPorqueTUVuelves_mail'),
(41, 'OnLine_Entrega');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `survey_encuesta_proceso`
--

CREATE TABLE IF NOT EXISTS `survey_encuesta_proceso` (
`id` int(11) NOT NULL,
  `encuesta_id` int(11) DEFAULT NULL,
  `proceso_id` int(11) DEFAULT NULL,
  `process_index` double DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `survey_encuesta_respuesta`
--

CREATE TABLE IF NOT EXISTS `survey_encuesta_respuesta` (
`id` int(11) NOT NULL,
  `encuesta_id` int(11) DEFAULT NULL,
  `encuesta_pregunta_id` int(11) DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `valor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ev` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `survey_encuesta_sub_bloque`
--

CREATE TABLE IF NOT EXISTS `survey_encuesta_sub_bloque` (
`id` int(11) NOT NULL,
  `bloque_id` int(11) DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `survey_encuesta_sub_bloque`
--

INSERT INTO `survey_encuesta_sub_bloque` (`id`, `bloque_id`, `nombre`) VALUES
(1, 1, 'Global'),
(2, 2, 'Alta/Contratación'),
(3, 2, 'Oferta/Fidelización'),
(4, 2, 'Pedido/Compra'),
(5, 2, 'Entrega/Suministro'),
(6, 2, 'Pagos/Cobros'),
(7, 2, 'Facturación'),
(8, 2, 'Gestión Cliente'),
(9, 2, 'Asistencia Técnica'),
(10, 2, 'Bajas'),
(11, 2, 'Seguridad'),
(12, 2, 'Producto/Servicio'),
(13, 3, 'Especifico Negocio'),
(14, 3, 'Modulo Opcional'),
(16, 4, 'Preguntas Abiertas');

-- --------------------------------------------------------

--
-- Estructura para la vista `encuestas_datatable`
--
DROP TABLE IF EXISTS `encuestas_datatable`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `encuestas_datatable` AS select `e`.`id` AS `encuesta_id`,`seg`.`nombre` AS `segmento`,`ser`.`nombre` AS `servicio`,`e`.`fecha` AS `fecha`,`neg`.`nombre` AS `negocio`,`org`.`nombre` AS `linea_negocio`,`can`.`nombre` AS `canal`,`det_can`.`nombre` AS `detalle_canal`,(select group_concat(`p`.`nombre` separator ', ') from (`survey_encuesta_proceso` `eproceso` join `admin_proceso` `p` on((`eproceso`.`proceso_id` = `p`.`id`))) where (`eproceso`.`encuesta_id` = `e`.`id`)) AS `procesos`,sum(if((`er`.`encuesta_pregunta_id` = 3),`er`.`valor`,0)) AS `sat`,sum(if((`er`.`encuesta_pregunta_id` = 2),`er`.`valor`,0)) AS `recomendacion`,sum(if((`er`.`encuesta_pregunta_id` = 1),`er`.`ev`,0)) AS `sentimiento`,(select if((sum(if((`er_warning`.`valor` < 5),1,0)) = 0),0,1) AS `warning` from (`survey_encuesta_respuesta` `er_warning` join `survey_encuesta_pregunta` `ep_warning` on((`er_warning`.`encuesta_pregunta_id` = `ep_warning`.`id`))) where ((`ep_warning`.`cem_tipo_id` = 4) and (`er_warning`.`encuesta_id` = `er`.`encuesta_id`))) AS `warning`,`opi`.`nombre` AS `opi` from ((((((((`survey_encuesta_respuesta` `er` join `survey_encuesta` `e` on((`e`.`id` = `er`.`encuesta_id`))) join `survey_encuesta_pregunta_opi` `opi` on((`opi`.`id` = `e`.`opi_pregunta_id`))) join `admin_negocio` `neg` on((`e`.`neg_id` = `neg`.`id`))) join `admin_canal_encuestacion` `can` on((`e`.`can_id` = `can`.`id`))) join `admin_linea_negocio` `org` on((`e`.`org_id` = `org`.`id`))) join `admin_determinacion_canal` `det_can` on((`e`.`det_can` = `det_can`.`id`))) join `admin_segmento` `seg` on((`e`.`seg_id` = `seg`.`id`))) join `admin_servicio` `ser` on((`e`.`ser1_id` = `ser`.`id`))) group by `e`.`id`;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `admin_canal_det_canal`
--
ALTER TABLE `admin_canal_det_canal`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_A779BD5F68DB5B2E` (`canal_id`), ADD KEY `IDX_A779BD5FD1EADC9C` (`determinacion_canal_id`);

--
-- Indices de la tabla `admin_canal_encuestacion`
--
ALTER TABLE `admin_canal_encuestacion`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `admin_cliente`
--
ALTER TABLE `admin_cliente`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `admin_determinacion_canal`
--
ALTER TABLE `admin_determinacion_canal`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `admin_experiencia`
--
ALTER TABLE `admin_experiencia`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `admin_filtros`
--
ALTER TABLE `admin_filtros`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_5A4E3CED613CEC58` (`padre_id`);

--
-- Indices de la tabla `admin_filtros_bloque`
--
ALTER TABLE `admin_filtros_bloque`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `admin_filtros_maqueta`
--
ALTER TABLE `admin_filtros_maqueta`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_57A3F11FCD91FAF2` (`id_maqueta`), ADD KEY `IDX_57A3F11F3F7602BF` (`id_filtro`), ADD KEY `IDX_57A3F11FF1DA68E8` (`bloque`);

--
-- Indices de la tabla `admin_linea_negocio`
--
ALTER TABLE `admin_linea_negocio`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `admin_localidad`
--
ALTER TABLE `admin_localidad`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `admin_negocio`
--
ALTER TABLE `admin_negocio`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `admin_organizacion`
--
ALTER TABLE `admin_organizacion`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `admin_pais`
--
ALTER TABLE `admin_pais`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `admin_permutaciones_vistas`
--
ALTER TABLE `admin_permutaciones_vistas`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_C5ABF97747D487D1` (`indicador_id`), ADD KEY `IDX_C5ABF977189FD802` (`sub_bloque_id`), ADD KEY `IDX_C5ABF9777D879E4F` (`negocio_id`), ADD KEY `IDX_C5ABF977640D1D53` (`proceso_id`), ADD KEY `IDX_C5ABF97768DB5B2E` (`canal_id`);

--
-- Indices de la tabla `admin_proceso`
--
ALTER TABLE `admin_proceso`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `admin_producto`
--
ALTER TABLE `admin_producto`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `admin_provincia`
--
ALTER TABLE `admin_provincia`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `admin_recomendacion`
--
ALTER TABLE `admin_recomendacion`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `admin_segmento`
--
ALTER TABLE `admin_segmento`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `admin_servicio`
--
ALTER TABLE `admin_servicio`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `admin_temporalidad`
--
ALTER TABLE `admin_temporalidad`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `admin_tipificacion_opinion`
--
ALTER TABLE `admin_tipificacion_opinion`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `admin_tipologia_cliente`
--
ALTER TABLE `admin_tipologia_cliente`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `admin_tipo_cem`
--
ALTER TABLE `admin_tipo_cem`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `admin_variables_organizacion_jerarquica`
--
ALTER TABLE `admin_variables_organizacion_jerarquica`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `admin_vista`
--
ALTER TABLE `admin_vista`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_71E59A34A326768E` (`filtro_id`);

--
-- Indices de la tabla `admin_zona`
--
ALTER TABLE `admin_zona`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_contenedor`
--
ALTER TABLE `cms_contenedor`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_D575F034104EA8FC` (`zona_id`);

--
-- Indices de la tabla `cms_maquetas`
--
ALTER TABLE `cms_maquetas`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_widget`
--
ALTER TABLE `cms_widget`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_B70F01A8C1A15772` (`contenedor_id`);

--
-- Indices de la tabla `cms_zona`
--
ALTER TABLE `cms_zona`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_6010A9631CD04871` (`maqueta_id`);

--
-- Indices de la tabla `fos_group`
--
ALTER TABLE `fos_group`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_4B019DDB5E237E06` (`name`);

--
-- Indices de la tabla `fos_user`
--
ALTER TABLE `fos_user`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`), ADD UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`);

--
-- Indices de la tabla `fos_user_user_group`
--
ALTER TABLE `fos_user_user_group`
 ADD PRIMARY KEY (`user_id`,`group_id`), ADD KEY `IDX_B3C77447A76ED395` (`user_id`), ADD KEY `IDX_B3C77447FE54D947` (`group_id`);

--
-- Indices de la tabla `grupo_filtro`
--
ALTER TABLE `grupo_filtro`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_1547186B9C833003` (`grupo_id`), ADD KEY `IDX_1547186B997E71E8` (`filtro_maqueta_id`);

--
-- Indices de la tabla `negocio_linea_negocio`
--
ALTER TABLE `negocio_linea_negocio`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_5D8285257D879E4F` (`negocio_id`), ADD KEY `IDX_5D828525D4B20BCE` (`linea_negocio_id`);

--
-- Indices de la tabla `negocio_producto`
--
ALTER TABLE `negocio_producto`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_7F99FFF0ABB54A09` (`id_negocio`), ADD KEY `IDX_7F99FFF0F760EA80` (`id_producto`);

--
-- Indices de la tabla `nosql_encuestas`
--
ALTER TABLE `nosql_encuestas`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_F4B9908E46844BA6` (`encuesta_id`), ADD KEY `IDX_F4B9908EB6A112E5` (`opi_pregunta_id`), ADD KEY `IDX_F4B9908EC604D5C6` (`pais_id`), ADD KEY `IDX_F4B9908E5257F569` (`seg_id`), ADD KEY `IDX_F4B9908EED1FE42C` (`neg_id`), ADD KEY `IDX_F4B9908EF4837C1B` (`org_id`), ADD KEY `IDX_F4B9908ED9091EB8` (`can_id`), ADD KEY `IDX_F4B9908EB83EB1B8` (`det_can_id`), ADD KEY `IDX_F4B9908E4A50BB68` (`ser1_id`);

--
-- Indices de la tabla `nosql_encuestas_datatable`
--
ALTER TABLE `nosql_encuestas_datatable`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_360E3D6846844BA6` (`encuesta_id`), ADD KEY `IDX_360E3D685257F569` (`seg_id`), ADD KEY `IDX_360E3D68ED1FE42C` (`neg_id`), ADD KEY `IDX_360E3D68F4837C1B` (`org_id`), ADD KEY `IDX_360E3D6842E0F38A` (`zon_id`), ADD KEY `IDX_360E3D686505CAD1` (`loc_id`), ADD KEY `IDX_360E3D68FBD8A061` (`prov_id`), ADD KEY `IDX_360E3D68C604D5C6` (`pais_id`), ADD KEY `IDX_360E3D684A50BB68` (`ser1_id`), ADD KEY `IDX_360E3D68B83EB1B8` (`det_can_id`);

--
-- Indices de la tabla `nosql_encuesta_procesos`
--
ALTER TABLE `nosql_encuesta_procesos`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_A95E9D7C46844BA6` (`encuesta_id`), ADD KEY `IDX_A95E9D7C640D1D53` (`proceso_id`), ADD KEY `IDX_A95E9D7CF4837C1B` (`org_id`), ADD KEY `IDX_A95E9D7C5257F569` (`seg_id`), ADD KEY `IDX_A95E9D7CED1FE42C` (`neg_id`), ADD KEY `IDX_A95E9D7CD9091EB8` (`can_id`), ADD KEY `IDX_A95E9D7CB83EB1B8` (`det_can_id`), ADD KEY `IDX_A95E9D7C4A50BB68` (`ser1_id`), ADD KEY `IDX_A95E9D7CC604D5C6` (`pais_id`), ADD KEY `IDX_A95E9D7CF5F60AA7` (`p1_pregunta_id`), ADD KEY `IDX_A95E9D7C483C6669` (`p2_pregunta_id`), ADD KEY `IDX_A95E9D7C95AABFEC` (`p3_pregunta_id`), ADD KEY `IDX_A95E9D7CE8D9B9B4` (`p4_pregunta_id`), ADD KEY `IDX_A95E9D7CB6A112E5` (`opi_pregunta_id`);

--
-- Indices de la tabla `survey_encuesta`
--
ALTER TABLE `survey_encuesta`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_EEAC2E66DE734E51` (`cliente_id`), ADD KEY `IDX_EEAC2E665257F569` (`seg_id`), ADD KEY `IDX_EEAC2E66ED1FE42C` (`neg_id`), ADD KEY `IDX_EEAC2E66F4837C1B` (`org_id`), ADD KEY `IDX_EEAC2E6642E0F38A` (`zon_id`), ADD KEY `IDX_EEAC2E666505CAD1` (`loc_id`), ADD KEY `IDX_EEAC2E66FBD8A061` (`prov_id`), ADD KEY `IDX_EEAC2E66C604D5C6` (`pais_id`), ADD KEY `IDX_EEAC2E66D9091EB8` (`can_id`), ADD KEY `IDX_EEAC2E6682F516FD` (`tip1_id`), ADD KEY `IDX_EEAC2E669040B913` (`tip2_id`), ADD KEY `IDX_EEAC2E6628FCDE76` (`tip3_id`), ADD KEY `IDX_EEAC2E66B52BE6CF` (`tip4_id`), ADD KEY `IDX_EEAC2E664A50BB68` (`ser1_id`), ADD KEY `IDX_EEAC2E6658E51486` (`ser2_id`), ADD KEY `IDX_EEAC2E66E05973E3` (`ser3_id`), ADD KEY `IDX_EEAC2E667D8E4B5A` (`ser4_id`), ADD KEY `IDX_EEAC2E66B9ED135A` (`det_can`), ADD KEY `IDX_EEAC2E66B6A112E5` (`opi_pregunta_id`);

--
-- Indices de la tabla `survey_encuesta_bloque`
--
ALTER TABLE `survey_encuesta_bloque`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `survey_encuesta_pregunta`
--
ALTER TABLE `survey_encuesta_pregunta`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_EFE0EF35189FD802` (`sub_bloque_id`), ADD KEY `IDX_EFE0EF35801EB86A` (`cem_tipo_id`);

--
-- Indices de la tabla `survey_encuesta_pregunta_opi`
--
ALTER TABLE `survey_encuesta_pregunta_opi`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `survey_encuesta_proceso`
--
ALTER TABLE `survey_encuesta_proceso`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_9668E67246844BA6` (`encuesta_id`), ADD KEY `IDX_9668E672640D1D53` (`proceso_id`);

--
-- Indices de la tabla `survey_encuesta_respuesta`
--
ALTER TABLE `survey_encuesta_respuesta`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_1945667C46844BA6` (`encuesta_id`), ADD KEY `IDX_1945667C9AF2BA6` (`encuesta_pregunta_id`);

--
-- Indices de la tabla `survey_encuesta_sub_bloque`
--
ALTER TABLE `survey_encuesta_sub_bloque`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_5DFFFE6FA929EBC` (`bloque_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `admin_canal_det_canal`
--
ALTER TABLE `admin_canal_det_canal`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `admin_canal_encuestacion`
--
ALTER TABLE `admin_canal_encuestacion`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `admin_cliente`
--
ALTER TABLE `admin_cliente`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `admin_determinacion_canal`
--
ALTER TABLE `admin_determinacion_canal`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `admin_experiencia`
--
ALTER TABLE `admin_experiencia`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `admin_filtros`
--
ALTER TABLE `admin_filtros`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT de la tabla `admin_filtros_bloque`
--
ALTER TABLE `admin_filtros_bloque`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=100;
--
-- AUTO_INCREMENT de la tabla `admin_filtros_maqueta`
--
ALTER TABLE `admin_filtros_maqueta`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=74;
--
-- AUTO_INCREMENT de la tabla `admin_linea_negocio`
--
ALTER TABLE `admin_linea_negocio`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `admin_localidad`
--
ALTER TABLE `admin_localidad`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `admin_negocio`
--
ALTER TABLE `admin_negocio`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `admin_organizacion`
--
ALTER TABLE `admin_organizacion`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `admin_pais`
--
ALTER TABLE `admin_pais`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `admin_permutaciones_vistas`
--
ALTER TABLE `admin_permutaciones_vistas`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `admin_proceso`
--
ALTER TABLE `admin_proceso`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `admin_producto`
--
ALTER TABLE `admin_producto`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `admin_provincia`
--
ALTER TABLE `admin_provincia`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `admin_recomendacion`
--
ALTER TABLE `admin_recomendacion`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `admin_segmento`
--
ALTER TABLE `admin_segmento`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `admin_servicio`
--
ALTER TABLE `admin_servicio`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `admin_temporalidad`
--
ALTER TABLE `admin_temporalidad`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `admin_tipificacion_opinion`
--
ALTER TABLE `admin_tipificacion_opinion`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `admin_tipologia_cliente`
--
ALTER TABLE `admin_tipologia_cliente`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `admin_tipo_cem`
--
ALTER TABLE `admin_tipo_cem`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `admin_variables_organizacion_jerarquica`
--
ALTER TABLE `admin_variables_organizacion_jerarquica`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `admin_vista`
--
ALTER TABLE `admin_vista`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `admin_zona`
--
ALTER TABLE `admin_zona`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `cms_contenedor`
--
ALTER TABLE `cms_contenedor`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT de la tabla `cms_maquetas`
--
ALTER TABLE `cms_maquetas`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `cms_widget`
--
ALTER TABLE `cms_widget`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=78;
--
-- AUTO_INCREMENT de la tabla `cms_zona`
--
ALTER TABLE `cms_zona`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT de la tabla `fos_group`
--
ALTER TABLE `fos_group`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `fos_user`
--
ALTER TABLE `fos_user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `grupo_filtro`
--
ALTER TABLE `grupo_filtro`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `negocio_linea_negocio`
--
ALTER TABLE `negocio_linea_negocio`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `negocio_producto`
--
ALTER TABLE `negocio_producto`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `nosql_encuestas`
--
ALTER TABLE `nosql_encuestas`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `nosql_encuestas_datatable`
--
ALTER TABLE `nosql_encuestas_datatable`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `nosql_encuesta_procesos`
--
ALTER TABLE `nosql_encuesta_procesos`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `survey_encuesta`
--
ALTER TABLE `survey_encuesta`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `survey_encuesta_bloque`
--
ALTER TABLE `survey_encuesta_bloque`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `survey_encuesta_pregunta`
--
ALTER TABLE `survey_encuesta_pregunta`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=104;
--
-- AUTO_INCREMENT de la tabla `survey_encuesta_pregunta_opi`
--
ALTER TABLE `survey_encuesta_pregunta_opi`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT de la tabla `survey_encuesta_proceso`
--
ALTER TABLE `survey_encuesta_proceso`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `survey_encuesta_respuesta`
--
ALTER TABLE `survey_encuesta_respuesta`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `survey_encuesta_sub_bloque`
--
ALTER TABLE `survey_encuesta_sub_bloque`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `admin_canal_det_canal`
--
ALTER TABLE `admin_canal_det_canal`
ADD CONSTRAINT `FK_A779BD5F68DB5B2E` FOREIGN KEY (`canal_id`) REFERENCES `admin_canal_encuestacion` (`id`),
ADD CONSTRAINT `FK_A779BD5FD1EADC9C` FOREIGN KEY (`determinacion_canal_id`) REFERENCES `admin_determinacion_canal` (`id`);

--
-- Filtros para la tabla `admin_filtros`
--
ALTER TABLE `admin_filtros`
ADD CONSTRAINT `FK_5A4E3CED613CEC58` FOREIGN KEY (`padre_id`) REFERENCES `admin_filtros` (`id`);

--
-- Filtros para la tabla `admin_filtros_maqueta`
--
ALTER TABLE `admin_filtros_maqueta`
ADD CONSTRAINT `FK_57A3F11F3F7602BF` FOREIGN KEY (`id_filtro`) REFERENCES `admin_filtros` (`id`),
ADD CONSTRAINT `FK_57A3F11FCD91FAF2` FOREIGN KEY (`id_maqueta`) REFERENCES `cms_maquetas` (`id`),
ADD CONSTRAINT `FK_57A3F11FF1DA68E8` FOREIGN KEY (`bloque`) REFERENCES `admin_filtros_bloque` (`id`);

--
-- Filtros para la tabla `admin_permutaciones_vistas`
--
ALTER TABLE `admin_permutaciones_vistas`
ADD CONSTRAINT `FK_C5ABF977189FD802` FOREIGN KEY (`sub_bloque_id`) REFERENCES `survey_encuesta_sub_bloque` (`id`),
ADD CONSTRAINT `FK_C5ABF97747D487D1` FOREIGN KEY (`indicador_id`) REFERENCES `survey_encuesta_pregunta` (`id`),
ADD CONSTRAINT `FK_C5ABF977640D1D53` FOREIGN KEY (`proceso_id`) REFERENCES `survey_encuesta_sub_bloque` (`id`),
ADD CONSTRAINT `FK_C5ABF97768DB5B2E` FOREIGN KEY (`canal_id`) REFERENCES `admin_canal_encuestacion` (`id`),
ADD CONSTRAINT `FK_C5ABF9777D879E4F` FOREIGN KEY (`negocio_id`) REFERENCES `admin_negocio` (`id`);

--
-- Filtros para la tabla `admin_vista`
--
ALTER TABLE `admin_vista`
ADD CONSTRAINT `FK_71E59A34A326768E` FOREIGN KEY (`filtro_id`) REFERENCES `admin_filtros` (`id`);

--
-- Filtros para la tabla `cms_contenedor`
--
ALTER TABLE `cms_contenedor`
ADD CONSTRAINT `FK_D575F034104EA8FC` FOREIGN KEY (`zona_id`) REFERENCES `cms_zona` (`id`);

--
-- Filtros para la tabla `cms_widget`
--
ALTER TABLE `cms_widget`
ADD CONSTRAINT `FK_B70F01A8C1A15772` FOREIGN KEY (`contenedor_id`) REFERENCES `cms_contenedor` (`id`);

--
-- Filtros para la tabla `cms_zona`
--
ALTER TABLE `cms_zona`
ADD CONSTRAINT `FK_6010A9631CD04871` FOREIGN KEY (`maqueta_id`) REFERENCES `cms_maquetas` (`id`);

--
-- Filtros para la tabla `fos_user_user_group`
--
ALTER TABLE `fos_user_user_group`
ADD CONSTRAINT `FK_B3C77447A76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`),
ADD CONSTRAINT `FK_B3C77447FE54D947` FOREIGN KEY (`group_id`) REFERENCES `fos_group` (`id`);

--
-- Filtros para la tabla `grupo_filtro`
--
ALTER TABLE `grupo_filtro`
ADD CONSTRAINT `FK_1547186B997E71E8` FOREIGN KEY (`filtro_maqueta_id`) REFERENCES `admin_filtros_maqueta` (`id`),
ADD CONSTRAINT `FK_1547186B9C833003` FOREIGN KEY (`grupo_id`) REFERENCES `fos_group` (`id`);

--
-- Filtros para la tabla `negocio_linea_negocio`
--
ALTER TABLE `negocio_linea_negocio`
ADD CONSTRAINT `FK_5D8285257D879E4F` FOREIGN KEY (`negocio_id`) REFERENCES `admin_negocio` (`id`),
ADD CONSTRAINT `FK_5D828525D4B20BCE` FOREIGN KEY (`linea_negocio_id`) REFERENCES `admin_linea_negocio` (`id`);

--
-- Filtros para la tabla `negocio_producto`
--
ALTER TABLE `negocio_producto`
ADD CONSTRAINT `FK_7F99FFF0ABB54A09` FOREIGN KEY (`id_negocio`) REFERENCES `admin_negocio` (`id`),
ADD CONSTRAINT `FK_7F99FFF0F760EA80` FOREIGN KEY (`id_producto`) REFERENCES `admin_producto` (`id`);

--
-- Filtros para la tabla `nosql_encuestas`
--
ALTER TABLE `nosql_encuestas`
ADD CONSTRAINT `FK_F4B9908E46844BA6` FOREIGN KEY (`encuesta_id`) REFERENCES `survey_encuesta` (`id`),
ADD CONSTRAINT `FK_F4B9908E4A50BB68` FOREIGN KEY (`ser1_id`) REFERENCES `admin_servicio` (`id`),
ADD CONSTRAINT `FK_F4B9908E5257F569` FOREIGN KEY (`seg_id`) REFERENCES `admin_segmento` (`id`),
ADD CONSTRAINT `FK_F4B9908EB6A112E5` FOREIGN KEY (`opi_pregunta_id`) REFERENCES `survey_encuesta_pregunta_opi` (`id`),
ADD CONSTRAINT `FK_F4B9908EB83EB1B8` FOREIGN KEY (`det_can_id`) REFERENCES `admin_determinacion_canal` (`id`),
ADD CONSTRAINT `FK_F4B9908EC604D5C6` FOREIGN KEY (`pais_id`) REFERENCES `admin_pais` (`id`),
ADD CONSTRAINT `FK_F4B9908ED9091EB8` FOREIGN KEY (`can_id`) REFERENCES `admin_canal_encuestacion` (`id`),
ADD CONSTRAINT `FK_F4B9908EED1FE42C` FOREIGN KEY (`neg_id`) REFERENCES `admin_negocio` (`id`),
ADD CONSTRAINT `FK_F4B9908EF4837C1B` FOREIGN KEY (`org_id`) REFERENCES `admin_linea_negocio` (`id`);

--
-- Filtros para la tabla `nosql_encuestas_datatable`
--
ALTER TABLE `nosql_encuestas_datatable`
ADD CONSTRAINT `FK_360E3D6842E0F38A` FOREIGN KEY (`zon_id`) REFERENCES `admin_zona` (`id`),
ADD CONSTRAINT `FK_360E3D6846844BA6` FOREIGN KEY (`encuesta_id`) REFERENCES `survey_encuesta` (`id`),
ADD CONSTRAINT `FK_360E3D684A50BB68` FOREIGN KEY (`ser1_id`) REFERENCES `admin_servicio` (`id`),
ADD CONSTRAINT `FK_360E3D685257F569` FOREIGN KEY (`seg_id`) REFERENCES `admin_segmento` (`id`),
ADD CONSTRAINT `FK_360E3D686505CAD1` FOREIGN KEY (`loc_id`) REFERENCES `admin_localidad` (`id`),
ADD CONSTRAINT `FK_360E3D68B83EB1B8` FOREIGN KEY (`det_can_id`) REFERENCES `admin_determinacion_canal` (`id`),
ADD CONSTRAINT `FK_360E3D68C604D5C6` FOREIGN KEY (`pais_id`) REFERENCES `admin_pais` (`id`),
ADD CONSTRAINT `FK_360E3D68ED1FE42C` FOREIGN KEY (`neg_id`) REFERENCES `admin_negocio` (`id`),
ADD CONSTRAINT `FK_360E3D68F4837C1B` FOREIGN KEY (`org_id`) REFERENCES `admin_linea_negocio` (`id`),
ADD CONSTRAINT `FK_360E3D68FBD8A061` FOREIGN KEY (`prov_id`) REFERENCES `admin_provincia` (`id`);

--
-- Filtros para la tabla `nosql_encuesta_procesos`
--
ALTER TABLE `nosql_encuesta_procesos`
ADD CONSTRAINT `FK_A95E9D7C46844BA6` FOREIGN KEY (`encuesta_id`) REFERENCES `survey_encuesta` (`id`),
ADD CONSTRAINT `FK_A95E9D7C483C6669` FOREIGN KEY (`p2_pregunta_id`) REFERENCES `survey_encuesta_pregunta` (`id`),
ADD CONSTRAINT `FK_A95E9D7C4A50BB68` FOREIGN KEY (`ser1_id`) REFERENCES `admin_servicio` (`id`),
ADD CONSTRAINT `FK_A95E9D7C5257F569` FOREIGN KEY (`seg_id`) REFERENCES `admin_segmento` (`id`),
ADD CONSTRAINT `FK_A95E9D7C640D1D53` FOREIGN KEY (`proceso_id`) REFERENCES `admin_proceso` (`id`),
ADD CONSTRAINT `FK_A95E9D7C95AABFEC` FOREIGN KEY (`p3_pregunta_id`) REFERENCES `survey_encuesta_pregunta` (`id`),
ADD CONSTRAINT `FK_A95E9D7CB6A112E5` FOREIGN KEY (`opi_pregunta_id`) REFERENCES `survey_encuesta_pregunta_opi` (`id`),
ADD CONSTRAINT `FK_A95E9D7CB83EB1B8` FOREIGN KEY (`det_can_id`) REFERENCES `admin_determinacion_canal` (`id`),
ADD CONSTRAINT `FK_A95E9D7CC604D5C6` FOREIGN KEY (`pais_id`) REFERENCES `admin_pais` (`id`),
ADD CONSTRAINT `FK_A95E9D7CD9091EB8` FOREIGN KEY (`can_id`) REFERENCES `admin_canal_encuestacion` (`id`),
ADD CONSTRAINT `FK_A95E9D7CE8D9B9B4` FOREIGN KEY (`p4_pregunta_id`) REFERENCES `survey_encuesta_pregunta` (`id`),
ADD CONSTRAINT `FK_A95E9D7CED1FE42C` FOREIGN KEY (`neg_id`) REFERENCES `admin_negocio` (`id`),
ADD CONSTRAINT `FK_A95E9D7CF4837C1B` FOREIGN KEY (`org_id`) REFERENCES `admin_linea_negocio` (`id`),
ADD CONSTRAINT `FK_A95E9D7CF5F60AA7` FOREIGN KEY (`p1_pregunta_id`) REFERENCES `survey_encuesta_pregunta` (`id`);

--
-- Filtros para la tabla `survey_encuesta`
--
ALTER TABLE `survey_encuesta`
ADD CONSTRAINT `FK_EEAC2E6628FCDE76` FOREIGN KEY (`tip3_id`) REFERENCES `admin_tipificacion_opinion` (`id`),
ADD CONSTRAINT `FK_EEAC2E6642E0F38A` FOREIGN KEY (`zon_id`) REFERENCES `admin_zona` (`id`),
ADD CONSTRAINT `FK_EEAC2E664A50BB68` FOREIGN KEY (`ser1_id`) REFERENCES `admin_servicio` (`id`),
ADD CONSTRAINT `FK_EEAC2E665257F569` FOREIGN KEY (`seg_id`) REFERENCES `admin_segmento` (`id`),
ADD CONSTRAINT `FK_EEAC2E6658E51486` FOREIGN KEY (`ser2_id`) REFERENCES `admin_servicio` (`id`),
ADD CONSTRAINT `FK_EEAC2E666505CAD1` FOREIGN KEY (`loc_id`) REFERENCES `admin_localidad` (`id`),
ADD CONSTRAINT `FK_EEAC2E667D8E4B5A` FOREIGN KEY (`ser4_id`) REFERENCES `admin_servicio` (`id`),
ADD CONSTRAINT `FK_EEAC2E6682F516FD` FOREIGN KEY (`tip1_id`) REFERENCES `admin_tipificacion_opinion` (`id`),
ADD CONSTRAINT `FK_EEAC2E669040B913` FOREIGN KEY (`tip2_id`) REFERENCES `admin_tipificacion_opinion` (`id`),
ADD CONSTRAINT `FK_EEAC2E66B52BE6CF` FOREIGN KEY (`tip4_id`) REFERENCES `admin_tipificacion_opinion` (`id`),
ADD CONSTRAINT `FK_EEAC2E66B6A112E5` FOREIGN KEY (`opi_pregunta_id`) REFERENCES `survey_encuesta_pregunta_opi` (`id`),
ADD CONSTRAINT `FK_EEAC2E66B9ED135A` FOREIGN KEY (`det_can`) REFERENCES `admin_determinacion_canal` (`id`),
ADD CONSTRAINT `FK_EEAC2E66C604D5C6` FOREIGN KEY (`pais_id`) REFERENCES `admin_pais` (`id`),
ADD CONSTRAINT `FK_EEAC2E66D9091EB8` FOREIGN KEY (`can_id`) REFERENCES `admin_canal_encuestacion` (`id`),
ADD CONSTRAINT `FK_EEAC2E66DE734E51` FOREIGN KEY (`cliente_id`) REFERENCES `admin_cliente` (`id`),
ADD CONSTRAINT `FK_EEAC2E66E05973E3` FOREIGN KEY (`ser3_id`) REFERENCES `admin_servicio` (`id`),
ADD CONSTRAINT `FK_EEAC2E66ED1FE42C` FOREIGN KEY (`neg_id`) REFERENCES `admin_negocio` (`id`),
ADD CONSTRAINT `FK_EEAC2E66F4837C1B` FOREIGN KEY (`org_id`) REFERENCES `admin_linea_negocio` (`id`),
ADD CONSTRAINT `FK_EEAC2E66FBD8A061` FOREIGN KEY (`prov_id`) REFERENCES `admin_provincia` (`id`);

--
-- Filtros para la tabla `survey_encuesta_pregunta`
--
ALTER TABLE `survey_encuesta_pregunta`
ADD CONSTRAINT `FK_EFE0EF35189FD802` FOREIGN KEY (`sub_bloque_id`) REFERENCES `survey_encuesta_sub_bloque` (`id`),
ADD CONSTRAINT `FK_EFE0EF35801EB86A` FOREIGN KEY (`cem_tipo_id`) REFERENCES `admin_tipo_cem` (`id`);

--
-- Filtros para la tabla `survey_encuesta_proceso`
--
ALTER TABLE `survey_encuesta_proceso`
ADD CONSTRAINT `FK_9668E67246844BA6` FOREIGN KEY (`encuesta_id`) REFERENCES `survey_encuesta` (`id`),
ADD CONSTRAINT `FK_9668E672640D1D53` FOREIGN KEY (`proceso_id`) REFERENCES `admin_proceso` (`id`);

--
-- Filtros para la tabla `survey_encuesta_respuesta`
--
ALTER TABLE `survey_encuesta_respuesta`
ADD CONSTRAINT `FK_1945667C46844BA6` FOREIGN KEY (`encuesta_id`) REFERENCES `survey_encuesta` (`id`),
ADD CONSTRAINT `FK_1945667C9AF2BA6` FOREIGN KEY (`encuesta_pregunta_id`) REFERENCES `survey_encuesta_pregunta` (`id`);

--
-- Filtros para la tabla `survey_encuesta_sub_bloque`
--
ALTER TABLE `survey_encuesta_sub_bloque`
ADD CONSTRAINT `FK_5DFFFE6FA929EBC` FOREIGN KEY (`bloque_id`) REFERENCES `survey_encuesta_bloque` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

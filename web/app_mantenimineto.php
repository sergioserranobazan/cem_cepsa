<?php

use Symfony\Component\HttpFoundation\Request;
/*if (isset($_SERVER['HTTP_CLIENT_IP'])
    || isset($_SERVER['HTTP_X_FORWARDED_FOR'])
    || !(in_array(@$_SERVER['REMOTE_ADDR'], ['127.0.0.1','10.100.0.17','10.100.8.15','10.100.8.182','10.100.8.90','194.179.126.144','194.179.126.154','194.179.126.131','194.179.126.140','194.179.126.150','194.179.126.151','194.179.126.152', '195.53.233.211', '195.53.233.212', '195.53.233.213','195.53.233.214','94.142.200.171','94.142.200.172','94.142.200.173', '94.142.200.174','10.100.8.94', '::1']) || php_sapi_name() === 'cli-server')
) {
    header('HTTP/1.0 403 Forbidden');
    exit('You are not allowed to access this file. Check '.basename(__FILE__).' for more information.');
}*/

/**
 * @var Composer\Autoload\ClassLoader
 */
$loader = require __DIR__.'/../app/autoload.php';
include_once __DIR__.'/../app/bootstrap.php.cache';

$kernel = new AppKernel('prod_mantenimiento', false);
$kernel->loadClassCache();
$kernel = new AppCache($kernel);

// When using the HttpCache, you need to call the method in your front controller instead of relying on the configuration parameter
//Request::enableHttpMethodParameterOverride();
$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);